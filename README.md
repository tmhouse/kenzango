# About Kenzan-Go #
  Kenzan-Go is the name of the japanese akita dog that Helen Keller's pet.
  Kenzan-Go were made for the deaf blind, it is an integrated accessibility services on Android.
  The following three elements are included in one package.

### Screen reading ###
    A string of below is displayed in Braille When tracing the screen, also to represent
    in the vibration in the order of Braille number 142536.

### Entering the caracter ###
    Braille can be entered only flick or tap operation, you can use the SmartBraille IME.

### Operating the android ###
    Available a simple home application.


So, blind deaf to read the screen of the smartphone, manipulate, and be able to enter the character.  


# About Kenzan-Go in Japanese #
# Kenzan-Goについて #
  Kenzan-Goとは、ヘレンケラーが飼っていた日本の秋田犬の名前です。
  Kenzan-Goは、見えない聞こえない盲ろう者のために作られた、Androidの統合アクセシビリティサービスです。
  以下の３つの要素がひとつのパッケージに含まれています。

### 画面を読む ###
    画面をなぞると下の文字列を点字で表示し、また点字番号142536の順で震動で表現します。

### 文字を入力する ###
    フリックやタップ操作だけで点字が入力できる、スマート点字を利用できます。

### 端末操作 ###
    操作が単純なホームアプリケーションを利用できます。

  これらにより、見えなくても聞こえなくてもスマートフォンの画面を読み、操作し、文字を入力
  できることを目指しています。


# Licence #

```
#!c
/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

```

# Disclaimer #
This app is based on Google Talkback, and licensed under the Apache License.
Apache License, Version 2.0: http://www.apache.org/licenses/LICENSE-2.0.html

# OpenSource Link #
https://code.google.com/p/eyes-free/
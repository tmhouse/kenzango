/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.android.kenzango;

import java.util.LinkedList;

import android.os.Handler;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;

/**
 * タスクをqueingして実行する.
 * 
 * @author mutoh
 *
 */
public class TmTaskRunner {
	private LinkedList<Data> m_taskQue = new LinkedList<Data>();
	private Thread	m_thread;
	private Handler	m_handler = new Handler();
	private Data	m_currentData;
	
	public abstract interface Task {
		public abstract Boolean doInBackground();
		public abstract void onPostExecute(Boolean result);
		public abstract void onCanceled();
	}

	/**
	 * データフォルダ.
	 * @author mutoh
	 *
	 */
	private class Data {
		public Data(Task task, long delayMs) {
			m_task = task;
			m_delayMs = delayMs;
		}
		public synchronized void cancel() {
			m_cancel = true;
		}
		public synchronized boolean isCanceled() {
			return(m_cancel);
		}
		boolean		m_cancel = false;
		long		m_delayMs;
		Task		m_task;
	}

	public TmTaskRunner() {
		m_thread = new Thread(m_run);
		m_thread.setName(getClass().getSimpleName());
		m_thread.start();
	}
	
	public boolean m_isDelaying = false;

	private Runnable m_run = new Runnable() {
		@Override
		public void run() {
			while(true) {
				try {
					final Data elm = getNext();
					if( elm.m_delayMs > 0 ) {
						synchronized(m_run) {
							m_isDelaying = true;
							m_run.wait(elm.m_delayMs);
							m_isDelaying = false;
						}
					}
					Boolean res = false;
					if( !elm.isCanceled() ) {
						res = elm.m_task.doInBackground();
					}
					if( !elm.isCanceled() ) {
						final Boolean final_res = res;
						m_handler.post(new Runnable() {
							@Override
							public void run() {
								elm.m_task.onPostExecute(final_res);
							}
						});
					} else {
						m_handler.post(new Runnable() {
							@Override
							public void run() {
								elm.m_task.onCanceled();
							}
						});
					}
				} catch (InterruptedException e) {
					// 中止要求
					TmLog.w("InterruptedException caught. to be normal shutdown");
					break;
				}
			}
		}
	};
	
	public synchronized void shutdown() {
		cancelAllTask();
		synchronized(m_thread) {
			m_thread.interrupt();
		}
	}
	
	public synchronized void cancelAllTask() {
		if( m_currentData != null ) {
			m_currentData.cancel();
			m_currentData = null;
		}
		m_taskQue.clear();
		notifyThread();
	}
	
	public synchronized void addTask(Task task, long delayMs) {
		m_taskQue.add(new Data(task, delayMs));
	}
	
	private synchronized Data pollFirstTask() {
		m_currentData = m_taskQue.pollFirst();
		return(m_currentData);
	}
	
	public synchronized void notifyThread() {
		synchronized(m_run) {
			if( m_isDelaying ) {
				m_run.notify();
			}
		}
		synchronized(m_thread) {
			m_thread.notify();
		}
	}
	
	public synchronized void setPriorityTask(Task task, long delayMs) {
		cancelAllTask();
		addTask(task, delayMs);
		notifyThread();
	}
	
	private Data getNext() throws InterruptedException {
		while(true) {
			Data d = pollFirstTask();
			if( d == null ) {
				synchronized(m_thread) {
					m_thread.wait();
				}
			} else {
				return(d);
			}
		}
	}
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.android.kenzango;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.PowerManager;

import com.google.android.marvin.utils.FailoverTextToSpeech;

/**
 * FailoverTextToSpeechのサブクラス.
 * @author mutoh
 *
 */
public class TmFailoverTextToSpeech extends FailoverTextToSpeech {
	private TmBodyBraille	m_bodyBraille = null;
	private SensorManager	m_sm;
	private PowerManager	m_pm;
	private Sensor			m_sensor;
	private volatile boolean m_isEnable = true;

	public TmFailoverTextToSpeech(Context context) {
		super(context);
		m_sm = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
		m_pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);

		List<Sensor> sensorList = m_sm.getSensorList(Sensor.TYPE_PROXIMITY);
		if( sensorList != null && sensorList.size() > 0 ) {
			m_sensor = sensorList.get(0);
		}

		m_bodyBraille = new TmBodyBraille(context);
	}
	
	public TmBodyBraille getBodyBraille() {
		return(m_bodyBraille);
	}

   	public void setEnabled(boolean isEnable) {
   		m_isEnable = isEnable;
   	}
   	public boolean isEnabled() {
   		return(m_isEnable);
   	}
	
	/**
	 * 近接センサー登録.
	 * @param ctx
	 */
    private void registerProximitySensor() {
    	unregisterProximitySensor();
    	if( m_sensor != null ) {
			m_sm.registerListener(m_sel, m_sensor, SensorManager.SENSOR_DELAY_FASTEST);
		}
    }
    /**
     * 近接センサー登録解除.
     * @param ctx
     */
    private void unregisterProximitySensor() {
		m_sm.unregisterListener(m_sel);
    }
	
	@Override
    public void speak(String text, 
    		float pitch, float rate, HashMap<String, String> params) {
		if( m_pm.isScreenOn() && isEnabled() ) {
			// proximity sensorオン
			registerProximitySensor();
		
			super.speak(text, pitch, rate, params);
		
			m_bodyBraille.playBrailleASync(text);
		}
	}
	
	public void hideAll() {
		// proximity sensorオフ
		unregisterProximitySensor();

		m_bodyBraille.stop();
	}

	@Override
	public void stopAll() {
		super.stopAll();
		// ここは親クラスのproximity sensorが反応して呼ばれる
		//hideAll();
	}

	@Override
	public void shutdown() {
		// このあと少しsleepするため、先にhideAllしてproximity sensorを切らないとダメ。
		setEnabled(false);
		hideAll();
		m_bodyBraille.finish();
		m_bodyBraille = null;

		// stopしてから少し待ってからshutdownしないとTTS側が死ぬのでちょっと待つ
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		super.shutdown();
	}
	
	/**
     * 近接センサーリスナー.
     */
    private MySensorEventListener m_sel = new MySensorEventListener();
    protected class MySensorEventListener implements SensorEventListener {
    	private static final long c_ABORT_COUNT_CLEAR_TIME = 2000;
    	private static final int c_ABORT_ACTION_TIMES = 3;
    	private static final int c_PAUSE_NEAR_TIME = 1000;

    	private int m_nearCount;
    	private long m_lastFireTime;
    	private Handler	m_handler = new Handler();
    	
		@Override
		public void onSensorChanged(SensorEvent event) {
			if( isEnabled() == false ) {
				TmLog.w("onSensorChanged", "m_isEnable == false, do nothing");
				cancelPauseTimer();
				return;
			}

			// Proximity Sensor以外はこのリスナーは扱わない
			if( event.sensor.getType() != Sensor.TYPE_PROXIMITY) {
				throw new RuntimeException();
			}
			
			long now = System.currentTimeMillis();
			if( now - m_lastFireTime > c_ABORT_COUNT_CLEAR_TIME ) {
				m_nearCount = 0;
				TmLog.e("proximity count cleared");
			}
			m_lastFireTime = now;

			if( event.values[0] == 0 ) {
				TmLog.w("near");
				setPauseTimer();
				m_nearCount++;
			} else {
				TmLog.w("far");
				cancelPauseTimer();
				getBodyBraille().unpause();
			}
			
			if( m_nearCount > c_ABORT_ACTION_TIMES ) {
				m_nearCount = 0;
				TmLog.d("body braille stopped.");
				hideAll();
			}
		}
		
		private void setPauseTimer() {
			m_handler.removeCallbacks(m_pauseRun);
			m_handler.postDelayed(m_pauseRun, c_PAUSE_NEAR_TIME);
		}
		private void cancelPauseTimer() {
			m_handler.removeCallbacks(m_pauseRun);
		}
		
		private Runnable m_pauseRun = new Runnable() {
			@Override
			public void run() {
				if( isEnabled() ) {
					TmLog.d("request pause at end of braille!!!!!");
					getBodyBraille().pauseAtEndOfBraille();
				}
			}
		};

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
    };
}

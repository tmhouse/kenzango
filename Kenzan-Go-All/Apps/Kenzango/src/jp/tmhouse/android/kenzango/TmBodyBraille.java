/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.android.kenzango;

import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmBrailleVibrator;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmKana2Braille;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmSoundVibrator;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmBrailleVibrator.MasuakeVibeKind;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmSoundVibrator.PauseKind;

public class TmBodyBraille {
    private static final String c_JPANALYZER_PKG = "jp.tmhouse.jpanalyzer";

	private TmTextToBrailleOverlay	m_overlay;
	private TmBrailleVibrator	m_brailleVibe;
    private TmKana2Braille		m_k2b;
    private TmJpConverter		m_jpconverter;
	private Context				m_ctx;
    private volatile boolean	mStopRequested = false;
	private TmTaskRunner		m_taskQue = new TmTaskRunner();
    
	public TmBodyBraille(Context ctx) {
		m_ctx = ctx;
		m_brailleVibe = createBrailleVibrator(ctx);
		m_k2b = new TmKana2Braille();
		m_k2b.setKatakanaBegin114Type(true); // タイプ1: [1, 14]
        m_jpconverter = new TmJpConverter(ctx);
		m_overlay = new TmTextToBrailleOverlay(ctx);
	}
	
    private TmBrailleVibrator createBrailleVibrator(Context ctx) {
        // TODO ●設定できる項目
        // - ボリューム
        // - 振動の速さ
        // - 振動子の個数(mono, streo)
        // - 文字ごとの無振動時間(短い、普通、長い、とても長い)
    	TmBrailleVibrator bvibe = new TmBrailleVibrator(ctx, 
    			jp.tmhouse.SixPointsKanjiLib.R.raw.sin_200hz_1000ms_16bit44k_gain20db);
        //bvibe.setSoundEnable(true);
        bvibe.setVibratorEnable(true);
        //bvibe.setVolumeRatio(1.0f);
        //bvibe.setVibrteRatio(GuideSetting.queryRecvVibrateSpeedValue(this));
        //bvibe.setSoundChannel(TmBrailleVibrator.SoundChannel.USE_STEREO);
        bvibe.playMuteContinuos(ctx, 5000); // 定期的無音
        //bvibe.setDualSoundNotifyer(m_notifier); // 再生終了時notifyされる
        
        return(bvibe);
    }
    
    public void finish() {
   		if( m_brailleVibe != null ) {
   			stopViberateMessage();
   			m_brailleVibe.finish();
   			m_brailleVibe = null;
   		}
   		
   		m_taskQue.shutdown();
    }
    
    public void stop() {
		TmLog.d("停止要求:開始");
        setIsStopped(true);
    	
		// 再生キューがclearされpause状態になる。
    	stopViberateMessage();

    	// overlayを非表示
		m_overlay.hide();

		TmLog.d("停止要求:終了");
    }
    
	/**
	 * 点字再生キャンセル.
	 */
	private void stopViberateMessage() {
		if( m_brailleVibe == null ) {
			return;
		}
		m_brailleVibe.pause();
		m_brailleVibe.clearAll();
	}
	
	private synchronized boolean getIsStopped() {
		return(mStopRequested);
	}
	private synchronized void setIsStopped(boolean bIsStop) {
		mStopRequested = bIsStop;
	}
	
	/**
	 * 点字再生停止/再開
	 */
	public void pause() {
		m_brailleVibe.pause();
	}
	public void unpause() {
		m_brailleVibe.unpause();
	}
	
	/**
	 * 点字ひとつを再生後に停止.
	 */
	public void pauseAtEndOfBraille() {
		m_brailleVibe.pauseAtEndOfBraille();
	}
	
    /**
     * 点字の再生.
     * @param msg
     */
	private boolean playVibrateMessageASync(
			ArrayList<BrailleSet> bset, 
			TmSoundVibrator.IOnPlayBrailleHandler callback)
	{
		if( bset == null || bset.size() == 0 ) {
			TmLog.d("no message.");
			return(false);
		}
		
		if( m_brailleVibe == null ) {
			return(false);
		}
		
		//
    	// 点字データを振動で再現する
		//
		
		// マスターボリューム設定する場合はここで都度行う
		// Galaxy Tabがボリュームを勝手に戻すらしいから。
    	//setupMasterVolume();
		
		// 短振動と長振動の比率
		//int shortLongRatio = UniChatXSettings.GuideSetting.
		//		queryRecvVibrateShortLongRatioValue(this);
		m_brailleVibe.setShortLongRatio(5);
		
		// 再生速度セット
		//float speed = UniChatXSettings.GuideSetting.
		//		queryRecvVibrateSpeedValue(this);
		//m_brailleVibe.setVibrteRatio(1.0f);
		
		// 文字間空白時間の取得
		short waitTime = 1500; //UniChatXSettings.GuideSetting.
				//queryRecvVibrateEachSpaceTimeValue(this);
		
		// 末尾省略モード
		m_brailleVibe.setMatubiSyoryaku(true);
			//UniChatXSettings.GuideSetting.queryRecvOmissionEndMode(this));

		// 無点時右短振動置換
		m_brailleVibe.setMutenjiMigiTanTikan(false);
			//UniChatXSettings.GuideSetting.queryRecvReplaceNone2ShortRightMode(this));

		// マスアケの振動設定
		m_brailleVibe.setMasuakeVibeKind(MasuakeVibeKind.SHORT_3);
			//UniChatXSettings.GuideSetting.queryRecvMasuakeVibeKind(this));
		
		// 再生遅延追加
		m_brailleVibe.addDefaultWait();
		
		// 点字データ追加
		m_brailleVibe.addBrailleSetArray(bset);
		
		// for debug
		//if( false && TmLog.isDebuggable(this) ) {
		//	StringBuilder sb82 = new StringBuilder(128);
    	//	for( BrailleSet bs : bset ) {
    	//		sb82.append(bs.getDebugString() + " ");
    	//	}
    	//	TmLog.d("8/2=" + sb82.toString());
		//}
		/***
		if( DBG ) {
			TmLog.d("--DEBUG PRINT--");
			StringBuilder sb6p = new StringBuilder(128);
    		for( BrailleSet bs : bset ) {
    			int intval = bs.toInteger();
    			if( intval == 0 ) {
    				sb6p.append(" [0]");
    				Log.i(TAG, sb6p.toString());
    				sb6p.setLength(0);
    			} else {
    				String str = bs.getDebugStringFor6p();
    				sb6p.append(str + " ");
    			}
    		}
    		Log.i(TAG, sb6p.toString());
		}
		***/
		
		// 点字再生ごとのhandlerをセット
		m_brailleVibe.setOnPlayBrailleHandler(callback);

		// stopしたときpauseしている。
		m_brailleVibe.unpause();
		

		// 再生停止要求はm_progressUpdCbでチェックする
		//m_brailleVibe.playDualSoundOnThread(m_progressUpdCb);

		return(true);
    }
	
	/**
     * Asyncで読み上げを行う。
	 * 
	 * @param text
	 * @param cb 
	 */
    public void playBrailleASync(String text) {
    	// mStopRequestedをリセット
        setIsStopped(false);

		// 以前の再生をキャンセル
		stopViberateMessage();
		
		if( text == null || text.length() == 0 ) {
			return;
		}
    	
		// 別スレッドのキューに突っ込む
		long delayTime = 100;
		int minLen = 10;
		int maxLen = 60;
		m_taskQue.cancelAllTask();
		m_taskQue.addTask(new ViberateMessageTask(text, minLen, maxLen), delayTime);
		m_taskQue.notifyThread();
    }
    
	/**
	 * 日本語を丁度良い感じで分断する.
	 * @param itr 
	 * @param minLen この長さは最低確保する.
     * @param maxLen この長さで強制的に分断する.
     * @return
     */
	private static String splitText(StringCharacterIterator itr, int minLen, int maxLen) {
		if( itr.getEndIndex() == itr.getIndex() ) {
			return(null);
		}
		StringBuilder sb = new StringBuilder(maxLen);
		
		int i = 0;
		boolean bDelHeadCR = true;
		for( char c = itr.current() ; itr.getEndIndex() != itr.getIndex(); c = itr.next() ) {
			if( bDelHeadCR && (c == '\r' || c == '\n') ) {
				continue;
			}
			bDelHeadCR = false;

			i++;
			sb.append(c);
			if( i > minLen ) {
				if( isBunmatu(c) ) {
					break;
				}
			}
			if( i >= maxLen ) {
				break;
			}
		}
		itr.next();
		return(sb.toString());
	}

	/**
	 * 文末を意味する定義.
	 */
	private static final Character[] c_bunmatu = {
		'　', ' ', '。', '、', '？', '！', '」', '）', ']', '}', '\n', '\r'
	};
	private static HashMap<Character, Boolean> s_bunmatuHash;
	static {
		s_bunmatuHash = new HashMap<Character, Boolean>(127);
		for( Character s : c_bunmatu ) {
			s_bunmatuHash.put(s, true);
		}
	}
	
	/**
	 * 文末か.
	 * @param c
	 * @return
	 */
	public static boolean isBunmatu(char c) {
		return(s_bunmatuHash.containsKey(c));
	}
    
	/**
	 * 漢字平仮名変換をbackgroundで行い、その結果を表示するのはmainスレッドで行う。
	 * AsyncTaskと違うのは、開始時間までの待ちを入れられること。
	 * 将来的にはCancelletionSingnalを導入してもっと高速に中断したい。
	 * @author mutoh
	 *
	 */
	private class ViberateMessageTask implements 
			TmTaskRunner.Task, TmSoundVibrator.IOnPlayBrailleHandler {
		private ArrayList<BrailleSet>	m_bs;
		private String					m_curText;
		private StringCharacterIterator m_itr;
		private int						m_minLen; // 文字列分割の最低長さ
		private int						m_maxLen; // 文字列分割の最大長さ

		public ViberateMessageTask(String text, int minLen, int maxLen) {
			m_minLen = minLen;
			m_maxLen = maxLen;
			m_itr = new StringCharacterIterator(text);
		}
		
		/**
		 * implements TmTaskRunner.Task
		 */
		@Override
		public Boolean doInBackground() {
			// 文末で分断する
			m_curText = splitText(m_itr, m_minLen, m_maxLen);
			
			TmLog.i("doInBackground:" + m_curText);
			// 漢字をひらがなに変換するか
			//boolean bKanji2Hira = BodyBrailleSettings.queryConvertToHiragana(this);
			boolean bKanji2Hira = true;
			String converted = null;
			if ( bKanji2Hira ) {
				converted = convertToHiragana(m_curText);
			}

			// テキストを点字データに変換
			String text = converted != null ? converted : m_curText;
			m_bs = m_k2b.convert(text);
			return true;
		}

		@Override
		public void onPostExecute(Boolean result) {
			//TmLog.i("onPostExecute:success. result=" + result);
			if( result == false ) {
				TmLog.w("onPostExecute: do nothing. beacuse result is false");
				return;
			}
			// overlayに墨字と点字を表示
			m_overlay.speak(m_curText, m_bs);
				
			// 体表点字再生
			playVibrateMessageASync(m_bs, this);
		}
		@Override
		public void onCanceled() {
			TmLog.e("onPostExecute:canceled!!");
		}
		
		/**
		 * implements TmSoundVibrator.IOnPlayBrailleHandler 
		 */
		@Override
		public void onVibrateStarted() {
			//TmLog.d("onVibrateStarted");
		}

		@Override
		public void onVibrateEnded() {
			//TmLog.d("onVibrateEnded");
			if( m_itr.getEndIndex() != m_itr.getIndex() ) {
				// 以前の再生をキャンセル
				stopViberateMessage();

				// 次の文字列を処理する
				m_taskQue.addTask(this, 0);
				m_taskQue.notifyThread();
			}
		}

		@Override
		public void onVibratePaused(TmSoundVibrator.PauseKind pauseKind) {
			//TmLog.d("onVibratePaused: isStopped=" + pauseKind);
		}

		@Override
		public void onBrailleViberateStarted(int position, int brailleNum1, int brailleNum2) {
			//TmLog.d("onViberateStart: pos=" + position + ", num1=" + brailleNum1 + ", num2=" + brailleNum2);
			m_overlay.setWaku(position, brailleNum1, brailleNum2);
		}

		@Override
		public void onBrailleViberateEnded(int position) {
			//TmLog.d("onViberateEnd: pos=" + position);
			m_overlay.clearWaku();
		}
	}
    
    /**
     *  漢字をひらがなに変換する.
     * @param text
     * @return
     */
    private String convertToHiragana(String text) {
		// 日本語アナライザがインストールされていないときはnullが返る。
		if ( isInstalled(c_JPANALYZER_PKG)) {
			String converted = m_jpconverter.convertToHiragana(text);
			if (converted != null) {
				return(converted);
			} else {
				TmLog.w("No Nihongo Analyzer. use 6point kanji.");
			}
		} else {
			//TmLog.i("NOT converted text=" + text);
		}
		return(null);
    }
    
    /**
     * パッケージがインストールされているか.
     * @param pkgName
     * @return
     */
    private boolean isInstalled(String pkgName) {
    	try {
    		PackageManager pm = m_ctx.getPackageManager();
            ApplicationInfo ai = pm.getApplicationInfo(pkgName, 0);
            return(true);
        } catch (NameNotFoundException e) {
            TmLog.d(pkgName + " is not found");
        }
    	return(false);
    }

}

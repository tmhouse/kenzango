/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.android.kenzango;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import jp.tmhouse.TmLibrary.Activity.SettingsActivity.GuideSetting;
import jp.tmhouse.TmLibrary.Utils.TmCommonValues;
import jp.tmhouse.TmLibrary.Utils.Debug.TmAsyncWaitForDebugger;
import jp.tmhouse.TmLibrary.Utils.Debug.TmSyncRunner;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;
//import jp.tmhouse.android.tmtalkback.BuildConfig;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Debug;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Toast;

import com.google.android.marvin.talkback.SpeechController;
import com.google.android.marvin.talkback.TalkBackService;
import com.google.android.marvin.talkback.TalkBackService.KeyEventListener;
import com.google.android.marvin.utils.FailoverTextToSpeech;
import com.google.android.marvin.utils.FailoverTextToSpeech.FailoverTtsListener;
import com.googlecode.eyesfree.utils.AccessibilityEventListener;

/**
 * TalkBackServiceのTM lab版.
 * @author mutoh
 *
 */
public class TmTalkBackService extends TalkBackService {
	
	private TmFailoverTextToSpeech	m_failoverTts;
    private Handler		m_handler = new Handler();
	
    private BroadcastReceiver screenStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                // OFF
        		if( m_failoverTts != null ) {
        			m_failoverTts.hideAll();
        		}
            }
        }
    };
    
    @Override
	public void onCreate() {
		if( BuildConfig.DEBUG )  {
			TmLog.setAsyncMode(false);
			TmLog.setSeparateLocation(true);
			TmLog.enable(true);
			TmLog.e("TmTalkBackService started");

			TmAsyncWaitForDebugger.start(8000);
		}
    	TmLog.i("onCreate");
		super.onCreate();
		
		// デフォルト値のセット
		GuideSetting.initDefaultValues(this);
		
		// mFailoverTtsを差し替える
		// mFailoverTtsは上記super.onCreate()で生成されている.
		resetFailoverTts();
		
		setIsTutorialAcvite2True();
		
		removeAccessilibityEventListeners();
		
		IntentFilter filter = new IntentFilter();
	    filter.addAction(Intent.ACTION_SCREEN_OFF);
	    registerReceiver(screenStatusReceiver, filter);
	    disableInitialTutrial();
	}
    
    @Override
    protected void onServiceConnected() {
    	TmLog.i("onServiceConnected");
    	super.onServiceConnected();
    	
    	// 元々のproximity sensorを切る
    	// やめた。SpeechController.handleSpeechStarting()で無理やり
    	// オンにされる。
    	//Log.w("TmTalkBackService", "original proximity sensor OFF");
    	//SpeechController spctrl = getSpeechController();
    	//spctrl.setSilenceOnProximity(false);
    }
    
	@Override
	public boolean onUnbind(Intent intent) {
		return super.onUnbind(intent);
	}

	@Override
	public void onRebind(Intent intent) {
		super.onRebind(intent);
	}


    
    @Override
	public void onDestroy() {
		super.onDestroy();
		
		// m_failoverTts.shutdownは済んでいるはず
		m_failoverTts = null;
		unregisterReceiver(screenStatusReceiver);
		TmLog.i("onDestroy");
	}
    
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (!isServiceActive()) {
            return;
        }
        if ( localShouldDropEvent(event) ) {
            return;
        }
        super.onAccessibilityEvent(event);
    }
    
    /**
     * このイベントを捨てるべきか.
     * @param event
     * @return
     */
    private boolean localShouldDropEvent(AccessibilityEvent event) {
    	int t = event.getEventType();
        boolean isNotification = 
        		(t == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED);
	
        // Notificationイベントは無条件で捨てる
        if( isNotification ) {
        	return(true);
        }
        return(false);
    }

    /**
     * tutrialを表示しない.
     */
    private void disableInitialTutrial() {
        String PREF_FIRST_TIME_USER = "first_time_user";
    	SharedPreferences prefs =
    			PreferenceManager.getDefaultSharedPreferences(this);
        if (!prefs.getBoolean(PREF_FIRST_TIME_USER, true)) {
            return;
        }
        
        final Editor editor = prefs.edit();
        editor.putBoolean(PREF_FIRST_TIME_USER, false);
        editor.commit();
    }
    
    /**
     * 不要なA11yEventListenerを削除する.
     */
    private void removeAccessilibityEventListeners() {
    	// 消去リスト
    	HashMap<String, Integer> hash = new HashMap<String, Integer>(8);
    	hash.put("ProcessorScrollPosition", 1); // リスト画面で何件中何件スクロールとか言うやつ
    	hash.put("ProcessorLongHover", 1); // 長押しすると"選択するにはタップします"とか言うやつ
    	//hash.put("processorVolumeStream", 1); // ボリュームキーの操作で"音量をxxに設定した"とか言うやつ

    	try {
			Field f = getClass().getSuperclass().getDeclaredField("mAccessibilityEventListeners");
			f.setAccessible(true);
			@SuppressWarnings("unchecked")
			final LinkedList<AccessibilityEventListener>  mAccessibilityEventListeners = 
					(LinkedList<AccessibilityEventListener>)f.get(this);
			if( mAccessibilityEventListeners != null && mAccessibilityEventListeners.size() > 0 ) {
				for( final AccessibilityEventListener l : mAccessibilityEventListeners ) {
					String className = l.getClass().getSimpleName();
					if( hash.get(className) != null ) {
						TmLog.i("remove a11y event listener: " + l.getClass().getName());
						
				    	m_handler.post(new Runnable() {
				            @Override
				            public void run() {
				                mAccessibilityEventListeners.remove(l);
				            }
				        });
					}
				}
			}
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
    }
    			
    /**
     * AccessibilityTutorialActivity.sTutorialIsActiveをtrueにする.
     * 
     * これをすると、LongHoverしたときに「選択するにはダブルタップします」とかしゃべるのを抑制できる。
     * @param bIsActive
     */
    private void setIsTutorialAcvite2True() {
    	Class c = com.google.android.marvin.talkback.tutorial.AccessibilityTutorialActivity.class;
    	try {
			Field f = c.getDeclaredField("sTutorialIsActive");
			f.setAccessible(true);
			f.setBoolean(null, true);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
    }

	/**
     * SpeechController.mFailoverTtsを差し替える.
     * 以下の２行を無理やり行う。
     *  mFailoverTts = new FailoverTextToSpeech(context);
     *  mFailoverTts.setListener(mFailoverTtsListener);
     */
    private void resetFailoverTts() {
    	SpeechController spctrl = getSpeechController();
    	
    	@SuppressWarnings("unchecked")
		Class<SpeechController> spctrlClass = (Class<SpeechController>)spctrl.getClass();
    	try {
    		// mFailoverTtsフィールドを取り出す
			Field f = spctrlClass.getDeclaredField("mFailoverTts");
			f.setAccessible(true);
			
			// mFailoverTtsオブジェクトを取り出し、shutdownする
			final FailoverTextToSpeech failoverTts = (FailoverTextToSpeech)f.get(spctrl);
			// すぐにshutdownすると、TTSがリークしてしまうので遅延させる
			m_handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					failoverTts.shutdown();
				}
			}, 3000);
			
			// TmFailoverTextToSpeechを産んで、リスナーをセットする
			m_failoverTts = new TmFailoverTextToSpeech(this);
			Field listenerFld = spctrlClass.getDeclaredField("mFailoverTtsListener");
			listenerFld.setAccessible(true);
			FailoverTtsListener listener = (FailoverTtsListener)listenerFld.get(spctrl);
			m_failoverTts.setListener(listener);
			
			// mFailoverTtsにTmFailoverTextToSpeechをセットする
			f.set(spctrl, m_failoverTts);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
    }
    
	/**
     * mutoh added
     */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if( intent != null ) {
			String command = intent.getStringExtra(TmReceiver.COMMAND_NAME);
			if( command != null ) {
				if( command.equals(TmCommonValues.ACTION_SUSPEND_TOUCH_EXPLORE) ) {
					TmLog.d("onStartCommand", "suspend touch explore");
					// 表示中のものを消す
					m_failoverTts.setEnabled(false);
					callTalkBackServiceMethod("suspendTalkBack");
					m_failoverTts.hideAll();
				}
				if( command.equals(TmCommonValues.ACTION_RESUME_TOUCH_EXPLORE) ) {
					TmLog.d("onStartCommand", "resume touch explore");
					callTalkBackServiceMethod("resumeTalkBack");
					m_failoverTts.setEnabled(true);
				}
				if( command.equals(TmCommonValues.ACTION_STOP_SERVICE) ) {
					TmLog.d("onStartCommand", "stop service");
					m_failoverTts.setEnabled(false);
					interruptAllFeedback();
					callTalkBackServiceMethod("suspendTalkBack");
					m_failoverTts.hideAll();
					this.stopSelf();
				}
			}
		}

		return super.onStartCommand(intent, flags, startId);
	}
	
	private boolean callTalkBackServiceMethod(String methodName) 
	{
		try {
			/****
			if( true ) {
				Method[] methods = this.getClass().getSuperclass().getDeclaredMethods();
				for( Method m : methods ) {
					TmLog.d(m.getName());
				}
			}
			****/
			
			Method m;
			m = this.getClass().getSuperclass().getDeclaredMethod(methodName);
			m.setAccessible(true);
			m.invoke(this);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			return(false);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return(false);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return(false);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return(false);
		}
		return(true);
	}
}

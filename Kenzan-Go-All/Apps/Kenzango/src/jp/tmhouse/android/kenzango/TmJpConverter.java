/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.android.kenzango;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/**
 * 漢字かな混じり文をひらがな文章に変換するクラス.
 * @author mutoh
 *
 */
public class TmJpConverter {
	private Context m_ctx;
	
	public TmJpConverter(Context ctx) {
		m_ctx = ctx;
	}

	/**
	 * 定数定義.
	 * @author mutoh
	 *
	 */
	private static class Const {
		public static final String AUTHORITY = "jp.tmhouse.jpanalyzer.provider";
		public static final String ANALYZE = "analyze";
		public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + ANALYZE);
		
		public static class Fields {
			public static final String SURFACE = "surface";
			public static final String COST = "cost";
			public static final String LENGTH = "length";
			public static final String START = "start";
			public static final String BASIC_FORM = "basicForm";
			public static final String CONJUGATIONAL_FORM = "conjugationalForm";
			public static final String CONJUGATIONAL_TYPE = "conjugationalType";
			public static final String PART_OF_SPEECH = "partOfSpeech";
			public static final String PRONUNCIATIONS = "pronunciations";
			public static final String READINGS = "readings";
			public static final String ADDITIONAL_INFO = "additionalInfo";
		}

		public static class ArgKey {
			public static final String ANALYZE = "keyAnalyze";
			public static final String PROJECTION = "keyProjection";
		}

		public static final String[] SummaryProjection  = new String[] {
				Fields.SURFACE, 
				//Fields.COST, 
				//Fields.LENGTH, 
				//Fields.START,
				Fields.BASIC_FORM, 
				Fields.CONJUGATIONAL_FORM,
				Fields.CONJUGATIONAL_TYPE, 
				Fields.PART_OF_SPEECH,
				Fields.PRONUNCIATIONS, 
				Fields.READINGS,
				//Fields.ADDITIONAL_INFO,
		};
	}
	
	/**
	 * 漢字かな混じり文字列を、点字表記用のひらがな文字列に変換する。
	 * @param ctx
	 * @param analyzeStr
	 * @return
	 */
	public String convertToHiragana(String analyzeStr) {
		ArrayList<HashMap<String, String>> results =
				analyzeSync(m_ctx, analyzeStr, Const.SummaryProjection);
		String ret = createHiragana(results);
		return(ret);
	}
	
	/**
	 * 日本語アナライザーに同期で解析をさせる.
	 * @param ctx
	 * @param analyzeStr
	 * @param projection
	 * @return
	 */
	private ArrayList<HashMap<String, String>> analyzeSync(
			Context ctx, String analyzeStr, String[] projection) {
		
		String selectionKey = Const.ArgKey.ANALYZE;
		Uri uri = Const.CONTENT_URI;
		
		String selection = selectionKey + "==?";
		String[] selectionArgs = new String[1];
		selectionArgs[0] = analyzeStr;
		
        Cursor cursor = ctx.getContentResolver().query(
        		uri, projection, selection, selectionArgs, null);
        
        if( cursor == null ) {
        	return(null);
        }

		ArrayList<HashMap<String, String>> results = 
				new ArrayList<HashMap<String, String>>(16);
		
		while( cursor.moveToNext() ) {
			int colCnt = cursor.getColumnCount();
			HashMap<String, String> data = new HashMap<String, String>(8);
			for( int i = 0; i < colCnt; i++ ) {
				String colName = cursor.getColumnName(i);
				String value = cursor.getString(i);
				data.put(colName, value);
			}
			results.add(data);
		}
		cursor.close();
		return(results);
	}
	
	private void printHashMap(HashMap<String, String> map) {
		Iterator<String> itr = map.keySet().iterator();
		StringBuilder sb = new StringBuilder(128);
		while( itr.hasNext() ) {
			String key = itr.next();
			String val = map.get(key);
			sb.append("(");
			sb.append(key);
			sb.append("=");
			sb.append(val);
			sb.append("),");
		}
		TmLog.d(sb.toString());
	}

	/**
	 * ひらがなを生成する.
	 * 
	 * @param results
	 * @return
	 */
	private String createHiragana(ArrayList<HashMap<String, String>> results) {
		if( results == null ) {
			return(null);
		}

		String pronunciations = null;
		String surface = null;
		String conjugationalForm = null;
		String partOfSpeech = null;
		StringBuilder sb = new StringBuilder(128);
		for( HashMap<String, String> data : results ) {
//printHashMap(data);

			// 読みの文字列を生成する
			pronunciations = getFirstValue(
					data.get(Const.Fields.PRONUNCIATIONS));
			surface = data.get(Const.Fields.SURFACE);
			conjugationalForm = data.get(Const.Fields.CONJUGATIONAL_FORM);
			partOfSpeech = data.get(Const.Fields.PART_OF_SPEECH);
			
			//String basicForm = data.get(Const.Fields.BASIC_FORM);
			//String conjugationalType = data.get(Const.Fields.CONJUGATIONAL_TYPE);
			//String readings = data.get(Const.Fields.READINGS);
		
			// 分かち書き
			/***
			if (sb.length() > 0 && partOfSpeech != null ) {
				// 未知語は無条件でマスアケ
				if( partOfSpeech.indexOf("未知語") >= 0 ) {
					sb.append("　");
				} else if (partOfSpeech.indexOf("名詞") >= 0
						//|| partOfSpeech.indexOf("未知語") >= 0
						|| partOfSpeech.indexOf("自立") >= 0) {
					// 除外要件
					if (partOfSpeech.indexOf("副詞可能") < 0 && // 後、末、とか。
						partOfSpeech.indexOf("接尾") < 0 && // で、とか。
						(conjugationalForm != null && 
							conjugationalForm.indexOf("連用形") < 0) // ～し、とか。
					) {
						// 「あいうえお=>あいうえ　お」となってしまうので消す。
						sb.append("　");
					}
				}
			}
			***/
			
			if( partOfSpeech != null && partOfSpeech.indexOf("名詞-数") >= 0) {
				if( isSanyoSuji(surface) ) {
					// 算用数字はそのまま
					sb.append(surface);
				} else {
					// それ以外は読みの発音
					// TODO 漢数字の連続などの読みをどうするとか、本当は結構むずい。
					sb.append(pronunciations);
				}

			} else if (pronunciations == null || conjugationalForm == null
				|| (partOfSpeech != null && partOfSpeech.indexOf("記号") >= 0)
				|| (partOfSpeech != null && partOfSpeech.indexOf("名詞-数") >= 0)) {
				// 未知の言葉 or 記号 or 算用数字
				// オリジナル文字を使用する
				// 全角カタカナの未知語の場合があるので、平仮名へ変換
				surface = zenKataToHira(surface);
				sb.append(surface);
			} else {
				sb.append(pronunciations);
			}
		}
		String analyzedStr = sb.toString();
		TmLog.d(analyzedStr);
		//Toast.makeText(this, analyzedStr, Toast.LENGTH_LONG).show();
		return(analyzedStr);
	}
	
	private boolean isSanyoSuji(String str) {
		char c = str.charAt(0);
		if( '0' <= c && c <= '9' ) {
			return(true);
		}
		if( '０' <= c && c <= '９' ) {
			return(true);
		}
		return(false);
	}
	
    /**
     * 全角カタカナを平仮名に変換.
     * @param s
     * @return
     */
	public static String zenKataToHira(String s) {
		if( s == null ) {
			return(null);
		}
		StringBuffer sb = new StringBuffer(s);
		for (int i = 0; i < sb.length(); i++) {
			char c = sb.charAt(i);
			if (c >= 'ァ' && c <= 'ン') {
				sb.setCharAt(i, (char) (c - 'ァ' + 'ぁ'));
			} else if (c == 'ヵ') {
				sb.setCharAt(i, 'か');
			} else if (c == 'ヶ') {
				sb.setCharAt(i, 'け');
			} else if (c == 'ヴ') {
				sb.setCharAt(i, 'う');
				sb.insert(i + 1, '゛');
				i++;
			}
		}
		return sb.toString();
	}
	
	private String getFirstValue(String value) {
		if( value == null ) {
			return(null);
		}
		
		String[] valueArr = value.split(", *");
		if( valueArr != null ) {
			return(valueArr[0]);
		}
		return(null);
	}
}

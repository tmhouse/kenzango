/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.android.kenzango;

import jp.tmhouse.TmLibrary.Utils.TmCommonValues;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Talkbackへ何かを行わせるためのReceiver.
 * @author mutoh
 *
 */
public class TmReceiver extends BroadcastReceiver {
	public static final String COMMAND_NAME = "commandName";

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		TmLog.i("onReceive ACTION=" + action);
		
		if( action.equals(TmCommonValues.ACTION_SUSPEND_TOUCH_EXPLORE) ) {
	        callTalkBack(context, TmCommonValues.ACTION_SUSPEND_TOUCH_EXPLORE);
		} else if( action.equals(TmCommonValues.ACTION_RESUME_TOUCH_EXPLORE) ) {
	        callTalkBack(context, TmCommonValues.ACTION_RESUME_TOUCH_EXPLORE);
		} else if( action.equals(TmCommonValues.ACTION_STOP_SERVICE) ) {
	        callTalkBack(context, TmCommonValues.ACTION_STOP_SERVICE);
		}
	}
	
	private void callTalkBack(Context context, String action) {
        Intent si = new Intent(context, TmTalkBackService.class);
        si.putExtra(COMMAND_NAME, action);
        context.startService(si);	
	}
	

}

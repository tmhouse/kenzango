/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.android.kenzango;

import java.util.ArrayList;

import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;
import jp.tmhouse.TmLibrary.View.TmAutoWrapLayout;
import jp.tmhouse.TmLibrary.View.TmTenjiView;

import android.content.Context;
import android.graphics.PixelFormat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.marvin.talkback.TextToSpeechOverlay;

/**
 * TextToSpeechOverlayは、しゃべるtextを画面下に表示するが、
 * TmTextToBrailleOverlayは点字で表示する.
 * @author mutoh
 *
 */
public class TmTextToBrailleOverlay extends TextToSpeechOverlay {
    private TextView mText;
    private LinearLayout m_ll;
    private TmAutoWrapLayout m_brailles;

    public TmTextToBrailleOverlay(Context context) {
        super(context);

        final WindowManager.LayoutParams params = getParams();
        params.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        params.format = PixelFormat.TRANSPARENT;
        params.flags |= WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        params.flags |= WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        params.flags |= WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //params.height = 600;
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
        setParams(params);

        mText = new TextView(context);
        mText.setTextSize(40.0f);
        //mText.setBackgroundColor(0x60FF0000);
        //mText.setBackgroundColor(0xc0c90060);
        mText.setPadding(10, 10, 10, 10);
        mText.setGravity(Gravity.CENTER);

        // 全体の枠
        m_ll = new LinearLayout(context);
        m_ll.setOrientation(LinearLayout.VERTICAL);
        m_ll.setBackgroundColor(0xc0c90060);
        m_ll.addView(mText);

        // 点字を表示する枠
        m_brailles = new TmAutoWrapLayout(context);
        
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
        		ViewGroup.LayoutParams.WRAP_CONTENT,
        		ViewGroup.LayoutParams.WRAP_CONTENT);
        //lp.setMargins(0, 0, 0, 0);
        m_brailles.setLayoutParams(lp);
        	
        //m_brailleLL.setOrientation(LinearLayout.HORIZONTAL);
        m_ll.addView(m_brailles);
        
        // test
        /***
        TmTenjiView tv = new TmTenjiView(context);
        tv.setLayoutParams(new ViewGroup.LayoutParams(150, 150));
        tv.setLeftSide(1);
        tv.setRightSide(2);
        tv.setString("あ");
        m_brailleLL.addView(tv);
        ***/
        
        setContentView(m_ll);
    }
    
    private TmAutoWrapLayout.LayoutParams m_tenjiLP = 
    		new TmAutoWrapLayout.LayoutParams(120, 160);
    
    /**
     * 点字を表示するためにLinearLayoutへViewをaddする.
     * @param bsArr
     */
    private void addTenjiView(ArrayList<BrailleSet> bsArr) {
    	// 前回のオブジェクトを消去
    	m_tenjiViewArr.clear();
    	m_brailles.removeAllViews();

    	TmTenjiView prevTv = null;
    	for( BrailleSet bs : bsArr ) {
   			for( int i = 0; i < bs.length(); i++ ) {
   				TmTenjiView tv = new TmTenjiView(getContext());
   				m_tenjiViewArr.add(tv);
   				tv.setLayoutParams(m_tenjiLP);
    			tv.set(bs.get(i));
    			m_brailles.addView(tv);
    			if( prevTv != null ) {
    				prevTv.append(tv);
    			}
    			prevTv = tv;
    			if( i == 0 ) {
    				// 最初だけ文字が入っている
    				tv.setString(bs.getString());
    			}
    		}
    	}
    }
    
    private ArrayList<TmTenjiView> m_tenjiViewArr = new ArrayList<TmTenjiView>(128);
    private TmTenjiView				m_lastWakuView;

    /**
     * 点字の点に目立つ枠を表示する.
     * @param position
     * @param brailleNum1
     * @param brailleNum2
     */
    public void setWaku(int position, int brailleNum1, int brailleNum2) {
    	if( position <= 0 ) {
    		throw new RuntimeException("position is out of index:" + position);
    	}
    	if( brailleNum1 < 0 || brailleNum1 > 6 ) {
    		throw new RuntimeException("brailleNum1 is out of index:" + brailleNum1);
    	}
    	if( brailleNum2 < 0 || brailleNum2 > 6 ) {
    		throw new RuntimeException("brailleNum2 is out of index:" + brailleNum2);
    	}
    	
    	if( m_tenjiViewArr.size() < position ) {
    		TmLog.e("error. m_tenjiViewArr.size() < position");
    		return;
    	}

    	clearWaku();
    	TmTenjiView tv = m_tenjiViewArr.get(position - 1);
    	tv.setWakuPoint(brailleNum1, brailleNum2);
    	m_lastWakuView = tv;
    }
    public void clearWaku() {
    	if( m_lastWakuView != null ) {
    		m_lastWakuView.clearWakuPoint();
    		m_lastWakuView = null;
    	}
    }

    public void speak(String text, ArrayList<BrailleSet> bs) {
        if (TextUtils.isEmpty(text)) {
            hide();
            return;
        }
        mText.setText(text.trim());
        
        addTenjiView(bs);

        show();
    }
}

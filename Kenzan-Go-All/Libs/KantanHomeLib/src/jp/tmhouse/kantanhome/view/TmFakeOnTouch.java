/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.view;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityManager;

public class TmFakeOnTouch {
	private View	m_view;
	private long	m_hoverDownTime;
	private AccessibilityManager mAccessibilityManager;
	private Method	m_isEBTmethod = null;
	private static final String c_ISEBT_METHODNAME = "isTouchExplorationEnabled";
	
	public TmFakeOnTouch(View view) {
		m_view = view;
		Context ctxt = view.getContext();
		mAccessibilityManager = (AccessibilityManager)ctxt.getSystemService(
        		Context.ACCESSIBILITY_SERVICE);
		
		try {
			m_isEBTmethod = mAccessibilityManager.getClass().getMethod(c_ISEBT_METHODNAME);
		} catch( NoSuchMethodException e ) {
			Log.e("TmFakeOnTouch", c_ISEBT_METHODNAME + " not found");
		}
	}

	/**
	 * ExploreByTouchが稼動しているときはdispatchTouchEvent()を呼ぶ.
	 * @param event
	 * @return
	 */
	public boolean dispatchFakeTouchEvent(MotionEvent event) {
		// イベント交換
		MotionEvent touchEvent = convertHover2TouchEvent(event);
		if( touchEvent != null ) {
			return(m_view.dispatchTouchEvent(touchEvent));
		}
		return(false);
	}
	
	/**
	 * ExploreByTouchが稼動しているときはhoverからtouchイベントを生成する.
	 * @param e
	 * @return touch event
	 */
	public MotionEvent convertHover2TouchEvent(MotionEvent e) {
		if( m_isEBTmethod == null ) {
			return(null);
		}
		
		Boolean isTEBMode = false;
		try {
			isTEBMode = (Boolean)m_isEBTmethod.invoke(mAccessibilityManager);
		} catch ( IllegalAccessException ex) {
			ex.printStackTrace();
		} catch ( IllegalArgumentException ex) {
			ex.printStackTrace();
		} catch ( InvocationTargetException ex) {
			ex.printStackTrace();
		}
		
		if( isTEBMode == false ) {
			return(null);
		}
		
		int action = e.getAction();
		long hoverDownTime = 0;
		if( action == MotionEvent.ACTION_HOVER_ENTER ) {
			action = MotionEvent.ACTION_DOWN;
			// hoverDownTime最初
			hoverDownTime = e.getEventTime();
			m_hoverDownTime = hoverDownTime;
		} else if( action == MotionEvent.ACTION_HOVER_EXIT) {
			action = MotionEvent.ACTION_UP;
			// hoverDownTimeはクリアするが今回のイベントは以前のまま
			hoverDownTime = m_hoverDownTime;
			m_hoverDownTime = 0;
		} else if( action == MotionEvent.ACTION_HOVER_MOVE ) {
			action = MotionEvent.ACTION_MOVE;
			// move中は最初の値そのまま
			hoverDownTime = m_hoverDownTime;
		} else {
			// はずれ
			return(null);
		}
		
		// ニセイベント生成
		MotionEvent touchEvent = MotionEvent.obtain(hoverDownTime,
               	e.getEventTime(), 
               	action, 
               	e.getRawX(), e.getRawY(), e.getMetaState());
		return(touchEvent);
	}
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.view;

import java.util.ArrayList;

import jp.tmhouse.kantanhome.R;
import jp.tmhouse.kantanhome.view.KantanLayout.ItemData;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * １ページ分のViewのかたまり.
 * @author mutoh
 *
 */
public class OnePageView extends LinearLayout {
	private String m_titleWithPage = "";
	private String m_pageStr = "";
	private static int MP = LayoutParams.MATCH_PARENT;
	private static int WC = LayoutParams.WRAP_CONTENT;
	private static int		c_BUTTON_PADDING = 0;
	//private Handler		m_handler = new Handler();
	private ArrayList<KantanLayout.ItemData>	m_itemAll;
	private int 	m_colNum;
	private int 	m_rowNum;
	private TextView	m_titleView;
	
	public OnePageView(Context context, 
			ArrayList<ItemData> itemAll, int rowNum, int colNum,
			String title, int page, int totalPage)
	{
		super(context);
		setOrientation(LinearLayout.VERTICAL);
		setLayoutParams(new LinearLayout.LayoutParams(MP, MP));
		
		m_itemAll = itemAll;
		m_colNum = colNum;
		m_rowNum = rowNum;
		
        // タイトル
		if( title != null ) {
			if( totalPage > 1) {
				m_pageStr = (page+1) + "/" + totalPage;
			}
			//m_titleWithPage = title + " " + m_pageStr;
			m_titleWithPage = m_pageStr + " " + title;
	        m_titleView = createTitleView(getContext(), m_titleWithPage);
	        addView(m_titleView);
		}
		
		// グリッド
		KantanGrid2 grid = new KantanGrid2(getContext(), rowNum);
		
		int itemInPageNum = m_colNum * m_rowNum;
		int startNum = page * itemInPageNum;
		int endNum = startNum + itemInPageNum;
		int itemAllSize = m_itemAll.size();
		for( int i = startNum; i < endNum; i++ ) {
			if( itemAllSize <= i ) {
				break;
			}
			grid.addItem(m_itemAll.get(i));
		}
		grid.addFinished();
		
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(MP, MP);
		addView(grid, lp);
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
	}
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		// 文字のサイズを調整する
		if( m_titleView != null ) {
			int height = MeasureSpec.getSize(heightMeasureSpec);
			int itemH = height / 8;
			m_titleView.setTextSize(TypedValue.COMPLEX_UNIT_PX, itemH/2);
			m_titleView.setHeight(itemH);
		}
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	public String getTitleWithPage() {
		return(m_titleWithPage);
	}
	public String getPageString() {
		return(m_pageStr);
	}
	
	private TextView createTitleView(Context ctx, String text) {
        TextView tv = new TextView(ctx);
        tv.setBackgroundColor(Color.BLACK);
        tv.setLines(1);
        tv.setText(text);
        tv.setSingleLine(true);
        tv.setFocusable(true);
        tv.setFocusableInTouchMode(true);
        tv.setGravity(Gravity.CENTER);
        tv.setEllipsize(android.text.TextUtils.TruncateAt.MARQUEE);
		tv.setMarqueeRepeatLimit(-1);
		tv.setHorizontalFadingEdgeEnabled(true);
		tv.requestFocus();
		
        return(tv);
	}
	
	/**
	 * 
	 * @author mutoh
	 *
	 */
	private interface GridInterface {
		public void addItem(ItemData data);
		public void addFinished();
	}
	
	/**
	 * 
	 * @author mutoh
	 *
	 */
	private class KantanGrid2 extends LinearLayout
		implements GridInterface
	{
		private int m_itemNumInPage;
		private int m_itemNum;
		private static final String TAG = "KantanGrid2";
		private ArrayList<TextView> m_dummyList = new ArrayList<TextView>(16);
		private ArrayList<Button>   m_buttonList = new ArrayList<Button>(16);
		
		public KantanGrid2(Context context, int itemNumInPage) {
			super(context);
			m_itemNumInPage = itemNumInPage;
			this.setContentDescription("");
			this.setOrientation(LinearLayout.VERTICAL);
			this.setBackgroundColor(Color.BLACK);
			this.setPadding(c_BUTTON_PADDING, c_BUTTON_PADDING, 
					c_BUTTON_PADDING, c_BUTTON_PADDING);
			this.setHapticFeedbackEnabled(false);
		}
		
		@Override
		public void onLayout(boolean changed, int l, int t, int r, int b) {
			Log.d(TAG, "onLayout:height=" + this.getHeight());
			// 文字のサイズを調整する
			int h = this.getHeight();
			int itemH = h / m_itemNumInPage / 3;
			for( Button btn : m_buttonList ) {
				btn.setTextSize(TypedValue.COMPLEX_UNIT_PX, itemH);
			}
			for( TextView txt : m_dummyList ) {
				txt.setTextSize(TypedValue.COMPLEX_UNIT_PX, itemH/2);
			}
			super.onLayout(changed, l, t, r, b);
		}
		
		/**
		 * アイテム追加.
		 * @param label
		 * @param l
		 */
		@Override
		public void addItem(ItemData data) {
			Log.d(TAG, "height=" + this.getHeight());
			
			Button b = new Button(getContext());
			m_buttonList.add(b);
			b.setText(data.m_label);
			b.setPadding(0, 0, 0, 0);
			b.setContentDescription(data.m_label);
            b.setOnClickListener(data.m_onclickListener);
            b.setEnabled(data.m_bEnabled); 
            
            LayoutParams ll = new LayoutParams(MP, MP);
            ll.weight = 1;
            ll.leftMargin = 0;
            addView(b, ll);
            m_itemNum++;
		}
		
		@Override
		public void addFinished() {
			boolean bIsFirst = true;
			for( int i = m_itemNum; i < m_itemNumInPage; i++ ) {
				TextView b = new TextView(getContext());
				m_dummyList.add(b);
				if( bIsFirst ) {
					b.setText(getContext().getString(R.string.noItems_under_here));
					bIsFirst = false;
				} else {
					b.setText("");
				}
				LayoutParams ll = new LayoutParams(MP, MP);
				ll.weight = 1;
				addView(b, ll);
			}
		}
	}
	
	/**
	 * 
	 * @author mutoh
	 *
	 */
	private class KantanGrid extends GridView 
		implements GridInterface
	{
		private MyGridAdapter	m_adapter;
		private Handler m_handler = new Handler();
		
		public KantanGrid(Context context) {
			super(context);
			this.setContentDescription("");
			this.setBackgroundColor(Color.BLACK);
			m_adapter = new MyGridAdapter(context);
			this.setAdapter(m_adapter);
			//this.setColumnWidth(200);
			//this.setNumColumns(4);
			this.setVerticalSpacing(c_BUTTON_PADDING);
			this.setHorizontalSpacing(c_BUTTON_PADDING);
			this.setPadding(c_BUTTON_PADDING, c_BUTTON_PADDING, 
					c_BUTTON_PADDING, c_BUTTON_PADDING);
			this.setHapticFeedbackEnabled(false);
		}
		
		/***
		@Override
		public boolean onHoverEvent(MotionEvent e) {
			//Log.d(TAG, "onHoverEvent(KantanGrid)");
			return(false);
		}
		***/
		
		/**
		 * アイテム追加.
		 * @param label
		 * @param l
		 */
		@Override
		public void addItem(ItemData data) {
			m_adapter.addItem(data);
		}
		
		@Override
		public void addFinished() {
			
		}
	
		private class MyGridAdapter extends BaseAdapter {
		    private Context mContext;
		    
		    private ArrayList<ItemData> m_items = new ArrayList<ItemData>(32);
	
		    public MyGridAdapter(Context c) {
		        mContext = c;
		    }
		    
		    public void addItem(ItemData item) {
		    	m_items.add(item);
		    }
	
			@Override
		    public int getCount() {
		        return m_items.size();
		    }
	
			@Override
		    public Object getItem(int position) {
		        return null;
		    }
	
			@Override
		    public long getItemId(int position) {
		        return 0;
		    }
	
		    // create a new ImageView for each item referenced by the Adapter
			@Override
		    public View getView(int position, View convertView, ViewGroup parent) {
				Button v = null;
		        if (convertView == null) {
		        	Button btn = new Button(mContext) {
		        		@SuppressLint("NewApi")
		        		@Override
		        		public boolean onHoverEvent(MotionEvent e) {
		        			super.onHoverEvent(e);
		        			// ボタンがenableでhoverされるとtrueを返すようだ対策。
		        			// このせいで「なにがしボタン」って言わなくなった。
		        			return(false);
		        		}
		        	};
		            btn.cancelLongPress();
		            btn.setBackgroundResource(R.drawable.gridbuttonstyle);
		            btn.setTextSize(25);
		            //setViewTextSize(btn);
		            //btn.setSingleLine(false);
		            //btn.setMaxLines(2);
		            //btn.setLines(2);
		            //AbsListView.LayoutParams tv_lp = new AbsListView.LayoutParams(MP, WC);
		            //btn.setLayoutParams(tv_lp);
		            //btn.setHeight(100);
		            //btn.setWidth(100);
		            
		            //btn.setHapticFeedbackEnabled(false);
		            v = btn;
		        } else {
		        	v = (Button)convertView;
		        }
		        
		        // 後で高さを調整する
		        // ※GridViewがバグっていて、先頭のViewを生成する段階
		        // では、GridViewのheightが取れないため。
		        m_handler.postAtFrontOfQueue(new AfterResizer(
		            		v, parent, c_BUTTON_PADDING, m_rowNum));
		            
		        // Viewに対する設定
		        ItemData data = m_items.get(position);
		        //String posLabel = String.valueOf(position + 1);
	            //String label = "" + posLabel + " " + data.m_label;
	            String label = data.m_label;
	            v.setText(label);
	            v.setContentDescription(label);
	            v.setOnClickListener(data.m_onclickListener);
	            v.setEnabled(data.m_bEnabled); 
	
				//Log.d(TAG, "label=" + label);
		        return v;
		    }
		}
	}
	
	/**
	 * Viewの高さをセットするRunnable.
	 * @author mutoh
	 *
	 */
	private class AfterResizer implements Runnable {
		private Button		m_resizeTargetView;
		private ViewGroup	m_fromView;
		private int 		m_padding;
		private int 		m_rowNum;
		public AfterResizer(
				Button resizeTargetView, ViewGroup fromView, 
				int padding, int rowNum) {
			m_resizeTargetView = resizeTargetView;
			m_fromView = fromView;
			m_padding = padding;
			m_rowNum = rowNum;
		}
		@Override
		public void run() {
			int gridHeight = m_fromView.getHeight();
        	int btnHeight = (gridHeight - (m_padding * (m_rowNum+1))) / m_rowNum;
			m_resizeTargetView.setHeight(btnHeight);
			
			Button b = m_resizeTargetView;
			float size = (btnHeight - b.getPaddingBottom() - 
					b.getPaddingTop()) / 2.5f;
			m_resizeTargetView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
		}
		
	}
}
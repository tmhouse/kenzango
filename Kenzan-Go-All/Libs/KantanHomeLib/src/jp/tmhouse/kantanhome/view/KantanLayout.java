/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.view;

import java.util.ArrayList;

import jp.tmhouse.TmLibrary.Utils.Speech.TmSpeech;
import jp.tmhouse.TmLibrary.Vibrate.TmAsyncVibrator;
import jp.tmhouse.kantanhome.util.SettingSaver;
import jp.tmhouse.kantanhome.view.TmFakeOnTouch;
import jp.tmhouse.kantanhome.view.TmLoopedViewPager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Vibrator;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.accessibility.AccessibilityManager;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 
 * @author mutoh
 *
 */
public class KantanLayout extends LinearLayout {
	private static final String	TAG = "KantanLayout";
	private static int MP = LayoutParams.MATCH_PARENT;
	private static int WC = LayoutParams.WRAP_CONTENT;
	

	public static int 		c_COL_NUM_DEFAULT = 1;
	private static int		c_ROW_NUM_DEFAULT = 4;
	private static int 	c_VIBRATE_TIME = 200;
	private static String	c_LAST_PAGE = "_lastPage";
	

	private Context			m_context;
	private TmSpeech		m_speech;
	private TmAsyncVibrator m_vibrator;
	private int 	m_colNum = c_COL_NUM_DEFAULT;
	private int 	m_rowNum = c_ROW_NUM_DEFAULT;
	private int	m_totalPageNum;
	private String 	m_title;
	private ArrayList<ItemData>	m_itemAll = new ArrayList<ItemData>(120);
	private TmLoopedViewPager	m_viewPager;
	private SettingSaver		m_setting;
	private TmFakeOnTouch		m_fakeOnTouch;
	private GestureDetector		m_gestureDetector;
	private AccessibilityManager m_accessibilityManager;
	
	/**
	 * TmLoopedViewPagerのリスナ.
	 * @author mutoh
	 *
	 */
	private class ViewPagerListener 
		implements TmLoopedViewPager.TmLoopedViewPagerListener
	{
		@Override
		public View OnInstantiateItem(int page) {
			// pageのViewを返す
			View v = new OnePageView(m_context, 
					m_itemAll, m_rowNum, m_colNum, 
					m_title, page, m_totalPageNum);
			return v;
		}

		@Override
		public void onPageScrollChanged(int page) {
			Log.d(TAG, "current page=" + page);
			
			// 遷移した最後のページを覚えておく
			saveLastPage(page);
		}
	}
	

	
	public KantanLayout(Context context, String title, int rowNum, int colNum) {
		super(context);
		init(context, title, rowNum, colNum);
	}
	public KantanLayout(Context context, String title) {
		super(context);
		init(context, title, c_ROW_NUM_DEFAULT, c_COL_NUM_DEFAULT);
	}
	
	private void init(Context context, String title, int rowNum, int colNum) {
		m_context = context;
		m_rowNum = rowNum;
		m_colNum = colNum;
		m_title = title;
		
		m_accessibilityManager = (AccessibilityManager)
				m_context.getSystemService(Context.ACCESSIBILITY_SERVICE);
		
		m_fakeOnTouch = new TmFakeOnTouch(this);
		m_gestureDetector = new GestureDetector(context, new MyOnGestureListener());
		
		m_setting = new SettingSaver(context);
		
		m_speech = new TmSpeech();
		//m_speech.initialize(m_context);
		
        m_vibrator = new TmAsyncVibrator(
    			((Vibrator)m_context.getSystemService(Context.VIBRATOR_SERVICE)));
        //m_vibrator.start();
        
		this.setOrientation(LinearLayout.VERTICAL);
		this.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.MATCH_PARENT));
	}
	
	private void setupViews(Context context, String title, int colNum) {
		//setBackgroundColor(Color.BLUE);
		
		// 全てのページのViewをaddViewする
		int itemAllSize = m_itemAll == null ? 0 : m_itemAll.size();
		int itemInPageNum = m_colNum * m_rowNum;
		m_totalPageNum = itemAllSize / itemInPageNum;
		if( itemAllSize % itemInPageNum > 0 ) {
			// 余りがあるから最後のページとして1足す
			m_totalPageNum++;
		}
		// 一切アイテムがない場合は１ページとする
		if( m_totalPageNum == 0 ) {
			m_totalPageNum = 1;
		}
		
		// ViewPager生成
		m_viewPager = new TmLoopedViewPager(
				context, m_totalPageNum, new ViewPagerListener());
		
		// 最終遷移ページを表示
		m_viewPager.setCurrentItem(getLastPage(), false);
			
		addView(m_viewPager);
	}
	
	/**
	 * 設定のキー名の取得.
	 * @return
	 */
	private String getLastPageKeyString() {
		return(m_title + c_LAST_PAGE);
	}
	
	private void saveLastPage(int page) {
		m_setting.saveInt(getLastPageKeyString(), page);
	}
	
	private int getLastPage() {
		int lastPage = m_setting.getInt(getLastPageKeyString(), 0);
		// 前回の最終遷移ページが大きすぎる場合
		if( m_totalPageNum <= lastPage ) {
			lastPage = 0;
		}
		return(lastPage);
	}
	
	public void clearSavedPage() {
		saveLastPage(0);
	}
	
	@SuppressLint("NewApi")
	@Override
	public boolean onHoverEvent(MotionEvent e) {
		// TouchEventシミュレート
		// ここではfakeOnTouch.dispatchFakeTouchEvent()は呼ばない。
		// なぜなら、ViewPagerが指に吸い付いて反応してしまうから。
		// やることは、フリックのdetectのみ。
		MotionEvent touchEvent = m_fakeOnTouch.convertHover2TouchEvent(e);
		if( touchEvent != null ) {
			m_gestureDetector.onTouchEvent(touchEvent);
		}
		
		return(super.onHoverEvent(e));
	}
	
	//複雑なタッチイベント処理
	private class MyOnGestureListener extends SimpleOnGestureListener {
		// 他にもいろいろあるが今はいらない。
		
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			Log.d("gesture", "onFling:velocityX=" + velocityX + ", velocityY=" + velocityY);
			float e1X = e1.getX();
			float e2X = e2.getX();
			float e1Y = e1.getY();
			float e2Y = e2.getY();

			float delta = (e2Y - e1Y)/(e2X - e1X);
			float deltaAbs =  Math.abs(delta);
			Log.d("gesture", "delta=" + delta);

			if( deltaAbs > 0.9f && deltaAbs < 1.1f ) {
				// あいまいなのでNGにする
				Log.d("gesture", "曖昧でわからん！！！");
				return(false);
			}
			
			if( deltaAbs < 1.0f ) {
				if( Math.abs(velocityX) < 2000 ) {
					Log.d("gesture", "フリック量不足！！！");
					return(false);
				}
				interruptAccessibility();
				if( e1X < e2X ) {
					// 右方向
					Log.d("gesture", "右");
					m_viewPager.scrollPrevItem(true);
				} else {
					// 左方向
					Log.d("gesture", "左");
					m_viewPager.scrollNextItem(true);
				}
			} else {
				if( e2Y < e1Y ) {
					// 上方向
					Log.d("gesture", "上");
				} else {
					// 下方向
					Log.d("gesture", "下");
				}
			}
			return true;
		}
	};
	
	private void interruptAccessibility() {
		if( m_accessibilityManager != null ) {
			m_accessibilityManager.interrupt();
		}
	}
	
	@Override
	protected void onAttachedToWindow () {
		m_speech.initialize(m_context);
        m_vibrator.start();
		
		this.setupViews(getContext(), m_title, m_colNum);
	}
	
	@Override
	protected void onDetachedFromWindow() {
		m_speech.shutdown();
		//m_speech = null;
		
        m_vibrator.end();
        //m_vibrator = null;
	}
	
	/**
	 * アイテム追加.
	 * @param label
	 * @param l
	 */
	public void addItem(String label, OnClickListener l, boolean bEnabled) {
		//int num = m_itemAll.size();
		//Log.i(TAG, "addItem:label[" + num + "=" + label);
		m_itemAll.add(new ItemData(label, l, bEnabled));
	}
	
	/**
	 * アイテムデータフォルダ.
	 * @author mutoh
	 *
	 */
	public class ItemData {
		String			m_label;
		OnClickListener	m_onclickListener;
		boolean			m_bEnabled;
		
		public ItemData(String label, OnClickListener l, boolean bEnabled) {
			m_label = label;
			m_onclickListener = l;
			m_bEnabled = bEnabled;
		}
	}
	
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.view;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

/**
 * LoopedViewPager.
 * ViewPagerの最終ページと最初のページが連結してる版.
 * 
 * コンストラクタでページ数とアイテムを生む要求リスナ
 * OnInstantiateItemListenerをセットすること。
 * 
 * OnInstantiateItemListenerの実装は、要求pageに対応する
 * Viewを返すこと。Viewのキャッシュはcallerに任せるので、
 * 呼ばれたからってばんばんnewすると知らないよ。
 * 
 * @author mutoh
 *
 */
public class TmLoopedViewPager extends ViewPager {
	private static final String TAG = "LoopedViewPager";
	private static final int c_ALL_PAGE_COUNT = Integer.MAX_VALUE;
	private int			m_pages;
	private int			m_firstPos;
	private int			m_currentPage;
	private int			m_adapterPages;
	private TmLoopedViewPagerListener	m_listener;

	/**
	 * リスナ.
	 * @author mutoh
	 *
	 */
	public interface TmLoopedViewPagerListener {
		/**
		 * ページのViewインスタンス生成要求.
		 * @param page	0から始まるページ番号.
		 * @return		表示するページのView
		 */
		public View OnInstantiateItem(int page);
		
		/**
		 * ページスクロール完了.
		 * @param page	現在のページ。0 <= page
		 */
		public void onPageScrollChanged(int page);
	}
	
	/**
	 *
	 * @param context
	 * @param pages
	 * @param listener
	 */
	public TmLoopedViewPager(Context context, int pages, 
			TmLoopedViewPagerListener listener) {
		super(context);
		if( context == null || listener == null ) {
			throw new RuntimeException("ゴルァ");
		}
		
		// 0以下のページは1ページ表示するため
		m_pages = pages <= 0 ? 1 : pages;
		
		// 1ページしかないときはadapterのページ数も1にする。
		if( m_pages == 1 ) {
			m_adapterPages = 1;
		} else {
			m_adapterPages = c_ALL_PAGE_COUNT;
		}
		
		m_listener = listener;
		setAdapter(new MyPagerAdapter());
		setOnPageChangeListener(new MyOnPageChangeListener());

		
		// 全positionがInteger.MAXあるので、その半分のところで
		// 各セット中の先頭が初期表示位置になるよう計算する。
		// [概念図] 各ページが3ページあるとしての図。
		// 先頭(行き止まり)     最初   次            最後(行き止まり)
		//                      *                              *:表示させたい
		// [0,1,2].............[0,1,2][0,1,2].......[0,1,2]    各セット
		// 0                   真ん中                    Int.MAX   (position数)
		// だから先頭のpositionは、最大セット数の半分をページ数で
		// 掛ければいいんでないか。
		int maxSets = Integer.MAX_VALUE / m_pages;
		m_firstPos = (maxSets / 2)* m_pages;
		setCurrentItem(-1, false);
	}
	
	/**
	 * Set the currently selected page.
	 * 先頭からのオフセットを計算する必要がある。
	 */
	@Override
    public void setCurrentItem(int item, boolean smoothScroll) {
		int pos = item < 0 ? m_firstPos : m_firstPos + item;
		super.setCurrentItem(pos, smoothScroll);
	}
	@Override
    public void setCurrentItem(int item) {
		int pos = item < 0 ? m_firstPos : m_firstPos + item;
		super.setCurrentItem(pos);
	}
	
	/**
	 * 次アイテムへスクロール.
	 * @param smoothScroll
	 */
	public void scrollNextItem(boolean smoothScroll) {
		int curItem = super.getCurrentItem();
		super.setCurrentItem(curItem + 1, smoothScroll);
	}
	
	/**
	 * 次アイテムへスクロール.
	 * @param smoothScroll
	 */
	public void scrollPrevItem(boolean smoothScroll) {
		int curItem = super.getCurrentItem();
		super.setCurrentItem(curItem - 1, smoothScroll);
	}
	
	/**
	 * 
	 * @author mutoh
	 *
	 */
	private class MyOnPageChangeListener implements OnPageChangeListener {
		@Override
		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {
		}

		@Override
		public void onPageSelected(int position) {
			m_currentPage = pos2page(position);
			//Log.d(TAG, "onPageSelected current page=" + m_currentPage);
		}
		
		@Override
		public void onPageScrollStateChanged(int state) {
			if ( state == ViewPager.SCROLL_STATE_IDLE ) {
				//Log.d(TAG, "onPageScrollStateChanged current page=" + m_currentPage);
				m_listener.onPageScrollChanged(m_currentPage);
			}
		}
	}
	
	/**
	 * ページはpositionを全ページ数で割った余り
	 * @param pos
	 * @return
	 */
	private int pos2page(int pos) {
		return(pos % m_pages);
	}
	
	/**
	 * 
	 * @author mutoh
	 *
	 */
	private class MyPagerAdapter extends PagerAdapter {
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			View v = m_listener.OnInstantiateItem(pos2page(position));
			container.addView(v);
			return(v);
		}
 
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager)container).removeView((View)object);
		}
 
		@Override
		public int getCount() {
			return(m_adapterPages);
		}
 
		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}
	}
}

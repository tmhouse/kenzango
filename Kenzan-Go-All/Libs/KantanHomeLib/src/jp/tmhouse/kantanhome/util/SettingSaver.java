/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.util;

import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

/**
 * 設定保管・取得
 * @author mutoh
 *
 */
public class SettingSaver {
	private Context		m_context;
	private SharedPreferences	m_sp;
	
	public SettingSaver(Context context) {
		m_context = context;
		m_sp = PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	private static SharedPreferences getAppSharedPreferences(Context ctx) {
		Context appCtx = ctx.getApplicationContext();
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(appCtx);
		return(sp);
	}
	
	public void saveString(String key, String val) {
		Editor ed = m_sp.edit();
		ed.putString(key, val);
		ed.commit();
	}
	public void saveBoolean(String key, boolean val) {
		Editor ed = m_sp.edit();
		ed.putBoolean(key, val);
		ed.commit();
	}
	public void saveFloat(String key, float val) {
		Editor ed = m_sp.edit();
		ed.putFloat(key, val);
		ed.commit();
	}
	public void saveInt(String key, int val) {
		Editor ed = m_sp.edit();
		ed.putInt(key, val);
		ed.commit();
	}
	public void saveLong(String key, long val) {
		Editor ed = m_sp.edit();
		ed.putLong(key, val);
		ed.commit();
	}
	public void saveStringSet(String key, Set<String> val) {
		Editor ed = m_sp.edit();
		ed.putStringSet(key, val);
		ed.commit();
	}
	
	public String getString(String key, String defaultVal) {
		return(m_sp.getString(key, defaultVal));
	}
	public boolean getBoolean(String key, boolean defaultVal) {
		return(m_sp.getBoolean(key, defaultVal));
	}
	public float getFloat(String key, float defaultVal) {
		return(m_sp.getFloat(key, defaultVal));
	}
	public int getInt(String key, int defaultVal) {
		return(m_sp.getInt(key, defaultVal));
	}
	public long getLong(String key, long defaultVal) {
		return(m_sp.getLong(key, defaultVal));
	}
	public Set<String> getStringSet(String key) {
		return(m_sp.getStringSet(key, null));
	}
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.tmhouse.kantanhome;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;

/**
 * Represents a launchable application. An application is made of a name (or title), an intent
 * and an icon.
 */
public class ApplicationInfo {
    /**
     * The application name.
     */
    public CharSequence m_title;

    /**
     * The intent used to start the application.
     */
    Intent m_intent;

    /**
     * The application icon.
     */
    //Drawable icon;

    /**
     * When set to true, indicates that the icon has been resized.
     */
    boolean filtered;

    /**
     * Creates the application intent based on a component name and various launch flags.
     *
     * @param className the class name of the component representing the intent
     * @param launchFlags the launch flags
     */
    /***
    final void setActivity(ComponentName className, int launchFlags) {
        m_intent = new Intent(Intent.ACTION_MAIN);
        m_intent.addCategory(Intent.CATEGORY_LAUNCHER);
        m_intent.setComponent(className);
        m_intent.setFlags(launchFlags);
    }
    ***/
    
    /**
     * Creates the intent
     * @param compName
     * @param title
     */
    public final void setApplication(ComponentName compName, CharSequence title, int launchFlags) {
    	m_intent = new Intent();
		m_intent.setComponent(compName);
        m_intent.setFlags(launchFlags);
		m_title = title;
    }
    
    public void setData(Uri uri) {
    	m_intent.setData(uri);
    	m_intent.setAction(Intent.ACTION_VIEW);
    }
    
    public void setTitle(CharSequence title) {
    	m_title = title;
    }
    
    public Intent getIntent() {
    	return(m_intent);
    }
    public CharSequence getTitle() {
    	return(m_title);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApplicationInfo)) {
            return false;
        }

        ApplicationInfo that = (ApplicationInfo) o;
        return m_title.equals(that.m_title) &&
                m_intent.getComponent().getClassName().equals(
                        that.m_intent.getComponent().getClassName());
    }

    @Override
    public int hashCode() {
        int result;
        result = (m_title != null ? m_title.hashCode() : 0);
        final String name = m_intent.getComponent().getClassName();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Settings;

import java.util.ArrayList;
import java.util.List;

import jp.tmhouse.kantanhome.view.KantanLayout;
import jp.tmhouse.kantanhome.R;
import jp.tmhouse.kantanhome.activity.Utils.AppStarterActivity;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class SettingsActivity extends AppStarterActivity {
	private static final String TAG = "SettingsActivity";
	
    @Override
    public void onCreate(Bundle icicle) {
    	Log.i(TAG, "onCreate");
        super.onCreate(icicle);
        
        setupApps(getLayoutView());
    }
    
    @Override
    protected String getTitleBarString() {
    	return(getString(R.string.label_activity_settings_app));
    }
    
    private void setupApps(KantanLayout panel) {
    	panel.addItem(getString(R.string.label_system_settings), 
        	new OpenActivityRunner(this, android.provider.Settings.ACTION_SETTINGS),
        	true);
    	
    	panel.addItem(getString(R.string.label_home_app_select), 
            	new OnClickListener() {
    				@Override
    				public void onClick(View v) {
    					openHomeChooser(SettingsActivity.this);
    				}
            	}, true);
    	
    	panel.addItem(getString(R.string.label_button_de_home_info), 
            	new OnClickListener() {
    				@Override
    				public void onClick(View v) {
    					/****アプリ情報
    					Intent intent = new Intent(
    						android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, 
    						Uri.parse("package:"+getPackageName()));
    					startActivity(intent);
    					****/
    					startActivity(new Intent(SettingsActivity.this, AboutActivity.class));
    				}
            	}, true);
    	
    	// TODO ボタンでホーム設定
    	// ボタンのrow/colの設定とかの予定
    	//panel.addItem(getString(R.string.label_button_de_home_setting), null, false);
    }
    
    /**
     * homeアプリ選択のために使う。実体はない. 
     * @author mutoh
     *
     */
    public static class FakeHome extends Activity {
    }
    
    /**
     * ニセのhomeアプリをenableすることで、homeアプリ選択画面を表示させる。
     * @param c
     */
    public static void openHomeChooser(Context c) {
        PackageManager p = c.getPackageManager();
        ComponentName cN = new ComponentName(c, FakeHome.class);
        p.setComponentEnabledSetting(cN, 
        		PackageManager.COMPONENT_ENABLED_STATE_ENABLED, 
        		PackageManager.DONT_KILL_APP);

        Intent selector = new Intent(Intent.ACTION_MAIN);
        selector.addCategory(Intent.CATEGORY_HOME);            
        c.startActivity(selector);

        p.setComponentEnabledSetting(cN, 
        		PackageManager.COMPONENT_ENABLED_STATE_DISABLED, 
        		PackageManager.DONT_KILL_APP);
    }
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Wizard;

import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.kantanhome.R;
import jp.tmhouse.kantanhome.activity.Settings.SettingsActivity;
import jp.tmhouse.kantanhome.activity.Settings.SettingsActivity.FakeHome;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ChangeHomeActivity extends WizardCommonActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_change_home);
		
		Button changeBtn = (Button)findViewById(R.id.changeHomeBtn);
		changeBtn.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				// ホーム変更ダイアログの表示
				openHomeChooser();
			}
		});
	}
	
    /**
     * ニセのhomeアプリをenableすることで、homeアプリ選択画面を表示させる。
     * @param c
     */
    public void openHomeChooser() {
        PackageManager p = getPackageManager();
        ComponentName cN = new ComponentName(this, FakeHome.class);
        p.setComponentEnabledSetting(cN, 
        		PackageManager.COMPONENT_ENABLED_STATE_ENABLED, 
        		PackageManager.DONT_KILL_APP);

        Intent selector = new Intent(Intent.ACTION_MAIN);
        selector.addCategory(Intent.CATEGORY_HOME);            
        startActivityForResult(selector, 100);

        p.setComponentEnabledSetting(cN, 
        		PackageManager.COMPONENT_ENABLED_STATE_DISABLED, 
        		PackageManager.DONT_KILL_APP);
    }
    
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if( requestCode == 100 ) {
			TmLog.d("Change Home returned");
		}
	} 

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}

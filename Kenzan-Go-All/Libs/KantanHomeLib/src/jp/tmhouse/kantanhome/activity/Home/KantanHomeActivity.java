/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Home;

import java.util.Collections;
import java.util.List;

import jp.tmhouse.TmLibrary.Utils.TmCommonValues;
import jp.tmhouse.TmLibrary.Utils.Debug.TmAsyncWaitForDebugger;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.TmLibrary.Utils.Speech.TmSpeech;
import jp.tmhouse.kantanhome.view.KantanLayout;
import jp.tmhouse.kantanhome.BuildConfig;
import jp.tmhouse.kantanhome.R;
import jp.tmhouse.kantanhome.activity.Applications.ApplicationsActivity;
import jp.tmhouse.kantanhome.activity.KantanApp.KantanAppActivity;
import jp.tmhouse.kantanhome.activity.Settings.SettingsActivity;
import jp.tmhouse.kantanhome.activity.Utils.AppStarterActivity;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;


public class KantanHomeActivity extends AppStarterActivity {
	private static final String TAG = "kantanhomeactivity";
    
    private BroadcastReceiver 	mApplicationsReceiver = new ApplicationsIntentReceiver();
    private Handler 			m_handler = new Handler();
	private static TmSpeech	m_speech;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        
    	if( BuildConfig.DEBUG ) {
    		TmLog.enable(true);
    		TmAsyncWaitForDebugger.start(5000);
    	}
        
    	// アプリ一覧の取得
    	loadApplications(this);
    	
        initializeViews();
        
        // 初期化を遅延する。
        // 理由：デフォルトのhomeアプリで起動したとき、
        //       onCreate直後にすぐ死んで、また立ち上がる。
        //       なのでいったん初期化を遅延しておき、
        //       onDestroyで発火していなければこのrunnableを削除する。
        m_handler.postDelayed(m_initRunnable, 3000);
        
        // wellcome画面を表示
        //showWellcomeActivity();
    }
    
    private static boolean s_wellcomeActivityShown = false;
    private void showWellcomeActivity() {
    	// 最初の一回目、かつbootから30秒以内のとき(ホームとして立ち上がった場合)のみ
    	// wellcomeを表示する
    	long fromElapsed = 30 * 1000;
    	long elapsedTime = SystemClock.elapsedRealtime();
    	if( elapsedTime < fromElapsed && s_wellcomeActivityShown == false ) {
    		Intent i = new Intent(this, WellcomeActivity.class);  
			startActivity(i); 
			s_wellcomeActivityShown = true;
    	}
    }
    
    /**
     * 電話が使えるか否か.
     * @return
     */
    private boolean getTelephonyStatus() {
        TelephonyManager telephonyMgr = 
        		(TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        if( telephonyMgr != null ) {
        	String number = telephonyMgr.getLine1Number();
        	if( number != null && number.length() > 0 ) {
        		Log.e(TAG, "TELEPHONY OK");
        		return(true);
        	}
        }
        return(false);
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    }
    
    @Override
    protected String getTitleBarString() {
    	return(getString(R.string.label_activity_kantan_home));
    }
    
    private void initializeViews() {
    	KantanLayout mainPanel = getLayoutView();
        
    	/****
        // お知らせ 
        mainPanel.addItem(getString(R.string.label_activity_osirase_app), 
        		null, false);
        ****/
        
        // かんたんアプリ
        mainPanel.addItem(getString(R.string.label_activity_kantan_app), 
        		new OpenActivityRunner(this, KantanAppActivity.class), true);
        
        /***
        // 電話
        mainPanel.addItem(getString(R.string.label_activity_telephone_app), 
        		new OpenActivityRunner(this, TelephoneAppActivity.class), 
        		true); // IP電話とかもあるから
        		//getTelephonyStatus());
        ****/
        
        // アプリ
        mainPanel.addItem(getString(R.string.label_activity_appliations_app), 
        		new OpenActivityRunner(this, ApplicationsActivity.class), true);
        
        // 設定
        mainPanel.addItem(getString(R.string.label_activity_settings_app), 
        		new OpenActivityRunner(this, SettingsActivity.class), true);
        
        /***
        if( false ) {
        	mainPanel.addItem("Start Profile", 
        			new OnClickListener() {
						@Override
						public void onClick(View v) {
							android.os.Debug.startMethodTracing("profile");  
						}
        			}, true);
        	mainPanel.addItem("Stop Profile", 
        			new OnClickListener() {
						@Override
						public void onClick(View v) {
							android.os.Debug.stopMethodTracing();
						}
        			}, true);
        }
        ***/
    }

    /****
	@Override
	public boolean dispatchTouchEvent(MotionEvent e) {
		return(m_mainPanel.dispatchTouchEvent(e) || super.dispatchTouchEvent(e));
	}
	****/
    
    @Override
    public void onDestroy() {
    	Log.i(TAG, "SmartBrailleHome onDestroy");
        super.onDestroy();
        
        // Remove the callback for the cached drawables or we leak
        // the previous Home screen on orientation change
        //final int count = mApplications.size();
        //for (int i = 0; i < count; i++) {
            //mApplications.get(i).icon.setCallback(null);
        //}
        
        if( m_initRunnable == null ) {
        	// 初期化が完了している
        	enableKeyguard();

        	unregisterReceiver(mApplicationsReceiver);
    		getApplicationContext().unregisterReceiver(m_screenOnReceiver);
    		getApplicationContext().unregisterReceiver(m_screenOffReceiver);
    		
    		m_speech.shutdown();
        } else {
        	// 初期化が完了していない
        	m_handler.removeCallbacks(m_initRunnable);
        }
    }
    
    /**
     * 初期化のRunnable
     */
    private Runnable	m_initRunnable = new Runnable() {
		@Override
		public void run() {
			initialize();
		}
    };
    
    /**
     * 初期化
     */
    private void initialize() {
    	if( m_initRunnable == null ) {
    		Log.e("initialize", "ALREADY INIT.");
    		return;
    	}
    	
    	m_speech = new TmSpeech();
        m_speech.initialize(this);
        //m_speech.setRate(TmSpeech.c_FAST_SPEECH_RATE);
        //m_speech.setRate(1.8f);
    	//m_speech.speechTextAppend("かんたんホームへようこそ。");
    	
       	// スクリーンオンのレシーバー
    	IntentFilter screenOnIF = new IntentFilter(Intent.ACTION_SCREEN_ON);
    	getApplicationContext().registerReceiver(m_screenOnReceiver, screenOnIF); 
    	// スクリーンオフのレシーバー
    	IntentFilter screenOffIF = new IntentFilter(Intent.ACTION_SCREEN_OFF);
    	getApplicationContext().registerReceiver(m_screenOffReceiver, screenOffIF); 
        
        // View生成
        //createBrailleInputView();

        setDefaultKeyMode(DEFAULT_KEYS_SEARCH_LOCAL);

        //setContentView(R.layout.home);

        //loadApplications(true);
        registerIntentReceivers();

        // キーガード解除
        disableKeyguradIfNeed();
        
        // 初期化終了
    	m_initRunnable = null;
    }
    
    /**
     * Registers various intent receivers. The current implementation registers
     * only a wallpaper intent receiver to let other applications change the
     * wallpaper.
     */
    private void registerIntentReceivers() {
        //IntentFilter filter = new IntentFilter(Intent.ACTION_WALLPAPER_CHANGED);
        //registerReceiver(mWallpaperReceiver, filter);

        IntentFilter filter = new IntentFilter(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        filter.addDataScheme("package");
        registerReceiver(mApplicationsReceiver, filter);
    }
    
    
    @Override
    public void onStop() {
    	super.onStop();
    }
    
    /**
     * 停止あるいは隠れた.
     */
    @Override
    public void onPause() {
    	super.onPause();
    }
    
    /**
     * 現れた.
     */
    @Override
    public void onResume() {
    	super.onResume();
    }
    
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        // Close the menu
        if (Intent.ACTION_MAIN.equals(intent.getAction())) {
            getWindow().closeAllPanels();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
        	Log.d("home", "long down BACK");
    	} else if(event.getKeyCode() == KeyEvent.KEYCODE_SEARCH) {
        	Log.d("home", "long down SEARCH");
    	} else if(event.getKeyCode() == KeyEvent.KEYCODE_HOME) {
        	Log.d("home", "long down HOME");
    	} else if(event.getKeyCode() == KeyEvent.KEYCODE_MENU) {
        	Log.d("home", "long down MENU");
    	}
    	return(super.onKeyDown(keyCode, event));
    }
    
    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
    	if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
        	Log.d("home", "long press BACK");
    	} else if(event.getKeyCode() == KeyEvent.KEYCODE_SEARCH) {
        	Log.d("home", "long press SEARCH");
    	} else if(event.getKeyCode() == KeyEvent.KEYCODE_HOME) {
        	Log.d("home", "long press HOME");
    	}
    	return(super.onKeyLongPress(keyCode, event));
    }
    
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_BACK:
                    //mBackDown = true;
                    return true;
                case KeyEvent.KEYCODE_HOME:
                    //mHomeDown = true;
                    return true;
            }
        } else if (event.getAction() == KeyEvent.ACTION_UP) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_BACK:
                    if (!event.isCanceled()) {
                        // Do BACK behavior.
                    }
                    //mBackDown = true;
                    return true;
                case KeyEvent.KEYCODE_HOME:
                    if (!event.isCanceled()) {
                        // Do HOME behavior.
                    }
                    //mHomeDown = true;
                    return true;
            }
        }

        return super.dispatchKeyEvent(event);
    }
    
    /***
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	Log.d("home", "action=" + event.getAction() + ", event time=" + event.getEventTime());
    	boolean ret = false;
    	//ret = m_gestureDetector.onTouchEvent(event);
    	return(ret);
    }
    ***/

    
    /**
     * Receives notifications when applications are added/removed.
     */
    private class ApplicationsIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
        	loadApplications(context);
        }
    }
    
    /*
     * アプリケーション一覧の取得.
     */
    private static void loadApplications(Context context) {
        PackageManager packageManager = context.getPackageManager();

        ResolveInfo.DisplayNameComparator comparator = 
        		new ResolveInfo.DisplayNameComparator(packageManager);

        // 全アプリ(カテゴリーLAUNCHER)の取得
        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        s_appInfoList = packageManager.queryIntentActivities(intent, 0);
        Collections.sort(s_appInfoList, comparator);
        
        // かんたんホーム用(c_KANTAN_LAUNCHER_CATE_STR
        intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(TmCommonValues.c_KANTAN_LAUNCHER_CATE_STR);
        s_kantanAppInfoList = packageManager.queryIntentActivities(intent, 0);
        Collections.sort(s_kantanAppInfoList, comparator);
    }
    
    private static List<ResolveInfo>	s_appInfoList;
    private static List<ResolveInfo>	s_kantanAppInfoList;
    
    /**
     * 全てのアプリケーション情報を返す.
     * @param context
     * @return
     */
    public static List<ResolveInfo> getAllApplicationInfo(Context context) {
    	if( s_appInfoList == null ) {
    		loadApplications(context);
    	}
    	return(s_appInfoList);
    }
    /**
     * かんたんアプリのアプリケーション情報を返す.
     * @param context
     * @return
     */
    public static List<ResolveInfo> getKantanApplicationInfo(Context context) {
    	if( s_kantanAppInfoList == null ) {
    		loadApplications(context);
    	}
    	return(s_kantanAppInfoList);
    }
    
    /**
     * スクリーンオンになったときのレシーバー
     */
    private BroadcastReceiver m_screenOnReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			m_speech.speechTextAppend(getString(R.string.screen_on));
		}
    };
    
    /**
     * スクリーンオフになったときのレシーバー
     */
    private BroadcastReceiver m_screenOffReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			m_speech.stop();
			m_speech.speechTextAppend(getString(R.string.screen_off));
		}
    };
    
	
    /**
     * ビープ音.
     */
    //private ToneGenerator m_toneGen = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
    
	// keyguard(画面ロック)の制御
	private KeyguardManager.KeyguardLock mKeyguardLock;
	
	// keyguardを解除する。
	// 現在guardされてなくても将来も含めてguardしない。
	private void disableKeyguradIfNeed() {
		if( mKeyguardLock == null ) {
			KeyguardManager km = (KeyguardManager)getSystemService(Context.KEYGUARD_SERVICE);
	        mKeyguardLock = km.newKeyguardLock(getClass().getName());
        	mKeyguardLock.disableKeyguard();
		}
    }
	private void enableKeyguard() {
    	if( mKeyguardLock != null ) {
    		mKeyguardLock.reenableKeyguard();
    		mKeyguardLock = null;
    	}
    }
}
    

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Applications;

import jp.tmhouse.kantanhome.view.KantanLayout;
import jp.tmhouse.kantanhome.R;
import jp.tmhouse.kantanhome.activity.Utils.AppStarterActivity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

public class ApplicationsActivity extends AppStarterActivity {
	private static final String TAG = "ApplicationsActivity";
	
    @Override
    public void onCreate(Bundle icicle) {
    	Log.i(TAG, "onCreate");
        super.onCreate(icicle);
        
        setupApps(getLayoutView());
    }
    
    private void setupApps(KantanLayout panel) {
        // 登録済みアプリ
    	panel.addItem(getString(R.string.label_activity_registerd_app), 
        		new OpenActivityRunner(this, RegisteredAppActivity.class), false); 
        
        // 最近使ったアプリ
    	panel.addItem(getString(R.string.label_activity_recent_app), 
        		new OpenActivityRunner(this, RecentAppActivity.class), true); 
        
        // 全てのアプリ
    	panel.addItem(getString(R.string.label_activity_all_app), 
        		new OpenActivityRunner(this, AllAppActivity.class), true);

    }
    
    @Override
    protected String getTitleBarString() {
    	return(getString(R.string.label_activity_appliations_app));
    }
}

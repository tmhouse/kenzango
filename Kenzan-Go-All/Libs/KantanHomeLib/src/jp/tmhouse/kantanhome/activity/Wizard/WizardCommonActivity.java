/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Wizard;

import java.util.List;

import jp.tmhouse.TmLibrary.Utils.TmCommonValues;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityManager.AccessibilityStateChangeListener;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;

public class WizardCommonActivity extends Activity {
	private OnResult m_ime_setting_result_handler;
	
	public interface OnResult {
		public void onActivityResult(int requestCode, int resultCode, Intent data);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		switch(requestCode) {
		case c_OPEN_IME_SETTING_REQCD:
			if( m_ime_setting_result_handler != null ) {
				m_ime_setting_result_handler.onActivityResult(requestCode, resultCode, data);
			}
			break;
		}
	}
	

	protected static final int c_OPEN_A11Y_SETTING_REQCD = 0;
	protected static final int c_OPEN_IME_SETTING_REQCD = 1;
	protected static final int c_OPEN_IME_SUBTYPE_SETTING_REQCD = 2;

	protected void openA11ySetting() {
		String action = android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS;
		startActivityForResult(new Intent(action), c_OPEN_A11Y_SETTING_REQCD);	
	}
	protected void openIMESetting(OnResult onResult) {
		m_ime_setting_result_handler = onResult;
		String action = android.provider.Settings.ACTION_INPUT_METHOD_SETTINGS;
		startActivityForResult(new Intent(action), c_OPEN_IME_SETTING_REQCD);	
	}
	protected void showInputMethodPicker() {
		InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
		imm.showInputMethodPicker();
	}
	
	protected boolean isIMEEnabled(String pkgName) {
		InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);	
		List<InputMethodInfo> list = imm.getEnabledInputMethodList();
		
		if( list != null ) {
			for( InputMethodInfo info : list ) {
				if( info.getPackageName().equals(pkgName) ) {
					TmLog.d("IME found: " + pkgName);
					return(true);
				}
			}
		}
		TmLog.d("IME not found: " + pkgName);
		return(false);
	}
	
	protected boolean isIMEChoiced(String pkgName) {
		InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);	
		List<InputMethodInfo> list = imm.getEnabledInputMethodList();
		
		if( list != null ) {
			for( InputMethodInfo info : list ) {
				if( info.getPackageName().equals(pkgName) ) {
					TmLog.d("IME found: " + pkgName);
					return(true);
				}
			}
		}
		TmLog.d("IME not found: " + pkgName);
		return(false);
	}
	
	/**
	 * Google Playを開ける.
	 * @param packageName
	 */
	protected void openMarket(String packageName) {
		try {
		    startActivity(new Intent(Intent.ACTION_VIEW, 
		    		Uri.parse("market://details?id=" + packageName)));
		} catch (android.content.ActivityNotFoundException anfe) {
		    startActivity(new Intent(Intent.ACTION_VIEW, 
		    		Uri.parse("https://play.google.com/store/apps/details?id=" + 
		    				packageName)));
		}
	}

	
	/*********************
	 * Talkback関係
	 */
	protected void suspendTmTalkback() {
		Intent bi = new Intent();
		bi.setAction(TmCommonValues.ACTION_SUSPEND_TOUCH_EXPLORE);
		getBaseContext().sendBroadcast(bi);
	}
	protected void resumeTmTalkback() {
		Intent bi = new Intent();
		bi.setAction(TmCommonValues.ACTION_RESUME_TOUCH_EXPLORE);
		getBaseContext().sendBroadcast(bi);
	}
	
	protected void stopTmTalkback() {
		Intent bi = new Intent();
		bi.setAction(TmCommonValues.ACTION_STOP_SERVICE);
		getBaseContext().sendBroadcast(bi);
	}
	
	protected void registerA11yStateChangeListener(
			AccessibilityStateChangeListener listener) {
		getAM().addAccessibilityStateChangeListener(listener);
	}
	protected void unregisterA11yStateChangeListener(
			AccessibilityStateChangeListener listener) {
		getAM().removeAccessibilityStateChangeListener(listener);
	}
	
	/**
	 * TmTalkBackServiceが現在稼働しているか.
	 * @param context
	 * @return
	 */
	protected boolean isTmTalkBackServiceEnabled(Context context) {
		AccessibilityServiceInfo info = getTmTalkBackServiceInfo(context, true);
		if( info != null ) {
			TmLog.d("isTmTalkBackServiceEnabled", info.getId() + " enabled");
			return(true);
		}

		TmLog.d("isTmTalkBackServiceEnabled", "disabled");
		return(false);
	}
	
	private AccessibilityManager getAM() {
		AccessibilityManager a11ymgr =
	        (AccessibilityManager)getSystemService(
	        		Context.ACCESSIBILITY_SERVICE);
		return(a11ymgr);
	}
	
	protected AccessibilityServiceInfo getTmTalkBackServiceInfo(
			Context context, boolean isNowEnabled) {
		String serviceName = "TmTalkBackService";
		
		List<AccessibilityServiceInfo> list = null;
		if( isNowEnabled ) {
			list = getAM().getEnabledAccessibilityServiceList(
					AccessibilityServiceInfo.FEEDBACK_SPOKEN);
		} else {
			list = getAM().getInstalledAccessibilityServiceList();
		}
		if( list == null ) {
			return(null);
		}

		for( AccessibilityServiceInfo info : list ) {
			String id = info.getId();
			if( id != null && id.endsWith(serviceName) ) {
				return(info);
			}
		}
		return(null);
	}
	
	/**
	 * TmTalkBackServiceをEnable or Disableにする.
	 * disableにすると、Accessibilityの選択肢から消える。
	 * @param context
	 * @param bEnable
	 */
	
	private static final String c_TmTalkBackService_PKGNAME = "jp.tmhouse.android.kenzango";
	private static final String c_TmTalkBackService_NAME = "jp.tmhouse.android.kenzango.TmTalkBackService";

	protected void setTmTalkBackServiceEnabled(Context context, boolean bEnable) {
        final PackageManager pm = getPackageManager();
        
        //AccessibilityServiceInfo info = getTmTalkBackServiceInfo(context, false);
        //if( info == null ) {
        	//throw new RuntimeException("no TmTalkBackService");
        //}
        //ResolveInfo ri = info.getResolveInfo();
        //ServiceInfo si = ri.serviceInfo;
        
        //TmLog.e("packageName=", si.packageName);
        //TmLog.e("name=", si.name);
        ComponentName cn = new ComponentName(c_TmTalkBackService_PKGNAME, c_TmTalkBackService_NAME);
 
        int newState = bEnable ? 
        		PackageManager.COMPONENT_ENABLED_STATE_ENABLED :
        			PackageManager.COMPONENT_ENABLED_STATE_DISABLED;

        pm.setComponentEnabledSetting(cn, newState, PackageManager.DONT_KILL_APP);
	}
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Telephone;

import jp.tmhouse.kantanhome.view.KantanLayout;
import jp.tmhouse.kantanhome.R;
import jp.tmhouse.kantanhome.activity.Applications.ApplicationsActivity;
import jp.tmhouse.kantanhome.activity.Utils.AppStarterActivity;
import jp.tmhouse.kantanhome.activity.Utils.AppStarterActivity.OpenActivityRunner;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public class TelephoneAppActivity extends AppStarterActivity {
	private static final String TAG = "telephoneAppActivity";
	
    @Override
    public void onCreate(Bundle icicle) {
    	Log.i(TAG, "onCreate");
        super.onCreate(icicle);
        
        setupApps(getLayoutView());
    }
    
    @Override
    protected String getTitleBarString() {
    	return(getString(R.string.label_activity_telephone_app));
    }
    
    private void setupApps(KantanLayout panel) {
    	// ワンプッシュ
    	panel.addItem(getString(R.string.label_one_push_call), null, false);
    	
    	// 通話履歴　
    	panel.addItem(getString(R.string.label_call_log), 
    			new OpenActivityRunner(this, CallLogActivity.class), true);
    	
    	// Contactの表示
    	Intent contactIt = new Intent(android.content.Intent.ACTION_VIEW);
    	contactIt.setData(Uri.parse("content://contacts/people/"));
    	panel.addItem(getString(R.string.label_contact), 
    			new OpenActivityRunner(this, contactIt), true);
    	
    	/***
    	// 発信履歴
    	panel.addItem(getString(R.string.label_outgoing_history), null, false);
    	
    	// 着信履歴
    	panel.addItem(getString(R.string.label_incoming_history), null, false);
    	***/
    	
    	// 電話パネル
    	panel.addItem(getString(R.string.label_phone_panel), 
    			new OpenActivityRunner(this, Intent.ACTION_DIAL), true);
    }
    
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Applications;

import java.util.List;

import jp.tmhouse.kantanhome.view.KantanLayout;
import jp.tmhouse.kantanhome.R;
import jp.tmhouse.kantanhome.activity.Home.KantanHomeActivity;
import jp.tmhouse.kantanhome.activity.Utils.AppStarterActivity;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.util.Log;

public class AllAppActivity extends AppStarterActivity {
	private static final String TAG = "allappactivity";
	
    private List<ResolveInfo> m_apps;

    @Override
    public void onCreate(Bundle icicle) {
    	Log.i(TAG, "onCreate");
        super.onCreate(icicle);
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	
    	// TODO ここだと毎回作り直しになってしまう。
    	// 全てのアプリ画面で何か起動したら、DBにフラグを立てておくかなあ。
       	m_apps = KantanHomeActivity.getAllApplicationInfo(this);
        setupApps(m_apps);
    }
    
    @Override
    protected String getTitleBarString() {
    	return(getString(R.string.label_activity_all_app));
    }
}

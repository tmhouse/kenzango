/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Telephone;

import java.util.Date;
import java.util.HashMap;

import jp.tmhouse.kantanhome.view.KantanLayout;
import jp.tmhouse.kantanhome.R;
import jp.tmhouse.kantanhome.activity.Utils.AppStarterActivity;
import jp.tmhouse.kantanhome.activity.Utils.ConfirmActivity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class CallLogActivity extends AppStarterActivity {
	private static final String TAG = "callLogActivity";
	
	public static final String		c_CALL_LOG_KIND_KEY = "call_log_kind";
	public static final int		c_CALL_LOG_KIND_OUTGOING = 0;
	public static final int		c_CALL_LOG_KIND_INCOMING = 1;
	public static final int		c_CALL_LOG_KIND_RECENT = 2;
	
	private int m_callLogKindId = c_CALL_LOG_KIND_RECENT;
	
    @Override
    public void onCreate(Bundle icicle) {
    	Log.i(TAG, "onCreate");
        super.onCreate(icicle);
        
        // この画面ではいつも先頭ページから開始
        clearSavedPage();
        
        m_callLogKindId = getIntent().getIntExtra(
        		c_CALL_LOG_KIND_KEY, c_CALL_LOG_KIND_RECENT);
        
    	addCallLog(getLayoutView());
        setupApps(getLayoutView());
    }
    
    @Override
    protected String getTitleBarString() {
    	switch( m_callLogKindId ) {
    	case c_CALL_LOG_KIND_OUTGOING:
    		return(getString(R.string.label_outgoing_history));
    	case c_CALL_LOG_KIND_INCOMING:
    		return(getString(R.string.label_incoming_history));
    	case c_CALL_LOG_KIND_RECENT:
    		return(getString(R.string.label_call_log));
    	}
    	return(null);
    }
    
    private void setupApps(KantanLayout panel) {
    	
    }
    
    private void addCallLog(KantanLayout panel) {
    	ContentResolver contentResolver = getContentResolver();
    	 
    	// ソートする条件
    	String order = CallLog.Calls.DEFAULT_SORT_ORDER;
    	 
    	Cursor cursor = contentResolver.query(
    			CallLog.Calls.CONTENT_URI,
    			null,
    			null,
    			null,
    			order
    	);
    	 
    	HashMap<String, String> map = new HashMap<String, String>(512);
    	if (cursor.moveToFirst()) {
    		do {
    			String number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
    			
    			// 番号でuniq化
    			if( map.containsKey(number) == false ) {
    				String cachedName = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
    				/***
    				String type = cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE));
    				Date date = new Date(cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DATE)));
    				Log.d(TAG, "DATE : " + date);
    				Log.d(TAG, "NUMBER : " + number);
    				Log.d(TAG, "CACHED_NAME : " + cachedName);
    				Log.d(TAG, "TYPE : " + type);
    				***/
    				String sepa = null;
    				cachedName = cachedName == null ? "" : cachedName;
    				if( cachedName.length() > 8 ) {
    					sepa = " ";
    				} else {
    					sepa = "\n";
    				}
    				
    				OpenConfirmRunner runner = new OpenConfirmRunner(cachedName, number);
    				panel.addItem(cachedName + sepa + number, runner, true);
    				map.put(number, null);
    			}
    			
    	} while (cursor.moveToNext());
    	}
    }
    
    private static final String	c_NUMBER_KEY  = "numberkey";
    
    @Override
    protected void onActivityResult(
    		int requestCode, int resultCode, Intent data) {
    	
    	// OKだったら電話する
    	if( resultCode == RESULT_OK ) {
    		String number = data.getStringExtra(c_NUMBER_KEY);
			Uri uri = Uri.parse("tel:" + number);
			Intent i = new Intent(Intent.ACTION_CALL, uri);
			startActivity(i);
    	}
    }
    
    /**
     * 電話をかけるクラス.
     * @author mutoh
     *
     */
    private class OpenConfirmRunner implements OnClickListener {
    	private String m_callNumber;
    	private String m_name;
    	public OpenConfirmRunner(String name, String callNumber) {
    		m_callNumber = callNumber;
    		m_name = name;
    	}

		@Override
		public void onClick(View v) {
			String msg = "";
			if( m_name.trim().length() == 0 ) {
				msg = String.format(
						getString(R.string.format_phone_call_noname), 
						m_callNumber);
			} else {
				msg = String.format(
						getString(R.string.format_phone_call),
						m_name, m_callNumber);
			}
			
			Intent i = new Intent(v.getContext(), ConfirmActivity.class);
			i.putExtra(ConfirmActivity.c_TITLE_KEY, msg);
			i.putExtra(c_NUMBER_KEY, m_callNumber);
			startActivityForResult(i, 0);
			
		}
    	
    }
    
}
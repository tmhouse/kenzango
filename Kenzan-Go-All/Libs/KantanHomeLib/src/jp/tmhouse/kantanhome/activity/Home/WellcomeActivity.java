/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Home;

import jp.tmhouse.TmLibrary.Utils.Debug.TmAsyncWaitForDebugger;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.kantanhome.BuildConfig;
import jp.tmhouse.kantanhome.R;
import jp.tmhouse.kantanhome.activity.Wizard.ChangeHomeActivity;
import jp.tmhouse.kantanhome.activity.Wizard.GetJPAnalyzerActivity;
import jp.tmhouse.kantanhome.activity.Wizard.SetupA11yActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WellcomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wellcome);
		
    	// デバッガのアタッチを待つ
		if( BuildConfig.DEBUG ) {
			TmLog.enable(true);
			TmAsyncWaitForDebugger.start(8*1000);
		}
		
		Button closeBtn = (Button)findViewById(R.id.closeBtn);
		closeBtn.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Button menuBtn = (Button)findViewById(R.id.startWizardBtn);
		menuBtn.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				// A11y設定開始
				Intent i = new Intent(WellcomeActivity.this, GetJPAnalyzerActivity.class);
				startActivity(i);
			}
		});
	}
	
}

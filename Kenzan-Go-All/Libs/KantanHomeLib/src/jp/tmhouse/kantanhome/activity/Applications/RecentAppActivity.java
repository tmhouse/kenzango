/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Applications;

import jp.tmhouse.kantanhome.view.KantanLayout;
import jp.tmhouse.kantanhome.LocalDB;
import jp.tmhouse.kantanhome.R;
import jp.tmhouse.kantanhome.activity.Utils.AppStarterActivity;
import jp.tmhouse.kantanhome.activity.Utils.ConfirmActivity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class RecentAppActivity extends AppStarterActivity {
	private static final String TAG = "recentAppActivity";
	
    private LocalDB.ClassDef[] m_recentApps;

    @Override
    public void onCreate(Bundle icicle) {
    	Log.i(TAG, "onCreate");
        super.onCreate(icicle);
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	
    	// TODO ここだと毎回作り直しになってしまう。
    	// 全てのアプリ画面で何か起動したら、DBにフラグを立てておくかなあ。
    	initializeViews();
    }
    
    @Override
    protected void onActivityResult(
    		int requestCode, int resultCode, Intent data) {
    	Log.i(TAG, "onActivityResult");
    	if( resultCode == RESULT_OK ) {
    		// 消す
			getDB().deleteAllRecentApp();
			
			// 再起動
			finish();
			Intent i = new Intent(this, getClass());
			startActivity(i);
    	}
    }
    
    @Override
    protected String getTitleBarString() {
    	return(getString(R.string.label_activity_recent_app));
    }
    
    private void initializeViews() {
        m_recentApps = getDB().getAllRecentApps();
        setupApps(m_recentApps, getLayoutView());
        
        addClearButton(getLayoutView());
    }
    
    private void setupApps(LocalDB.ClassDef[] apps, KantanLayout panel) {
    	if( apps == null ) {
    		return;
    	}
    	PackageManager pm = getPackageManager();
    	for( LocalDB.ClassDef app : apps ) {
    		// Packageを探す
    		ComponentName cn = new ComponentName(
					app.getPackageName(), app.getClassName());
    		try {
    			pm.getActivityInfo(cn, 0);
    		} catch (NameNotFoundException e) {
    			Log.e(TAG, "class Not exists.");
    			cn = null;
    		}
    		
    		if( cn != null ) {
    			String label = app.getLabelName();
    			AppStarter as = new AppStarter(this, cn, label);
    			panel.addItem(label, as, true);
    		}
    	}
    }
    
    private void addClearButton(KantanLayout panel) {
    	panel.addItem(getString(R.string.clear_items), 
    	new OnClickListener() {
			@Override
			public void onClick(View v) {
				// ConfirmActivity
				String title = getString(R.string.dialog_clear_recent_apps);
				Intent i = new Intent(v.getContext(), ConfirmActivity.class);
				i.putExtra(ConfirmActivity.c_TITLE_KEY, title);
				startActivityForResult(i, 0);
			}
    	}, true);
    }
    
}

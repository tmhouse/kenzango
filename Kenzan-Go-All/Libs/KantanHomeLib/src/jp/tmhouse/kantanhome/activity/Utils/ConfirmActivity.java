/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Utils;

import jp.tmhouse.kantanhome.view.KantanLayout;
import jp.tmhouse.kantanhome.R;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class ConfirmActivity extends AppStarterActivity {
	private static final String TAG = "ConfirmActivity";
	
	// keys
	public static final String c_TITLE_KEY = "title";
	public static final String c_OK_LABEL_KEY = "ok_label";
	public static final String c_CANCEL_LABEL_KEY = "cancel_label";
	
	private static final String c_DEFAULT_TITLE = "unknown";

	private String	m_ok_label;
	private String	m_cancel_label;

    @Override
    public void onCreate(Bundle icicle) {
    	Log.i(TAG, "onCreate");
        super.onCreate(icicle);
        
        // この画面ではいつも先頭ページから開始
        clearSavedPage();
        
        m_ok_label = getIntent().getStringExtra(c_OK_LABEL_KEY);
        m_ok_label = m_ok_label == null ? 
        		getString(R.string.label_ok_button) : m_ok_label;
        
        m_cancel_label = getIntent().getStringExtra(c_CANCEL_LABEL_KEY);
        m_cancel_label = m_cancel_label == null ? 
        		getString(R.string.label_cancel_button) : m_cancel_label;
        
        setupButton(getLayoutView());
    }
	
	@Override
	protected String getTitleBarString() {
        String title = getIntent().getStringExtra(c_TITLE_KEY);
        title = title == null ? c_DEFAULT_TITLE : title;
		return(title);
	}
	
	private void setupButton(KantanLayout panel) {
		panel.addItem(m_ok_label,
				new ClickListener(RESULT_OK), true);

		panel.addItem(m_cancel_label,
				new ClickListener(RESULT_CANCELED), true);
	}

	/**
	 * 
	 * @author mutoh
	 *
	 */
    private class ClickListener implements OnClickListener {
    	private int m_resultId;
    	public ClickListener(int resultId) {
    		m_resultId = resultId;
    	}

		@Override
		public void onClick(View v) {
			Intent data = getIntent();
	        //data = new Intent();
	        data.putExtra("What are these?", "These are pens.");
	        data.putExtra("How many pens?", 3);
	        setResult(m_resultId, data);
	        finish();
		}
    }
	
}

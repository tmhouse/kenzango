/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Utils;

import java.util.List;

import jp.tmhouse.kantanhome.view.KantanLayout;
import jp.tmhouse.kantanhome.LocalDB;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public abstract class AppStarterActivity extends Activity {
	private static final String TAG = "appstarteractivity";
	private LocalDB				m_db;
	private KantanLayout		m_layout;
	
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        Log.d(TAG, "onCreate");
        
        m_layout = new KantanLayout(this, getTitleBarString(), 5, 1);
        setContentView(m_layout);
    }
    
    @Override
    public void onStart() {
    	super.onStart();
        Log.d(TAG, "onStart");
    }
    
    @Override
    public void onResume() {
    	super.onResume();
        Log.d(TAG, "onResume");
    }
    
    @Override
    public void onPause() {
    	super.onPause();
        Log.d(TAG, "onPause");
    }
    
    /**
     * その他API
     */
    protected KantanLayout getLayoutView() {
    	return(m_layout);
    }
    
    protected void clearSavedPage() {
    	getLayoutView().clearSavedPage();
    }

	public LocalDB getDB() {
		if( m_db == null ) {
			m_db = new LocalDB(this);
		}
		return(m_db);
	}
	
	
	/**
	 * アプリ情報のリストをKantanLayoutに突っ込む.
	 * @param apps
	 */
    public void setupApps(List<ResolveInfo> apps) {
    	KantanLayout panel = getLayoutView();
    	PackageManager pm = getPackageManager();
    	for( ResolveInfo info : apps ) {
    		String label = (String)info.loadLabel(pm);
    		// ラベルがない場合はパッケージ名
    		if( label == null || label.length() == 0 ) {
    			label = info.activityInfo.packageName;
    		}
    		AppStarter as = new AppStarter(this, info, label);
    		panel.addItem(label, as, true);
    	}
    }
    
    /**
     * 
     * @author mutoh
     *
     */
    public class AppStarter implements OnClickListener {
		private Intent 			m_intent;
		private Context 		m_ctx;
		private String 			m_label;
		
    	public AppStarter(Context ctx, ResolveInfo info, String label) {
    		this(ctx, info.activityInfo.applicationInfo.packageName,
    				info.activityInfo.name, label);
    	}
    	public AppStarter(Context ctx, ComponentName cname, String label) {
    		this(ctx, cname.getPackageName(), cname.getClassName(), label);
    	}
    	public AppStarter(Context ctx, String packageName, 
    			String className, String label) {
    		m_ctx = ctx;
    		m_label = label;
    		m_intent = createIntent(
    				packageName, className);
    	}
    	
		@Override
		public void onClick(View v) {
			// よく使うアプリへ記録する
			ComponentName cn = m_intent.getComponent();
			/***テスト
			LocalDB.ClassDef[] apps = db.getAllRecentApps();
			if( apps != null ) {
				for( LocalDB.ClassDef classDef : apps ) {
					Log.d(TAG, "recent app=" + classDef.getPackageName());
				}
			}
			***/
			getDB().saveRecentApp(cn, m_label);
			
			m_ctx.startActivity(m_intent);
		}
	    
	    private Intent createIntent(String pkgName, String className) {
			ComponentName cname = new ComponentName(pkgName, className);
			
			int flags = Intent.FLAG_ACTIVITY_NEW_TASK | 
					Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED;
			
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_LAUNCHER);
			intent.setComponent(cname);
			intent.setFlags(flags);
	    	return(intent);
	    }
    }

    /**
     * 
     * @author mutoh
     *
     */
    public class OpenActivityRunner implements OnClickListener {
		private Intent 				m_intent;
		private Context 			m_ctx;
    	public OpenActivityRunner(Context ctx, Class appClass) {
    		m_ctx = ctx;
    		m_intent = new Intent();
    		m_intent.setClass(ctx, appClass);
    	}
    	public OpenActivityRunner(Context ctx, String action) {
    		m_ctx = ctx;
    		m_intent = new Intent(action);
    	}
    	
    	public OpenActivityRunner(Context ctx, Intent intent) {
    		m_ctx = ctx;
    		m_intent = intent;
    	}
    	
		@Override
		public void onClick(View v) {
			m_ctx.startActivity(m_intent);
		}
    }
    
    /**
     * subclassが実装すべきAPI.
     */
    protected abstract String getTitleBarString();
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Wizard;

import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.kantanhome.R;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.accessibility.AccessibilityManager.AccessibilityStateChangeListener;
import android.widget.Button;

public class SetupA11yActivity extends WizardCommonActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_a11y_setting);
		
		Button settingStartBtn = (Button)findViewById(R.id.a11y_setting_start_btn);
		settingStartBtn.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Accessibility Serviceの設定を開く
				openA11ySetting();
			}
		});

		Button nextBtn = (Button)findViewById(R.id.nextBtn);
		nextBtn.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(SetupA11yActivity.this, ChangeIMEActivity.class));
			}
		});
		
		// A11y設定を変更したときのリスナー
		registerA11yStateChangeListener(m_a11yStateChangeListener);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		unregisterA11yStateChangeListener(m_a11yStateChangeListener);
	}
	
	private AccessibilityStateChangeListener m_a11yStateChangeListener = 
		new AccessibilityStateChangeListener() {
		@Override
		public void onAccessibilityStateChanged(boolean enabled) {
			if( enabled ) {
				// TmTalkbackの停止
				TmLog.i("it will stop tm talkback");
				m_handler.postDelayed(m_stopTmTalkbackRunnable, 3000);
				m_handler.postDelayed(m_stopTmTalkbackRunnable, 5000);
				m_handler.postDelayed(m_stopTmTalkbackRunnable, 7000);
				m_handler.postDelayed(m_stopTmTalkbackRunnable, 9000);
				m_handler.postDelayed(m_stopTmTalkbackRunnable, 12000);
				m_handler.postDelayed(m_stopTmTalkbackRunnable, 14000);
			}
		}
	};
	
	private Runnable m_stopTmTalkbackRunnable = new Runnable() {
		@Override
		public void run() {
			// TmTalkbackが稼働しているなら、停止させる
			if( isTmTalkBackServiceEnabled(SetupA11yActivity.this) ) {
				TmLog.i("stop tm talkback");
				stopTmTalkback();
			} else {
				TmLog.i("talkback is not anabled. do nothing.");
			}
		}
	};
	
	private Handler m_handler = new Handler();
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if( requestCode == c_OPEN_A11Y_SETTING_REQCD ) {
			TmLog.d("A11y Settings returned");
			if( isTmTalkBackServiceEnabled(this) ) {
				TmLog.i("kenzan-go a11y enabled");
			} else {
				TmLog.i("kenzan-go a11y disabled");
			}
		}
	}

	
}

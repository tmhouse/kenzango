/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Wizard;

import jp.tmhouse.TmLibrary.Utils.TmCommonValues;
import jp.tmhouse.kantanhome.R;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GetJPAnalyzerActivity extends WizardCommonActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if( isInstalledJpAnalyzer() ) {
			startActivity(new Intent(GetJPAnalyzerActivity.this, SetupA11yActivity.class));
			finish();
			return;
		}
		
		setContentView(R.layout.activity_get_jpanalyzer);
		
		Button getJPABtn = (Button)findViewById(R.id.getJpAnalyzerBtn);
		getJPABtn.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				// 日本語アナライザのインストールボタン
				openMarket(TmCommonValues.c_JP_ANALYZER_PKG_NAME);
			}
		});

		Button nextBtn = (Button)findViewById(R.id.nextBtn);
		nextBtn.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(GetJPAnalyzerActivity.this, SetupA11yActivity.class));
			}
		});
	}
	
	private boolean isInstalledJpAnalyzer() {
		PackageManager pm = getPackageManager();
		try {
			pm.getApplicationInfo(TmCommonValues.c_JP_ANALYZER_PKG_NAME, 0);
		} catch (NameNotFoundException e) {
			return(false);
		}
		return(true);
	}
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
}

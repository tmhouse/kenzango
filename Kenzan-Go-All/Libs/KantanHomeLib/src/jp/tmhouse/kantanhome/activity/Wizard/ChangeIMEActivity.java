/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome.activity.Wizard;

import jp.tmhouse.kantanhome.R;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

public class ChangeIMEActivity extends WizardCommonActivity {
	private Handler	m_handler = new Handler();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ime_setting);
		
		Button settingStartBtn = (Button)findViewById(R.id.ime_setting_start_btn);
		settingStartBtn.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				// IMEの設定を開く
				openIMESetting( new OnResult() {
					@Override
					public void onActivityResult(int requestCode, int resultCode, Intent data) {
						// IMEを選択するダイアログを表示する
						m_handler.postDelayed(new Runnable() {
							@Override
							public void run() {
								showInputMethodPicker();
							}
						}, 100);
					}
				});
			}
		});

		Button nextBtn = (Button)findViewById(R.id.nextBtn);
		nextBtn.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ChangeIMEActivity.this, ChangeHomeActivity.class));
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.kantanhome;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class LocalDB extends SQLiteOpenHelper {
	private static final int c_version = 1;
	private static String c_dbName = "localDB";
	private final String c_RecentAppTableStr = "RecentAppTable";
	
	private static final String	c_packageName = "packageName";
	private static final String	c_className = "className";
	private static final String	c_labelName = "labelName";
	private static final String	c_updateTime = "updateTime";
	
	/**
	 * 
	 * @author mutoh
	 *
	 */
    public class ClassDef {
    	public ClassDef(
    			String packageName, String className,
    			String labelName) {
    		m_packageName = packageName;
    		m_className = className;
    		m_labelName = labelName;
    	}
    	public String getPackageName() {
    		return(m_packageName);
    	}
    	public String getClassName() {
    		return(m_className);
    	}
    	public String getLabelName() {
    		return(m_labelName);
    	}
    	String	m_packageName;
    	String	m_className;
    	String	m_labelName;
    }
    
    /**
     * コンストラクタ.
     * @param context
     */
	public LocalDB(Context context) {
		super(context, c_dbName, null, c_version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// テーブルを生成
		db.execSQL(createTableString(c_RecentAppTableStr));
	}
	
	@Override
    public void onOpen(SQLiteDatabase db) {
		
		///////////////////////
		// TEST
		///////////////////////
		//dropTable(db);
		//db.execSQL(createTableString(c_RecentAppTableStr));
	}
	
	public boolean saveRecentApp(ComponentName cname, String label) {
		// 存在してたら消す
		ClassDef cdef = _searchRecentApp(cname.getPackageName());
		if( cdef != null ) {
			deleteRecentApp(cname.getPackageName());
		}
		
		_addRecentApp(
			cname.getPackageName(), cname.getClassName(), label);
		return(true);
	}
	
	/**
	 * finish line テーブルの生成.
	 * @param tableName
	 * @return
	 */
	private String createTableString(String tableName) {
		String str = "create table " + tableName + 
				" (" + c_packageName + " text primary key, " + 
				c_className + " text," + 
				c_labelName + " text," + 
				c_updateTime + " integer" + 
				");";
		return(str);
	}
	
	/**
	 * テーブルの削除.
	 */
	private void dropTable(SQLiteDatabase db) {
		String sql = "DROP TABLE IF EXISTS " + c_RecentAppTableStr + ";";
		db.beginTransaction();
		try {
			db.execSQL(sql);
			db.setTransactionSuccessful();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
   			db.endTransaction();
		}
	}
	
	/**
	 * プリセットデータの生成.
	 */
	/***
	private void createPresettedData(SQLiteDatabase db) {
		// テーブル捨てる
		dropPresettedData(db);
		// テーブル作る
		db.execSQL(createTableString(c_PresettedFinishLineTableStr));
		
		// データ生成
		CourceDef[] presettedCourceArr = createPresettedCourceArr();
		for( CourceDef c : presettedCourceArr ) {
			_addFinishLine(db, c_PresettedFinishLineTableStr, c.m_name, c.m_pointA, c.m_pointB);
		}
	}
	***/
	
	/**
	 * ユーザー定義finish lineの追加.
	 * @param name
	 * @param pointA
	 * @param pointB
	 */
	/***
	public void addUsersFinishLine(String name, PointD pointA, PointD pointB) {
		_addFinishLine(getWritableDatabase(), c_FinishLineTableStr, name, pointA, pointB);
	}
	***/
	
	/**
	 * finish lineの追加.
	 * @param tableName
	 * @param name
	 * @param pointA
	 * @param pointB
	 */
	/***
	private void _addFinishLine(SQLiteDatabase db, String tableName, String name, PointD pointA, PointD pointB) {
		db.beginTransaction();
		try {
			ContentValues cv = new ContentValues();
			cv.put(c_fld_name, name);
			cv.put(c_fld_A_x, pointA.x);
			cv.put(c_fld_A_y, pointA.y);
			cv.put(c_fld_B_x, pointB.x);
			cv.put(c_fld_B_y, pointB.y);
			db.insert(tableName, null, cv);
			db.setTransactionSuccessful();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
   			db.endTransaction();
       		//db.close();
		}
	}
	***/
	
	private void _addRecentApp(
			String packageName, String className, String labelName) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.beginTransaction();
		long time = System.currentTimeMillis();
		try {
			ContentValues cv = new ContentValues();
			cv.put(c_packageName, packageName);
			cv.put(c_className, className);
			cv.put(c_labelName, labelName);
			cv.put(c_updateTime, time);
			db.insert(c_RecentAppTableStr, null, cv);
			db.setTransactionSuccessful();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
   			db.endTransaction();
       		db.close();
		}
	}
	
	/**
	 * 削除.
	 * @param name
	 */
	public void deleteRecentApp(String packageName) {
		ClassDef cdef = _searchRecentApp(packageName);
		if( cdef == null ) {
			return;
		}
		
		SQLiteDatabase db = getWritableDatabase();
		db.beginTransaction();
		try {
			db.delete(c_RecentAppTableStr, c_packageName + "=" + "\"" + 
					packageName + "\"", null);
			db.setTransactionSuccessful();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
   			db.endTransaction();
       		db.close();
		}
	}
	
	/**
	 * すべての最近使ったアプリを消す.
	 */
	public void deleteAllRecentApp() {
		SQLiteDatabase db = getWritableDatabase();
		db.beginTransaction();
		try {
			db.delete(c_RecentAppTableStr, null , null);
			db.setTransactionSuccessful();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
   			db.endTransaction();
       		db.close();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if( newVersion > oldVersion ) {
		}
	}
	
	/**
	 * すべてのRecent appを取得する.
	 * @return
	 */
	public LocalDB.ClassDef[] getAllRecentApps() {
	    return(_getRecentApps(c_RecentAppTableStr));
	}
	
	/**
	 * 
	 * @param tableName
	 * @return
	 */
	private LocalDB.ClassDef[] _getRecentApps(String tableName) {
		final String sql = "select * from " + tableName + 
				" order by " + c_updateTime + " desc " +
				";";
		
		ClassDef[] retObjArr = null;
		SQLiteCursor c = null;
		SQLiteDatabase db = getReadableDatabase();
        try {
        	c = (SQLiteCursor)db.rawQuery(sql, null);
        	if( c != null && c.getCount() > 0 ) {
	            int rowcount = c.getCount();
	            c.moveToFirst();
	            
	            retObjArr = new ClassDef[rowcount];
	            for (int i = 0; i < rowcount ; i++) {
	            	retObjArr[i] = new ClassDef(
	            		c.getString(0), c.getString(1), c.getString(2));
	                c.moveToNext();
	            }
        	}
        } catch (SQLException e) {
        	e.printStackTrace();
            android.util.Log.e("ERROR", e.toString());
        } finally {
        	if( c != null ) {c.close();	}
        	if( db != null ) {db.close();}
        }
        return(retObjArr);
	}
	
	/**
	 * 
	 * @param tableName
	 * @param courceName
	 * @return
	 */
	private LocalDB.ClassDef _searchRecentApp(String packageName) {
		String sql = "select * from " + c_RecentAppTableStr + 
			" where " + c_packageName + "=\"" + packageName + "\";";
		
		ClassDef retObj = null;
		SQLiteCursor c = null;
		SQLiteDatabase db = getReadableDatabase();
        try {
        	c = (SQLiteCursor)db.rawQuery(sql, null);

            int rowcount = c.getCount();
            c.moveToFirst();
            if( rowcount > 0 ) {
            	retObj = new ClassDef(
            		c.getString(0), c.getString(1), c.getString(2));
            }
        } catch (SQLException e) {
        	e.printStackTrace();
            android.util.Log.e("ERROR", e.toString());
        } finally {
        	if( c != null ) {c.close();	}
        	if( db != null ) {db.close();}
        }
        return(retObj);
	}
}

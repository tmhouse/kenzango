/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Timer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/**
 * AlarmManagerを使ったタイマー.
 * 
 * Class<hoge>にhogeのサブクラスを代入しようとするため面倒
 * <サンプル>
 * 	Class<BroadcastReceiver> br = (Class<BroadcastReceiver>)
 *      WakeupReceiver.class.asSubclass(BroadcastReceiver.class);
 * @author mutoh
 *
 */
public class TmWakeupTimer {
	public static final String c_ACTION_SETLOCATION = "WAKEUP_TIMER";
	
	/**
	 * 
	 * @param ctx
	 * @param receiverClass
	 * @return
	 */
	private static PendingIntent createSender(
			Context ctx, Class<BroadcastReceiver> receiverClass)
	{
		Intent intent = new Intent(ctx, receiverClass);
		//intent.putExtra(E_expandNum, bunkatu);
		intent.setAction(c_ACTION_SETLOCATION);
		PendingIntent sender = PendingIntent.getBroadcast(
				ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		return(sender);
	}
	
	/**
	 * 一回だけ呼ばれるタイマー.
	 * @param ctx
	 * @param receiverClass
	 * @param delayMS
	 */
	public static void onceWakeup(
			Context ctx, Class<BroadcastReceiver> receiverClass, long delayMS)
	{
		ctx = ctx.getApplicationContext();
		cancel(ctx, receiverClass);
		PendingIntent sender = createSender(ctx, receiverClass);
		
		long triggerAtTime = SystemClock.elapsedRealtime() + delayMS;
		AlarmManager am = (AlarmManager)ctx.getSystemService(Context.ALARM_SERVICE);
		am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerAtTime, sender);
	}
	
	/**
	 * 繰り返し呼ばれるタイマー.
	 * @param ctx
	 * @param receiverClass
	 * @param firstDelayMS
	 * @param intervalMS
	 */
	public static void repeatWakeup(
			Context ctx, Class<BroadcastReceiver> receiverClass, 
			long firstDelayMS, long intervalMS)
	{
		ctx = ctx.getApplicationContext();
		cancel(ctx, receiverClass);
		PendingIntent sender = createSender(ctx, receiverClass);

		long firstTime = SystemClock.elapsedRealtime();
		firstTime += firstDelayMS;
		AlarmManager am = (AlarmManager)ctx.getSystemService(Context.ALARM_SERVICE);
		am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 
				firstTime, intervalMS, sender);
	}
	
	/**
	 * タイマーのキャンセル.
	 * @param ctx
	 * @param receiverClass
	 */
	public static void cancel(
			Context ctx, Class<BroadcastReceiver> receiverClass)
	{
		ctx = ctx.getApplicationContext();
		Intent intent = new Intent(ctx, receiverClass);
		intent.setAction(c_ACTION_SETLOCATION);
		PendingIntent sender = PendingIntent.getBroadcast(ctx, 0, intent, 0);

		AlarmManager am = (AlarmManager)ctx.getSystemService(Context.ALARM_SERVICE);
		am.cancel(sender);
	}

}
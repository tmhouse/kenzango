/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

public class TmBattStatus {
	/**
	 * 電源接続されているか.
	 * @param ctx
	 * @return
	 */
    public static boolean isPowerConnected(Context ctx) {
        int pluggedIn = -1;
        try {
        	BroadcastReceiver brcv = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
				}
            };
            Intent intent = ctx.registerReceiver(brcv, 
            		new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            ctx.unregisterReceiver(brcv);
            
            // sticky intent
            pluggedIn = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        } catch (Exception ex) {
            Log.e("IsPowerConnected", ex.toString());
        }
        return pluggedIn == BatteryManager.BATTERY_PLUGGED_AC || 
        		pluggedIn == BatteryManager.BATTERY_PLUGGED_USB;
    }
}

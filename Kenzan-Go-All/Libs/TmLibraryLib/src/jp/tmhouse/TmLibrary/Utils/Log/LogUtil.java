/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.Utils.Log;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

public class LogUtil {
	
	@SuppressWarnings("unused")
	private LogUtil() {}
	
	public LogUtil(Context context) {
		s_isDebug = isDebuggable(context);
	}
	
    /**
     * マニフェストファイルを読んでデバッグモードかどうかを取得
     */
    public static boolean isDebuggable(Context context) {
    	boolean isDebug = false;
   		try {
   			PackageManager manager = context.getPackageManager();
   			ApplicationInfo appInfo = null;
   			appInfo = manager.getApplicationInfo(context.getPackageName(), 0);
   			if ((appInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) == ApplicationInfo.FLAG_DEBUGGABLE) {
   				isDebug = true;
   			}
   		} catch (NameNotFoundException e) {
   			isDebug = false;
    	}
    	return(isDebug);
    }

    /**
    * デバッグログを出力する マニュフェストファイルでデバッグモードになっていなければ出力しない
    */
    public void d(String tag, String msg) {
        if ( s_isDebug ) {
        	msg = (msg == null) ? "<no message>" : msg;
        	tag = (tag == null) ? "<no tag>" : tag;
            Log.d(tag, msg);
        }
    }
    
    public void e(String tag, String msg) {
        if ( s_isDebug && msg != null ) {
        	msg = (msg == null) ? "<no message>" : msg;
        	tag = (tag == null) ? "<no tag>" : tag;
            Log.e(tag, msg);
        }
    }
    
    private static boolean s_isDebug = false;
}

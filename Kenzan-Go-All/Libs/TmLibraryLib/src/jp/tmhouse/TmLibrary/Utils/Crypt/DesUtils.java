/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Crypt;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/** 
 * DESアルゴリズム 
 * @author http://www.syboos.jp/ 
 *  
 *  
 */  
public class DesUtils {
    /** 
     * DES ALGORITHM <br> 
     *  
     * <pre> 
     *   DES                  アルゴリズム 
     *   DESede(TripleDES)     アルゴリズム 
     *   AES                  アルゴリズム 
     *   Blowfish             アルゴリズム 
     *   RC2                  アルゴリズム 
     *   RC4(ARCFOUR)         アルゴリズム 
     * </pre> 
     *  
     *  
     */  
    public static final String DES_ALGORITHM = "DES";
    public static final String DESEDE_ALGORITHM = "DESede";
    public static final String AES_ALGORITHM = "AES";
    public static final String BLOWFISH_ALGORITHM = "Blowfish";
    public static final String RC2_ALGORITHM = "RC2";
    public static final String RC4_ALGORITHM = "RC4";
    public static final String SELECTED_ALGORITHM = DES_ALGORITHM;    //select algorithm  
  
    /** 
     * キー生成<br> 
     *  
     * @param key 
     * @return 
     * @throws Exception 
     */  
    private static Key toKey(byte[] key) {
        // if DES algorithm, you can use the following:  
        // DESKeySpec dks = new DESKeySpec(key);  
        // SecretKeyFactory keyFactory =  
        // SecretKeyFactory.getInstance(DES_ALGORITHM);  
        // SecretKey secretKey = keyFactory.generateSecret(dks);  
  
        // or if AES、Blowfish etc, you can use..  
        SecretKey secretKey = new SecretKeySpec(key, SELECTED_ALGORITHM);
  
        return secretKey;
    }  
  
    /** 
     * 復号化 
     *  
     * @param data 
     * @param key 
     * @return 
     * @throws IOException 
     * @throws NoSuchPaddingException 
     * @throws NoSuchAlgorithmException 
     * @throws InvalidKeyException 
     * @throws BadPaddingException 
     * @throws IllegalBlockSizeException 
     * @throws Exception 
     */  
    public static byte[] decrypt(byte[] data, String key) throws IOException,  
            NoSuchAlgorithmException, NoSuchPaddingException,  
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {  
        Key k = toKey(Base64.decode(key));  
  
        Cipher cipher = Cipher.getInstance(SELECTED_ALGORITHM);  
        cipher.init(Cipher.DECRYPT_MODE, k);  
  
        return cipher.doFinal(data);  
    }  
  
    /** 
     * 暗号化 
     *  
     * @param data 
     * @param key 
     * @return 
     * @throws NoSuchPaddingException 
     * @throws NoSuchAlgorithmException 
     * @throws InvalidKeyException 
     * @throws BadPaddingException 
     * @throws IllegalBlockSizeException 
     * @throws 
     * @throws IOException 
     * @throws Exception 
     */  
    public static byte[] encrypt(byte[] data, String key) throws IOException,  
            NoSuchAlgorithmException, NoSuchPaddingException,  
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {  
        Key k = toKey(Base64.decode(key));  
        Cipher cipher = Cipher.getInstance(SELECTED_ALGORITHM);  
        cipher.init(Cipher.ENCRYPT_MODE, k);  
  
        return cipher.doFinal(data);  
    }  
  
    /** 
     * SEEDキー生成（ランダム） 
     *  
     * @return ランダムSEEDキー 
     * @throws IOException 
     * @throws NoSuchAlgorithmException 
     * @throws Exception 
     */  
    public static String createKey() throws NoSuchAlgorithmException,  
            IOException {  
        return createKey(null);  
    }  
  
    /** 
     * SEEDキー生成（指定） 
     *  
     * @param seed 
     *            seed 
     * @return SEEDキー 
     * @throws NoSuchAlgorithmException 
     * @throws IOException 
     *  
     */  
    public static String createKey(String seed)  
            throws NoSuchAlgorithmException, IOException {  
        SecureRandom secureRandom = null;  
  
        if (seed != null) {  
            secureRandom = new SecureRandom(Base64.decode(seed));  
        } else {  
            secureRandom = new SecureRandom();  
        }
  
        KeyGenerator kg = KeyGenerator.getInstance(SELECTED_ALGORITHM);  
        kg.init(secureRandom);  
  
        SecretKey secretKey = kg.generateKey();  
  
        return Base64.encodeBytes(secretKey.getEncoded());  
    }  
  
    /***
    public static void main(String[] args) {  
        try {  
            String key = DES.createKey("abcdef");  
            byte[] bytes = DES.encrypt("あいうえお12345abcdefg".getBytes(), key);  
            System.out.println("[BASE64] " + BASE64.encryptBASE64(bytes));    //表示のため、DESで暗号化⇒BASE64変換  
  
            byte[] decryptBytes = DES.decrypt(bytes, key);  
            System.out.println("[復号化] " + new String(decryptBytes));  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }  
    ***/
}

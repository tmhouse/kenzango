/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Log;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Debug;
import android.util.Log;
import android.widget.Toast;

/**
 * Loggerの便利クラス.
 * 機能
 * - debuggerble==trueのときだけ出力する。
 * - ログの直前行に、VERBOSEで"at location"という形式で吐くので、
 *   LogCatでダブルクリックするとその行へ飛べて便利。
 *   LogCatの出力レベルを普段はDEBUGにしておき、出力行が見たいときは
 *   VERBOSEに切り替えて使用する(といいよ)。
 * - tagにはクラス名とメソッド名が自動で入る。
 * 
 * @author mutoh
 *
 */
public class TmLog {
	// デフォルトはDEBUGだったらenable
	private static Boolean s_bEnable = null;

	private static boolean s_isFirst = true;
	private static LogThread	m_logThread = new LogThread();
	@SuppressLint("SimpleDateFormat")
	private static SimpleDateFormat s_df = new SimpleDateFormat("HH:mm:ss.SSS");
	private static Boolean		s_isDebug = null;
	
	/**
	 * flag.
	 */
	private static boolean s_flag_asyncOut = false;
	private static boolean s_flag_separateLocation = true;

	/**
	 * ログを吐くスレッド.
	 * @author mutoh
	 *
	 */
	private static class LogThread extends Thread {
		private ArrayList<PdThrowableForLog> m_que = 
				new ArrayList<PdThrowableForLog>(16);

		@Override
		public void run() {
			try {
				while(true) {
					PdThrowableForLog log = top();
					if( log == null ) {
						synchronized(this) {
							wait(0);
						}
					} else {
						printLog(log);
					}
				}
			} catch (InterruptedException e) {
				//e.printStackTrace();
			}
		}
		
		public synchronized void add(PdThrowableForLog log) {
			m_que.add(log);
			notify();
		}
		
		public synchronized PdThrowableForLog top() {
			if( m_que.size() > 0 ) {
				PdThrowableForLog log = m_que.get(0);
				m_que.remove(0);
				return(log);
			}
			return(null);
		}
		
	}
	
	/**
	 * ログを吐く.
	 * @param log
	 */
	private static void printLog(PdThrowableForLog log) {
		if( s_isFirst ) {
			s_isFirst = false;
			// Log.eで一発何か出しておかないとLogCatのフィルターが有効
			// にならないための処置。
			Log.e(TmLog.class.getSimpleName(), "start.(this is not an error message)");
		}
		
		String locStr = log.createCallerFileAndLine();
		String methodStr = log.createCallerMethodName();
		// thread idは, Thread.getId()じゃ違う値がとれるよ。
		//long tid = android.os.Process.myTid();
		String tname = log.getThread().getName();
		//String time = s_df.format(new Date(log.getTime()));
		String msg = log.getMsg() == null ? "" : log.getMsg();
		
		if( s_flag_separateLocation ) {
			// ２行ログ
			//String verboseTag = time + ", TID=" + tid + ", TNM=" + tname;
			String verboseTag = "TNAME:" + tname;
			Log.v(verboseTag, locStr);
			Log.println(log.getKind(), methodStr, msg);
		} else {
			// １行ログ
			Log.println(log.getKind(), methodStr, locStr + ", " + msg);
		}
	}

	/**
	 * enable/disable.
	 * よく使うサンプル.
	 * TmLog.enable(TmLog.isDebuggable(context));
	 * @param bEnable
	 */
	public static void enable(boolean bEnable) {
		s_bEnable = bEnable;
	}

	/**
	 * 自動的にon/offを行う.
	 * @param ctx
	 */
	public static void enableAuto(Context ctx) {
		s_bEnable = isDebugBuild(ctx);
	}
	
	/**
	 * 非同期出力モードの設定.
	 * @param val
	 */
	public static void setAsyncMode(boolean val) {
		s_flag_asyncOut = val;
	}
	
	/**
	 * 場所の出力をverboseで別に出力する.
	 * @param val
	 */
	public static void setSeparateLocation(boolean val) {
		s_flag_separateLocation = val;
	}
	
	/**
	 * call stackを得るためのThrowableクラス.
	 * @author mutoh
	 *
	 */
	private static class PdThrowableForLog extends Throwable {
		private static final long serialVersionUID = 1L;
		private StackTraceElement	m_caller;
		private int m_kind;
		private String m_msg;
		private Thread m_thread;
		private long m_time;
		
		public PdThrowableForLog(int kind, String msg) {
			m_kind = kind;
			m_msg = msg;
			m_thread = Thread.currentThread();
			m_time = System.currentTimeMillis();
		}
		
		public int getKind() {
			return(m_kind);
		}
		
		public String getMsg() {
			return(m_msg);
		}
		
		public Thread getThread() {
			return(m_thread);
		}
		
		public long getTime() {
			return(m_time);
		}
		
		/**
		 * 呼び出し元のファイル名と行番号を返す.
		 * @return
		 */
		public String createCallerFileAndLine() {
			StackTraceElement s = getCaller();
			if( s != null ) {
				String str = "at (" + 
					getFilePath() + ":" + s.getLineNumber() + ")";
				return(str);
			}
			return(null);
		}
		
		/**
		 * 呼び出し元のファイル名のフルパスを返す.
		 * @return
		 */
		public String getFilePath() {
			StackTraceElement s = getCaller();
			if( s != null ) {
				String fullClassName = s.getClassName();
				// inner classから呼ばれた場合は'$n'が付くので消す
				// TODO これ以外のパターンてないのか？
				fullClassName = fullClassName.replaceAll("\\$.*$", "");
				return(fullClassName + ".java");
			}
			return(null);
		}
		
		/**
		 * 呼び出し元のクラス名とメソッド名を返す.
		 * @return
		 */
		public String createCallerMethodName() {
			StackTraceElement s = getCaller();
			if( s != null ) {
				String str = getSimpleClassName(s) + "." +
						s.getMethodName();
				return(str);
			}
			return(null);
		}

		/**
		 * クラス名だけを返す.
		 * @param s
		 * @return
		 */
		private String getSimpleClassName(StackTraceElement s) {
			String fullClassName = s.getClassName();
			String[] arr = fullClassName.split("\\.");
			if( arr != null && arr.length > 0 ) {
				return(arr[arr.length - 1]);
			}
			return(null);
		}

		/**
		 * 呼び出し元のスタックフレーム情報を返す.
		 * @return
		 */
		private StackTraceElement getCaller() {
			if( m_caller == null ) {
				StackTraceElement[] stacks = getStackTrace();
				String logClassName = TmLog.class.getName();
				for( StackTraceElement s : stacks ) {
					String className = s.getClassName();
					// TmLogクラスのstackは除外
					if( className.equals(logClassName) ) {
						continue;
					}
					m_caller = s;
					break;
				}
			}
			return(m_caller);
		}
	}
	
	/**
	 * パッケージがdebuggerble=trueか否か.
	 * @param context
	 * @return
	 */
	/***
	public static boolean isDebuggable(Context context) {
		if( s_isDebug == null ) {
			s_isDebug = false;
			try {
				PackageManager manager = context.getPackageManager();
				ApplicationInfo appInfo = manager.getApplicationInfo(
						context.getPackageName(), 0);
				if ((appInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) == 
						ApplicationInfo.FLAG_DEBUGGABLE) {
					s_isDebug = true;
				}
			} catch (NameNotFoundException e) {
			}
		}
		return(s_isDebug);
	}
	***/
	
	/**
	 * デバッグビルドか.
	 * @param ctx
	 * @return
	 */
	public static boolean isDebugBuild(Context ctx) {
		if (s_isDebug == null) {
			s_isDebug = false;
			try {
				String packageName = ctx.getPackageName();

				Class c = ctx.getClassLoader().loadClass(
						packageName + ".BuildConfig");
				Field f = c.getDeclaredField("DEBUG");
				s_isDebug = f.getBoolean(null);
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		return (s_isDebug);
	}
	
	/**
	 * 自己署名されたapkか.
	 * @param ctx
	 * @return
	 */
	public static boolean isSelfSignedApk(Context ctx) {
		boolean isDebug = false;
		try {
			PackageManager pm = ctx.getPackageManager();
			String packageName = ctx.getPackageName();
			PackageInfo pi = pm.getPackageInfo(packageName, PackageManager.GET_SIGNATURES);

			Signature sig = pi.signatures[0];
			final byte[] sigBytes = sig.toByteArray();
	        InputStream certStream = new ByteArrayInputStream(sigBytes);

	        CertificateFactory cf = CertificateFactory.getInstance("X509");
	        X509Certificate x509 = (X509Certificate)cf.generateCertificate(certStream);
	        
	        //Principal p = x509.getSubjectDN();
	        //TmLog.d("principal getName = " + p.getName());
	        if( isSelfSigned(x509) ) {
	        	isDebug = true;
	        }
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("could not get signature.");
		}
		return(isDebug);
	}
	
	/**
	 * Checks whether given X.509 certificate is self-signed.
	 * from http://www.nakov.com/blog/2009/12/01/x509-certificate-validation-in-java-build-and-verify-chain-and-verify-clr-with-bouncy-castle/
	 */
	public static boolean isSelfSigned(X509Certificate cert)
			throws CertificateException, NoSuchAlgorithmException,
			NoSuchProviderException {
		try {
			// Try to verify certificate signature with its own public key
			PublicKey key = cert.getPublicKey();
			cert.verify(key);
			return true;
		} catch (SignatureException sigEx) {
			// Invalid signature --> not self-signed
			return false;
		} catch (InvalidKeyException keyEx) {
			// Invalid key --> not self-signed
			return false;
		}
	}
	
	/**
	 * d.
	 * @param msg
	 */
	public static void d(String msg) {
		callLogMethod(Log.DEBUG, msg);
	}
	public static void d(String tag, String msg) {
		d(tag == null ? msg : tag + ", " + msg);
	}

	/**
	 * w.
	 * @param msg
	 */
	public static void w(String msg) {
		callLogMethod(Log.WARN, msg);
	}
	public static void w(String tag, String msg) {
		w(tag == null ? msg : tag + ", " + msg);
	}

	/**
	 * e.
	 * @param msg
	 */
	public static void e(String msg) {
		callLogMethod(Log.ERROR, msg);
	}
	public static void e(String tag, String msg) {
		e(tag == null ? msg : tag + ", " + msg);
	}

	/**
	 * i.
	 * @param msg
	 */
	public static void i(String msg) {
		callLogMethod(Log.INFO, msg);
	}
	public static void i(String tag, String msg) {
		i(tag == null ? msg : tag + ", " + msg);
	}

	/**
	 * v.
	 * @param msg
	 */
	public static void v(String msg) {
		callLogMethod(Log.VERBOSE, msg);
	}
	public static void v(String tag, String msg) {
		v(tag == null ? msg : tag + ", " + msg);
	}
	
	/**
	 * a.
	 * @param msg
	 */
	public static void a(String msg) {
		callLogMethod(Log.ASSERT, msg);
	}
	public static void a(String tag, String msg) {
		a(tag == null ? msg : tag + ", " + msg);
	}
	
	/**
	 * ログを吐くメソッド.
	 * @param kind
	 * @param tag
	 * @param msg
	 */
	private static void callLogMethod(int kind, String msg) {
		if( s_bEnable == null || s_bEnable == false ) {
			return;
		}
		
		/***
		// 非同期出力時にスレッドがいなかったらstartする
		if( s_flag_asyncOut && !m_logThread.isAlive() ) {
			m_logThread.start();
		}

		PdThrowableForLog log = new PdThrowableForLog(kind, msg);
		if( s_flag_asyncOut ) {
			// 非同期
			m_logThread.add(new PdThrowableForLog(kind, msg));
		} else {
			// 同期
			printLog(log);
		}
		***/
		PdThrowableForLog log = new PdThrowableForLog(kind, msg);
		printLog(log);
	}
}

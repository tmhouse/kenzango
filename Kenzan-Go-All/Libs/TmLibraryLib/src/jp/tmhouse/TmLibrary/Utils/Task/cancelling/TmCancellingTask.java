/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Task.cancelling;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import jp.tmhouse.TmLibrary.Utils.Log.TmLog;

/**
 * 実行キャンセルできるRunnable executer.
 * @author mutoh
 *
 */
public class TmCancellingTask {

	private final ExecutorService mExec = Executors.newCachedThreadPool();
	private final Cancelable mTask;
	private final long mTimeout;
	private final TimeUnit mUnit;

	/**
	 * 
	 * @author mutoh
	 *
	 */
	public interface Cancelable extends Runnable {
		void cancel();
	}

	/**
	 * 
	 * @param exec
	 * @param task
	 */
	public TmCancellingTask(Cancelable task) {
		this(task, 0, null);
	}

	public TmCancellingTask(Cancelable task, long timeout, TimeUnit unit) {
		mTask = task;
		mTimeout = timeout;
		mUnit = unit;
		((ThreadPoolExecutor)mExec).setRejectedExecutionHandler(m_rejectedHandler);
	}
	
	private RejectedExecutionHandler m_rejectedHandler = 
			new RejectedExecutionHandler() {
		@Override
		public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
			throw new RuntimeException("rejectedExecution occured");
		}
	};
	
	public void purge() {
		((ThreadPoolExecutor)mExec).purge();
	}

	//public void cancel() {
	//	mTask.doCancel();
	//}

	public void run() {
		Future<?> future = null;
		try {
			future = mExec.submit(mTask);
			waitForCompletionOrTimeout(future);
		} catch (InterruptedException e) {
			mTask.cancel();
		} catch (ExecutionException e) {
			TmLog.e(e.toString());
			mTask.cancel();
		}
	}
	
	private void waitForCompletionOrTimeout(Future<?> future)
			throws InterruptedException, ExecutionException {
		if (mTimeout <= 0) {
			future.get();
			return;
		}
		try {
			future.get(mTimeout, mUnit);
		} catch (TimeoutException e) {
			// TODO timeoutすると単にtask.cancel()を呼ぶだけ。
			// つまり結局worker threadを止める方法はてめーでやれだ。
			mTask.cancel();
		}
	}
}

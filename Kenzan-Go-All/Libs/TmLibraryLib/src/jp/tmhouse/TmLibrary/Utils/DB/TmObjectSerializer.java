/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.DB;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;


/**
 * オブジェクトをserialize, desirializeするutilクラス.
 * @author mutoh
 *
 * @param <T>
 */
public class TmObjectSerializer<T> {

	public byte[] serializeObject(T o) throws IOException { 
		if( o == null ) {
			return(null);
		}
		ByteArrayOutputStream bos = new ByteArrayOutputStream(); 

		ObjectOutput out = new ObjectOutputStream(bos); 
		out.writeObject(o); 
		out.close(); 

		// Get the bytes of the serialized object 
		byte[] buf = bos.toByteArray(); 

		return buf; 
	}

	@SuppressWarnings("unchecked")
	public T deserializeObject(byte[] b) throws ClassNotFoundException, IOException { 
		if( b == null ) {
			return(null);
		}

		ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(b)); 
		Object object = in.readObject(); 
		in.close(); 

		return (T)object; 
	}
}
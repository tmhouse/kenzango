/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Speech;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import jp.tmhouse.TmLibrary.Utils.HanKana2ZenKana;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import android.content.Context;
import android.os.Handler;
import android.speech.tts.TextToSpeech;

/**
 * しゃべる.
 * append(text)にて非同期に文字列をキューイングする。
 * 
 * [特徴]
 * どしどしキューイングしたとしても、textの再生開始と終了時にリスナが
 * 発火するため、再生中の文字列ごとに行いたい処理を記述できる。
 * 
 */
public class TmSpeech3 {

	private TextToSpeech			m_tts;
	private boolean				m_ttsIgnore = false;
	private float					m_rate = c_NORMAL_SPEECH_RATE;
	private HanKana2ZenKana			m_han2zen = new HanKana2ZenKana();
	private	 HashMap<String, String> m_param = new HashMap<String, String>();
	// しゃべるQue
	private	 ArrayList<SpeakElm>	m_speakQue = new ArrayList<SpeakElm>();
	private	 ArrayList<SpeakElm>	m_speakNowQue = new ArrayList<SpeakElm>();
	private int					m_speakQueIdx = 0;
   	private int					m_loopCnt;
	
	private _speechThread			m_thread;
	private Handler					m_handler = new Handler();
	private IOnInitListener			m_initCB;
	private String					m_targetIso3Lang;
	
	public static final float c_FAST_SPEECH_RATE = 2.0f;
	public static final float c_NORMAL_SPEECH_RATE = 1.0f;
	
    private static final int c_SPEAK_DEFAULT_INTERVAL = 100; // デフォルトのけつかっちん待ち時間(ms)
    
    /**
     * Speechスレッド.
     * @author mutoh
     *
     */
    private class _speechThread extends Thread {
    	private boolean m_stop = false;
    	private boolean m_pause = false;
    	private long	m_nextWaitTime = c_defaultNextWaitTime;
    	private static final long c_defaultNextWaitTime = 10;
    	
    	synchronized public boolean isPaused() {
    		return(m_pause);
    	}
    	synchronized public void pauseThread() {
    		m_pause = true;
    	}
    	synchronized public void continueThread() {
    		m_pause = false;
    		notifyThread();
    	}
    	synchronized public void stopThread() {
    		m_pause = false;
    		m_stop = true;
    		notifyThread();
    	}
    	synchronized public void notifyThread() {
   			TmLog.d("notifyThread", "notify!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
   			notify();
		}
    	
    	synchronized public void setNextWaitTime(long time) {
    		m_nextWaitTime = time;
    	}
    	
		@Override
		public void run() {
			boolean bStop = m_stop;
			for (;bStop == false;) {
				TmLog.e("run", "loop count=" + String.valueOf(m_loopCnt++));
				SpeakElm elm = getNextSpeakNowQue();
				if( elm == null ) {
					if( m_pause == false ) {
						elm = getSpeakQue();
					} else {
						TmLog.d("Pause", "Pause Now...");
					}
				} else {
					TmLog.d("SpeakNow", elm.m_text);
				}
				if( elm != null && elm.m_text != null) {
					try {
						// 1.呼び出し元へコールバック(Handlerへpost)
						fireBeginOfUttenrance(elm);
						
						// 2.speak
						// 半角カナの発音がおかしいので全カナに補正
						String text = m_han2zen.Han2Zen(elm.m_text);
						text = text.replace("・", "　点　");
						
						// しゃべり終了時に待っているindex番号を設定
						m_speechEndListener.setWaitingElm(elm);
							
						// 次再生の待ち時間をクリア
						setNextWaitTime(c_defaultNextWaitTime);
						
						// 文章が長いと再生開始までに時間がかかるので、「。」で分割。
						String[] textArr = text.split("。");
						TmLog.d("run", "textArr.size = " + textArr.length);
						int i = 0;
						for( String str : textArr ) {
							TmLog.d("run", "text[" + i + "] = " + str);
							String clientVal = null;
							if( textArr.length == ++i ) {
								clientVal = String.valueOf(elm.m_index);
							} else {
								// -1にしておけば、再生終了のコールバックは呼ばれない。
								clientVal = "-1";
							}
							m_param.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, clientVal);
							m_tts.speak(str, TextToSpeech.QUEUE_ADD, m_param);
						}
						
						// 3.speckが完了したタイミングで呼び出し元へコールバック(Handlerへpost)
						// OnUtteranceCompletedListener で受ける。
							
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					TmLog.w("TmSpeech2.run", "do nothing..........");
				}
				
				synchronized(this) {
					try {
						TmLog.i("wait(0)", "before");
						wait(0);
						TmLog.i("wait(0)", "after, and next wait time = " + m_nextWaitTime);
						
						// 次の再生までの待ち
						wait(m_nextWaitTime);
						m_nextWaitTime = c_defaultNextWaitTime;
						TmLog.i("wait(nextWaitTime)", "after");
					} catch (Exception e) {
						TmLog.d("TmSpeech2.run", "wait error. " + e);
					}
					bStop = m_stop;
				}
			}
		}
    }
    
    /*
     * 呼び出し元へコールバックするRunnable
     */
    private class Callback2Client implements Runnable {
    	public static final int CALLID_UNKNOWN = 0;
    	public static final int CALLID_BEGIN = 1;
    	public static final int CALLID_END = 2;
    	
    	private	 int		m_callId = CALLID_UNKNOWN;
    	private SpeakElm	m_speakElm;
    	
    	public Callback2Client(SpeakElm speakElm, int callId) {
    		m_speakElm = speakElm;
    		m_callId = callId;
    	}
    	
		@Override
		public void run() {
			if( m_speakElm == null || m_speakElm.m_listener == null ) {
				return;
			}
			switch( m_callId ) {
			case CALLID_BEGIN:
				m_speakElm.m_listener.onSpeakBegin(m_speakElm.m_index, m_speakElm.m_text);
				break;
			case CALLID_END:
				m_speakElm.m_listener.onSpeakEnd(m_speakElm.m_index, m_speakElm.m_text);
				break;
			} 
		}
    }
	
    /**
     * コンストラクタ
     */
	public TmSpeech3() {
        m_thread = new _speechThread();
	}
	
	/*
	 * 初期化
	 */
	public interface IOnInitListener {
		public void onSuccess();
		public void onError();
	}

	public void initialize(Context ctx, Locale loc, IOnInitListener initCB) {
		shutdown();
		
		if( loc == null ) {
			loc = Locale.getDefault();
		}
		m_targetIso3Lang = loc.getISO3Language();
		m_initCB = initCB;
        m_tts = new TextToSpeech(ctx, m_ttsInitListener);
	}
	
	/**
	 * 終了
	 */
    public void shutdown() {
        // Don't forget to shutdown!
        if (m_tts != null) {
            m_tts.stop();
            m_tts.shutdown();
            m_tts = null;
            m_thread.stopThread();
        }
        _clearSpeakQue();
    }
    
    /**
     * 呼び出し元が使用するリスナ.
     * @author mutoh
     *
     */
    public interface ISpeakListener {
    	/**
    	 * 再生が開始される.
    	 * @param index
    	 * @param tag
    	 * @param text
    	 */
    	public void onSpeakBegin(int index, String text);
    	
    	/**
    	 * 再生が終了した.
    	 * @param index
    	 * @param tag
    	 * @param text
    	 */
    	public void onSpeakEnd(int index, String text);
    }
    
    /**
     * テキストをqueingするデータフォルダ.
     * @author mutoh
     *
     */
    private class SpeakElm {
    	public ISpeakListener		m_listener;
    	public String				m_text;
    	public int					m_index;
    	public int					m_afterWait;
    	
    	/**
    	 * コンストラクタ.
    	 * @param text
    	 * @param listener
    	 */
    	public SpeakElm(int index, String text, int afterWait, ISpeakListener listener) {
    		m_index = index;
    		m_listener = listener;
    		m_text = text;
    		m_afterWait = afterWait > 0 ? afterWait : 0;
    	}
    }
    
    /**
     * TTSの初期化完了リスナ
     */
	private TextToSpeech.OnInitListener m_ttsInitListener = 
		new TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
		    	if (status == TextToSpeech.SUCCESS) {
		    		if( m_tts == null ) {
		    			return;
		    		}
		    		
		    		//TmLog.i("TTS default engine:" + m_tts.getDefaultEngine());
		    		Locale loc = m_tts.getLanguage();
		    		String iso3lang = loc.getISO3Language();
		    		TmLog.i("TTS lang:" + iso3lang);
		    		if( iso3lang.equals(m_targetIso3Lang) == false ) {
		    			// 指定の言語のTTSがインストールされていない
		    			if( m_initCB != null ) {
		    				m_initCB.onError();
		    				return;
		    			}
		    		}

		    		m_tts.setSpeechRate(m_rate);
		    		
		    		// onInit以降でないと効かない
		    		// 一回の再生が終了したときにコールバックしてもらう
		    		m_tts.setOnUtteranceCompletedListener(m_speechEndListener);
			
		    		// いろいろ頑張ったが、N2TTSとPicoとでの切り替えはできなかった
		    		//setTtsEngine(getLocale());
		        	//speechTextAppend(m_wellComeSpeech);
		    		
		    		// しゃべるスレッド開始
		    		m_thread.start();
		    		
	    			if( m_initCB != null ) {
	    				m_initCB.onSuccess();
	    			}
		        } else {
		            TmLog.e("onInit", "Could not initialize TextToSpeech.");
		            shutdown();
	    			if( m_initCB != null ) {
	    				m_initCB.onError();
	    				return;
	    			}
		        }
			}
		};
	
	/**
	 * TTSが一回の再生が終了した時のリスナ
	 */
	private SpeechEndListener m_speechEndListener = new SpeechEndListener();
			
	private class SpeechEndListener implements TextToSpeech.OnUtteranceCompletedListener
	{
		private SpeakElm m_waitingElm;
		
		// 現在終了を待っているSpeakElm
		synchronized public void setWaitingElm(SpeakElm elm) {
			m_waitingElm = elm;
		}
		
		@Override
		synchronized public void onUtteranceCompleted(String utteranceId) {
			int index = -1;
			try { 
				index = Integer.valueOf(utteranceId);
			} catch (NumberFormatException e) {
				e.printStackTrace();
				TmLog.e("ERROR", "onUtteranceCompleted, integer parse error");
				return;
			}
			
			// 再生が中断されたとき、TTSはこのリスナを発火するかどうか分からない。
			// なので、再生開始時のindexと現在のm_speakQueIdxが異なる場合は、
			// 再生停止などの処理があったとして無視する。
			SpeakElm elm = m_waitingElm;
			if( index != elm.m_index ) {
				TmLog.w("onUtteranceCompleted", "index mismatch: utteranceId=" + utteranceId + 
					", waitingIndex=" + elm.m_index + ", elm.m_text = " + elm.m_text);
				return;
			}
			TmLog.d("onUtteranceCompleted", "OK, utteranceId=" + utteranceId + 
						", waitingIndex=" + elm.m_index + ", text = " + elm.m_text);
			
			// 一回の再生が終わった。
			fireEndOfUttenrance(elm);
				
			long waitTime = 0;
			waitTime = elm.m_afterWait > 0 ? elm.m_afterWait : 1;
				
			m_thread.setNextWaitTime(waitTime);
			m_thread.notifyThread();
		}
	}
	
	/**
	 * 一回の再生が終わった.
	 */
	synchronized private void fireEndOfUttenrance(SpeakElm elm) {
		if( elm != null ) {
			//TmLog.d("fireEndOfUttenrance", "end of id:" + elm.m_clientData);
			if( elm.m_listener != null ) {
				// 呼び出し元へコールバック(Handlerへpost)
				m_handler.post(new Callback2Client(elm, Callback2Client.CALLID_END));
			}
		} else {
			TmLog.d("fireEndOfUttenrance", "NO SpeackElm");
		}
	}
	
	/**
	 * 一回の再生を開始.
	 * @param elm
	 */
	synchronized private void fireBeginOfUttenrance(SpeakElm elm) {
		if( elm != null && elm.m_listener != null ) {
			// 呼び出し元へコールバック(Handlerへpost)
			m_handler.post(new Callback2Client(elm, Callback2Client.CALLID_BEGIN));
		}
	}

	public void setRate(float rate) {
		m_rate = rate;
		if( m_tts != null ) {
			m_tts.setSpeechRate(rate);
		}
	}
	
    
    /**
     * しゃべる文字列をキューイング.
     * @param text
     * @param mode
     * @return
     */
    synchronized public void append(String text, int afterWait, ISpeakListener listener) {
    	int beforeQueSize = m_speakQue.size();
   		_appendQue(m_speakQue, beforeQueSize, text, afterWait, listener);
    }
    
    /**
     * 強制的にしゃべる.
     * @param text
     */
    synchronized public void speakNow(String text, boolean bImmediate) {
    	TmLog.d("speakNow", "append text:" + text);
    	int afterWait = 0;
    	
    	if( m_tts == null ) {
    		_appendQue(m_speakNowQue, -1, text, afterWait, null);
    		return;
    	}
    	
    	// 今しゃべってなくてもリクエスト中の場合があるため常にstop
    	m_tts.stop();
    	
    	// Immediateモードの場合はqueをclearする
    	if( bImmediate ) {
    		m_speakNowQue.clear();
    	}
    	
   		_appendQue(m_speakNowQue, -1, text, afterWait, null);
   		
   		// スレッド再開は少し間をおいた方がバチバチ鳴りにくいようだ
   		/***
   		m_handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				m_thread.notifyThread();
			}
   		}, 300);
   		***/
		m_thread.notifyThread();
    }
    
    synchronized public boolean hasSpeakNow() {
    	return(m_speakNowQue.size() > 0);
    }
    
    /**
     * キューに突っ込む内部関数.
     * @param list
     * @param text
     * @param afterWait
     * @param listener
     */
    synchronized private void _appendQue(ArrayList<SpeakElm> list, int index, 
    		String text, int afterWait, ISpeakListener listener)
    {
    	if( m_ttsIgnore ) {
    		return;
    	}
    	if( text == null || text.length() == 0 ) {
    		return;
    	}
    	
    	//int index = m_speakQue.size();
    	afterWait = afterWait <= 0 ? c_SPEAK_DEFAULT_INTERVAL : afterWait;
   		list.add(new SpeakElm(index, text, afterWait, listener));
    }
    
    /**
     * 一個のテキストをキューから取り出す.
     * @return
     */
    synchronized private SpeakElm getSpeakQue() {
    	if( m_speakQue.size() == 0 ) {
    		TmLog.d("getSpeakQue", "no elements");
    		return(null);
    	}
    	if( m_speakQue.size() <= m_speakQueIdx ) {
    		TmLog.d("getSpeakQue", "END OF LIST");
    		return(null);
    	}
    	SpeakElm elm = m_speakQue.get(m_speakQueIdx++);
    	return(elm);
    }
    
    /**
     * すぐしゃべるべきテキストをキューから取り出す.
     * @return
     */
    synchronized private SpeakElm getNextSpeakNowQue() {
    	if( m_speakNowQue.size() == 0 ) {
    		return(null);
    	}
    	SpeakElm elm = m_speakNowQue.remove(0);
    	return(elm);
    }
    
    /**
     * キューを全部消す.
     */
    synchronized private void _clearSpeakQue() {
    	m_speakQue.clear();
    	m_speakQueIdx = 0;
    }
    
	/**
	 * データ破棄.
	 */
	synchronized public void clearSpeakQue() {
		_clearSpeakQue();
		//fireEndOfUttenrance(getCurSpeakElm());
		//if( bStop && m_tts != null && m_tts.isSpeaking() ) {
		//	m_tts.stop();
		//}
	}
	
	synchronized public void clearAllSpeakQue() {
		m_speakNowQue.clear();
		clearSpeakQue();
	}
	
	/**
	 * 再生停止.
	 */
	synchronized public void pauseOnSpeak() {
		m_thread.pauseThread();
		//if( m_tts.isSpeaking() ) {
			//m_tts.stop();
		//}
	}
	
	/**
	 * 再生継続.
	 */
	synchronized public void pauseOffSpeak() {
		m_thread.continueThread();
	}
	
	/**
	 * 再生が停止中か.
	 */
	synchronized public boolean isPaused() {
		return(m_thread.isPaused());
	}
	
	/**
	 * 再生開始行の設定.
	 * @param lineIncDec
	 */
	synchronized public void setSpeakLineNo(int lineNo) {
		boolean paused = false;
		if( isPaused() == false ) {
			pauseOnSpeak();
			paused = true;
		}
		
		TmLog.i("setSpeakLineNo", "lineNo = " + lineNo);
		//m_tts.stop();
		m_speakQueIdx = lineNo;
		
		int que_size = m_speakQue.size();
		if( lineNo > que_size ) {
			//m_speakQueIdx = que_size - 1;
			m_speakQueIdx = que_size;
		}
		if( lineNo < 0 ) {
			m_speakQueIdx = 0;
		}
		
		if( paused ) {
			pauseOffSpeak();
		}
	}
	
	/**
	 * 現在の行数を返す.
	 * @return
	 */
	synchronized public int getCurrentLineNo() {
		int chousei = 0;
		if( isPaused() ) {
			// m_speakQueIdxは常に一個先を指しているので、
			// 止まっているときは、そのまま。
			chousei = 0;
		} else {
			chousei--;
		}
		if( m_speakQueIdx <= 0 ) {
			return(0);
		}
		return(m_speakQueIdx + chousei);
	}
	
	/**
	 * 合計行数を返す.
	 * @return
	 */
	synchronized public int getTotalLineNo() {
		return(m_speakQue.size());
	}
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Debug;

/**
 * 別スレッドを生成してRunnableを実行し、タイムアウトするまで戻らない.
 * @author mutoh
 *
 */
public class TmSyncRunner {

	public static void start(Runnable run, long timeout) {
		Thread thread = new Thread(run);
		thread.start();
		try {
			// threadが終わるまで待つ
			thread.join(timeout);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			thread.interrupt();
		}
	}
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.DB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jp.tmhouse.TmLibrary.Utils.Log.TmLog;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TmBlobDB<T> extends SQLiteOpenHelper {
	private String m_tableName;

	private static String c_DB_NAME = "TmBlobDB";

	private static final String c_COLUMN_RECID = "_id";
	private static final String c_COLUMN_KEY = "_key";
	private static final String c_COLUMN_VALUE = "_value";
	
	private TmObjectSerializer<T>	m_serializer = new TmObjectSerializer<T>();

	/**
	 * db name固定のコンストラクタ.
	 * @param c
	 * @param tableName
	 * @param dbVer
	 */
	public TmBlobDB(Context c, String tableName, int dbVer){
		super(c, c_DB_NAME, null, dbVer);
		m_tableName = tableName;
	}
	
	/**
	 * db nameを変更できるコンストラクタ.
	 * dbName==nullだとon memory dbになる。
	 * @param c
	 * @param dbName
	 * @param tableName
	 * @param dbVer
	 */
	public TmBlobDB(Context c, String dbName, String tableName, int dbVer){
		super(c, dbName, null, dbVer);
		m_tableName = tableName;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table " + m_tableName + " ("
                + c_COLUMN_RECID + " integer primary key autoincrement not null, "
                + c_COLUMN_KEY + " text not null UNIQUE, "
                + c_COLUMN_VALUE + " blob "
                + ");" );	
	}
	
    @Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		
		//dropTable(db);
	}
    
    /**
     * tableごと消す.
     * @param db
     */
    public void dropTable(SQLiteDatabase db) {
		String sql = "drop table " + m_tableName + ";";
		try {
		    db.execSQL(sql);
		} catch (SQLException e) {
		    TmLog.e("ERROR", e.toString());
		}
    }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
	
	/**
	 * Tをsetする。
	 * @param key
	 * @param val
	 * @throws IOException 
	 */
	public void set(String key, T val) throws IOException {
		setBlob(key, m_serializer.serializeObject(val));
	}

	/**
	 * Tをgetする.
	 * @param key
	 * @return
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public T get(String key) throws ClassNotFoundException, IOException {
		return(m_serializer.deserializeObject(getBlob(key)));
	}
	
	/**
	 * 全件取得する.
	 * @return
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public List<T> getAll() throws ClassNotFoundException, IOException {
		return(getBlobAll());
	}
	
	/**
	 * insert
	 * @param mailId
	 * @param blob
	 * @return
	 */
	private long insertBlob(String key, byte[] value) { 
		SQLiteDatabase db = getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			values.put(c_COLUMN_KEY, key);
			values.put(c_COLUMN_VALUE, value);
			return db.insert(m_tableName, null, values);
		} finally {
			db.close();
		}
	}
	
	/**
	 * update
	 * @param key
	 * @param value
	 */
	private void updateBlob(String key, byte[] value) {
    	SQLiteDatabase db = getWritableDatabase();
    	try {
    		ContentValues val = new ContentValues();
    		val.put(c_COLUMN_VALUE, value);
    		int cnt = db.update(m_tableName, val, 
    				s_key_where, new String[]{key});
    		if( cnt != 1 ) {
    			throw new RuntimeException("can not write db");
    		}
    	} finally {
    		db.close();
    	}
	}
	private static final String s_key_where = c_COLUMN_KEY + "=?"; 
	
	
	/**
	 * set
	 * @param key
	 * @param value
	 */
	protected void setBlob(String key, byte[] value) {
		if( getBlob(key) != null ) {
			updateBlob(key, value);
		} else {
			insertBlob(key, value);
		}
	}

	/**
	 * get
	 * @param key
	 * @return
	 */
	protected byte[] getBlob(String key) {
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try {
			db = getReadableDatabase();
        	final String[] columns = { c_COLUMN_KEY };
        	final String[] selectionArgs = { key };

        	cursor = db.query(m_tableName, columns, s_key_where, 
        			selectionArgs, null, null, null);

			if (cursor.moveToFirst()) {
				return cursor.getBlob(0);
			}
			return null;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}			
		}
	}
	
	protected List<T> getBlobAll() throws ClassNotFoundException, IOException {
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try {
			db = getReadableDatabase();

        	cursor = db.query(m_tableName, null, null, 
        			null, null, null, null);

        	int valueIdx = cursor.getColumnIndexOrThrow(c_COLUMN_VALUE);
        	
        	ArrayList<T> list = new ArrayList<T>(cursor.getCount());
        	while( cursor.moveToNext() ){
				byte[] val = cursor.getBlob(valueIdx);
				T t = m_serializer.deserializeObject(val);
				list.add(t);
			}
			return list;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}			
		}
	}
	
}


/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Telephony;

import android.content.Context;
import android.telephony.TelephonyManager;

public class TmTelephonyUtils {

	/**
	 * 電話中か否か.
	 * @param ctx
	 * @return
	 */
	public static boolean isOffHook(Context ctx) {
		TelephonyManager tManager = (TelephonyManager)
				ctx.getSystemService(Context.TELEPHONY_SERVICE);
		switch (tManager.getCallState()) {
		case TelephonyManager.CALL_STATE_IDLE:
			// 待機状態
			break;
		case TelephonyManager.CALL_STATE_OFFHOOK:
			// 通話中
			return(true);
		case TelephonyManager.CALL_STATE_RINGING:
			// 着信中
			return(true);
		}
		return(false);
	}
}

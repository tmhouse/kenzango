/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Time;

import java.util.Calendar;

import jp.tmhouse.TmLibrary.Utils.Log.TmLog;

public class TmTimeUtil {

	/**
	 * checkTimeがstartHour以上endHour以下か.
	 * たとえばstartHour=23時、endHour=1時の時間範囲の中に、
	 * checkTimeが該当するか否かを返す.
	 * @param checkTime
	 * @param startHour
	 * @param endHour
	 * @return
	 */
	public static boolean isNowInRange(
			Calendar checkTime, int startHour, int endHour) {
		// 開始
		Calendar start = Calendar.getInstance();
		start.set(Calendar.HOUR_OF_DAY, startHour);
		// 時刻をきっかりにあわせる
		start.set(Calendar.MINUTE, 0);
		start.set(Calendar.SECOND, 0);
		start.set(Calendar.MILLISECOND, 0);

		// 終了
		Calendar end = (Calendar)start.clone();
		int dist = startHour <= endHour ?
				endHour - startHour : // ex) 22 to 23
				(24 - startHour) + endHour; // ex) 23 to 1
		// 開始と終了の間の時間をendに足し込む
		end.add(Calendar.HOUR_OF_DAY, dist);
		// 時刻をきっかりにあわせる
		// ※ (注意)終了時刻はきっかりを指しているので、checkTimeが同じhourを持っている
		//    ケースでは、checkTimeは範囲外となる。
		end.set(Calendar.MINUTE, 0);
		end.set(Calendar.SECOND, 0);
		end.set(Calendar.MILLISECOND, 0);
		
		//TmLog.d("now hour   = " + checkTime.toString());
		//TmLog.d("start hour = " + start.toString());
		//TmLog.d("end hour   = " + end.toString());
		if( checkTime.after(start) && checkTime.before(end) ) {
			//TmLog.i("Now is disable time");
			return(true);
		}
		return(false);
	}
}

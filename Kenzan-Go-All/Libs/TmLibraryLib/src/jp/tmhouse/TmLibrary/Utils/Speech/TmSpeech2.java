/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Speech;

import java.util.ArrayList;
import java.util.HashMap;

import jp.tmhouse.TmLibrary.Utils.HanKana2ZenKana;
import android.content.Context;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;

/**
 * しゃべる.
 * append(text)にて非同期に文字列をキューイングする。
 * 
 * [特徴]
 * どしどしキューイングしたとしても、textの再生開始と終了時にリスナが
 * 発火するため、再生中の文字列ごとに行いたい処理を記述できる。
 * 
 */
public class TmSpeech2 {

	private TextToSpeech			m_tts;
	private boolean				m_ttsIgnore = false;
	private float					m_rate = c_NORMAL_SPEECH_RATE;
	private HanKana2ZenKana			m_han2zen = new HanKana2ZenKana();
	private	 HashMap<String, String> m_param = new HashMap<String, String>();
	// しゃべるQue
	private	 ArrayList<SpeakElm>	m_speakQue = new ArrayList<SpeakElm>();
	private	 ArrayList<SpeakElm>	m_speakNowQue = new ArrayList<SpeakElm>();
	private int					m_speakQueIdx = 0;
   	private int					m_loopCnt;
	
	private _speechThread			m_thread;
	private Handler					m_handler = new Handler();
	
	public static final float c_FAST_SPEECH_RATE = 2.0f;
	public static final float c_NORMAL_SPEECH_RATE = 1.0f;
	
    /**
     * Speechスレッド.
     * @author mutoh
     *
     */
    private class _speechThread extends Thread {
    	private boolean m_stop = false;
    	private boolean m_pause = false;
    	
    	synchronized public boolean isPaused() {
    		return(m_pause);
    	}
    	synchronized public void pauseThread() {
    		m_pause = true;
    	}
    	synchronized public void continueThread() {
    		m_pause = false;
    		notifyThread();
    	}
    	synchronized public void stopThread() {
    		m_pause = false;
    		m_stop = true;
    		notifyThread();
    	}
    	synchronized public void notifyThread() {
   			Log.d("notifyThread", "notify!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
   			notify();
		}
    	
		@Override
		public void run() {
			boolean bStop = m_stop;
			for (;bStop == false;) {
				Log.e("run", "loop count=" + String.valueOf(m_loopCnt++));
				SpeakElm elm = getNextSpeakNowQue();
				if( elm == null ) {
					if( m_pause == false ) {
						elm = getSpeakQue(m_speakQueIdx++);
					} else {
						Log.d("Pause", "Pause Now...");
					}
				} else {
					Log.d("SpeakNow", elm.m_text);
				}
				
				if( elm != null && elm.m_text != null) {
					try {
					
						// 1.呼び出し元へコールバック(Handlerへpost)
						fireBeginOfUttenrance(elm);
						
						// 2.speak
						synchronized(this) {
							// しゃべるの開始
							// 半角カナの発音がおかしいので全カナに補正
							String text = m_han2zen.Han2Zen(elm.m_text);
							
							// しゃべり終了時に待っているindex番号を設定
							m_speechEndListener.setWaitingElm(elm);
							
							String clientVal = String.valueOf(elm.m_index);
							m_param.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, clientVal);
							if( TextToSpeech.SUCCESS == m_tts.speak(text, TextToSpeech.QUEUE_ADD, m_param)) {
								Log.d("speach success", "clientVal=" + clientVal + ", text=" + elm.m_text);
								//m_speaking = true;
							} else {
								Log.e("speach error", "clientVal=" + clientVal + ", text=" + elm.m_text);
							}
						}
						
						// 3.speckが完了したタイミングで呼び出し元へコールバック(Handlerへpost)
						// OnUtteranceCompletedListener で受ける。
							
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					Log.w("TmSpeech2.run", "do nothing..........");
				}
				
				synchronized(this) {
					try {
						wait(0);
					} catch (Exception e) {
						android.util.Log.d("TmSpeech2.run", "wait error. " + e);
					}
					bStop = m_stop;
				}
			}
		}
    }
    
    /*
     * 呼び出し元へコールバックするRunnable
     */
    private class Callback2Client implements Runnable {
    	public static final int CALLID_UNKNOWN = 0;
    	public static final int CALLID_BEGIN = 1;
    	public static final int CALLID_END = 2;
    	
    	private	 int		m_callId = CALLID_UNKNOWN;
    	private SpeakElm	m_speakElm;
    	
    	public Callback2Client(SpeakElm speakElm, int callId) {
    		m_speakElm = speakElm;
    		m_callId = callId;
    	}
    	
		@Override
		public void run() {
			if( m_speakElm == null || m_speakElm.m_listener == null ) {
				return;
			}
			switch( m_callId ) {
			case CALLID_BEGIN:
				m_speakElm.m_listener.onSpeakBegin(m_speakElm.m_index, m_speakElm.m_text);
				break;
			case CALLID_END:
				m_speakElm.m_listener.onSpeakEnd(m_speakElm.m_index, m_speakElm.m_text);
				break;
			} 
		}
    }
	
    /**
     * コンストラクタ
     */
	public TmSpeech2() {
        m_thread = new _speechThread();
	}
	
	/*
	 * 初期化
	 */
	public void initialize(Context ctx) {
		shutdown();
		
        m_tts = new TextToSpeech(ctx, m_ttsInitListener);
	}
	
	/**
	 * 終了
	 */
    public void shutdown() {
        // Don't forget to shutdown!
        if (m_tts != null) {
            m_tts.stop();
            m_tts.shutdown();
            m_tts = null;
            m_thread.stopThread();
        }
        clearSpeakQue();
    }
    
    /**
     * 呼び出し元が使用するリスナ.
     * @author mutoh
     *
     */
    public interface ISpeakListener {
    	/**
    	 * 再生が開始される.
    	 * @param index
    	 * @param tag
    	 * @param text
    	 */
    	public void onSpeakBegin(int index, String text);
    	
    	/**
    	 * 再生が終了した.
    	 * @param index
    	 * @param tag
    	 * @param text
    	 */
    	public void onSpeakEnd(int index, String text);
    }
    
    /**
     * テキストをqueingするデータフォルダ.
     * @author mutoh
     *
     */
    private class SpeakElm {
    	public ISpeakListener		m_listener;
    	public String				m_text;
    	public int					m_index;
    	public int					m_afterWait;
    	
    	/**
    	 * コンストラクタ.
    	 * @param text
    	 * @param listener
    	 */
    	public SpeakElm(int index, String text, int afterWait, ISpeakListener listener) {
    		m_index = index;
    		m_listener = listener;
    		m_text = text;
    		m_afterWait = afterWait > 0 ? afterWait : 0;
    	}
    }
    
    /**
     * TTSの初期化完了リスナ
     */
	private TextToSpeech.OnInitListener m_ttsInitListener = 
		new TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
		    	if (status == TextToSpeech.SUCCESS) {
		    		m_tts.setSpeechRate(m_rate);
		    		
		    		// onInit以降でないと効かない
		    		// 一回の再生が終了したときにコールバックしてもらう
		    		m_tts.setOnUtteranceCompletedListener(m_speechEndListener);
			
		    		// いろいろ頑張ったが、N2TTSとPicoとでの切り替えはできなかった
		    		//setTtsEngine(getLocale());
		        	//speechTextAppend(m_wellComeSpeech);
		    		
		    		// しゃべるスレッド開始
		    		m_thread.start();
		        } else {
		            Log.e("onInit", "Could not initialize TextToSpeech.");
		            shutdown();
		        }
			}
		};
	
	/**
	 * TTSが一回の再生が終了した時のリスナ
	 */
	private SpeechEndListener m_speechEndListener = new SpeechEndListener();
			
	private class SpeechEndListener implements TextToSpeech.OnUtteranceCompletedListener
	{
		private SpeakElm m_waitingElm;
		
		// 現在終了を待っているSpeakElm
		synchronized public void setWaitingElm(SpeakElm elm) {
			m_waitingElm = elm;
		}
		
		@Override
		synchronized public void onUtteranceCompleted(String utteranceId) {
			int index = -1;
			try { 
				index = Integer.valueOf(utteranceId);
			} catch (NumberFormatException e) {
				e.printStackTrace();
				Log.e("ERROR", "onUtteranceCompleted, integer parse error");
				return;
			}
			
			// 再生が中断されたとき、TTSはこのリスナを発火するかどうか分からない。
			// なので、再生開始時のindexと現在のm_speakQueIdxが異なる場合は、
			// 再生停止などの処理があったとして無視する。
			if( index != m_waitingElm.m_index ) {
				Log.w("WARN", "index mismatch: utteranceId=" + utteranceId + 
						", waitingIndex=" + m_waitingElm.m_index);
				return;
			}
			
			// 一回の再生が終わった。
			fireEndOfUttenrance(m_waitingElm);
				
			long waitTime = 0;
			waitTime = m_waitingElm.m_afterWait > 0 ? m_waitingElm.m_afterWait : 1;
				
			NotifyThreadRunnable runnalbe = new NotifyThreadRunnable();
			runnalbe.setText("wait=" + waitTime + ", text=" + m_waitingElm.m_text);
			
			// すぐにしゃべる内容があるときは直ちにnotifyする
			if( hasSpeakNow() ) {
				m_handler.postAtFrontOfQueue(runnalbe);
			} else {
				// 次の再生を開始する。
				m_handler.postDelayed(runnalbe, waitTime);
			}
		}
	}
		
	private class NotifyThreadRunnable implements Runnable {
		private String m_text = "なし";
		public void setText(String text) {
			m_text = text != null ? text : m_text;
		}
		@Override
		public void run() {
			// しゃべるのが終わった
			Log.d("_notifyThreadRunnable", "End of:" + m_text);
			m_thread.notifyThread();
		}
	};
		
	/**
	 * 一回の再生が終わった.
	 */
	synchronized private void fireEndOfUttenrance(SpeakElm elm) {
		if( elm != null ) {
			//Log.d("fireEndOfUttenrance", "end of id:" + elm.m_clientData);
			if( elm.m_listener != null ) {
				// 呼び出し元へコールバック(Handlerへpost)
				m_handler.post(new Callback2Client(elm, Callback2Client.CALLID_END));
			}
		} else {
			Log.d("fireEndOfUttenrance", "NO SpeackElm");
		}
	}
	
	/**
	 * 一回の再生を開始.
	 * @param elm
	 */
	synchronized private void fireBeginOfUttenrance(SpeakElm elm) {
		if( elm != null && elm.m_listener != null ) {
			// 呼び出し元へコールバック(Handlerへpost)
			m_handler.post(new Callback2Client(elm, Callback2Client.CALLID_BEGIN));
		}
	}

	public void setRate(float rate) {
		m_rate = rate;
		if( m_tts != null ) {
			m_tts.setSpeechRate(rate);
		}
	}
	
    
    /**
     * しゃべる文字列をキューイング.
     * @param text
     * @param mode
     * @return
     */
    synchronized public void append(String text, int afterWait, ISpeakListener listener) {
    	int beforeQueSize = m_speakQue.size();
   		_appendQue(m_speakQue, beforeQueSize, text, afterWait, listener);
   		
   		// queが空だったときだけnotifyする
   		if( beforeQueSize == 0 ) {
   			m_thread.notifyThread();
   		}
    }
    
    /**
     * 強制的にしゃべる.
     * @param text
     */
    synchronized public void speakNow(String text) {
    	Log.d("speakNow", "append text:" + text);
    	
    	// indexは-1
   		_appendQue(m_speakNowQue, -1, text, 100, null);
   		m_tts.stop();
   		
   		// 少し待ってから開始
   		m_handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				m_thread.notifyThread();
			}
   		}, 200);
    }
    
    synchronized public boolean hasSpeakNow() {
    	return(m_speakNowQue.size() > 0);
    }
    
    /**
     * キューに突っ込む内部関数.
     * @param list
     * @param text
     * @param afterWait
     * @param listener
     */
    private void _appendQue(ArrayList<SpeakElm> list, int index, 
    		String text, int afterWait, ISpeakListener listener)
    {
    	if( m_ttsIgnore ) {
    		return;
    	}
    	if( text == null || text.length() == 0 ) {
    		return;
    	}
    	
    	//int index = m_speakQue.size();
   		list.add(new SpeakElm(index, text, afterWait, listener));
    }
    
    /**
     * 一個のテキストをキューから取り出す.
     * @return
     */
    synchronized private SpeakElm getSpeakQue(int index) {
    	if( m_speakQue.size() == 0 ) {
    		Log.d("getSpeakQue", "no elements");
    		return(null);
    	}
    	if( m_speakQue.size() <= index ) {
    		Log.d("getSpeakQue", "END OF LIST");
    		return(null);
    	}
    	SpeakElm elm = m_speakQue.get(index);
    	return(elm);
    }
    
    /**
     * すぐしゃべるべきテキストをキューから取り出す.
     * @return
     */
    synchronized private SpeakElm getNextSpeakNowQue() {
    	if( m_speakNowQue.size() == 0 ) {
    		return(null);
    	}
    	SpeakElm elm = m_speakNowQue.remove(0);
    	return(elm);
    }
    
    /**
     * キューを全部消す.
     */
    synchronized private void clearSpeakQue() {
    	m_speakQue.clear();
    	m_speakQueIdx = 0;
    }
    
	/**
	 * 停止およびデータ破棄.
	 */
	synchronized public void cancelSpeak() {
		clearSpeakQue();
		//fireEndOfUttenrance(getCurSpeakElm());
		if( m_tts.isSpeaking() ) {
			m_tts.stop();
		}
	}
	
	/**
	 * 再生停止.
	 */
	synchronized public void pauseSpeak() {
		m_thread.pauseThread();
		if( m_tts.isSpeaking() ) {
			m_tts.stop();
		}
		
		//if( m_speakQueIdx > 0 ) {
		//	m_speakQueIdx--;
		//}
	}
	
	/**
	 * 再生継続.
	 */
	synchronized public void continueSpeak() {
		m_thread.continueThread();
	}
	
	/**
	 * 再生が停止中か.
	 */
	synchronized public boolean isPaused() {
		return(m_thread.isPaused());
	}
	
	/**
	 * 再生開始.
	 */
	//private void startSpeak() {
	//	m_thread.notifyThread();
	//}
	
	//private void stopSpeak() {
	//	m_tts.stop();
	//}
	
	/**
	 * 再生開始行の設定.
	 * @param lineIncDec
	 */
	synchronized public void setSpeakLineNo(int lineNo) {
		boolean paused = false;
		if( isPaused() == false ) {
			pauseSpeak();
			paused = true;
		}
		m_speakQueIdx = lineNo;
		
		int que_size = m_speakQue.size();
		if( lineNo > que_size ) {
			//m_speakQueIdx = que_size - 1;
			m_speakQueIdx = que_size;
		}
		if( lineNo < 0 ) {
			m_speakQueIdx = 0;
		}
		
		if( paused ) {
			continueSpeak();
		}
	}
	
	/**
	 * 現在の行数を返す.
	 * @return
	 */
	synchronized public int getCurrentLineNo() {
		if( m_speakQueIdx <= 0 ) {
			return(0);
		}
		return(m_speakQueIdx - 1);
	}
	
	/**
	 * 合計行数を返す.
	 * @return
	 */
	synchronized public int getTotalLineNo() {
		return(m_speakQue.size());
	}
}

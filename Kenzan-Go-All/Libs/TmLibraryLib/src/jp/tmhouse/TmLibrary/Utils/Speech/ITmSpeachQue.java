/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Speech;


public interface ITmSpeachQue {
	public void shutdown();
	public void setRate(float rate);
	public void stop(boolean bClear);
	public void append(int clientId, String text);
	public void appendArr(
    		int clientId, String[] textArr, 
    		boolean bStop, boolean bClearQue);
	public void setSpeakListener(ISpeakListener listener);
	public void clearQue();
	
    
	/**
	 * 初期化リスナー.
	 * @author mutoh
	 *
	 */
	public interface IOnInitListener {
		public void onSuccess();
		public void onError();
	}
	
	
    /**
     * 呼び出し元が使用するリスナ.
     * @author mutoh
     *
     */
    public interface ISpeakListener {
    	/**
    	 * 再生が開始される.
    	 * @param clientId
    	 */
    	public void onSpeakBegin(int clientId, String text);
    	
    	/**
    	 * 再生が終了した.
    	 * @param clientId
    	 */
    	public void onSpeakEnd(int clientId, String text);
    }
	
	/**
	 * 話す要素のデータフォルダ.
	 * @author mutoh
	 *
	 */
    public class SpeakElm {
    	public String	m_text;
    	public int		m_clientId;
    	
    	public SpeakElm(int clientId, String text) {
    		m_clientId = clientId;
    		m_text = text;
    	}
    }
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.Utils.Data;

/**
 * ６点式点字の一文字を表す。濁音符なども一文字とする。
 * @author mutoh
 *
 */
public class TmBraille {
	private static final String[] m_num2str = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
	private static final String c_NULL_STR = "";
	
	// リスト
	private TmBraille		m_prev = null;
	private TmBraille		m_next = null;
	
	private int	m_index; // リストの何番目か。0はまだリストに入っていない。
	
	//点字番号
	// 1   4
	// 2   5
	// 3   6
	
	// 下6ビットを使う。右から左に向かって点字番号１からはじめる。
	// ビットと点字番号並び：654321
	// 例: 000001 は1番が黒
	//     100000 は6番が黒
	private int m_brailleBit = 0;
	
	// セットした点のビットを立てる
	private int m_seted_pos = SETTED_NOTHING;
	
	private static final int SETTED_NOTHING = 0;
	
	// 位置
	private static final int SETTED_6 = 1 << 5;
	private static final int SETTED_5 = 1 << 4;
	private static final int SETTED_4 = 1 << 3;
	private static final int SETTED_3 = 1 << 2;
	private static final int SETTED_2 = 1 << 1;
	private static final int SETTED_1 = 1 << 0;
	
	public TmBraille() {
		clear();
	}
	
	public TmBraille(int left, int right) {
		setLeft(left);
		setRight(right);
	}
	
	// リスト番号。0からはじまる。
	public int getIndex() {
		return(m_index);
	}
	
	// リスト関係
	public void append(TmBraille append) {
		m_next = append;
		append.m_prev = this;
		append.m_index = m_index + 1;
	}
	public void remove() {
		if( m_next != null ) {
			m_next.m_prev = m_prev;
		}
		if( m_prev != null ) {
			m_prev.m_next = m_next;
		}
		m_prev = null;
		m_next = null;
	}
	
	public TmBraille getNext() {
		return(m_next);
	}
	public TmBraille getPrev() {
		return(m_prev);
	}
	
	/**
	 * クリア
	 */
	public void clear() {
		m_brailleBit = SETTED_NOTHING;
		m_seted_pos = SETTED_NOTHING;
		m_index = 0;
	}
	
	/**
	 * 左右の点をセット.
	 * @param left
	 * @param right
	 */
	public void set(int left, int right) {
		setLeft(left);
		setRight(right);
	}
	
	/**
	 * 一の位を右、十の位を左として左右の点をセット.
	 * @param val
	 */
	public void set(int val) {
		set(val / 10, val % 10);
	}
	
	/**
	 * 左(1,2,3番)をセット
	 * @param left
	 */
	public void setLeft(int left) {
		if( left < 0 || left > 7 ) {
			throw new RuntimeException();
		}
		
		//m_brailleBit = m_brailleBit & (0x7 << 3); // 左の3ビット以外はクリア
		// leftでORして1,2,3を上書き。
		m_brailleBit = m_brailleBit | left;
		
		// 1,2,3の点をセット
		m_seted_pos = m_seted_pos | (SETTED_1 | SETTED_2 | SETTED_3);
	}
	
	/**
	 * 右(4,5,6番)をセット
	 * @param right
	 */
	public void setRight(int right) {
		if( right < 0 || right > 7 ) {
			throw new RuntimeException();
		}
		
		//m_brailleBit = m_brailleBit & 0x07; // 右の3ビット以外はクリア
		// rightは3bit左だからshiftしてからORして4,5,6を上書き。
		m_brailleBit = m_brailleBit | (right << 3);
		
		// 4,5,6の点をセット
		m_seted_pos = m_seted_pos | (SETTED_4 | SETTED_5 | SETTED_6);
	}
	
	/**
	 * 上二個(1,4番)をセット
	 * @param left
	 * @param right
	 */
	public void setTop(boolean left, boolean right) {
		if( left ) {
			m_brailleBit = m_brailleBit | SETTED_1;
		}
		if( right ) {
			m_brailleBit = m_brailleBit | SETTED_4;
		}
		// 1,4の点をセット
		m_seted_pos = m_seted_pos | (SETTED_1 | SETTED_4);
	}
	public void setMiddle(boolean left, boolean right) {
		if( left ) {
			m_brailleBit = m_brailleBit | SETTED_2;
		}
		if( right ) {
			m_brailleBit = m_brailleBit | SETTED_5;
		}
		// 2,5の点をセット
		m_seted_pos = m_seted_pos | (SETTED_2 | SETTED_5);
	}
	public void setLower(boolean left, boolean right) {
		if( left ) {
			m_brailleBit = m_brailleBit | SETTED_3;
		}
		if( right ) {
			m_brailleBit = m_brailleBit | SETTED_6;
		}
		// 3,6の点をセット
		m_seted_pos = m_seted_pos | (SETTED_3 | SETTED_6);
	}
	
	/**
	 * 点字でいう[1,2,3,4,5,6]の点は黒か？
	 * @param pointNumber
	 * @return
	 */
	public boolean isPointBlack(int pointNumber) {
		if( pointNumber == 0 || pointNumber > 6 ) {
			throw new RuntimeException();
		}
		
		// セットされているか
		if( isBit(m_seted_pos, number2mask(pointNumber)) ) {
			// 黒か
			return(isBit(m_brailleBit, number2mask(pointNumber)));
		}
		return(false);
	}
	
	/**
	 * 点字番号からビットマスクを得る.
	 * @param number
	 * @return
	 */
	private int number2mask(int number) {
		return( 1 << (number - 1));
	}
	
	/**
	 * valの中のnumber番ビットは立っているか？
	 * @param number
	 * @param val
	 * @return
	 */
	private boolean isBit(int number, int mask) {
		return( mask == (number & mask) );
	}
	
	// is系
	public boolean isTopSetted() {
		return(isBit(m_seted_pos, SETTED_1 | SETTED_4));
	}
	public boolean isMiddleSetted() {
		return(isBit(m_seted_pos, SETTED_2 | SETTED_5));
	}
	public boolean isLowSetted() {
		return(isBit(m_seted_pos, SETTED_3 | SETTED_6));
	}
	
	public boolean isSpace() {
		return( isAllSetted() && m_brailleBit == 0 );
	}
	
	public boolean isCleared() {
		return( m_seted_pos == 0 );
	}
	
	public boolean isLeftSetted() {
		return(isBit(m_seted_pos, SETTED_1 | SETTED_2 | SETTED_3));
	}
	public boolean isRightSetted() {
		return(isBit(m_seted_pos, SETTED_4 | SETTED_5 | SETTED_6));
	}
	
	public boolean isPointSetted(int number) {
		if( 1 <= number && 6 >= number ) {
			return(isBit(m_seted_pos, number2mask(number)));
		}
		return(false);
	}
	
	/**
	 * 全ての点がセットされているか？
	 * @return
	 */
	public boolean isAllSetted() {
		return(isBit(m_seted_pos, SETTED_1 | SETTED_2 | SETTED_3 | SETTED_4 | SETTED_5 | SETTED_6));
	}
	
	// get系
	// 4,5,6の点の8/2式値
	public int getRight() {
		// 111000でマスク
		int val = m_brailleBit & (0x7 << 3);
		val = val >> 3; // 3bit移動
		return(val);
	}
	// 1,2,3の点の8/2式値
	public int getLeft() {
		// 000111でマスク
		int val = m_brailleBit & 0x7;
		return(val);
	}
	
	// 文字列化
	public String getRightString() {
		if( !isRightSetted() ) {
			return(c_NULL_STR);
		}
		return(m_num2str[getRight()]);
	}
	public String getLeftString() {
		if( !isLeftSetted() ) {
			return(c_NULL_STR);
		}
		return(m_num2str[getLeft()]);
	}
	
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils;

/**
 * いろいろ定数など.
 * @author mutoh
 *
 */
public class TmCommonValues {
	/**
	 * TmTalkback
	 * ex)
	 * 		Intent bi = new Intent();
	 *		bi.setAction(ACTION_SUSPEND_TOUCH_EXPLORE);
	 *		getBaseContext().sendBroadcast(bi)
	 */
	public static final String ACTION_SUSPEND_TOUCH_EXPLORE = 
			"jp.tmhouse.android.tmtalkback.receiver.ACTION_SUSPEND_TOUCH_EXPLORE";
	public static final String ACTION_RESUME_TOUCH_EXPLORE = 
			"jp.tmhouse.android.tmtalkback.receiver.ACTION_RESUME_TOUCH_EXPLORE";
	public static final String ACTION_STOP_SERVICE = 
			"jp.tmhouse.android.tmtalkback.receiver.ACTION_STOP_SERVICE";
	
	/**
	 * かんたんホーム
	 */
	// <intent-filter><category>にこれを入れておくと、「かんたんアプリ」として
	// 登録される.
	public static final String c_KANTAN_LAUNCHER_CATE_STR = 
    		"jp.tmhouse.intent.category.KANTAN_LAUNCHER";
	
	/**
	 * package name
	 */
	public static final String c_SMART_BRAILLE_IME_PKG_NAME = "jp.tmhouse.android.kenzango";
	public static final String c_JP_ANALYZER_PKG_NAME = "jp.tmhouse.jpanalyzer";

}

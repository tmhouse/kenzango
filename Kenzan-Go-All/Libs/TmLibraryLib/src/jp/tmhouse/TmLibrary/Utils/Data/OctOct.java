/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.Utils.Data;

/**
 * ８進数を２個持つデータフォルダ.
 * @author mutoh
 *
 */
public class OctOct extends TmBraille {
	private boolean m_centerPassed;// 中央をdownかmoveしたか.
	private boolean m_isLeftCompleted; // leftだけ確定しているときtrue.
	private boolean m_isRightCompleted; // 文字確定しているときtrue.
	private boolean m_errored = false;
	public OctOct() {
		clear();
	}
	
	public OctOct(int left, int right) {
		clear();
		setLeft(left);
		setRight(right);
	}
	
	public void clear() {
		super.clear();
		m_centerPassed = false;
		m_errored = false;
		m_isRightCompleted = false;
		m_isLeftCompleted = false;
	}
	public void setError() {
		m_errored = true;
	}
	public void setLeftComplete() {
		m_isLeftCompleted = true;
	}
	public void setRightComplete() {
		m_isRightCompleted = true;
	}
	public void centerPassed() {
		if( !isErrored() ) {
			m_centerPassed = true;
		}
	}
	
	public boolean isErrored() {
		return(m_errored);
	}
	public boolean isLeftCompleted() {
		return(m_isLeftCompleted);
	}
	public boolean isRightCompleted() {
		return(m_isRightCompleted);
	}
	
	public boolean isCenterPassed() {
		return(m_centerPassed);
	}
	
}

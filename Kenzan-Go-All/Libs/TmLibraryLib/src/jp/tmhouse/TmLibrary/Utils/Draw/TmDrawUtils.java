/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.Utils.Draw;

import android.graphics.Canvas;
import android.graphics.Paint;

public class TmDrawUtils {
	/**
	 * centerX, centerYを中心にテキストを描画.
	 * @param text
	 * @param centerX
	 * @param centerY
	 * @param c
	 * @param paint
	 */
	static public void drawTextCenterXY(
			String text, float centerX, float centerY, 
			Canvas c, Paint paint)
	{
		if( text != null ) {
			Paint.FontMetrics fm = paint.getFontMetrics();
		
			float textWidth = paint.measureText(text);
			// 中心にしたいX座標から文字列の幅の半分を引く
			float baseX = centerX - textWidth / 2;
			// 中心にしたいY座標からAscentとDescentの半分を引く
			float baseY = centerY - (fm.ascent + fm.descent) / 2.0f;

			// テキストの描画
			c.drawText( text, baseX, baseY, paint);
		}
	}
	
	/**
	 * カメラのpreviewデータをrgbに変換する.
	 * http://code.google.com/p/android/issues/detail?id=823
	 * 
	 * @param rgb
	 * @param yuv420sp
	 * @param width
	 * @param height
	 */
	static public void decodeYUV420SP(
			int[] rgb, byte[] yuv420sp, int width, int height, int topRows) {
    	final int frameSize = width * height;
    	
    	for (int j = 0, yp = 0; j < height; j++) {
    		// 上部からtopRowsピクセル処理したら戻る
    		if( j >= topRows ) {
    			return;
    		}
    		int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
    		for (int i = 0; i < width; i++, yp++) {
    			int y = (0xff & ((int) yuv420sp[yp])) - 16;
    			if (y < 0) y = 0;
    			if ((i & 1) == 0) {
    				v = (0xff & yuv420sp[uvp++]) - 128;
    				u = (0xff & yuv420sp[uvp++]) - 128;
    			}
    			
    			int y1192 = 1192 * y;
    			int r = (y1192 + 1634 * v);
    			int g = (y1192 - 833 * v - 400 * u);
    			int b = (y1192 + 2066 * u);
    			
    			if (r < 0) r = 0; else if (r > 262143) r = 262143;
    			if (g < 0) g = 0; else if (g > 262143) g = 262143;
    			if (b < 0) b = 0; else if (b > 262143) b = 262143;
    			
    			rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
    		}
    	}
    }
	
	public static void decodeYUV420SP2RGB(
			int[] rgb_bitmap, byte[] data, int width, int height)
	{
		int camera_width = 640;
		int camera_height = 480;

		int total = camera_width * camera_height;
		double ratiox = camera_width / (double) width;
		double ratioy = camera_height / (double) height;
		int rx = (int) (ratiox * (1 << 20));
		int ry = (int) (ratioy * (1 << 20));
		int cnt = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int camerax = x * rx >> 20;
				int cameray = y * ry >> 20;
				int addr = cameray * camera_width + camerax;
				int posx = (camerax >> 1) << 1;
				int posy = (cameray >> 1);
				int addr2 = posy * camera_width + posx;

				int yy = (data[addr]) & 0xff;
				// カメラからは-128 to 127で帰ってくる。まず、&0xffで 128 -- 255, 1 --- 127
				// にしてやり、
				// 一律に128を引くことで 0 -- 127 -127 -1という事にしてやる。
				int vv = ((data[total + addr2]) & 0xff) - 128;
				int uu = ((data[total + addr2 + 1]) & 0xff) - 128;
				// 高速化のため２０ビット固定小数点化
				// int R = (int)chop(yy + 1.403 * vv);
				// int G = (int)chop(yy - 0.344* uu - 0.714 * vv);
				// int B = (int)chop(yy + 1.770 * uu);
				int R = chop(yy + ((+1471152 * vv) >> 20));
				int G = chop(yy + ((-360710 * uu - 748683 * vv) >> 20));
				int B = chop(yy + ((+1855979 * uu) >> 20));
				rgb_bitmap[cnt] = 0xff000000 | ((R << 16) + (G << 8) + B);
				cnt++;
			}
		}
	}
    
    private static int chop(int  v) {
        return (v < 0 )? 0:(v>255)? 255:v;
    }
}

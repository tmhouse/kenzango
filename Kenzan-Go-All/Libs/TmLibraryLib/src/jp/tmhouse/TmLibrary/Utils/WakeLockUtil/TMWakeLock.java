/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.Utils.WakeLockUtil;

import android.content.Context;
import android.os.PowerManager;

public class TMWakeLock {

	private PowerManager.WakeLock m_neverSleepWakeLock;
	
    // スリープさせない
    public void neverSleepOn(Context ctx) {
		if( m_neverSleepWakeLock == null ) {
			PowerManager pm = (PowerManager)ctx.getSystemService(Context.POWER_SERVICE);
			m_neverSleepWakeLock = pm.newWakeLock(
					PowerManager.PARTIAL_WAKE_LOCK, this.getClass().getName());
	
			m_neverSleepWakeLock.acquire();
		}
    }
    // スリープさせないのをやめる
    public void neverSleepOff() {
		if( m_neverSleepWakeLock != null ) {
			m_neverSleepWakeLock.release();
			m_neverSleepWakeLock = null;
		}
    }
}

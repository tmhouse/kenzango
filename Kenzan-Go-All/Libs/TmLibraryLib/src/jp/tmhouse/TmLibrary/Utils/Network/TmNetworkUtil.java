/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Network;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import org.apache.http.conn.util.InetAddressUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

public class TmNetworkUtil {
	
	/**
	 * wifiのipを取得する.
	 * @param ctx
	 * @return
	 */
	public static String getWifiIpv4Address(Context ctx) {
		WifiManager wifiManager = (WifiManager)ctx.getSystemService(Activity.WIFI_SERVICE);
        WifiInfo w_info = wifiManager.getConnectionInfo();
        
        int ip = w_info.getIpAddress();
        return convertIntIp2String(ip);
	}
	
	private static String convertIntIp2String(int ip_addr_i) {
        String ip_addr = ((ip_addr_i >> 0) & 0xFF) + 
        		"." + ((ip_addr_i >> 8) & 0xFF) + 
        		"." + ((ip_addr_i >> 16) & 0xFF) + 
        		"." + ((ip_addr_i >> 24) & 0xFF);
        return(ip_addr);
	}
	
	@SuppressLint("NewApi")
	public static boolean isLocalIpAddress(String ip) {
		try {
			Enumeration<NetworkInterface> niEnum;
			for (niEnum = NetworkInterface.getNetworkInterfaces();
					niEnum.hasMoreElements();)
			{
				NetworkInterface ni = niEnum.nextElement();
				if( ni.isUp() == false || ni.isVirtual() ) {
					continue;
				}
				
				Enumeration<InetAddress> ipEnum;
				for (ipEnum = ni.getInetAddresses();
						ipEnum.hasMoreElements();)
				{
					InetAddress addr = (InetAddress)ipEnum.nextElement();
					if (!addr.isLoopbackAddress()
							&& InetAddressUtils.isIPv4Address(addr.getHostAddress())) {
						if( addr.getHostAddress().equals(ip) ) {
							return(true);
						}
					}
				}
			}
		} catch (SocketException ex) {
			Log.e("getLocalIpv4Address", ex.toString());
		}
		return(false);
	}

	/**
	 * ipを取得する.
	 * @return
	 */
	/*** 見当違いのipを返すことがあるので却下。
	// NetworkInterface.isUp()がAPI Level9
	@SuppressLint("NewApi")
	public static String getLocalIpv4Address() {
		try {
			Enumeration<NetworkInterface> networkInterfaceEnum;
			for (networkInterfaceEnum = NetworkInterface.getNetworkInterfaces();
					networkInterfaceEnum.hasMoreElements();)
			{
				NetworkInterface networkInterface = networkInterfaceEnum.nextElement();
				if( networkInterface.isUp() == false || 
						networkInterface.isVirtual() ) {
					continue;
				}
				
				
				Enumeration<InetAddress> ipAddressEnum;
				for (ipAddressEnum = networkInterface.getInetAddresses();
						ipAddressEnum.hasMoreElements();)
				{
					InetAddress inetAddress = (InetAddress) ipAddressEnum.nextElement();

					// ---check that it is not a loopback address and it is
					// ipv4---
					if (!inetAddress.isLoopbackAddress()
							&& InetAddressUtils.isIPv4Address(inetAddress.getHostAddress())) {
						return inetAddress.getHostAddress();
					}
				}
			}
		} catch (SocketException ex) {
			Log.e("getLocalIpv4Address", ex.toString());
		}
		return (null);
	}

	public static String getLlocalIpv6Address() {
		try {
			for (Enumeration<NetworkInterface> networkInterfaceEnum = NetworkInterface
					.getNetworkInterfaces(); networkInterfaceEnum
					.hasMoreElements();) {
				NetworkInterface networkInterface = networkInterfaceEnum
						.nextElement();
				for (Enumeration<InetAddress> ipAddressEnum = networkInterface
						.getInetAddresses(); ipAddressEnum.hasMoreElements();) {
					InetAddress inetAddress = (InetAddress) ipAddressEnum
							.nextElement();
					// ---check that it is not a loopback address and it is not
					// IPv4---
					if (!inetAddress.isLoopbackAddress()
							&& !InetAddressUtils.isIPv4Address(inetAddress
									.getHostAddress())) {
						return inetAddress.getHostAddress().toString();
					}
				}
			}
		} catch (SocketException ex) {
			Log.d("getLocalIpv6Address", ex.toString());
		}
		return null;
	}
	***/

}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import android.os.Handler;
import android.util.Log;

public class TmUdpMessenger {
	/**
	 * メッセージ受信リスナ.
	 * @author mutoh
	 *
	 */
	public interface IMessageReceiveListener {
		public void onMessageReceived(String msg);
	}
	
	//private static final String c_IP = "127.0.0.1";
	// 255.255.255.255はルータを超えない
	public static final String c_HOST_BROADCAST = "255.255.255.255";
	
	private RecvThread m_recvThred;
	private SendThread m_sendThred;
	private boolean	m_bIgnoreLocal = false;
	private int		m_port = 0;
	private String		m_host;
	
	public TmUdpMessenger(String host, int port, IMessageReceiveListener lstnr) {
		m_port = port;
		m_host = host;
		
		// port range check
		if( !(m_port >= 49152 && m_port <= 65535) ) {
			throw new RuntimeException("bad port number. use 49152 to 65535:" + m_port);
		}
		
		if( m_host == null ) {
			throw new RuntimeException("bad host.");
		}
		
		m_recvThred = new RecvThread(lstnr);
		m_sendThred = new SendThread();
	}
	
	/**
	 * ローカルアドレスからの受信を無視するか.
	 * @param bIgnoreLocal
	 */
	public void setIgnoreLocalAddress(boolean bIgnoreLocal) {
		m_bIgnoreLocal = bIgnoreLocal;
	}
	
	public void start() {
		m_sendThred.start();
		m_recvThred.start();
	}
	
	public void stop() {
		m_sendThred.stopThread();
		m_recvThred.stopThread();
	}
	
	/**
	 * メッセージを送る.
	 * @param msg
	 */
	public synchronized void sendMessage(String msg) {
		m_sendThred.sendMessage(msg);
	}
	
	/**
	 * 受信するThread.
	 * @author mutoh
	 *
	 */
	private class RecvThread extends Thread implements Runnable {
		private IMessageReceiveListener m_lstnr;
		private Handler		m_hanlder = new Handler();
		private DatagramSocket m_receiveSocket;
		
		public RecvThread(IMessageReceiveListener lstnr) {
			m_lstnr = lstnr;
		}
		@Override
		public void run() {
			getMessage(m_lstnr);
			Log.d("RecvThread", "run finished.");
		}
		
		public synchronized void stopThread() {
			m_receiveSocket.close();
			try {
				this.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		// 別スレッドで実行がほぼ必須
		private void getMessage(IMessageReceiveListener lstnr) {
			// 5100番ポートを監視するUDPソケットを生成
			try {
				m_receiveSocket = new DatagramSocket(m_port);
			} catch (SocketException e) {
				e.printStackTrace();
			}

			// 受け付けるデータバッファとUDPパケットを作成
			byte receiveBuffer[] = new byte[1024*100];
			DatagramPacket receivePacket = new DatagramPacket(receiveBuffer,
					receiveBuffer.length);

			while ( !m_receiveSocket.isClosed() ) {
				// UDPパケットを受信
				try {
					m_receiveSocket.receive(receivePacket);
				} catch (java.net.SocketException ee) {
					// closeしたときはここに来る
					Log.d("RecvThread", ee.getMessage());
					continue;
				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
				
				// 自分が送信したパケットではないか？
				if( m_bIgnoreLocal ) {
					String senderIp = receivePacket.getAddress().getHostAddress();
					Log.d("RecvThread", "sender ip=" + senderIp);
					if( TmNetworkUtil.isLocalIpAddress(senderIp) ) {
						Log.d("RecvThread", "received from same host. ignored recv.");
						continue;
					}
				}
				
				if( receivePacket.getLength() > 0 ) {
					String message = new String(receivePacket.getData(), 0,
						receivePacket.getLength());
					Log.d("TEST", "receive:" + message);
					
					// mainスレッドでリスナを呼ぶ
					m_hanlder.post(new FireListener(message));
				} else {
					Log.d("recv thread", "NO DATA RECEIVED");
				}
			}
		}
		
		/**
		 * リスナを発火する.
		 * @author mutoh
		 *
		 */
		private class FireListener implements Runnable {
			private String			m_msg;
			public FireListener(String msg) {
				m_msg = msg;
			}
			@Override
			public void run() {
				m_lstnr.onMessageReceived(m_msg);
			}
		};
	}
	
	/**
	 * 送信するThread.
	 * @author mutoh
	 *
	 */
	private class SendThread extends Thread implements Runnable {
		private String m_msg;
		private boolean	m_stop = false;
		
		private synchronized String getAndClearMessage() {
			String msg = m_msg;
			m_msg = null;
			return(msg);
		}
		public synchronized void sendMessage(String msg) {
			m_msg = msg;
			notify();
		}
		
		@Override
		public void run() {
			while ( ! getStop() ) {
				try {
					synchronized(this) {
						wait(0);
					}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
					return;
				}
				// UDPパケットを送信
				String msg = getAndClearMessage();
				if( msg != null ) {
					sendMessageInternal(msg);
					Log.d("TEST", "send:" + msg);
				}
			}
			Log.d("SendThread", "run finished.");
		}
		
		private synchronized void setStop() {
			m_stop = true;
		}
		private synchronized boolean getStop() {
			return(m_stop);
		}
		
		public void stopThread() {
			setStop();
			synchronized(this) {
				this.notify();
			}
			try {
				this.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		private void sendMessageInternal(String message) {
			InetSocketAddress remoteAddress = 
					new InetSocketAddress(m_host, m_port);

			// UDPパケットに含めるデータ
			byte[] sendBuffer = message.getBytes();

			// UDPパケット
			DatagramPacket sendPacket;

			try {
				// DatagramSocketインスタンスを生成して、UDPパケットを送信
				sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length,
						remoteAddress);
				new DatagramSocket().send(sendPacket);
			} catch (SocketException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
	
}

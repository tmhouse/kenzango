/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Debug;

import java.util.concurrent.TimeUnit;

import jp.tmhouse.TmLibrary.Utils.Task.cancelling.TmCancellingTask;
import android.os.Debug;

/**
 * WaitForDebuggerすると、mainスレッドでずっと待ってしまうので、非同期にして
 * 最大待ち時間を設定できるようにしたもの。
 * @author mutoh
 *
 */
public class TmAsyncWaitForDebugger {
	
	private TmAsyncWaitForDebugger() {}
	
	private static class MyTask implements TmCancellingTask.Cancelable {
		private Object s_sema = new Object();
		private Thread	m_thread;
		@Override
		public void run() {
			synchronized(s_sema) {
				m_thread = Thread.currentThread();
			}
			Debug.waitForDebugger();
			synchronized(s_sema) {
				m_thread = null;
			}
		}

		@Override
		public void cancel() {
			synchronized(s_sema) {
				if( m_thread != null ) {
					m_thread.interrupt();
					m_thread = null;
				}
			}
		}
	}
	
	public static TmCancellingTask start(long waitLimitMs) {
		if( Debug.isDebuggerConnected() ) {
			return(null);
		}

		TmCancellingTask currentTask = new TmCancellingTask(
				new MyTask(), waitLimitMs, TimeUnit.MILLISECONDS);
		currentTask.run();

		return(currentTask);
	}

}

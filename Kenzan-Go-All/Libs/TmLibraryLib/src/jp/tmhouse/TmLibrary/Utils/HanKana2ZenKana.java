/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils;

import java.util.HashMap;
/**
 * 半角全角変換 Ver1.1 - Javaソース
 * コマンドライン引数で入力された半角英数カナ文字を全角文字に変換する。
 *
 * @author Copyright(C) 2008-2009 東郷　功
 * @since 1.5
 * @version 1.5
 * @see 特定非営利活動法人　電脳世代
 */
public class HanKana2ZenKana {

	/**
    public static void main(String[] args) {
            if (args.length == 0) {
                // ヘルプ表示
                System.out.println("");
                System.out.println("<<使用方法>>");
                System.out.println("  java HankakuToZenkaku [文字列]");
                System.out.println("");
            } else if (args.length == 1) {
                System.out.println(Han2Zen(args[0]));
            }
    }
    **/
	
	private HashMap<String,String> m_hmCharTbl = new HashMap<String,String>();
	public HanKana2ZenKana() {
		/**
    	m_hmCharTbl.put("0", "０");
    	m_hmCharTbl.put("1", "１");
        m_hmCharTbl.put("2", "２");
        m_hmCharTbl.put("3", "３");
        m_hmCharTbl.put("4", "４");
        m_hmCharTbl.put("5", "５");
        m_hmCharTbl.put("6", "６");
        m_hmCharTbl.put("7", "７");
        m_hmCharTbl.put("8", "８");
        m_hmCharTbl.put("9", "９");
        m_hmCharTbl.put("a", "ａ");
        m_hmCharTbl.put("b", "ｂ");
        m_hmCharTbl.put("c", "ｃ");
        m_hmCharTbl.put("d", "ｄ");
        m_hmCharTbl.put("e", "ｅ");
        m_hmCharTbl.put("f", "ｆ");
        m_hmCharTbl.put("g", "ｇ");
        m_hmCharTbl.put("h", "ｈ");
        m_hmCharTbl.put("i", "ｉ");
        m_hmCharTbl.put("j", "ｊ");
        m_hmCharTbl.put("k", "ｋ");
        m_hmCharTbl.put("l", "ｌ");
        m_hmCharTbl.put("m", "ｍ");
        m_hmCharTbl.put("n", "ｎ");
        m_hmCharTbl.put("o", "ｏ");
        m_hmCharTbl.put("p", "ｐ");
        m_hmCharTbl.put("q", "ｑ");
        m_hmCharTbl.put("r", "ｒ");
        m_hmCharTbl.put("s", "ｓ");
        m_hmCharTbl.put("t", "ｔ");
        m_hmCharTbl.put("u", "ｕ");
        m_hmCharTbl.put("v", "ｖ");
        m_hmCharTbl.put("w", "ｗ");
        m_hmCharTbl.put("x", "ｘ");
        m_hmCharTbl.put("y", "ｙ");
        m_hmCharTbl.put("z", "ｚ");
        m_hmCharTbl.put("A", "Ａ");
        m_hmCharTbl.put("B", "Ｂ");
        m_hmCharTbl.put("C", "Ｃ");
        m_hmCharTbl.put("D", "Ｄ");
        m_hmCharTbl.put("E", "Ｅ");
        m_hmCharTbl.put("F", "Ｆ");
        m_hmCharTbl.put("G", "Ｇ");
        m_hmCharTbl.put("H", "Ｈ");
        m_hmCharTbl.put("I", "Ｉ");
        m_hmCharTbl.put("J", "Ｊ");
        m_hmCharTbl.put("K", "Ｋ");
        m_hmCharTbl.put("L", "Ｌ");
        m_hmCharTbl.put("M", "Ｍ");
        m_hmCharTbl.put("N", "Ｎ");
        m_hmCharTbl.put("O", "Ｏ");
        m_hmCharTbl.put("P", "Ｐ");
        m_hmCharTbl.put("Q", "Ｑ");
        m_hmCharTbl.put("R", "Ｒ");
        m_hmCharTbl.put("S", "Ｓ");
        m_hmCharTbl.put("T", "Ｔ");
        m_hmCharTbl.put("U", "Ｕ");
        m_hmCharTbl.put("V", "Ｖ");
        m_hmCharTbl.put("W", "Ｗ");
        m_hmCharTbl.put("X", "Ｘ");
        m_hmCharTbl.put("Y", "Ｙ");
        m_hmCharTbl.put("Z", "Ｚ");
        **/
        m_hmCharTbl.put("ｱ", "ア");
        m_hmCharTbl.put("ｲ", "イ");
        m_hmCharTbl.put("ｳ", "ウ");
        m_hmCharTbl.put("ｴ", "エ");
        m_hmCharTbl.put("ｵ", "オ");
        m_hmCharTbl.put("ｶ", "カ");
        m_hmCharTbl.put("ｷ", "キ");
        m_hmCharTbl.put("ｸ", "ク");
        m_hmCharTbl.put("ｹ", "ケ");
        m_hmCharTbl.put("ｺ", "コ");
        m_hmCharTbl.put("ｻ", "サ");
        m_hmCharTbl.put("ｼ", "シ");
        m_hmCharTbl.put("ｽ", "ス");
        m_hmCharTbl.put("ｾ", "セ");
        m_hmCharTbl.put("ｿ", "ソ");
        m_hmCharTbl.put("ﾀ", "タ");
        m_hmCharTbl.put("ﾁ", "チ");
        m_hmCharTbl.put("ﾂ", "ツ");
        m_hmCharTbl.put("ﾃ", "テ");
        m_hmCharTbl.put("ﾄ", "ト");
        m_hmCharTbl.put("ﾅ", "ナ");
        m_hmCharTbl.put("ﾆ", "ニ");
        m_hmCharTbl.put("ﾇ", "ヌ");
        m_hmCharTbl.put("ﾈ", "ネ");
        m_hmCharTbl.put("ﾉ", "ノ");
        m_hmCharTbl.put("ﾊ", "ハ");
        m_hmCharTbl.put("ﾋ", "ヒ");
        m_hmCharTbl.put("ﾌ", "フ");
        m_hmCharTbl.put("ﾍ", "ヘ");
        m_hmCharTbl.put("ﾎ", "ホ");
        m_hmCharTbl.put("ﾏ", "マ");
        m_hmCharTbl.put("ﾐ", "ミ");
        m_hmCharTbl.put("ﾑ", "ム");
        m_hmCharTbl.put("ﾒ", "メ");
        m_hmCharTbl.put("ﾓ", "モ");
        m_hmCharTbl.put("ﾔ", "ヤ");
        m_hmCharTbl.put("ﾕ", "ユ");
        m_hmCharTbl.put("ﾖ", "ヨ");
        m_hmCharTbl.put("ﾗ", "ラ");
        m_hmCharTbl.put("ﾘ", "リ");
        m_hmCharTbl.put("ﾙ", "ル");
        m_hmCharTbl.put("ﾚ", "レ");
        m_hmCharTbl.put("ﾛ", "ロ");
        m_hmCharTbl.put("ﾜ", "ワ");
        m_hmCharTbl.put("ｦ", "ヲ");
        m_hmCharTbl.put("ﾝ", "ン");
        m_hmCharTbl.put("ｧ", "ァ");
        m_hmCharTbl.put("ｨ", "ィ");
        m_hmCharTbl.put("ｩ", "ゥ");
        m_hmCharTbl.put("ｪ", "ェ");
        m_hmCharTbl.put("ｫ", "ォ");
        m_hmCharTbl.put("ｯ", "ッ");
        m_hmCharTbl.put("ｬ", "ャ");
        m_hmCharTbl.put("ｭ", "ュ");
        m_hmCharTbl.put("ｮ", "ョ");
        m_hmCharTbl.put("ｰ", "ー");
        m_hmCharTbl.put("｡", "。");
        m_hmCharTbl.put("､", "、");
        m_hmCharTbl.put("･", "・");
        m_hmCharTbl.put("｢", "「");
        m_hmCharTbl.put("｣", "」");
        m_hmCharTbl.put("ﾞ", "゛");
        m_hmCharTbl.put("ﾟ", "゜");
        m_hmCharTbl.put("ｳﾞ", "ヴ");
        m_hmCharTbl.put("ｶﾞ", "ガ");
        m_hmCharTbl.put("ｷﾞ", "ギ");
        m_hmCharTbl.put("ｸﾞ", "グ");
        m_hmCharTbl.put("ｹﾞ", "ゲ");
        m_hmCharTbl.put("ｺﾞ", "ゴ");
        m_hmCharTbl.put("ｻﾞ", "ザ");
        m_hmCharTbl.put("ｼﾞ", "ジ");
        m_hmCharTbl.put("ｽﾞ", "ズ");
        m_hmCharTbl.put("ｾﾞ", "ゼ");
        m_hmCharTbl.put("ｿﾞ", "ゾ");
        m_hmCharTbl.put("ﾀﾞ", "ダ");
        m_hmCharTbl.put("ﾁﾞ", "ヂ");
        m_hmCharTbl.put("ﾂﾞ", "ヅ");
        m_hmCharTbl.put("ﾃﾞ", "デ");
        m_hmCharTbl.put("ﾄﾞ", "ド");
        m_hmCharTbl.put("ﾊﾞ", "バ");
        m_hmCharTbl.put("ﾋﾞ", "ビ");
        m_hmCharTbl.put("ﾌﾞ", "ブ");
        m_hmCharTbl.put("ﾍﾞ", "ベ");
        m_hmCharTbl.put("ﾎﾞ", "ボ");
        m_hmCharTbl.put("ﾊﾟ", "パ");
        m_hmCharTbl.put("ﾋﾟ", "ピ");
        m_hmCharTbl.put("ﾌﾟ", "プ");
        m_hmCharTbl.put("ﾍﾟ", "ペ");
        m_hmCharTbl.put("ﾎﾟ", "ポ");

	}

    public String Han2Zen(String s) {
        StringBuffer sbBuf = new StringBuffer();

        String  strKey;  // HashMapのKEY
        String  strChar1 = "";          // １文字目
        char    c1 = ' ';               // １文字目
        char    c2 = ' ';               // ２文字目
        int     i;
        for (i=0; i<s.length(); i++) {

            c1 = ' ';
            c2 = ' ';
            c1 = s.charAt(i);
            if ((i+1) < s.length()) {
                c2 = s.charAt(i+1);
            }

            strChar1 = "";
            if (c2 == 'ﾞ' || c2 == 'ﾟ') {
                strKey = String.valueOf(c1) + String.valueOf(c2);
                strChar1 = (String)m_hmCharTbl.get(strKey);
                if (strChar1 == null) {
                    strKey = String.valueOf(c1);
                    strChar1 = (String)m_hmCharTbl.get(strKey);
                    if (strChar1 == null) {
                        sbBuf.append(c1);
                    } else {
                        sbBuf.append(strChar1);
                    }
                } else {
                    sbBuf.append(strChar1);
                    i++;
                }
            } else {
                strKey = String.valueOf(c1);
                strChar1 = (String)m_hmCharTbl.get(strKey);
                if (strChar1 == null) {
                    sbBuf.append(c1);
                } else {
                    sbBuf.append(strChar1);
                }
            }
        }
        return sbBuf.toString();
    }
}

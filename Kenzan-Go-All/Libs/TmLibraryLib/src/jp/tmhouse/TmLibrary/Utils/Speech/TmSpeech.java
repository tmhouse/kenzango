/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Speech;

import jp.tmhouse.TmLibrary.Utils.HanKana2ZenKana;
import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;

/**
 * しゃべる.
 * @param num
 */
public class TmSpeech {

	private TextToSpeech			m_tts;
	private boolean				m_ttsStatus = false;
	private boolean				m_ttsIgnore = false;
	private String					m_wellComeSpeech;
	private float					m_rate = c_NORMAL_SPEECH_RATE;
	private HanKana2ZenKana			m_han2zen = new HanKana2ZenKana();
	
	public static final float c_FAST_SPEECH_RATE = 2.0f;
	public static final float c_NORMAL_SPEECH_RATE = 1.0f;
	
    private static final String	c_CR = "\n";
	
	public TmSpeech() {
	}
	
	public void initialize(Context ctx) {
		shutdown();
        m_tts = new TextToSpeech(ctx, m_ttsInitListener);
	}
	
    public void shutdown() {
        // Don't forget to shutdown!
        if (m_tts != null) {
            m_tts.stop();
            m_tts.shutdown();
            m_tts = null;
        }
    }
    
	private TextToSpeech.OnInitListener m_ttsInitListener = 
		new TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
		    	if (status == TextToSpeech.SUCCESS) {
		    		m_ttsStatus = true;
		    		
		    		m_tts.setSpeechRate(m_rate);
			
		    		// いろいろ頑張ったが、N2TTSとPicoとでの切り替えはできなかった
		    		//setTtsEngine(getLocale());
		        	speechTextAppend(m_wellComeSpeech);
		        } else {
		            Log.e("onInit", "Could not initialize TextToSpeech.");
		            shutdown();
		        }
			}
		};

	public void setRate(float rate) {
		m_rate = rate;
		if( m_tts != null ) {
			m_tts.setSpeechRate(rate);
		}
	}
	
    public void speechText(String text) {
    	if( text == null ) {
    		return;
    	}
    	speechSub(text, TextToSpeech.QUEUE_FLUSH);
    }
    public void speechTextAppend(String text) {
    	speechSub(text, TextToSpeech.QUEUE_ADD);
    }
    
    private void speechSub(String text, int mode) {
    	if( m_ttsIgnore ) {
    		return;
    	}
    	if( text == null || text.length() == 0 ) {
    		return;
    	}
    	
    	// 半角カナの発音がおかしいので全カナに補正
    	text = m_han2zen.Han2Zen(text);
    	if( m_tts != null && m_ttsStatus ) {
    		if( mode == TextToSpeech.QUEUE_FLUSH ) {
    			if( m_tts.isSpeaking() ) {
    				m_tts.stop();
    			}
    		}
    		
    		String[] arr = text.split(c_CR);
    		for( String elm : arr ) {
    			m_tts.speak(elm, mode, null);
    		}
    	} else {
    		setWellcomeSpeech(text);
    	}
    }
    
	/**
	 * TextToSpeechが利用可能になったときにしゃべる.
	 * @param speechText
	 */
	private void setWellcomeSpeech(String speechText) {
		if( m_wellComeSpeech != null && speechText != null ) {
			m_wellComeSpeech = m_wellComeSpeech + c_CR + speechText;
		} else {
			m_wellComeSpeech = speechText;
		}
	}

	/**
	 * 再生停止.
	 */
	public void stop() {
		m_tts.stop();
	}
}

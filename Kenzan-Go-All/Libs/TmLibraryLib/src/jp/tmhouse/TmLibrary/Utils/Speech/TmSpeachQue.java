/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Speech;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;

import jp.tmhouse.TmLibrary.Utils.HanKana2ZenKana;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;

/**
 * しゃべる文字列をqueしてしゃべる.
 * @author mutoh
 *
 */
public class TmSpeachQue implements ITmSpeachQue {
	private Handler		m_handler = new Handler();
	private TextToSpeech			m_tts;
	private float					m_rate = c_NORMAL_SPEECH_RATE;
	private HanKana2ZenKana			m_han2zen = new HanKana2ZenKana();
	private	 LinkedList<SpeakElm>	m_speakQue = new LinkedList<SpeakElm>();
	
	private _speechThread			m_thread;
	private ISpeakListener			m_speakListener;
	private IOnInitListener			m_onInitListener;
	private String					m_targetIso3Lang;
	
	public static final float c_FAST_SPEECH_RATE = 2.0f;
	public static final float c_NORMAL_SPEECH_RATE = 1.0f;


    /**
     * Speechスレッド.
     * @author mutoh
     *
     */
    private class _speechThread extends Thread {

    	public void speechNext() {
       		synchronized(TmSpeachQue.this) {
       			TmSpeachQue.this.notify();
       		}
    	}
    	
    	public void stopThread() {
    		this.interrupt();
    	}
    	
		@SuppressLint("NewApi")
		@Override
		public void run() {
			boolean bStop = false;
			for (; bStop == false;) {
				SpeakElm elm = getSpeakQue();
				if (elm != null && elm.m_text != null) {
					try {
						// 半角カナの発音がおかしいので全カナに補正
						String text = m_han2zen.Han2Zen(elm.m_text);
						text = text.replace("・", "　点　");

						// 文章が長いと再生開始までに時間がかかるので、「。」で分割。
						String[] textArr = text.split("。");
						for (String str : textArr) {
							//TmLog.d("run", "text[" + i + "] = " + str);
							HashMap<String, String> param = new HashMap<String, String>(1);
							
							// 超無理やり...
							String val = String.valueOf(elm.m_clientId) + ":" + elm.m_text;
							param.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, val);
							m_tts.speak(str, TextToSpeech.QUEUE_ADD, param);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					//TmLog.w("TmSpeech2.run", "do nothing..........");
				}

				synchronized(TmSpeachQue.this) {
					try {
						//TmLog.i("TmSpeech2.run", "wait begin. ");
						TmSpeachQue.this.wait(0);
						//TmLog.i("TmSpeech2.run", "wait end. ");
					} catch (InterruptedException e) {
						// normal end. maybe cause by interrapt().
						return;
					}
				}
			}
		}
    }
	
    /**
     * コンストラクタ
     */
	public TmSpeachQue(Context ctx, Locale loc, IOnInitListener onInitListener) {
		if( loc == null ) {
			loc = Locale.getDefault();
		}
		m_targetIso3Lang = loc.getISO3Language();
		m_onInitListener = onInitListener;
        m_thread = new _speechThread();
        m_tts = new TextToSpeech(ctx, m_ttsInitListener);
	}
	
	/**
	 * 終了
	 */
    public void shutdown() {
        // Don't forget to shutdown!
        if (m_tts != null) {
            m_tts.stop();
            m_tts.shutdown();
            m_tts = null;
            m_thread.stopThread();
            //m_thread.stop();
        }
        clearQue();
    }

    
    /**
     * TTSの初期化完了リスナ
     */
	private TextToSpeech.OnInitListener m_ttsInitListener = 
		new TextToSpeech.OnInitListener() {
			@SuppressLint("NewApi")
			@Override
			public void onInit(int status) {
		    	if (status == TextToSpeech.SUCCESS) {
		    		if( m_tts == null ) {
		    			return;
		    		}
		    		
		    		Locale loc = m_tts.getLanguage();
		    		String iso3lang = loc.getISO3Language();
		    		TmLog.i("TTS lang:" + iso3lang);
		    		if( iso3lang.equals(m_targetIso3Lang) == false ) {
		    			// 指定の言語のTTSがインストールされていない
		    			if( m_onInitListener != null ) {
		    				m_onInitListener.onError();
		    				return;
		    			}
		    		}
		    		
		    		m_tts.setSpeechRate(m_rate);
		    		
		    		// onInit以降でないと効かない
		    		// 一回の再生が終了したときにコールバックしてもらう
		    		m_tts.setOnUtteranceProgressListener(m_speechEndListener);
			
		    		// しゃべるスレッド開始
		    		m_thread.start();
		    		
	    			if( m_onInitListener != null ) {
	    				m_onInitListener.onSuccess();
	    			}
		        } else {
		            TmLog.e("onInit", "Could not initialize TextToSpeech.");
		            shutdown();
	    			if( m_onInitListener != null ) {
	    				m_onInitListener.onError();
	    				return;
	    			}
		        }
			}
		};
	
	/**
	 * TTSが一回の再生が終了した時のリスナ
	 */
		
	private SpeechEndListener m_speechEndListener = new SpeechEndListener();
			
	@SuppressLint("NewApi")
	private class SpeechEndListener extends UtteranceProgressListener
	{
		@Override
		public void onStart(final String utteranceId) {
			TmLog.d("speech start:" + utteranceId);
			if( m_speakListener != null ) {
				m_handler.post(new Runnable() {
					@Override
					public void run() {
						String[] arr = utteranceId.split(":", 2);
						m_speakListener.onSpeakBegin(Integer.valueOf(arr[0]), arr[1]);
					}
				});
			}
		}

		@Override
		public void onDone(final String utteranceId) {
			TmLog.d("speech done:" + utteranceId);
			if( m_speakListener != null ) {
				m_handler.post(new Runnable() {
					@Override
					public void run() {
						String[] arr = utteranceId.split(":", 2);
						m_speakListener.onSpeakEnd(Integer.valueOf(arr[0]), arr[1]);
						m_thread.speechNext();
					}
				});
			}
		}

		@Override
		public void onError(String utteranceId) {
			TmLog.d("speech error:" + utteranceId);
			m_thread.speechNext();
		}
	}
	
	public void setRate(float rate) {
		m_rate = rate;
		if( m_tts != null ) {
			m_tts.setSpeechRate(rate);
		}
	}
	
	synchronized public void stop(boolean bClear) {
		if( bClear ) {
			clearQue();
		}
		m_tts.stop();
	}
    
    /**
     * しゃべる文字列をキューイング.
     * @param text
     * @param mode
     * @return
     */
    synchronized public void append(int clientId, String text) {
		TmLog.d("append:" + text);
   		m_speakQue.add(new SpeakElm(clientId, text));
    }

    
    /**
     * 配列でしゃべる文字列をキューイング.
     * @param textArr
     * @param listener
     * @param bStop
     * @param bClearQue
     */
    synchronized public void appendArr(
    		int clientId, String[] textArr, 
    		boolean bStop, boolean bClearQue)
    {
    	if( textArr == null ) {
    		return;
    	}
    	if( m_tts != null && bStop ) {
    		m_tts.stop();
    	}
    	if( bClearQue ) {
    		clearQue();
    	}
    	for( String text : textArr ) {
    		append(clientId, text);
    	}
    	appendEnd();
    }
    
    synchronized public void setSpeakListener(ISpeakListener listener) {
    	m_speakListener = listener;
    }

    /**
     * 
     */
    synchronized private void appendEnd() {
   		if( m_tts.isSpeaking() == false ) {
   			TmLog.d("appendしたときにttsが何もしていないならnotifyする");
   			m_thread.speechNext();
   		}
    }
    
    /**
     * 一個のテキストをキューから取り出す.
     * @return
     */
    synchronized private SpeakElm getSpeakQue() {
    	return(m_speakQue.pollFirst());
    }
    
	synchronized public void clearQue() {
    	m_speakQue.clear();
	}
	
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.Utils.Compat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class ServiceHelper extends Service {
    private static final Class<?>[] mSetForegroundSignature = new Class[] { boolean.class };
    private static final Class<?>[] mStartForegroundSignature = new Class[] { int.class, Notification.class };
    private static final Class<?>[] mStopForegroundSignature = new Class[] { boolean.class };

    private NotificationManager mNM;
    private Method mSetForeground;
    private Method mStartForeground;
    private Method mStopForeground;
    private Object[] mSetForegroundArgs = new Object[1];
    private Object[] mStartForegroundArgs = new Object[2];
    private Object[] mStopForegroundArgs = new Object[1];

    public ServiceHelper() {
    }

    /*
     * appId : R.string.app_name
     */
    /***
    public void showNotification(int appNameId, int iconid, Class<?> pendingClassObj) {
    	init();
    	Notification notification = new Notification(iconid, "hoge", System.currentTimeMillis());
    	PendingIntent contentIntent = PendingIntent.getActivity(this, 0, 
    			new Intent(this, pendingClassObj), Intent.FLAG_ACTIVITY_NEW_TASK);
    	notification.setLatestEventInfo(this, "hoge", "hogehoge", contentIntent);
    	startForegroundCompat(appNameId, notification);
    }

    public void clearNotification(int appNameId) {
    	stopForegroundCompat(appNameId);
    }
    ***/
    
    @Override
    public void onStart(Intent intent,int startID) {
    	super.onStart(intent, startID);
    	init();
    }
    

	@Override
	public IBinder onBind(Intent intent) {
    	init();
		return null;
	}

    public void init() {
    	if( mNM != null ) {
    		return;
    	}
        mNM = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        try {
        	// for API level 5
            mStartForeground = getClass().getMethod("startForeground", mStartForegroundSignature);
            mStopForeground = getClass().getMethod("stopForeground", mStopForegroundSignature);
        } catch (NoSuchMethodException e) {
            // Running on an older platform.
            mStartForeground = mStopForeground = null;
            //return;
        }
        try {
            mSetForeground = getClass().getMethod("setForeground", mSetForegroundSignature);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException("OS doesn't have Service.startForeground OR Service.setForeground!");
        }
    }

    private void invokeMethod(Method method, Object[] args) {
        try {
        	method.invoke(this, args);
        } catch (InvocationTargetException e) {
            Log.w("ApiDemos", "Unable to invoke method", e);
        } catch (IllegalAccessException e) {
            Log.w("ApiDemos", "Unable to invoke method", e);
        } catch (IllegalArgumentException e) {
            Log.w("ApiDemos", "Unable to invoke method", e);
        }
    }

    /**
     * This is a wrapper around the new startForeground method, using the older
     * APIs if it is not available.
     */
    public void startForegroundCompat(int id, Notification notification) {
        // If we have the new startForeground API, then use it.
        if (mStartForeground != null) {
        	// for API level 5
            mStartForegroundArgs[0] = Integer.valueOf(id);
            mStartForegroundArgs[1] = notification;
            invokeMethod(mStartForeground, mStartForegroundArgs);
            return;
        }

        if( mSetForeground != null ) {
        	// Fall back on the old API.
        	mSetForegroundArgs[0] = Boolean.TRUE;
        	invokeMethod(mSetForeground, mSetForegroundArgs);
        	mNM.notify(id, notification);
        }
    }

    /**
     * This is a wrapper around the new stopForeground method, using the older
     * APIs if it is not available.
     */
    public void stopForegroundCompat(int id) {
        // If we have the new stopForeground API, then use it.
        if (mStopForeground != null) {
            mStopForegroundArgs[0] = Boolean.TRUE;
            try {
                mStopForeground.invoke(this, mStopForegroundArgs);
            } catch (InvocationTargetException e) {
                // Should not happen.
                Log.w("ApiDemos", "Unable to invoke stopForeground", e);
            } catch (IllegalAccessException e) {
                // Should not happen.
                Log.w("ApiDemos", "Unable to invoke stopForeground", e);
            }
            return;
        }

        if( mSetForeground != null ) {
        	// Fall back on the old API. Note to cancel BEFORE changing the
        	// foreground state, since we could be killed at that point.
        	mNM.cancel(id);
        	mSetForegroundArgs[0] = Boolean.FALSE;
        	invokeMethod(mSetForeground, mSetForegroundArgs);
        }
    }

}

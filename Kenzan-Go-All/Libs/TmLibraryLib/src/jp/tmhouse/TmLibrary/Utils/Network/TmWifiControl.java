/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.Network;

import java.lang.reflect.Method;

import jp.tmhouse.TmLibrary.Utils.Log.TmLog;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.net.TrafficStats;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.provider.Settings.SettingNotFoundException;

public class TmWifiControl {
	private static Handler	m_handler = new Handler();
	
	public static void enable(final Context context, final boolean bEnable) {
		
		m_handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				Context ctx = context.getApplicationContext();
				WifiManager wifiMgr = 
						(WifiManager)ctx.getSystemService(Context.WIFI_SERVICE);
				//if( wifiMgr != null && wifiMgr.isWifiEnabled() != bEnable ) {
					TmLog.d("wifi status set to be = " + bEnable);
					wifiMgr.setWifiEnabled(bEnable);
				//if ( bEnable && wifiMgr.startScan() == false ) {
				//	Log.w(TAG, "startScan failed");
				//}
				//}	
			}
		}, 1000);

	}
	
	public static boolean isWifiEnabled(Context ctx) {
		WifiManager wifiMgr = getWifiManager(ctx);
		return(wifiMgr.isWifiEnabled());
	}
	
	public static boolean isWifiApEnabled(Context ctx) {
		Boolean isWifiAPenabled = false;
		try {
			WifiManager wifiMgr = getWifiManager(ctx);
			Method method = wifiMgr.getClass().getMethod("isWifiApEnabled");
			if( method != null ) {
				isWifiAPenabled = (Boolean)method.invoke(wifiMgr);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return(isWifiAPenabled);
	}
	
	private static WifiManager getWifiManager(Context ctx) {
		ctx = ctx.getApplicationContext();
		WifiManager wifiMgr = 
				(WifiManager)ctx.getSystemService(Context.WIFI_SERVICE);
		return(wifiMgr);
	}
	
	
	/**
	 * wifi通信量を返す.
	 * @author mutoh
	 *
	 */
	@SuppressLint("NewApi")
	public static long getTotalWifiBytes() {
		long wifi_rx = TrafficStats.getTotalRxBytes() - TrafficStats.getMobileRxBytes();
		long wifi_tx = TrafficStats.getTotalTxBytes() - TrafficStats.getMobileTxBytes();
			
		if( TrafficStats.UNSUPPORTED == (int)wifi_rx || 
				TrafficStats.UNSUPPORTED == (int)wifi_tx ) {
			// サポートされていない
			TmLog.e("TrafficStats not worked.");
			return(TrafficStats.UNSUPPORTED);
		} else {
			//Log.d(TAG, "traffic rx=" + rx + ", tx=" + tx);
		}
		return(wifi_rx + wifi_tx);
	}
	
	/** TrafficStatsが2.3系以降サポート
	public static long getTotalBytes() {
		long rx = TrafficStats.getTotalRxBytes();
		long tx = TrafficStats.getTotalTxBytes();
		
		if( TrafficStats.UNSUPPORTED == (int)rx || 
				TrafficStats.UNSUPPORTED == (int)tx ) {
			// サポートされていない
			Log.e(TAG, "TrafficStats not worked.");
			return(0);
		} else {
			//Log.d(TAG, "traffic rx=" + rx + ", tx=" + tx);
		}
		return(rx + tx);
	}
	**/
	

	/**
	 * wifiスリープポリシーを変更
	 * <uses-permission android:name="android.permission.WRITE_SETTINGS"/>
	 * @param ctx
	 */
	public static void setWifiSleepPolicyNever(Context ctx) {
		ContentResolver cr = ctx.getContentResolver();
		String sleepPolcyName = 
				android.provider.Settings.System.WIFI_SLEEP_POLICY;
		int sleepPolcyNever = 
				android.provider.Settings.System.WIFI_SLEEP_POLICY_NEVER;

		try {
			int sleepPolcy = android.provider.Settings.System.getInt(
					cr, sleepPolcyName);
			if( sleepPolcy != sleepPolcyNever ) {
				android.provider.Settings.System.putInt(
					cr, sleepPolcyName, sleepPolcyNever);
			}
		} catch (SettingNotFoundException e) {
			e.printStackTrace();
		}
	}
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.Vibrate;

import java.util.ArrayList;

import android.os.Vibrator;
import android.util.Log;

public class TmAsyncVibrator {
	private Vibrator m_vibratorService;
	private Thread m_thread;
	private boolean m_ignoreFlag = false;
	private RunRunable		m_run = new RunRunable();
	
	private TmAsyncVibrator() {};
	public TmAsyncVibrator(Vibrator vb) {
		m_vibratorService = vb;
	}
	
	public void clear() {
		m_run.clear();
	}
	public boolean hasData() {
		return(m_run.hasData());
	}

	/***
	private Runnable m_run = new Runnable() {
		@Override
		public void run() {
			myrun();
		}
	};
	***/
	
	private class RunRunable implements Runnable {
		private boolean m_shutdownRequested = false;
		// ブルっとするデータのキュー
		private ArrayList<VibrateData> m_dataList = new ArrayList<VibrateData>(10);
		private Object m_waitLock = new Object();
		
		// ブルっとするデータのキューの要素
		private class VibrateData {
			public VibrateData(long time, int count, long afterWait) {
				m_vibTime = time;
				m_vibCount = count;
				if( afterWait > 0 ) {
					m_afterWait = afterWait;
				}
			}
			private long	m_vibTime = 0;
			private int	m_vibCount = 0;
			private long	m_afterWait = 300; // default 200msec
		}
		
		public synchronized void addData(long time, int count, long wait) {
			//Log.d("addData", "time=" + time + ", count=" + count + ", wait=" + wait);
			m_dataList.add(new VibrateData(time, count, wait));
		}
		
		public synchronized void clear() {
			m_dataList.clear();
			notifyWaitLock();
		}
		
		public synchronized void stop() {
			clear();
			m_shutdownRequested = true;
			this.notify();
		}
		
		private void notifyWaitLock() {
			synchronized(m_waitLock) {
				m_waitLock.notify();
			}
		}
		
		public synchronized boolean hasData() {
			return(m_dataList.size() > 0);
		}
		
		@Override
		public void run() {
			myrun();
		}
		
		private void myrun() {
			while(m_shutdownRequested == false) {
				VibrateData vd = null;
				synchronized(this) {
					try {
						// 要素が入ってないときだけwaitする
						if( m_dataList.size() == 0 ) {
							this.wait();
						}
						if( m_dataList.size() == 0 ) {
							continue;
						}
						vd = m_dataList.get(0);
						m_dataList.remove(vd);
					} catch(InterruptedException e) {
						//e.printStackTrace();
						return;
					}
				}
				if( vd != null ) {
					for( int i = 0; i < vd.m_vibCount; i++ ) {
						if( vd.m_vibTime > 0 ) {
							m_vibratorService.vibrate(vd.m_vibTime);
						}
						// 振動後、少し待つ
						synchronized(m_waitLock) {
							try {
								// 待ち時間＝振動時間＋指定待ち時間
								long waitTime = vd.m_vibTime + vd.m_afterWait;
								m_waitLock.wait(waitTime);
							} catch(InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
			Log.d("TmAsyncVibrator", "Thread terminate");
		}
				
	}
	

	public void start() {
		m_thread = new Thread(m_run);
		m_thread.start();
	};
	
	public void end() {
		if( m_thread != null && m_thread.isAlive() ) {
			m_run.stop();
			m_thread.interrupt();
		}
		m_thread = null;
	}
	
	public void asyncVibrate(long msec) {
		addVibrate(msec, 0);
		notifyVibrate();
	}
	
	/**
	public void asyncVibrateTwice(long msec) {
		addVibrate(msec, 0);
		addVibrate(msec, 0);
		notifyVibrate();
	}
	**/
	
	public void addVibrate(long msec, long wait) {
		if( getIgnore() ) {
			return;
		}
		synchronized(m_run) {
			m_run.addData(msec, 1, wait);
		}
	}
	public void notifyVibrate() {
		if( getIgnore() ) {
			return;
		}
		synchronized(m_run) {
			m_run.notify();
		}
	}
	
	/**
	 * 
	 * @param msec		振動時間
	 * @param count		繰り返し回数
	 * @param interval	振動間隔
	 */
	/***
	private void asyncVibrate2(long msec, int count) {
		//Log.d("vibrate", "vibrate msec=" + msec);
		if( getIgnore() ) {
			return;
		}
		synchronized(m_run) {
			m_run.addData(msec, count);
			m_run.notify();
		}
	}
	***/
	
	// 無視させるときtrueをセットする
	public void setIgnore(boolean bIgnore) {
		m_ignoreFlag = bIgnore;
	}
	public boolean getIgnore() {
		return(m_ignoreFlag);
	}
}

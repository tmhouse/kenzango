/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.View;

import java.util.List;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class TmBalloon extends Dialog implements SensorEventListener {
	private TextView	m_textView;
	private SensorManager	m_sensorManager;
	private LinearLayout	m_llt;
	private long		m_sensorEventTime;
	private boolean	m_curUserIsMe = true;
    private RotateAnimation m_rotateToYou;
    private RotateAnimation m_rotateToMe;
    private boolean m_bOnTouchDissmiss = true;
    
    private int c_HANDLE_DISMISS	= 1;
    private int c_AUTO_DISMISS_DELAY_TIME	= 30 * 1000;// msec
    private Handler m_handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if( msg.what == c_HANDLE_DISMISS ) {
				// ダイアログ閉じ
				if( isShowing() ) {
					dismiss();
				}
			}
		}
    };
    
    /**
     * 自動ダイアログ閉じ設定.
     */
    public void setAutoDismissTime(long delayTime) {
    	long time = c_AUTO_DISMISS_DELAY_TIME;
    	if( delayTime > 0 ) {
    		time = delayTime;
    	}
    	m_handler.sendEmptyMessageDelayed(c_HANDLE_DISMISS, time);
    }
    
    /**
     * 自動ダイアログ閉じ設定の解除.
     */
    public void cancelAutoDismiss() {
    	m_handler.removeMessages(c_HANDLE_DISMISS);
    }
    
    public void cancelOnTouchDismiss() {
    	m_bOnTouchDissmiss = false;
    }
  
    public TmBalloon(Context context, float textSize, int widthPx, SensorManager senserMgr) {  
        super(context);  
        
        m_sensorManager = senserMgr;
  
    	// dialog
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
       	WindowManager.LayoutParams wmlp = getWindow().getAttributes();
       	wmlp.gravity = Gravity.TOP;	//画面上部に表示
       	getWindow().setAttributes(wmlp);
    	setCancelable(true);
    	setCanceledOnTouchOutside(true);
    	
    	// layout
    	// onStartなどでもlayouterが終わってなくて取得できないので、無理やりサイズを取得する。
    	m_llt = new LinearLayout(context) {
    		@Override
    		protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    			m_rotateToYou = new RotateAnimation(0, 180, w/2.0f, h/2.0f);
    			m_rotateToMe = new RotateAnimation(180, 0, w/2.0f, h/2.0f);
    		}
    	};
    	m_llt.setBackgroundColor(Color.WHITE);
    	m_llt.setOrientation(LinearLayout.VERTICAL);
    	m_llt.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    	
    	// textview
    	m_textView = new TextView(context);
    	//m_textView.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL);
    	m_textView.setGravity(Gravity.LEFT);
    	m_textView.setPadding(5, 5, 5, 5);
    	m_textView.setBackgroundColor(Color.WHITE);
    	m_textView.setTextColor(Color.BLACK);
    	m_textView.setTextSize(textSize);
    	if( widthPx > 0 ) {
    		m_textView.setWidth(widthPx);
    	}
    	m_textView.setHorizontallyScrolling(false);
    	m_textView.setVerticalScrollBarEnabled(true);
    	
    	m_llt.addView(m_textView);
    	
    	// scroll view
    	ScrollView scrollView = new ScrollView(context);
    	scrollView.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    	scrollView.addView(m_llt);
    	
    	setContentView(scrollView);
    }
    
    /**
     * close button
     * @param context
     * @param textSize
     */
    public void setShowCloseButton(Context context, String label, float textSize) {
    	Button closeBtn = new Button(context);
    	closeBtn.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    	closeBtn.setText(label);
    	closeBtn.setTextSize(textSize);
    	closeBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TmBalloon.this.dismiss();
			}
    	});
    	m_llt.addView(closeBtn);
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    }
    
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		
		if( m_bOnTouchDissmiss ) {
			dismiss();
		}
		
		return(true);
	}
	
    public void setText(String text) {
   		m_textView.setText(text);
   		m_textView.forceLayout();
   		m_llt.forceLayout();
    }
    
    @Override
    public void show() {
    	try {
    		m_curUserIsMe = true;
    		super.show();
    	} catch( android.view.WindowManager.BadTokenException e) {
    		Log.e("dialog show", "caught BadTokenException exception, so no dialog");
    	} catch( java.lang.IllegalStateException e) {
    		Log.e("dialog show", "caught IllegalStateException exception, so no dialog");
    	}
    }
    
    /**
     * 回転アニメーションする.
     */
    private void animate(boolean isUserIsMe) {
    	RotateAnimation rotate = getRoteter(isUserIsMe);
    	if( rotate != null ) {
    		rotate.setDuration(500);
    		rotate.setFillAfter(true);
    		m_textView.startAnimation(rotate);	
    	}
    }
    
    /**
     * RotateAnimationの取得.
     * @param isUserIsMe
     * @return
     */
    private RotateAnimation getRoteter(boolean isUserIsMe) {
    	if( isUserIsMe == false ) {
    		return(m_rotateToYou);
    	} else {
    		return(m_rotateToMe);
    	}
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	setupSensors();
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        cancelAutoDismiss();
        unsetupSensors();
    }
    
    private void setupSensors() {
    	if( m_sensorManager != null ) {
    		List<Sensor> sensors = m_sensorManager.getSensorList(Sensor.TYPE_ORIENTATION);
        	if( sensors.size() > 0 ) {
        		Sensor s = sensors.get(0);
        		m_sensorManager.registerListener(this, s, SensorManager.SENSOR_DELAY_UI);
        	}
    	}
    }
    
    private void unsetupSensors() {
    	if( m_sensorManager != null ) {
    		m_sensorManager.unregisterListener(this);
    	}
    }

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// 振動するとやだし、少し遅れたぐらいの方がアニメしているのが見えていいか.
		if( m_sensorEventTime + 500 > event.timestamp )  {
			return;
		}
		
		// 端末によって(SC-04D)ずっとこれが出てしまうのでやめ
		//Log.i("onSensorChanged", "accuracy=" + event.accuracy);
		//if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
		//	return;
		//}
		
        if (event.sensor.getType() == Sensor.TYPE_ORIENTATION) {
        	float x = event.values[1];
        	//Log.d("onSensorChanged", "x=" +x);
        	// 少し幅を持たせないとバタバタ振動する
        	if( x <= -5 ) {
        		if( m_curUserIsMe == false ) {
        			m_curUserIsMe = true;
        			animate(true);
        		}
        	} else if( x >= 5 ){
        		if( m_curUserIsMe == true ) {
        			m_curUserIsMe = false;
        			animate(false);
        		}
        	}
        }
		m_sensorEventTime = event.timestamp;
	}
}

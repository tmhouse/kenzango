/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

/**
 * ふちが付いたTextView.
 * @author mutoh
 *
 */
public class TmBorderedTextView extends TextView {

    private Paint		m_paint = new Paint();
    private Border[]	m_borders;

    public static final int BORDER_TOP = 0x00000001;
    public static final int BORDER_RIGHT = 0x00000002;
    public static final int BORDER_BOTTOM = 0x00000004;
    public static final int BORDER_LEFT = 0x00000008;
    
    public class Border {
        private int orientation;
        private int width;
        private int color = Color.BLACK;
        private int style;
        public int getWidth() {
            return width;
        }
        public void setWidth(int width) {
            this.width = width;
        }
        public int getColor() {
            return color;
        }
        public void setColor(int color) {
            this.color = color;
        }
        public int getStyle() {
            return style;
        }
        public void setStyle(int style) {
            this.style = style;
        }
        public int getOrientation() {
            return orientation;
        }
        public void setOrientation(int orientation) {
            this.orientation = orientation;
        }
        public Border(int Style) {
            this.style = Style;
        }
    }

    public TmBorderedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public TmBorderedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TmBorderedTextView(Context context) {
        super(context);
    }

    private void init(){
        m_paint.setStyle(Paint.Style.STROKE);
        m_paint.setColor(Color.BLACK);
        m_paint.setStrokeWidth(4);
        
        Border[] borderArr = {
        		new Border(BORDER_TOP),
        		new Border(BORDER_RIGHT),
        		new Border(BORDER_BOTTOM),
        		new Border(BORDER_LEFT)};

        for( Border b : borderArr ) {
        	b.setColor(m_defaultBoarderColor);
        	b.setWidth(m_defaultBoarderWidth);
        }
        setBorders(borderArr);
        
        transAnimationTest(this);
    }
    private void uninit() {
    	setBorders(null);
    }
    
    // invalidateの間隔(ms)
    private long m_invalidateInterval = 50;
    // 枠のビカビカの間隔(ms)
    private long m_bikabikaInterval = 250;
    
    // デフォルト値:枠の太さ
    private int m_defaultBoarderWidth = 50;
    // デフォルト値:枠の色
    private int m_defaultBoarderColor = Color.RED;
    
    private Runnable m_invalidater = new Runnable() {
		@Override
		public void run() {
			invalidate();
			m_handler.postDelayed(this, m_invalidateInterval);
		}
	};
	
	public void startAnimation() {
		stopAnimation();
		init();
		m_handler.post(m_invalidater);
	}
    
    public void stopAnimation() {
    	uninit();
    	m_handler.removeCallbacks(m_invalidater);
    }
    
    // 移動アニメーションの例
    void transAnimationTest( View v ){
        final TranslateAnimation trans = new TranslateAnimation(
                // 自分の幅の2倍左の位置から開始
                Animation.RELATIVE_TO_SELF, 2,
                // 自分の幅の5倍左の位置（元の位置）で終了
                Animation.RELATIVE_TO_SELF, 0,
                // 縦には移動しない
                Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0);
 
        // 2秒かけてアニメーションする
        trans.setDuration( 2000 );
        //trans.setRepeatCount(10);
        //trans.setRepeatMode(Animation.REVERSE);
 
        // アニメーション終了時の表示状態を維持する
        trans.setFillEnabled(true);
        trans.setFillAfter  (true);
 
        // アニメーションを開始
        v.startAnimation(trans);
    }
    private Handler m_handler = new Handler();
    
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(m_borders == null) return;

       	long curTime = System.currentTimeMillis();
        if( curTime % (m_bikabikaInterval*2) > m_bikabikaInterval ) {
        	return;
        }

        for(Border border : m_borders){
            m_paint.setColor(border.getColor());
            m_paint.setStrokeWidth(border.getWidth());

            if(border.getStyle() == BORDER_TOP){
                canvas.drawLine(0, 0, getWidth(), 0, m_paint);                
            } else
            if(border.getStyle() == BORDER_RIGHT){
                canvas.drawLine(getWidth(), 0, getWidth(), getHeight(), m_paint);
            } else
            if(border.getStyle() == BORDER_BOTTOM){
                canvas.drawLine(0, getHeight(), getWidth(), getHeight(), m_paint);
            } else
            if(border.getStyle() == BORDER_LEFT){
                canvas.drawLine(0, 0, 0, getHeight(), m_paint);
            }
        }
    }

    public Border[] getBorders() {
        return m_borders;
    }

    public void setBorders(Border[] borders) {
        this.m_borders = borders;
    }
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.View;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.widget.EditText;

/*
 * ひとつのテキスト入力を行うダイアログ.
 */
public class TmSimpleEditTextDialog extends AlertDialog.Builder {
	private Context		m_ctx;
	private AlertDialog		m_dialog;
	public TmSimpleEditTextDialog(Context ctx) {
		super(ctx);
		m_ctx = ctx;
	}

	public interface OnGetTextListener {
		public void onGetText(String text);
	}

	public void openEditTextDialog(String title, String positiveLabel,
			String cancelLabel, String defaultText, final OnGetTextListener textListener) {
		final EditText edit = new EditText(m_ctx);
		edit.setInputType(InputType.TYPE_CLASS_TEXT);
		edit.setText(defaultText);

		AlertDialog.Builder ab = this;
		ab.setTitle(title);
		ab.setView(edit);
		ab.setCancelable(false);
    	ab.setNegativeButton(cancelLabel, 
        		new DialogInterface.OnClickListener() {
    			@Override
    			public void onClick(DialogInterface dialog, int which) {
    				dialog.dismiss();
    			}
        	});

		DialogInterface.OnClickListener l = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String txt = edit.getText().toString();
				textListener.onGetText(txt);
			}

		};
		m_dialog = ab.setPositiveButton(positiveLabel, l).create();
		m_dialog.show();
	}
	
	public boolean isShowing() {
		if( m_dialog != null ) {
			return(m_dialog.isShowing());
		}
		return(false);
	}
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.View;

import java.util.Random;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class TmAutoWrapLayout extends ViewGroup {
//public class TmAutoWrapLayout extends RelativeLayout {
  
    public TmAutoWrapLayout(Context context) {  
        super(context);  
    	//this.setBackgroundColor(0x30ffffff);
    	this.setBackgroundColor(0x0000ff);
    	//this.setPadding(0, 0, 0, 0);
    }
    
    @Override
    public void addView(View child) {
        // idが未設定の場合は乱数でどうにかする。
        if (child.getId() == -1) {
            child.setId(new Random(System.currentTimeMillis()).nextInt(Integer.MAX_VALUE));
        }
 
        super.addView(child);
    }
 
    /**
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int l = this.getChildCount();
        if (l > 0) {
            int max = MeasureSpec.getSize(widthMeasureSpec);
            View pline = this.getChildAt(0);
            View prev = this.getChildAt(0);
            prev.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
            int currentTotal = pline.getMeasuredWidth();
            for (int i = 1; i < l; i++) {
                View child = this.getChildAt(i);
                child.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
                int width = child.getMeasuredWidth();
                RelativeLayout.LayoutParams layoutParams =
                        (RelativeLayout.LayoutParams) child.getLayoutParams();
                if (max > currentTotal + width) {
                    currentTotal += width;
                    layoutParams.addRule(RelativeLayout.ALIGN_TOP, prev.getId());
                    layoutParams.addRule(RelativeLayout.RIGHT_OF, prev.getId());
                } else {
                    layoutParams.addRule(RelativeLayout.BELOW, pline.getId());
                    layoutParams.addRule(RelativeLayout.ALIGN_LEFT, pline.getId());
                    pline = child;
                    currentTotal = pline.getMeasuredWidth();
                }
                prev = child;
            }
        }
 
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    } 
    ***/
 
	@Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    	// TODO Auto-generated method stub
    	final int count = getChildCount();
    	int curWidth, curHeight, curLeft, curTop, maxHeight;

    	//get the available size of child view    
    	int childLeft = this.getPaddingLeft();
    	int childTop = this.getPaddingTop();
    	int childRight = this.getMeasuredWidth() - this.getPaddingRight();
    	int childBottom = this.getMeasuredHeight() - this.getPaddingBottom();
    	int childWidth = childRight - childLeft;
    	int childHeight = childBottom - childTop;

    	maxHeight = 0;
    	curLeft = childLeft;
    	curTop = childTop;
    	//walk through each child, and arrange it from left to right
    	for (int i = 0; i < count; i++) {
    		View child = getChildAt(i);
    		if (child.getVisibility() != GONE) {
    			
    			//Get the maximum size of the child
    			//child.measure(MeasureSpec.makeMeasureSpec(childWidth, MeasureSpec.AT_MOST), 
    			//		MeasureSpec.makeMeasureSpec(childHeight, MeasureSpec.AT_MOST));
    			//curWidth = child.getMeasuredWidth();
    			//curHeight = child.getMeasuredHeight();
    			
    			curWidth = child.getLayoutParams().width;
    			curHeight = child.getLayoutParams().height;

    			//wrap is reach to the end
    			if (curLeft + curWidth >= childRight) {
    				curLeft = childLeft;
    				curTop += maxHeight;
    				maxHeight = 0;
    			}
    			//do the layout
    			child.layout(curLeft, curTop, curLeft + curWidth, curTop + curHeight);
    			//store the max height
    			if (maxHeight < curHeight)
    				maxHeight = curHeight;
    			curLeft += curWidth;
    		}
    	}
    }
}

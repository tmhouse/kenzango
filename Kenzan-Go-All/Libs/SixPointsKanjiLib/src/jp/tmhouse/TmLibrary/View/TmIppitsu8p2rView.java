/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所), 長谷川貞夫（社会福祉法人 桜雲会 理事）
// 著作権は作者である「武藤　繁夫」および「長谷川　貞夫」が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.View;

import jp.tmhouse.TmLibrary.Utils.Data.OctOct;

import jp.tmhouse.TmLibrary.Utils.Draw.TmDrawUtils;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

/**
 * 点字一筆8進入力View
 * @author mutoh
 *
 */
public class TmIppitsu8p2rView extends TmBrailleInputView {
	private Paint	m_innerCirclePaint;
	private Paint	m_linePaint;
	private	 Paint	m_numberPaint;
	private float	m_width;
	private float	m_height;
	private float m_inner_chokkei; // 中心の円の直径
	private float m_chokkeiScaling = 1.0f; // 中心の円の直径の拡大縮小の率
	private int	s_lastHitNumber = 0;
	
	private Line m_line0_3 = new Line();
	private Line m_line1_4 = new Line();
	private Line m_line2_5 = new Line();
	private Line m_line3_6 = new Line();
	private OctOct		m_curOctOct = new OctOct();
	private GosiGosiDetector m_verticalDetector = new GosiGosiDetector(false);
	private GosiGosiDetector m_horizontalDetector = new GosiGosiDetector(true);
	
	private Point	m_p = new Point();
	private Point	m_circleCenter = new Point();
	private int 	m_loc[] = new int[2];
	
	// hitNumberの定義
	public static final int c_HIT_NONE = -1;
	public static final int c_HIT_CENTER = 100;
	
	private static final float c_route2 = 1.41421356f;
	
	public TmIppitsu8p2rView(Context context) {
		super(context);
	}
	
	public TmIppitsu8p2rView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public TmIppitsu8p2rView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	/**
	 * 中心の円の拡大縮小率の設定.
	 * @param scale
	 */
	public void setChokkeiScaling(float scale) {
		m_chokkeiScaling = scale;
		invalidate();
	}
	
	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    	super.onLayout(changed, l, t, r, b);
    	init(r - l, b - t);
    }
	
	private void init(int width, int height) {
		// 最初は文字が完成した状態にしておく。
		// もし数値を最初に触ったときに無視したいから。
		//m_curOctOct.setRightComplete();
		
		m_width = width;
		m_height = height;
		
    	// Paint
    	m_innerCirclePaint = new Paint();
    	m_innerCirclePaint.setAntiAlias(true);
    	m_innerCirclePaint.setColor(Color.GRAY);
    	
    	m_linePaint = new Paint();
    	m_linePaint.setAntiAlias(true);
    	m_linePaint.setColor(Color.GRAY);
		m_linePaint.setStrokeWidth(2.0f);
		
		m_numberPaint = new Paint();
		m_numberPaint.setColor(Color.YELLOW);
		float textSize = m_width / 5.0f;
		m_numberPaint.setTextSize(textSize);
    	m_numberPaint.setAntiAlias(true);
    	
		m_circleCenter.set((int)m_width/2, (int)m_height/2);
		getLocationInWindow(m_loc);
	}
	
	public void onDraw(Canvas c){
		if( m_innerCirclePaint == null ) {
			return;
		}
		//c.drawColor(Color.BLUE);
		
		// 縦長矩形の際に下に描画するための、上部からのマージン
		float top_margin = 0;
		if( m_height > m_width ) {
			top_margin = m_height - m_width;
		}
		
		// 中心の円
		float inner_margin = m_width / 4.0f;
		m_inner_chokkei = m_width - (inner_margin * 2.0f);
		// 中心の円の大きさをカスタマイズする場合
		m_inner_chokkei = m_inner_chokkei * m_chokkeiScaling;
		
		c.drawCircle(m_width/2.0f, m_width/2.0f + top_margin, m_inner_chokkei/2.0f, m_innerCirclePaint);
		
		// 放射状の線
		// <--------k---------> 
		//    m     l       m
		// +----+-------+-----+
		// |    /
		//m|  /
		// |/
		// +
		// |
		//l|            ・八角形の中心
		// |
		// +
		// |
		//r|
		// |
		// +----+-------+----+
		// m = k / (√2 + 2)
		// l = k - (m * 2)
		
		float m = m_width / (c_route2 + 2.0f);
		float l = m_width - (m * 2);
		
		// 線の座標を覚える
		m_line0_3.set((int)m,       (int)top_margin,     	(int)(m + l),	(int)(m_width + top_margin));
		m_line1_4.set((int)(m + l), (int)top_margin,     	(int)m,			(int)(m_width + top_margin));
		m_line2_5.set((int)m_width, (int)(m + top_margin),    (int)0,		(int)(l + m + top_margin));
		m_line3_6.set((int)m_width, (int)(m + l + top_margin),(int)0,		(int)(m + top_margin));
		
		c.drawLine(m_line0_3.m_start.x, m_line0_3.m_start.y, m_line0_3.m_end.x, m_line0_3.m_end.y, m_linePaint);
		c.drawLine(m_line1_4.m_start.x, m_line1_4.m_start.y, m_line1_4.m_end.x, m_line1_4.m_end.y, m_linePaint);
		c.drawLine(m_line2_5.m_start.x, m_line2_5.m_start.y, m_line2_5.m_end.x, m_line2_5.m_end.y, m_linePaint);
		c.drawLine(m_line3_6.m_start.x, m_line3_6.m_start.y, m_line3_6.m_end.x, m_line3_6.m_end.y, m_linePaint);
		
//		c.drawLine(m      ,     0, m + l, m_width, m_linePaint);
//		c.drawLine(m + l  ,     0,     m, m_width, m_linePaint);
//		c.drawLine(m_width,     m,     0,   l + m, m_linePaint);
//		c.drawLine(m_width, m + l,     0,       m, m_linePaint);
		
		
		// 数字
		float cd = (m_width/2 - m_inner_chokkei/2) / 2 + m_inner_chokkei/2;
		float half_w = m_width / 2.0f;// 正方形の一辺の半分
		float lx = cd * (1 / c_route2);// 斜め位置の数字のx座標のoffset
		
		// 中心座標
		float centerX = getWidth() / 2;
		float centerY = getWidth() / 2;
		
    	m_numberPaint.setTextAlign(Align.LEFT);
		TmDrawUtils.drawTextCenterXY("0", centerX, centerY - cd + top_margin, c, m_numberPaint);
		TmDrawUtils.drawTextCenterXY("1", centerX + lx, centerY - lx + top_margin, c, m_numberPaint);
		TmDrawUtils.drawTextCenterXY("2", centerX + cd, half_w + top_margin, c, m_numberPaint);
		TmDrawUtils.drawTextCenterXY("3", centerX + lx, half_w + lx + top_margin, c, m_numberPaint);
		TmDrawUtils.drawTextCenterXY("4", centerX, half_w + cd + top_margin, c, m_numberPaint);
		TmDrawUtils.drawTextCenterXY("5", centerX - lx, half_w + lx + top_margin, c, m_numberPaint);
		TmDrawUtils.drawTextCenterXY("6", centerX - cd, half_w + top_margin, c, m_numberPaint);
		TmDrawUtils.drawTextCenterXY("7", centerX - lx, centerY - lx + top_margin, c, m_numberPaint);
	}
	
		
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		Log.d("onTouchEvent", "action=" + event.getAction());
		
		int rowX = (int)event.getRawX();
		int rowY = (int)event.getRawY();

		int x = rowX - m_loc[0];
		int y = rowY - m_loc[1];
		
		Log.d("onTouchEvent", "(x,y)=(" + x + ", " + y + ")");
		Log.d("onTouchEvent", "(rowX,rowY)=(" + rowX + ", " + rowY + ")");
		Log.d("onTouchEvent", "(m_loc[0],m_loc[1])=(" + m_loc[0] + ", " + m_loc[1] + ")");
		if( x < 0 || y < 0 ) {
			return(false);
		}
		
		//Log.d("onTouchEvent", "(x,y)=(" + x + ", " + y + ")");

		m_p.x = x;
		m_p.y = y;
		int hitNumber = hitTest(m_p);
		
		// -センター通過:左右存在すれば一文字確定。そして次開始。
		// -数値通過：左セット
		// -センター通過:左確定
		// -数値通過：右セット
		// -センター通過:左右確定。一文字確定。そして開始
		// 繰り返し
		int action = event.getAction();
		if (action == MotionEvent.ACTION_MOVE) {
			if( hitNumber < 0 ) {
				//Log.d("Gesture", "MOVE:何もないところ");
			} else if( hitNumber >= 0 && hitNumber <= 7 ){
				// 変化があるか
				if( s_lastHitNumber != hitNumber && m_curOctOct.isCenterPassed() ) {
					//Log.d("Gesture", "MOVE:数値=" + hitNumber);
					if( m_curOctOct.isLeftCompleted() ) {
						// 右の数値をセット
						onRightPassed(hitNumber, false);
					} else {
						// 左の数値をセット
						onLeftPassed(hitNumber, false);
					}
				}
			} else if( hitNumber == c_HIT_CENTER ) {
				if( s_lastHitNumber != hitNumber ) {
					//Log.d("Gesture", "MOVE:真ん中");
					// 右側が入ってれば完了
					if( m_curOctOct.isRightSetted() ) {
						// 完了処理
						//Log.d("Gesture", "MOVE:右確定");
						m_curOctOct.setRightComplete();
						onRightPassed(m_curOctOct.getRight(), true);
						onOneInputCompleted(m_curOctOct);
							
						// 連続入力の開始
						onInputStart();
						onCenterPassed();
					} else if( m_curOctOct.isLeftSetted() ){
						// 左側完了
						//Log.d("Gesture", "MOVE:左確定");
						m_curOctOct.setLeftComplete();
						onLeftPassed(m_curOctOct.getLeft(), true);
					} else {
						// 開始処理
						//Log.d("Gesture", "MOVE:開始");
						onUserReady();
						onInputStart();
						onCenterPassed();
					}
				}
				// 一文字消去判定
				if( m_verticalDetector.detect(event) ) {
					onDeleteCurrent();
				} else if( m_horizontalDetector.detect(event) ) {
					//Log.d("ALLDELTE","###################################");
					onDeleteAll();
				}
			}
		} else if (action == MotionEvent.ACTION_DOWN) {
			if( hitNumber == c_HIT_CENTER ) {
				// 開始処理
				//Log.d("Gesture", "DOWN:開始");
				onUserReady();
				onInputStart();
				onCenterPassed();
			}
		} else if (action == MotionEvent.ACTION_UP) {
			// 文字列の入力完了
			//onWordInputCompleted();
		}
		s_lastHitNumber = hitNumber;
		
		// 真ん中以外ならば常にgosigosiDetectorはクリア
		if( hitNumber != c_HIT_CENTER || action == MotionEvent.ACTION_UP ) {
			m_verticalDetector.clear();
			m_horizontalDetector.clear();
		}
		//return  m_detctor.onTouchEvent(event);
		return(true);
	}
	

	/**
	 * 入力系エラー.
	 * @param msg
	 */
	private void onInputSyntaxError(String msg) {
		if( !m_curOctOct.isErrored() ) {
			//Log.d("Gesture", msg);
			m_curOctOct.setError();
			
			if( getListener() != null ) {
				getListener().onInputSyntaxError(msg);
			}
		}
		return;
	}
	
	/**
	 * ユーザーが入力準備に入った.
	 */
	private void onUserReady() {
		m_curOctOct.clear();
		if( getListener() != null ) {
			getListener().onUserReady();
		}
	}
	
	/**
	 * 入力がはじまった.
	 */
	private void onInputStart() {
		// 数値でdownがきたら必ずクリア
		m_curOctOct.clear();
		
		if( getListener() != null ) {
			getListener().onInputStart();
		}
	}
	
	/**
	 * 正しく中央を通過.
	 */
	private void onCenterPassed() {
		//Log.d("Gesture", "MOVE, center passed");
		m_curOctOct.centerPassed();
	}
	
	/**
	 * 左側入力完了.
	 * @param hitNumber
	 */
	private void onLeftPassed(int hitNumber, boolean completed) {
		Log.d("Gesture", "hit 1st number=" + hitNumber);
		m_curOctOct.setLeft(hitNumber);
		
		if( getListener() != null ) {
			getListener().onLeftPassed(hitNumber, completed);
		}
	}
	
	/**
	 * 右側入力完了.
	 * @param hitNumber
	 */
	private void onRightPassed(int hitNumber, boolean completed) {
		Log.d("Gesture", "hit 2nd number=" + hitNumber);
		m_curOctOct.setRight(hitNumber);
		
		if( getListener() != null ) {
			getListener().onRightPassed(hitNumber, completed);
		}
	}
	
	/**
	 * 一文字入力が完了.
	 * @param oct
	 */
	private void onOneInputCompleted(OctOct oct) {
		Log.d("Gesture", "complete. number=" + m_curOctOct.getLeft() + ", " + m_curOctOct.getRight());
		
		if( getListener() != null ) {
			getListener().onOneInputCompleted(m_curOctOct.getLeft(), m_curOctOct.getRight());
		}
		m_curOctOct.clear();
	}
	
	/**
	 * 連続した文字列の入力が完了.
	 */
	/***
	private void onWordInputCompleted() {
		Log.d("Gesture", "word input completed.");
		
		if( getListener() != null ) {
			getListener().onWordInputCompleted();
		}
		m_curOctOct.clear();
	}
	***/
	
	private void onDeleteCurrent() {
		Log.d("Gesture", "delete.");
		if( getListener() != null ) {
			getListener().onDeleteCurrent();
		}
		m_curOctOct.clear();
		m_curOctOct.centerPassed();
	}
	
	private void onDeleteAll() {
		Log.d("Gesture", "delete all.");
		if( getListener() != null ) {
			getListener().onDeleteAll();
		}
		m_curOctOct.clear();
		m_curOctOct.centerPassed();
	}
	
	/**
	 * 線分.
	 * @author mutoh
	 *
	 */
	class Line {
		public Line() {
		}
		public Line(int startX, int startY, int endX, int endY) {
			set(startX, startY, endX, endY);
		}

		public void set(int startX, int startY, int endX, int endY) {
			m_start.set(startX, startY);
			m_end.set(endX, endY);
		}
		public Point m_start = new Point();
		public Point m_end = new Point();
	}
	
	/**
	 * 三角.
	 * @author mutoh
	 *
	 */
	/**
	class Triangle {
		public Point a;
		public Point b;
		public Point c;
	}
	**/
	
	/**
	 * 点 p が有向線分 e(a,b) の左右どちら側にあるか調べる。
	 * 点 p が有向線分 e の　左側にある場合  c_LEFT を、
	 *	 　   　有向線分 e の直線上にある場合  c_ONLINE を、
	 *	 　   　有向線分 e の　右側にある場合 c_RIGHT を返す。
	 * @param p1
	 * @param e
	 * @return
	 */
	static final int c_ONLINE = 0;
	static final int c_LEFT = 1;
	static final int c_RIGHT = 2;
	private int side( Point p1, Line e) {
		Point p2 = e.m_start; // 有向線分 e の始点
		Point p3 = e.m_end; // 有向線分 e の終点

		// 有向線分 (p2,p1), (p2,p3) の外積の z 成分を求める
		int n  = p1.x * (p2.y - p3.y) + p2.x * (p3.y - p1.y) + p3.x * (p1.y - p2.y);
		if ( n > 0 ) 
			return  c_LEFT; // 左
		else if ( n < 0 ) 
			return c_RIGHT; // 右
		else 
			return  c_ONLINE; // 線上
	}
	
	/**
	 * 
	 * 円と点の判定
	 * 円１（X1,Y1,R1）　点（X2,Y2）
	 */
	private boolean circleDotHit(Point circleCenter, int circleR, Point p) {
		boolean result = false;
		if ((circleCenter.x - p.x)*(circleCenter.x - p.x) + 
				(circleCenter.y - p.y)*(circleCenter.y - p.y) <= 
					circleR*circleR )
		{
			result = true;
		}
		return(result);
	}
	
	/**
	 * hittestして、該当する番号を返す。
	 * 中央の場合はc_HIT_CENTERを返す.
	 * @param x
	 * @param y
	 * @return
	 */
	public int hitTest(Point p) {
		int number = c_HIT_NONE;
			
		// 中央円の中か否かは優先
		if( circleDotHit(m_circleCenter, (int)m_inner_chokkei/2, p) ) {
			//Log.d("hitTest", "num=center circle");
			return(c_HIT_CENTER);
		}
		
		// 各線の右側か否かを調べる
		boolean L03right = side(p, m_line0_3) == c_RIGHT;
		boolean L14right = side(p, m_line1_4) == c_RIGHT;
		boolean L25right = side(p, m_line2_5) == c_RIGHT;
		boolean L36right = side(p, m_line3_6) == c_RIGHT;
		
		// どの数値のエリアか調べる
		if( L03right && !L14right ) {
			number = 0;
		} else if( L14right && !L25right ) {
			number = 1;
		} else if( L25right && !L36right ) {
			number = 2;
		} else if( L36right && L03right ) {
			number = 3;
		} else if( L14right && !L03right ) {
			number = 4;
		} else if( L25right && !L14right ) {
			number = 5;
		} else if( L36right && !L25right ) {
			number = 6;
		} else if( !L36right && !L03right ) {
			number = 7;
		}
		
		//Log.d("hitTest", "num=" + number);
		return(number);
	}
	
}

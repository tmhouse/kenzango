/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityManager;

public class TmBrailleInputView extends View {
	private IIppitsuListener m_listener;
	private TmFakeOnTouch	m_fakeOnTouch;
	private Vibrator		m_vibrator;
	private Boolean			m_isExploreByTouchEnabled = null;

	public TmBrailleInputView(Context context) {
		super(context);
		m_vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
	}
	
	public TmBrailleInputView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public TmBrailleInputView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		m_isExploreByTouchEnabled = null;
	}
	@Override
	public void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		m_isExploreByTouchEnabled = null;
	}

	/**
	 * イベントリスナの設定.
	 * @param listener
	 */
	public void setListener(IIppitsuListener listener) {
		m_listener = listener;
	}
	
	/**
	 * 
	 * @param listener
	 */
	public IIppitsuListener getListener() {
		return(m_listener);
	}
	
	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		//int widthMode = MeasureSpec.getMode(widthMeasureSpec);//高さのモードを取得
		//int heightMode = MeasureSpec.getMode(heightMeasureSpec);//幅のモードを取得
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);//幅を取得
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);//高さを取得
		heightSize = (int)(heightSize * 0.8f);

		setMeasuredDimension(widthSize, heightSize);
	}
	
    @SuppressLint("DrawAllocation")
	@Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    	super.onLayout(changed, l, t, r, b);
		m_fakeOnTouch = new TmFakeOnTouch(this);
    }
	

    private long[] m_pattern = {100, 100, 100}; 
    
	@SuppressLint("NewApi")
	@Override
	public boolean onHoverEvent(MotionEvent event) {
		//TmLog.d("Action=" + getActionName(event));
		
		boolean ret = false;

		if( isExploreByTouchEnabled() ) {
			// TouchEventシミュレート
			// ExploreByTouchが稼動しているときはdispatchTouchEvent()を呼ぶ
			ret = m_fakeOnTouch.fakeOnTouchEvent(event);

			// Hover Enterが来たらブルッとする
			if( m_vibrator != null && 
				event.getAction() == MotionEvent.ACTION_HOVER_ENTER ) {
				m_vibrator.cancel();
				m_vibrator.vibrate(m_pattern, -1);
			}
		}
		
		return(ret);
	}
	
	/**
	 * Accesibilityがonで、かつExploreByTouchがonのときtrueを返す.
	 * @return
	 */
	public boolean isExploreByTouchEnabled() {
		if( m_isExploreByTouchEnabled == null ) {
			m_isExploreByTouchEnabled = false;
			Context ctx = this.getContext();
			AccessibilityManager am = (AccessibilityManager)
					ctx.getSystemService(Context.ACCESSIBILITY_SERVICE);

			if( am.isEnabled() ) {
				if( am.isTouchExplorationEnabled() ) {
					m_isExploreByTouchEnabled = true;
				}
			}
		}
		return(m_isExploreByTouchEnabled);
	}
	
	/**
	 * 
	 * @param event
	 * @return
	 */
	public static String getActionName(MotionEvent event) {
		int action = event.getAction();
		int idx = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> 
			MotionEvent.ACTION_POINTER_INDEX_SHIFT;
		String actionStr = "action=" + String.valueOf(action);
		
		switch(action & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_HOVER_ENTER:
			actionStr = "HOVER_ENTER";
			break;
		case MotionEvent.ACTION_HOVER_EXIT:
			actionStr = "HOVER_EXIT";
			break;
		case MotionEvent.ACTION_HOVER_MOVE:
			actionStr = "HOVER_MOVE";
			break;
		case MotionEvent.ACTION_DOWN:
			actionStr = "ACTION_DOWN";
			break;
		case MotionEvent.ACTION_UP:
			actionStr = "ACTION_UP";
			break;
		case MotionEvent.ACTION_CANCEL:
			actionStr = "ACTION_CANCEL";
			break;
		case MotionEvent.ACTION_MOVE:
			actionStr = "ACTION_MOVE";
			break;
		case MotionEvent.ACTION_OUTSIDE:
			actionStr = "ACTION_OUTSIDE";
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			actionStr = "ACTION_POINTER_DOWN";
			break;
		case MotionEvent.ACTION_POINTER_UP:
			actionStr = "ACTION_POINTER_UP";
			break;
		case MotionEvent.ACTION_SCROLL:
			actionStr = "ACTION_SCROLL";
		default:
			Log.d("getActionName", "what?");
			break;
		}
		actionStr = actionStr + idx;
		
		int count = event.getPointerCount();
		for(int i=0; i < count; i++) {
			Log.v("Multi", " X=" + event.getX(i) + ", Y=" + event.getY(i) + ", id=" + event.getPointerId(i) );
		}
		return(actionStr);
	}
}

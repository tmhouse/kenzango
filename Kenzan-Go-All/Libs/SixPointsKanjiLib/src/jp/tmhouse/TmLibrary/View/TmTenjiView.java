/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.View;

import java.util.Locale;

import jp.tmhouse.TmLibrary.Utils.Data.TmBraille;
import jp.tmhouse.TmLibrary.Utils.Data.TenjiConverter.TmTenjiConverterImpl;
import jp.tmhouse.TmLibrary.Utils.Draw.TmDrawUtils;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

public class TmTenjiView extends View {
	// 描画関係
	private Paint	m_blackPt = new Paint();
	private Paint	m_whitePt = new Paint();
	private Paint	m_yellowPt = new Paint();
	private Paint	m_numPt = new Paint();
	private Paint	m_mojiPt = new Paint();
	private Paint	m_fugoMojiPt = new Paint();
	private RectF[] m_rects = new RectF[7];//0は使わない
	private boolean	m_showNumber = false;
	private int		m_waku1 = 0;
	private int		m_waku2 = 0;
	
	private Locale		m_locale;
	private TmBraille	m_braille = new TmBraille();
	private String		m_characterStr;		// 強制的に文字を設定する場合使用する
	
	// 表示形式
	private int	m_style = STYLE_82;
	
	private static final int STYLE_82 = 1;		// 8/2形式
	private static final int STYLE_222 = 2;		// 2/2/2形式
	
	// リスト
	private TmTenjiView		m_prev = null;
	private TmTenjiView		m_next = null;
	
	// バッファー
	private StringBuilder m_sb = new StringBuilder(8);
	private TmTenjiConverterImpl	m_converter;
	
	// static
	private static final int c_tenColor = Color.rgb(0xa0, 0xc9, 0x33);
	private static final int c_wakuColor = Color.rgb(0xf0, 0xff, 0x00);
	private static final String c_SPACE = " ";
	
	// switch
	// blinkするか否か
	private static final boolean	m_blincFunc = true;

	public TmTenjiView(Context context) {
		super(context);
	}
	
	public TmTenjiView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public TmTenjiView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	// 枠を付ける点を指定
	public void setWakuPoint(int waku1, int waku2) {
		m_waku1 = waku1;
		m_waku2 = waku2;
		invalidate();
	}
	public void clearWakuPoint() {
		m_waku1 = 0;
		m_waku2 = 0;
		invalidate();
	}
	
	// 描画形式の設定
	public void setDrawStyle82() {
		m_style = STYLE_82;
	}
	public void setDrawStyle222() {
		m_style = STYLE_222;
	}
	
	// リスト構造I/F
	public TmTenjiView append(TmTenjiView newone) {
		m_next = newone;
		newone.m_prev = this;
		
		getBraille().append(newone.getBraille());
		
		return(newone);
	}
	public void remove() {
		if( m_next != null ) {
			m_next.m_prev = m_prev;
		}
		if( m_prev != null ) {
			m_prev.m_next = m_next;
		}
		m_prev = null;
		m_next = null;
		
		// TmBrailleのリストもremove
		getBraille().remove();
	}
	public TmTenjiView prev() {
		return(m_prev);
	}
	public TmTenjiView next() {
		return(m_next);
	}
	
	// View I/F
	/****
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    //int size = 0;
	    //int width = 180;
	    //int height = 140;
	 
	    int wmode = MeasureSpec.getMode(widthMeasureSpec);
	    int width = MeasureSpec.getSize(widthMeasureSpec);
	    int height = MeasureSpec.getSize(heightMeasureSpec);
    	TmLog.d("", "width=" + width + ", height=" + height);
	    
    	widthMeasureSpec = MeasureSpec.makeMeasureSpec(140, MeasureSpec.getMode(widthMeasureSpec));
    	heightMeasureSpec = MeasureSpec.makeMeasureSpec(160, MeasureSpec.getMode(heightMeasureSpec));

	    switch(wmode) {
	    case MeasureSpec.AT_MOST:
	    case MeasureSpec.EXACTLY:
	    	setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
	    	break;
	    case MeasureSpec.UNSPECIFIED:
	    	setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
	    	break;
	    }
	    
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	***/
	
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    	super.onLayout(changed, l, t, r, b);
    	init(r - l, b - t);
    }
	
	private void init(int width, int height) {
		// ６点の場所
		final float w = (float)width;
		final float h = (float)height;
		final float square = w <= h ? w : h;
//		final float mojiMargin = square * 0.3f;
//		final float topMargin = square * 0.1f;
//		final float btmMargin = square * 0.1f;
//		final float tenTopMargin = square * 0.1f; // 点と点の上下間隔隙間
		final float mojiMargin = square * 0.3f;
		final float topMargin = square * 0.05f;
		final float btmMargin = square * 0.05f;
		final float tenTopMargin = square * 0.08f; // 点と点の上下間隔隙間
		
		float tenChokkei = (h - topMargin - btmMargin - tenTopMargin*2 - mojiMargin)/3.0f;
		
		final float dotDotMargin = tenTopMargin;
		final float leftRightMargin = w/2.0f - tenChokkei - dotDotMargin/2.0f;
		
		// 黒点
		m_blackPt.setColor(c_tenColor);
		m_blackPt.setAntiAlias(true);
		
		// 白点
		m_whitePt.setColor(c_tenColor);
		m_whitePt.setStyle(Paint.Style.STROKE);
		m_whitePt.setStrokeWidth(2);
		m_whitePt.setAntiAlias(true);
		
		// 黄色い枠
		m_yellowPt.setColor(c_wakuColor);
		m_yellowPt.setStyle(Paint.Style.STROKE);
		m_yellowPt.setStrokeWidth(8);
		m_yellowPt.setAntiAlias(true);
		
		// 数字
		m_numPt.setColor(Color.YELLOW);
		m_numPt.setTextSize(tenChokkei/2);
		m_numPt.setAntiAlias(true);
		
		// 文字
		m_mojiPt.setColor(Color.WHITE);
		m_mojiPt.setTextSize(mojiMargin * 0.8f);
		m_mojiPt.setAntiAlias(true);
		
		// 前置符号
		m_fugoMojiPt.setColor(Color.LTGRAY);
		m_fugoMojiPt.setTextSize(mojiMargin * 0.6f);
		m_fugoMojiPt.setAntiAlias(true);
		
		// 左上の点の位置とサイズ
		RectF startL = new RectF(leftRightMargin, topMargin, tenChokkei + leftRightMargin, tenChokkei + btmMargin);
		m_rects[1] = new RectF(startL.left,startL.top,  startL.right, startL.bottom);
		m_rects[2] = new RectF(startL.left, m_rects[1].bottom + tenTopMargin, startL.right, m_rects[1].bottom + tenChokkei + tenTopMargin);
		m_rects[3] = new RectF(startL.left, m_rects[2].bottom + tenTopMargin, startL.right, m_rects[2].bottom + tenChokkei + tenTopMargin);
		
		// 右上の点の位置とサイズ
		RectF startR = new RectF(w - startL.left - tenChokkei, startL.top, w - startL.left, startL.bottom);
		m_rects[4] = new RectF(startR.left, startR.top, startR.right, startR.bottom);
		m_rects[5] = new RectF(startR.left, m_rects[4].bottom + tenTopMargin, startR.right, m_rects[4].bottom + tenChokkei + tenTopMargin);
		m_rects[6] = new RectF(startR.left, m_rects[5].bottom + tenTopMargin, startR.right, m_rects[5].bottom + tenChokkei + tenTopMargin);
	}
	
	/**
	 * 描画.
	 */
	public void onDraw(Canvas c){
		if( m_style == STYLE_82 ) {
			onDraw82(c);
		} else if( m_style == STYLE_222 ) {
			onDraw222(c);
		}
		
		// 文字表示
		if( true ) {
			// 下に文字を表示
			Paint pt;
			if( false == isSignCode() ) {
				// 清音
				pt = m_mojiPt;
			} else {
				pt = m_fugoMojiPt;
			}
			
			TmDrawUtils.drawTextCenterXY(getCharacterString(), 
					getWidth()/2, getHeight() - m_mojiPt.getTextSize(), c, pt);
			
			// 何番目かを点字の上に表示
			float textSize = getHeight() / 15;
			m_numPt.setTextSize(textSize);
			TmDrawUtils.drawTextCenterXY(String.valueOf(m_braille.getIndex() + 1), 
					getWidth()/2, textSize / 2.0f, c, m_numPt);
		}
	}
	
	/**
	 * 82形式の描画.
	 * @param c
	 */
	private void onDraw82(Canvas c){
		//c.drawColor(Color.RED);
		
		if( m_bShowLeft ) {
			int numColor = c_tenColor;
			if( m_braille.isPointBlack(1) ) {
				drawCircleByRect(m_rects[1], c, m_blackPt);
			} else {
				drawCircleByRect(m_rects[1], c, m_whitePt);
			}
			if( m_braille.isPointBlack(2) ) {
				drawCircleByRect(m_rects[2], c, m_blackPt);
				numColor = Color.BLACK;
			} else {
				drawCircleByRect(m_rects[2], c, m_whitePt);
			}
			if( m_braille.isPointBlack(3) ) {
				drawCircleByRect(m_rects[3], c, m_blackPt);
			} else {
				drawCircleByRect(m_rects[3], c, m_whitePt);
			}
			// 数字
			if( m_showNumber ) {
				m_numPt.setColor(numColor);
				drawTextCenter(m_braille.getLeftString(), m_rects[2], c, m_numPt);
			}
		}
		
		if( m_bShowRight ) {
			int numColor = c_tenColor;
			if( m_braille.isPointBlack(4) ) {
				drawCircleByRect(m_rects[4], c, m_blackPt);
			} else {
				drawCircleByRect(m_rects[4], c, m_whitePt);
			}
			if( m_braille.isPointBlack(5) ) {
				drawCircleByRect(m_rects[5], c, m_blackPt);
				numColor = Color.BLACK;
			} else {
				drawCircleByRect(m_rects[5], c, m_whitePt);
			}
			if( m_braille.isPointBlack(6) ) {
				drawCircleByRect(m_rects[6], c, m_blackPt);
			} else {
				drawCircleByRect(m_rects[6], c, m_whitePt);
			}
			// 数字
			if( m_showNumber ) {
				m_numPt.setColor(numColor);
				drawTextCenter(m_braille.getRightString(), m_rects[5], c, m_numPt);
			}
		}
			
		// 枠
		if( m_waku1 > 0 ) {
			drawCircleByRect(m_rects[m_waku1], c, m_yellowPt);
		}
		if( m_waku2 > 0 ) {
			drawCircleByRect(m_rects[m_waku2], c, m_yellowPt);
		}
	}
	
	/**
	 * 222形式の描画.
	 * @param c
	 */
	private void onDraw222(Canvas c){
		if( m_braille.isTopSetted() ) {
			drawBraillePoint(c, 1);
			drawBraillePoint(c, 4);
		}
		if( m_braille.isMiddleSetted() ) {
			drawBraillePoint(c, 2);
			drawBraillePoint(c, 5);
		}
		if( m_braille.isLowSetted() ) {
			drawBraillePoint(c, 3);
			drawBraillePoint(c, 6);
		}
	}
	
	private void drawBraillePoint(Canvas c, int point) {
		if( m_braille.isPointBlack(point) ) {
			drawCircleByRect(m_rects[point], c, m_blackPt);
		} else {
			drawCircleByRect(m_rects[point], c, m_whitePt);
		}
	}
	
	/**
	 * 前置符号をゲット.
	 * @return
	 */
	/***
	public int getPrevSignCode() {
		if( m_prev != null ) {
			return(m_prev.getSignCode());
		}
		return(TmTenjiConverterImpl.c_SIGN_NOT_SIGN);
	}
	****/
	
	/**
	 * この文字のSignCode(前置符号)を取得.
	 * @return
	 */
	public int getSignCode() {
		if( m_converter != null ) {
			return(m_converter.getSignCode(m_braille));
		}
		return(TmTenjiConverterImpl.c_SIGN_NOT_SIGN);
	}
	
	/**
	 * この文字は前置符号か.
	 * @return
	 */
	public boolean isSignCode() {
		if( getSignCode() == TmTenjiConverterImpl.c_SIGN_NOT_SIGN )
		{
			return(false);
		}
		// これは前置符号
		return(true);
	}
	
	/**
	 * localeの取得.
	 * @return
	 */
	public Locale getLocale() {
		if( m_locale == null ) {
			m_locale = Locale.getDefault();
		}
		return(m_locale);
	}
	/**
	 * localeの設定.
	 * @param locale
	 */
	public void setLocale(Locale locale) {
		m_locale = locale;
	}
	
	/**
	 * Rect内に収まるように円を描く.
	 */
	private void drawCircleByRect(RectF rect, Canvas c, Paint pt) {
		float cx = (rect.right - rect.left)/2.0f + rect.left;
		float cy = (rect.bottom - rect.top)/2.0f + rect.top;
		float radius = cx - rect.left;
		c.drawCircle(cx, cy, radius, pt);
	}
	
	/**
	 *  rect内に収まるように文字を描く.
	 */
	private void drawTextCenter(String text, RectF rect, Canvas c, Paint pt) {
		TmDrawUtils.drawTextCenterXY(text,
				(rect.right - rect.left)/2.0f + rect.left, 
				(rect.bottom - rect.top)/2.0f + rect.top, c, pt);
	}
	
	/**
	 * 現在の文字単体をStringで返す。
	 * @return
	 */
	public String getCharacterString() {
		if( m_converter != null ) {
			String moji = m_converter.convert(m_braille);
			return(moji);
		}
		
		// setStringで設定された文字を使う　
		return(m_characterStr);
	}
	
	/**
	 * 強制的に文字単体を設定する.
	 * @param str
	 */
	public void setString(String str) {
		m_characterStr = str;
	}
	
	/**
	 * 発音での現在の文字単体をStringで返す
	 * @param bIsSpeech
	 * @return
	 */
	public String getSpeechString() {
		if( m_converter == null ) {
			return(null);
		}
		
		Locale locale = getLocale();
		String moji = m_converter.convertSpeechText(m_braille);
		if( TmTenjiConverterImpl.isJapaneseLocale(locale) ) {
			// 日本語の英語しゃべり文字
			m_sb.setLength(0);
			if( moji != null && moji.length() > 0 ) {
				m_sb.append(moji);
				m_sb.append(c_SPACE);
				moji = m_sb.toString();
			}
		}
		return(moji);
	}

	public void reset() {
		m_braille.clear();
		invalidate();
	}
	
	public void setLeftSide(int leftNum) {
		m_braille.setLeft(leftNum);
		invalidate();
	}
	
	public void setRightSide(int rightNum) {
		m_braille.setRight(rightNum);
		invalidate();
	}
	public int getLeftNumber() {
		return(m_braille.getLeft());
	}
	public int getRightNumber() {
		return(m_braille.getRight());
	}
	
	/**
	 * 8/2式(10だったらleft=1, right=0)で点字をセット.
	 * @param braille82
	 */
	public void set(int val) {
		m_braille.set(val);
		invalidate();
	}
	
	/**
	 * 二個ずつセットシリーズ.
	 * @param left
	 * @param right
	 */
	public void setTop(boolean left, boolean right) {
		m_braille.setTop(left, right);
		invalidate();
	}
	public void setMiddle(boolean left, boolean right) {
		m_braille.setMiddle(left, right);
		invalidate();
	}
	public void setLower(boolean left, boolean right) {
		m_braille.setLower(left, right);
		invalidate();
	}
	
	public void setShowNumber(boolean bShowNumber) {
		m_showNumber = bShowNumber;
		invalidate();
	}
	
	public void setConverter(TmTenjiConverterImpl converter) {
		m_converter = converter;
	}
	
	/**
	 * 入力確定の通知.
	 */
	public void onInputCompleted() {
		//Log.d("onInputCompleted","");
		
		
		// test
		//String moji = m_converter.convert(m_braille);
		//Log.d("onInputCompleted","moji=[" + moji + "]");
		//invalidate();
		
		
		// 一つ前の符号が、この符号により確定するケースの対応。
		if( m_prev != null ) {
			// このm_brailleのデータはできているので、m_prevを再描画させれば
			// このbrailleとの関連をみて、再度文字を作りなおして表示する。
			m_prev.invalidate();
		}
	}
	
	public TmBraille getBraille() {
		return(m_braille);
	}
	
	public TmBraille getNextBraille() {
		return(m_braille.getNext());
	}
	public TmBraille getPrevBraille() {
		return(m_braille.getPrev());
	}
	
	//////////////////////////////////////////////////////
	// ビカビカさせる実装
	//////////////////////////////////////////////////////
	private boolean	m_bShowLeft = true;
	private boolean	m_bShowRight = true;
	private boolean	m_isLeftBlink = false;
	private boolean	m_isRightBlink = false;
	
	private static final int c_START_BLINK_MSG_NO = 1;
	private Handler m_handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if( msg.what == c_START_BLINK_MSG_NO ) {
				// ブリンクさせる
				//Log.d("Blink", "blink, left=" + m_isLeftBlink + ", right=" + m_isRightBlink);
				if( m_isLeftBlink ) {
					m_bShowLeft = !m_bShowLeft;
				}
				if( m_isRightBlink ) {
					m_bShowRight = !m_bShowRight;
				}
				
				invalidate();
				
				// 再依頼
				m_handler.sendEmptyMessageDelayed(c_START_BLINK_MSG_NO, 400);
			}
		}
	};
	
	public void startBlink(boolean isLeft, boolean isRight) {
		if( m_blincFunc == true ) {
			resetBlink();
			m_isLeftBlink = isLeft;
			m_isRightBlink = isRight;
			if( isLeft || isRight ) {
				m_handler.sendEmptyMessageDelayed(c_START_BLINK_MSG_NO, 100);
			}
		}
	}
	public void stopBlink() {
		if( m_blincFunc == true ) {
			resetBlink();
			invalidate();
		}
	}
	private void resetBlink() {
		if( m_blincFunc == true ) {
			m_handler.removeMessages(c_START_BLINK_MSG_NO);
			m_isLeftBlink = false;
			m_isRightBlink = false;
			m_bShowLeft = true;
			m_bShowRight = true;
		}
	}
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所), 長谷川貞夫（社会福祉法人 桜雲会 理事）
// 著作権は作者である「武藤　繁夫」および「長谷川　貞夫」が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.View;

import java.util.Locale;
import jp.tmhouse.TmLibrary.Utils.Data.TenjiConverter.TmTenjiConverterFactory;
import jp.tmhouse.TmLibrary.Utils.Data.TenjiConverter.TmTenjiConverterImpl;
import android.content.Context;
import android.util.AttributeSet;

public class TmIntegratedIppitsu8p2rView extends TmIntegratedIppitsu8p2rCommonView 
{
	private StringBuilder			m_sb = new StringBuilder(128);
    private StringBuilder			m_wordBuf = new StringBuilder(64);
	private TmTenjiConverterImpl	m_converter;
	private boolean				m_playInputSound = true; // 入力中サウンド
	
	//////////////////////////////////////////////////////
	//
	// constructor
	//
	//////////////////////////////////////////////////////
	public TmIntegratedIppitsu8p2rView(Context context) {
		super(context);
	}
	public TmIntegratedIppitsu8p2rView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public TmIntegratedIppitsu8p2rView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs);
	}
	
	
	/**
	 * 一文字入力する.
	 * @param oct
	 */
	@Override
	protected void doNewBraille(int left, int right) {
		if( m_playInputSound ) {
			TmTenjiView tv = getLastTenjiView();
			String speechStr = tv.getCharacterString();
			speechTextAppend(speechStr, c_NUMBER_SPEECH_RATE);
		}
	}
	
	/**
	 * 最後の点字を削除する.
	 */
	@Override
	protected void doDeleteLast() {
	}
	
	/**
	 * 全ての点字を削除する.
	 */
	@Override
	protected void doDeleteALL() {
	}
	
	/**
	 * 入力された文字を連結して返す.
	 * @param bIsSpeech
	 * @return
	 */
	@Override
	public String getInputedString(boolean bIsSpeech) {
		m_sb.setLength(0);
		m_wordBuf.setLength(0);
		//for( TmTenjiView tv : m_curTenjiView ) {
			
		TmTenjiView tv = getFirstTenjiView();
		for( ; tv != null; tv = tv.next() ) {
			// 前置符号は入れない
			if( !tv.isSignCode() ) {
				String str = null;
				if( bIsSpeech ) {
					str = tv.getSpeechString();
				} else {
					str = tv.getCharacterString();
				}
				// 区切り文字か
				if( m_converter.isPunctuation(str) ) {
					// 区切り文字なので、単語単位で切り取って略字を展開する
					String word = m_wordBuf.toString();
					m_wordBuf.setLength(0);
						
					// 略字が得られた場合は文字列入れ替え
					String expand = m_converter.expandShortFormWords(word);
					if( expand != null ) {
						// 展開文字を足す
						m_sb.append(expand);
					} else {
						// 略字じゃないのでそのまま足す
						m_sb.append(word);
					}
					// strはwhitespaceなので足す
					m_sb.append(str);
				} else {
					// 単語の一部として足し続ける
					m_wordBuf.append(str);
				}
			}
		}
		// 最後にwhitespaceが入ってない場合がある
		if( m_wordBuf.length() > 0 ) {
			m_sb.append(m_wordBuf);
		}
		return(m_sb.toString());
	}
	

	/**
	 * 画面に現れたとき.
	 */
	@Override
	protected void onAttachedToWindow() {
    	// 自分自身の設定
		super.onAttachedToWindow();
		
		// 点字コンバータの生成とセット
		m_converter = TmTenjiConverterFactory.createConverter(getLocale());
		setTenjiConverter(m_converter);
    }
    
    @Override
   	protected void onDetachedFromWindow() {
    	//android.os.Debug.stopMethodTracing();
        super.onDetachedFromWindow();
    }
    
    /**
     * ロケールの変更.
     */
    @Override
    public void setLocale(Locale locale) {
    	super.setLocale(locale);
    	
		// 点字コンバータの生成とセット
		m_converter = TmTenjiConverterFactory.createConverter(getLocale());
		setTenjiConverter(m_converter);
    }

    /**
     * クリア.
     */
    @Override
    public void allClear(boolean bClearInputModeStack) {
    	super.allClear(bClearInputModeStack);
    	// たぶん英語のときはこのサブクラスで何もしなくていいはず。
    }
    
    // 英語テスト
    private class EngTest extends Test{
    	public EngTest() {
    		addTest(new Data("My name is Charlie Brown.", new int[][]
    				//      M     y          n  a  m e         i     s
    				{{0,4},{5,1},{5,7},{0,0},{0,2},{5,3},{0,0},{2,1},{6,1},{0,0},
    				//     Ch     ar   l     i      e                 B     r     ow    n      .
    		    	{0,4},{1,4},{4,3},{7,0},{2,1},{1,2},{0,0},{0,4},{3,0},{7,2},{2,5},{5,3},{2,6}}, null));
    		
    		addTest(new Data("Can you come over today or tomorrow?", new int[][]
        			//      Can          you        come    e          o    v      er
        			{{0,4},{1,1},{0,0},{5,7},{0,0},{4,4},{1,2},{0,0},{5,2},{7,4},{3,7},{0,0},
        			//t  o d a y          o    r           tomorrow    ?
        			{6,3},{1,3},{0,0},{5,2},{7,2},{0,0},{6,3},{5,1},{6,4}}, null));
    		
    		addTest(new Data("Daddy dislikes my new friend.", new int[][]
    		        //       D     a     dd     y           dis    l    i      k    e    s 
    		        {{0,4},{1,3},{1,0},{2,6},{5,7},{0,0},{2,6},{7,0},{2,1},{5,0},{1,2},{6,1},{0,0},
    	            //   m    y         n      e     w         f r ie nd   .
    			    {5,1},{5,7},{0,0},{5,3},{1,2},{2,7},{0,0},{3,1},{7,2},{2,6}}, null));
    		
    		addTest(new Data("Little Andy was born in 1983.", new int[][]
                    //     Li t t le               And   y           was         b     o     r     n
    	            {{0,4},{7,0},{7,0},{0,0},{0,4},{7,5},{5,7},{0,0},{4,6},{0,0},{3,0},{5,2},{7,2},{5,3},{0,0},
    	            // in        [num]  1    9     8      3    .
    	            {4,2},{0,0},{4,7},{1,0},{2,1},{3,2},{1,1},{2,6}}, null));
    		                                                                   
    		addTest(new Data("Some children are very fast readers.", new int[][]
    				//      S o m e s       chidlren           ar    e          very        f     a     st   
    				{{0,4},{0,2},{6,1},{0,0},{1,4},{5,3},{0,0},{4,3},{1,2},{0,0},{7,4},{0,0},{3,1},{1,0},{4,1},{0,0},
    				// r   ea   d      er    s      .
    				{7,2},{2,0},{1,3},{3,7},{6,1},{2,6}}, null));
    		
    		addTest(new Data("Everyone needs a good educAtion.", new int[][]
    				//    e   v   e r  y     o   n   e        n      e     ed     s          a           g  o  o d        ed     u     c    [大]   ation
    				{{0,4},{0,2},{1,2},{5,7},{0,2},{5,2},{0,0},{5,3},{1,2},{3,5},{6,1},{0,0},{1,0},{0,0},{3,3},{1,3},{0,0},{3,5},{5,4},{1,1},{0,4},{5,3},{2,6}}, null));
    		
    		addTest(new Data("Mr. Bill said he would be able to finish the assignment you", new int[][]
    	            //       M     r      .                B    i     l      l           s a   i d          h    e          w o u l d          b     e
    	            {{0,4},{5,1},{7,2},{2,6},{0,0},{0,4},{3,0},{2,1},{7,0},{7,0},{0,0},{6,1},{1,3},{0,0},{3,2},{1,2},{0,0},{2,7},{1,3},{0,0},{3,0},{1,2},{0,0},
    				// a   ble         to    f     in    i     sh         the          a     s       s    i    g     n        m e n t        you
    				{1,0},{4,7},{0,0},{6,2},{3,1},{4,2},{2,1},{1,5},{0,0},{6,5},{0,0},{1,0},{6,1},{6,1},{2,1},{3,3},{5,3},{0,6},{6,3},{0,0},{5,7}}, null));
    				
    		addTest(new Data("gave him sometime this afternoon.", new int[][]
    				//g    a    v       e          h   i  m         s  o   m e   t  i m e          this        afn(afternoon)    .
    				{{3,3},{1,0},{7,4},{1,2},{0,0},{3,2},{5,1},{0,0},{0,2},{6,1},{0,2},{6,3},{0,0},{1,7},{0,0},{1,0},{3,1},{5,3},{2,6}}, null));
    		
    		addTest(new Data("Mother always said, `what part of no don't you understand?'", new int[][]
    				//    m o t h e r        alw(always)              said(sd)    ,            "    wh    a     t           p a r t           of
    				{{0,4},{0,2},{5,1},{0,0}, {1,0},{7,0},{2,7},{0,0},{6,1},{1,3},{2,0},{0,0},{6,4},{1,6},{1,0},{6,3},{0,0},{0,2},{7,1},{0,0},{7,6},{0,0},
    				//  n   o           d      o     n     '     t         you         u n d e r   st    and    ?   "
    				{5,3},{5,2},{0,0},{1,3},{5,2},{5,3},{4,6},{6,3},{0,0},{5,7},{0,0},{0,2},{5,4},{4,1},{7,5},{6,4},{4,6}}, null));
    		
    		addTest(new Data("InformAtion about vision loss.", new int[][]
    				//      In  for     m    A t i o n         a b out(ab)       v     i     s   i o n         l     o     s     s      .
    				{{0,4},{4,2},{7,7},{5,1},{0,4},{5,3},{0,0},{1,0},{3,0},{0,0},{7,4},{2,1},{0,5},{5,3},{0,0},{7,0},{5,2},{6,1},{6,1},{2,6}}, null));
    		
    		
    		addTest(new Data("This is the day which the lord hath made; we will rejoice and be glad in it.", new int[][]
    				//      This        i     s           the          d  a  y          which        the         l  o r  d        h       a    th
    				{{0,4},{1,7},{0,0},{2,1},{6,1},{0,0},{6,5},{0,0},{0,2},{1,3},{0,0},{1,6},{0,0},{6,5},{0,0},{0,2},{7,0},{0,0},{3,2},{1,0},{1,7},{0,0},
    				// m    a    d      e    ;           w      e         will         rejoice(rjc)           and          be    g    l      a     d          in          it(x)   .
    				{5,1},{1,0},{1,3},{1,2},{6,0},{0,0},{2,7},{1,2},{0,0},{2,7},{0,0},{7,2},{2,3},{1,1},{0,0},{7,5},{0,0},{6,0},{3,3},{7,0},{1,0},{1,3},{0,0},{4,2},{0,0},{5,5},{2,6}}, null));
    		
    		addTest(new Data("The country to the people and of the people.", new int[][]
    		        // The               country                           to     the      people        and    of   the       people  .
    	            {{0,4},{6,5},{0,0},{1,1},{0,5},{6,3},{7,2},{5,7},{0,0},{6,2},{6,5},{0,0},{7,1},{0,0},{7,5},{7,6},{6,5},{0,0},{7,1},{2,6}}, null));
    		
    		addTest(new Data("He is out for a walk.", new int[][]
    	            //     H      e           i     s          out          for  a           w     a     l      k     .
    	            {{0,4},{3,2},{1,2},{0,0},{2,1},{6,1},{0,0},{3,6},{0,0},{7,7},{1,0},{0,0},{2,7},{1,0},{7,0},{5,0},{2,6}}, null));
    		
    		addTest(new Data("in my child. in my chi. in my child's.", new int[][]
    				// in         m      y         child 
    				{{4,2},{0,0},{5,1},{5,7},{0,0},{1,4},{2,6},{0,0},
    				//in         m       y          ch   i      .
    				{4,2},{0,0},{5,1},{5,7},{0,0},{1,4},{2,1},{2,6},{0,0},
    				//in         m       y          child  '   s     .
    				{4,2},{0,0},{5,1},{5,7},{0,0},{1,4},{4,0},{6,1},{2,6}}, null));
    		
    		addTest(new Data("Yet, she do it without that!", new int[][]
    				//      Y     e     t     ,          sh     e           do          it        with   ou     t         that    !
    				{{0,4},{5,7},{1,2},{6,3},{2,0},{0,0},{1,5},{1,2},{0,0},{1,3},{0,0},{5,5},{0,0},{6,7},{3,6},{6,3},{0,0},{6,3},{6,2}}, null));
    		
    		addTest(new Data("They were bigger (in water).", new int[][]
    				//     The   y          were         b     i     gg   er            (    in         w      a     t    er     )     .
    				{{0,4},{6,5},{5,7},{0,0},{6,6},{0,0},{3,0},{2,1},{6,6},{3,7},{0,0},{6,6},{4,2},{0,0},{2,7},{1,0},{6,3},{3,7},{6,6},{2,6}}, null));
    		
    		addTest(new Data("Get into here to be", new int[][]
                    //       G      e     t         in   to      h e r e          to    b      e     
    	            {{0,4},{3,3},{1,2},{6,3},{0,0},{4,2},{6,2},{0,2},{3,2},{0,0},{6,2},{3,0},{1,2}}, null));
    		
    		addTest(new Data("Stand by", new int[][]
    	            //     St   and           b      y
    	            {{0,4},{4,1},{7,5},{0,0},{3,0},{5,7}}, null));
    	}
    }
    
    // 日本語テスト
    private class JpnTest extends Test{
    	public JpnTest() {
    		addTest(new Data("わたしのなまえ", new int[][]
    				// わ   た　　し　　の　　な　　ま　　え　　　　
    				{{4,0},{5,2},{3,6},{6,1},{5,0},{5,6},{3,1},{0,0},
    		    	}, null));
    		
    	}
    }
    
    
    /**
     * テストダイアログを表示する
     */
    private Test m_test;
    @Override
    public void openTestDialog() {
   		Locale loc = getLocale();
   		if( loc.equals(Locale.JAPANESE) ) {
   			m_test = new JpnTest();
   		} else {
   			m_test = new EngTest();
    	}
    	m_test.openTestDialog();
    }
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.View;


import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.TmLibrary.Utils.MotionDetector.TmGestureDetector;
import jp.tmhouse.TmLibrary.Utils.MotionDetector.TmGestureDetector.TmSimpleOnGestureListener;
import junit.framework.Assert;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * スマート点字入力View.
 * @author mutoh
 *
 */
public class TmSmartBrailleView extends TmBrailleInputView {
	private TmGestureDetector		m_gestureDetector;
	private GosiGosiDetector	m_verticalDetector = new GosiGosiDetector(false, 1000);
	private GosiGosiDetector	m_horizontalDetector = new GosiGosiDetector(true, 1000);
	// onLongPressが起きている間はtrue
	private boolean	m_bIsOnLongPressProcessing = false;
	
	public TmSmartBrailleView(Context context) {
		super(context);
		this.setBackgroundColor(Color.GREEN);
		
		m_gestureDetector = new TmGestureDetector(
				context, m_simpleOnGestureListener, null);
		
		if( isExploreByTouchEnabled() ) {
			// Talkback onのとき、長押し後にフリック開始を認識するためには、
			// Longpressはオフにしないとダメ.
			m_gestureDetector.setIsLongpressEnabled(false);
		} else {
			m_gestureDetector.setIsLongpressEnabled(true);
		}
		
		// TouchSlopを拡大する
		int touchSlop = m_gestureDetector.getTouchSlop();
		if( touchSlop < 32 ) {
			m_gestureDetector.setTouchSlop((int)(touchSlop * 1.5));
		}
	}
	public TmSmartBrailleView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public TmSmartBrailleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs);
	}
	
	//タッチ時に呼ばれる
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		//String tag = "TmSmartBrailleView.onTouchEvent";
		//TmLog.d("Action=" + getActionName(event) + 
		//		", downTime=" + event.getDownTime() + 
		//		", index=" + event.getActionIndex() + 
		//		", histoSize=" + event.getHistorySize());
		
		int actionMasked = event.getActionMasked();
		//int actionMasked = event.getAction();
		if( actionMasked == MotionEvent.ACTION_POINTER_DOWN ||
				actionMasked == MotionEvent.ACTION_POINTER_UP ) {
			//Log.d(tag, "pointerCount=" + m_lastPointerCount + 
			//		", マルチタッチのうち、一部のタッチが増えた or 減った");
			
			// up/downしたらゴシゴシはclear
			m_verticalDetector.clear();
			m_horizontalDetector.clear();
			// マルチタッチの長押しをGesutreDetectorにハンドルさせないとダメ！
			//return(true);
		}
			
		// タッチしてすぐに離したときは無視する
		if( actionMasked == MotionEvent.ACTION_CANCEL ) {
			//Log.d(tag, "ACTION_CANCEL event. ignored");
			// マルチタッチの長押しをGesutreDetectorにハンドルさせないとダメ！
			//return(true);
		}

		Assert.assertTrue(m_ignoreDownTime != 0);
		
		// このDownTimeのイベントは破棄する
		if( m_ignoreDownTime != -1 &&
				event.getDownTime() == m_ignoreDownTime ) {
			//Log.d(tag, "Ignore DownTime:" + m_ignoreDownTime);
			return(true);
		}
		m_ignoreDownTime = -1;
		
		boolean ret = m_gestureDetector.onTouchEvent(event);
		
		// ハンドルされなかったときだけ消去判定
		if( ret == false && m_bIsOnLongPressProcessing == false ) {
			// 一文字消去判定
			if( m_verticalDetector.detect(event) ) {
				//Log.d(tag, "vertical gosigosi detect");
				m_ignoreDownTime = event.getDownTime();
				onDeleteCurrent();
			} else if( m_horizontalDetector.detect(event) ) {
				//Log.d(tag, "horizontal gosigosi detect");
				m_ignoreDownTime = event.getDownTime();
				onDeleteAll();
			}
		}
		Assert.assertTrue(m_ignoreDownTime != 0);
		
		// up/cancelが来たらgosigosiはクリア
		if( actionMasked == MotionEvent.ACTION_UP || 
				actionMasked == MotionEvent.ACTION_CANCEL ){
			// cancelOnLongPressHandlingの後falseにする
			m_bIsOnLongPressProcessing = false;
						
			m_verticalDetector.clear();
			m_horizontalDetector.clear();
			
			// 離したイベントを発火
			getListener().onRelease();
		}
		
		return true;
	}
	
	// このdownTimeのイベントは破棄する
	private long		m_ignoreDownTime = -1;
	
	//複雑なタッチイベント処理
	private final TmSimpleOnGestureListener m_simpleOnGestureListener = 
		new TmSimpleOnGestureListener() {
		@Override
		public boolean onDoubleTap(MotionEvent e) {
			//Log.d("gesture", "onDoubleTap");
			inputData(false, false);
			return true;
		}

		@Override
		public boolean onDoubleTapEvent(MotionEvent e) {
			//Log.d("gesture", "onDoubleTapEvent");
			return false;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			//Log.d("gesture", "onSingleTapConfirmed");
			return false;
		}

		@Override
		public boolean onDown(MotionEvent e) {
			//Log.d("gesture", "onDown");
			return false;
		}

		private static final float c_MAX_FLING_TIME = 1000.0f;
		private static final float c_MIN_FLING_TIME = 180.0f;
		private static final float c_MIN_FLING_LEN = 30.0f;

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float vX, float vY) {
			float e1X = e1.getX();
			float e2X = e2.getX();
			float e1Y = e1.getY();
			float e2Y = e2.getY();

			float lenX = e2X - e1X;
			float lenY = e2Y - e1Y;
			float delta = lenX == 0 ? 0 : lenY/lenX;
			float deltaAbs =  Math.abs(delta);
			//TmLog.d("onFling:(vX=" + vX + ", vY=" + vY + "), dT=" + delta +
					//"(lenX=" + lenX + ", lenY=" + lenY + ")");
			
			// 少々オーバーにフリックしないと反応しないようにする
			long eventDistTime = e2.getEventTime() - e1.getEventTime();
			//TmLog.d("event time=" + eventDistTime);
			if( eventDistTime < c_MIN_FLING_TIME  && 
				Math.abs(lenX) < c_MIN_FLING_LEN && 
				Math.abs(lenY) < c_MIN_FLING_LEN)
			{
				TmLog.d("時間と移動距離が短かすぎなのでタップ扱い");
				return(onSingleTapUp(e2));
			}

			// 向きとしての認識が可能か
			if( deltaAbs > 0.9f && deltaAbs < 1.1f ) {
				TmLog.d("向きが曖昧でわからん！！！");
				return(false);
			}
			
			if( ! isExploreByTouchEnabled() ) {
				// Talkbackとの共存のため長押しでは何もしない。
				// 一回長押し後にフリック開始を許容するため。
				if( eventDistTime > c_MAX_FLING_TIME ) {
					TmLog.d("フリックにかかった時間が長すぎる！！！");
					return(false);
				}
			}

			if( deltaAbs < 1.0f ) {
				if( e1X < e2X ) {
					// 右方向
					//TmLog.d("右");
					getListener().onFlick(IIppitsuListener.DIRECTION_RIGHT);
					inputData(false, true);
				} else {
					// 左方向
					//TmLog.d("左");
					getListener().onFlick(IIppitsuListener.DIRECTION_LEFT);
					inputData(true, false);
				}
			} else {
				if( e2Y < e1Y ) {
					// 上方向
					//TmLog.d("上");
					getListener().onFlick(IIppitsuListener.DIRECTION_UP);
				} else {
					// 下方向
					//TmLog.d("下");
					getListener().onFlick(IIppitsuListener.DIRECTION_DOWN);
					inputData(true, true);
				}
			}
			return true;
		}

		@Override
		public void onLongPress(MotionEvent e, int pointerCount) {
			TmLog.d("onLongPress:pointerCount=" + pointerCount);
			m_bIsOnLongPressProcessing = true;
			getListener().onLongPress(pointerCount);
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			return false;
		}

		@Override
		public void onShowPress(MotionEvent e) {
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			//TmLog.d("タップ");
			inputData(false, false);
			return true;
		}
	};
	
	/**
	 * 点字データ.
	 * @author mutoh
	 *
	 */
	private class BrailleData {
		/**
		 * 点字六点と配列の関係
		 * (1)m_data[0][0], (4)m_data[0][1]
		 * (2)m_data[1][0], (5)m_data[1][1]
		 * (3)m_data[2][0], (6)m_data[2][1]
		 */
		private boolean	m_data[][] = new boolean[3][2];
		private int		m_pos = -1;
		
		/**
		 * 上から順に２つずつセットする.
		 * @param left
		 * @param right
		 */
		public void appendTwo(boolean left, boolean right) {
			if( m_pos < 2 ) {
				m_pos++;
				m_data[m_pos][0] = left;
				m_data[m_pos][1] = right;
			}
			return;
		}
		
		public void clear() {
			m_pos = -1;
			m_data[0][0] = false;
			m_data[0][1] = false;
			m_data[1][0] = false;
			m_data[1][1] = false;
			m_data[2][0] = false;
			m_data[2][1] = false;
		}
		
		/**
		 * 上からデータを2個取得する.
		 * @param count
		 * @return
		 */
		public boolean[] get(int pos) {
			return(m_data[pos]);
		}
		
		/**
		 * いっぱいかどうか.
		 * @return
		 */
		public boolean isFull() {
			if( m_pos >= 2 ) {
				// いっぱいです
				return(true);
			}
			return(false);
		}
		
		/**
		 * 最後の位置.
		 * @return
		 */
		public int getLastPos() {
			return(m_pos);
		}
		
		/**
		 * 8/2形式の左の値.
		 * @return
		 */
		public int getLeft82Value() {
			return(get82value(true));
		}
		/**
		 * 8/2形式の右の値.
		 * @return
		 */
		public int getRight82Value() {
			return(get82value(false));
		}
		private int get82value(boolean isLeft) {
			int index = isLeft ? 0 : 1;
			int val = 0;
			val += m_data[0][index] ? 1 : 0;
			val += m_data[1][index] ? 2 : 0;
			val += m_data[2][index] ? 4 : 0;
			return(val);
		}
	}
	
	private BrailleData		m_data = new BrailleData();
	
	/**
	 * 何か点字入力があったとき.
	 * @param left
	 * @param right
	 */
	private void inputData(boolean left, boolean right) {
		if( m_data.isFull() ) {
			return;
		}
		m_data.appendTwo(left, right);
		callPassedListener();
		
		if( m_data.isFull() ) {
			// 完成
			getListener().onOneInputCompleted(m_data.getLeft82Value(), m_data.getRight82Value());
			m_data.clear();
		}
	}
	
	/**
	 * Passed関係リスナーを呼ぶ.
	 */
	private void callPassedListener() {
		if( getListener() == null || m_data.getLastPos() < 0 ) {
			return;
		}
		int pos = m_data.getLastPos();
		boolean[] data = m_data.get(pos);
		switch( pos ) {
		case 0:
			getListener().onTopPassed(data[0], data[1]);
			break;
		case 1:
			getListener().onMiddlePassed(data[0], data[1]);
			break;
		case 2:
			getListener().onLowerPassed(data[0], data[1]);
			break;
		default:
			
		}
	}
	
	private void onDeleteCurrent() {
		TmLog.d("delete current.");
		if( getListener() != null ) {
			getListener().onDeleteCurrent();
		}
		m_data.clear();
	}
	
	private void onDeleteAll() {
		TmLog.d("delete all.");
		if( getListener() != null ) {
			getListener().onDeleteAll();
		}
		m_data.clear();
	}
	
	/**
	 * 強制的に現在入力中の点字を破棄したい場合に呼ぶ.
	 */
	public void resetCurrent() {
		m_data.clear();
	}
}

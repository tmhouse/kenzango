/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所), 長谷川貞夫（社会福祉法人 桜雲会 理事）
// 著作権は作者である「武藤　繁夫」および「長谷川　貞夫」が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.View;

import jp.tmhouse.SixPointsKanjiLib.R;
import jp.tmhouse.TmLibrary.Activity.SettingsActivity;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmSixPointsKanjiTokenizer;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmSixPointsKanjiTokenizer.TokenizeResult;
import android.content.Context;
import android.util.AttributeSet;

/**
 * 六点漢字用Ippitsu8p2rView
 * @author mutoh
 *
 */
public class TmIntegratedIppitsu8p2rKanjiView extends TmIntegratedIppitsu8p2rCommonView 
{
    private TmSixPointsKanjiTokenizer m_kanjiTokenizer = new TmSixPointsKanjiTokenizer();
	
	//////////////////////////////////////////////////////
	//
	// constructor
	//
	//////////////////////////////////////////////////////
	public TmIntegratedIppitsu8p2rKanjiView(Context context) {
		super(context);
	}
	public TmIntegratedIppitsu8p2rKanjiView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public TmIntegratedIppitsu8p2rKanjiView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs);
	}
	
	@Override
	public void doNewBraille(int left, int right) {
		TokenizeResult result = m_kanjiTokenizer.appendAndTokenize(left, right);
		if( result.isError ) {
			// エラー処理
			intputError(result.errorBrailleSet);
		} else {
			if( result.newModeStr != null ) {
				// モード変更をしゃべる
				speechTextAppend(result.newModeStr, c_WORDS_SPEECH_RATE);
			}
			if( result.resultStr != null ) {
				//Log.d("test", "onComplete:" + str);
				TmTenjiView tv = getLastTenjiView();
				tv.setString(result.resultStr);
				
				// 墨字の音声出力スイッチの検査
				Context ctx = getContext();
				if( SettingsActivity.GuideSetting.queryEnableSpeechInputChar(ctx)) {
					// "く"などの発音が悪いので、空白を付ける
					speechTextAppend(result.resultStr + " ", c_WORDS_SPEECH_RATE);
				}
			}
		}
	}
	
	/**
	 * 最後の点字を削除する.
	 */
	@Override
	protected void doDeleteLast() {
		// 6点すべてを入力しているときはtokenizerから削除する
		TmTenjiView ttv = getLastTenjiView();
		if( ttv != null && ttv.getBraille().isAllSetted() ) {
			// 最後の文字を削除
			m_kanjiTokenizer.pop(1);
		}	
	}
	
	/**
	 * 全ての点字を削除する.
	 */
	@Override
	protected void doDeleteALL() {
		// 全削除
    	allClear(true);
		m_kanjiTokenizer.clear(true);
	}
	
	
	/**
	 * 入力エラー処理.
	 * @param bset
	 */
	private void intputError(BrailleSet bset) {
		TmLog.d("onError:" + bset.getString());
		clickVibrate(false);
		
		int len = bset.length();
		String errMsg = null;
		Context ctx = getContext();
		if( len > 0 ) {
			// BrailleSet内の点字個数だけ削除
			for( int i = 0; i < len; i++ ) {
				removeLastTenjiView();
			}
			
			// 音声で何個消去したかを伝える
			StringBuilder sb = new StringBuilder(128);
			sb.append(ctx.getString(R.string.ACTinputError));
			sb.append(len);
			sb.append(ctx.getString(R.string.ACTbraillesWereDeleted));
			errMsg = sb.toString();
		} else {
			// インプットエラーとだけ伝える
			errMsg = ctx.getString(R.string.ACTinputError);
		}
		
		if( errMsg != null ) {
			speechText(errMsg, c_WORDS_SPEECH_RATE);
			// エラー音声を出した後は少し待つ。次の入力が行われると音声が消えてしまうから。
			synchronized(this) {
				try {
					wait(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 入力された文字を連結して返す.
	 * @param bIsSpeech
	 * @return
	 */
	@Override
	public String getInputedString(boolean bIsSpeech) {
		String words = m_kanjiTokenizer.getString();
		return(words);
	}
	

	/**
	 * 画面に現れたとき.
	 */
	@Override
	protected void onAttachedToWindow() {
		// 現在の入力モードをしゃべる
		String modeName = m_kanjiTokenizer.getCurrentModeName();
		speechTextAppend(modeName, c_WORDS_SPEECH_RATE);
		
    	// 自分自身の設定
		super.onAttachedToWindow();
    }
    
    @Override
   	protected void onDetachedFromWindow() {
    	//android.os.Debug.stopMethodTracing();
        super.onDetachedFromWindow();
        
    }
    
    @Override
    public void allClear(boolean bClearInputModeStack) {
    	super.allClear(bClearInputModeStack);
    	m_kanjiTokenizer.clear(bClearInputModeStack);
    }

    ////////////////////////////////////////////////////////////////////////////
    // test code
    ////////////////////////////////////////////////////////////////////////////
    private class TestSixPKanji extends Test {
    	
    	public TestSixPKanji() {
    		addTest(new Data("私の名前はイッピツ８２Ｒと申します。", 
    				new int[][] {{0,6},{3,6},{4,0},{6,1}, // 私の
    				{0,5},{7,7},{5,0}, {0,7},{6,6},{3,1},{5,4},// 名前は
    				{0,4},{4,4},{3,0},{2,0},{0,4},{7,4},{5,3},{6,0}, 
    				{4,7},{3,2},{3,0},			// ８２
    				{6,4},{0,4},{7,2},{4,6},	// Ｒ
    				{6,3},						//と
    				{0,7},{5,1},{6,7},			// 申
    				{3,6},{5,6},{1,7},{2,6},{0,0}, //します。
    				{0,0} }, null));
    		
    		addTest(new Data("１０時３分４５秒", new int[][]
    				{{4,7},{1,0},{2,3}, // １０
    				{0,6},{6,4},{6,3},	// 時
    				{4,7},{1,1},		// ３
    				{0,7},{0,3},{4,0},	// 分
    				{4,7},{1,3},{1,2},	// ４５
    				{0,5},{4,7},{6,1},	// 秒
    				{0,0}}, null));
    		
    		addTest(new Data("十時三分四十五秒", new int[][]
    		        {{0,2},{4,7},{4,6}, // 十
    		        {0,6},{6,4},{6,3},	// 時
    		        {0,2},{4,7},{1,1},	// 三
    		        {0,7},{0,3},{4,0},	// 分
    		        {0,2},{4,7},{1,3},{4,6},{1,2},	// 四十五
    		        {0,5},{4,7},{6,1},	// 秒
    		        {0,0}}, null));
    		
    		addTest(new Data("Do your BEST!", new int[][]
    		        {{6,4},{0,4},{1,3},{5,2},{0,0}, // Do 
    		        {5,7},{5,2},{5,4},{7,2},{0,0},	// your 
    		        {0,4},{0,4},{3,0},{1,2},{6,1},{6,3},	// BEST
    		        {6,2},{0,0},{4,6},				// !
    		        {0,0}}, null));
    		
    	}
    	
    	// テストする
    	@Override
        protected void doTest(Data dt) {
        	doDeleteALL();
        	int[][] arr = dt.m_brailles;
        	for( int[] br : arr ) {
        		slideTenjiViews();
        		getLastTenjiView().setLeftSide(br[0]);
        		getLastTenjiView().setRightSide(br[1]);
        		getLastTenjiView().onInputCompleted();
        		
        		doNewBraille(br[0], br[1]);
        		
        	}
        	String str = getInputedString(true);
        	
        	String answere;
        	if( dt.m_answer == null ) {
        		answere = dt.m_name;
        	} else {
        		answere = dt.m_answer;
        	}
        	
        	if( str.compareTo(answere) == 0 ) {
        		TmLog.d("Test OK", str);
        	} else {
        		TmLog.e("Test NG STR", "[" + str + "]");
        		TmLog.e("     ANSWER", "[" + answere + "]");
        	}
        	
        }
    }
    
    /**
     * テストダイアログを表示する
     */
    private Test m_test;
    @Override
    public void openTestDialog() {
    	m_test = new TestSixPKanji();
    	m_test.openTestDialog();
    }
}

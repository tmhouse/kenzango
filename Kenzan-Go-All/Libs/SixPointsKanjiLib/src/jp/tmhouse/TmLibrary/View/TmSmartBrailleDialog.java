/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.View;

import java.util.ArrayList;

import jp.tmhouse.TmLibrary.Activity.SettingsActivity;
import jp.tmhouse.TmLibrary.Activity.SettingsActivity.GuideSetting;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmSoundVibrator;
import jp.tmhouse.TmLibrary.View.IIppitsuListener;
import jp.tmhouse.TmLibrary.View.TmIntegratedIppitsu8p2rCommonView;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

/**
 * スマート点字入力ダイアログ.
 * newしてshowする。
 * @author mutoh
 *
 */
public class TmSmartBrailleDialog extends Dialog {
	private static final String TAG = "TmSmartBrailleDialog";
	private TmIntegratedIppitsu8p2rCommonView m_sbView;
	private OnInputedListener m_listener;
	private TmSoundVibrator		m_feedbackVibe; // user feedback用
	
	/**
	 * 入力完了のリスナ.
	 * @author mutoh
	 *
	 */
	public interface OnInputedListener {
		public void onInputed(ArrayList<Integer> list, String str);
		//public void onLongPress(int pointerCount, ArrayList<Integer> list, String str);
		public void onRelease();
	}
	
	/**
	 * コンストラクタ.
	 * @param ctx
	 * @param wm
	 */
	public TmSmartBrailleDialog(Context ctx, OnInputedListener listener) {
		super(ctx);
		
    	// ダイアログの設定
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
    	//this.setTitle("スマート点字");
    	
    	// 一番はじっこまで広がらないから諦め。　
    	DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
    	WindowManager.LayoutParams win_lp = getWindow().getAttributes();  
        win_lp.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        win_lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        win_lp.gravity = Gravity.CENTER;
        getWindow().setAttributes(win_lp); 
		
        // User feedback用バイブレータの設定
        // Singleだけ使う。
        m_feedbackVibe = new TmSoundVibrator(ctx, -1);
        //m_feedbackVibe.setSoundEnable(false);
        m_feedbackVibe.setVibratorEnable(true);
        //m_feedbackVibe.setVibrteRatio(feedbackVibeRatio);
 
		setCancelable(false);
		setCanceledOnTouchOutside(false);
		
		m_listener = listener;
		
    	m_sbView = SettingsActivity.createSmartBrailleView(ctx);
    	// 入力方法を設定する.
    	m_sbView.set222Style();
    	m_sbView.setPubIppitsuListener(m_sbListener);
    	
    	// 画面いっぱいに広げる
		int w = (int)(metrics.widthPixels * 1.0f);
		int h = (int)(metrics.heightPixels * 1.0f);
    	ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(w, h);
    	addContentView(m_sbView, lp);
    	
    	// 閉じたときに振動で知らせる
    	setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				m_feedbackVibe.clearAll();
				vibeAsHide();
			}
    	});
	}
	
    /**
     * キーイベントをハンドルする.
     */
	/****
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        switch (event.getAction()) {
        case KeyEvent.ACTION_DOWN:
            switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            	String msg = getContext().getString(R.string.volumeKeyIsIgnoredMsg);
            	showToast(msg);
                return true;
                 
            default:
                break;
            }
            break;
             
        case KeyEvent.ACTION_UP:
            switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                // キーが離された場合はイベントを捨てる
                return true;
            default:
                break;
            }
            break;
             
        default:
            break;
        }
        return super.dispatchKeyEvent(event);
    }
    
    private Toast 		m_toast;
    private void showToast(String msg) {
    	if( m_toast != null ) {
    		m_toast.cancel();
    	}
    	m_toast = Toast.makeText(getContext(), msg, Toast.LENGTH_LONG);
    	m_toast.show();
    }
    ***/
	
	/**
	 * 現在の文字を消す.
	 */
	public void clear() {
		// allClear()の引数bClearInputModeStack=falseで、
		// 入力モードスタックは保持。
		m_sbView.allClear(false);
	}
	
	public void finish() {
		m_sbView.finish();
	}
	
	@Override
	public void show() {
   		// 振動でfeedback
		vibeAsShow();
		
		super.show();
	}
	
	long smallTime = 200;
	long longTime = 500;
	private void vibeAsShow() {
		m_feedbackVibe.clearAll();
   		m_feedbackVibe.addSingleSoundWait(100);
   		m_feedbackVibe.addSingleSoundVibrate(smallTime);
   		m_feedbackVibe.addSingleSoundWait(100);
   		m_feedbackVibe.addSingleSoundVibrate(smallTime);
   		m_feedbackVibe.addSingleSoundWait(100);
   		m_feedbackVibe.addSingleSoundVibrate(smallTime);
   		m_feedbackVibe.addSingleSoundWait(100);
   		m_feedbackVibe.addSingleSoundVibrate(longTime);
   		m_feedbackVibe.addSingleSoundWait(500);
   		m_feedbackVibe.playDataAsync();
	}
	
	private void vibeAsHide() {
		m_feedbackVibe.clearAll();
   		m_feedbackVibe.addSingleSoundWait(100);
   		m_feedbackVibe.addSingleSoundVibrate(longTime);
   		m_feedbackVibe.addSingleSoundWait(100);
   		m_feedbackVibe.addSingleSoundVibrate(smallTime);
   		m_feedbackVibe.addSingleSoundWait(100);
   		m_feedbackVibe.addSingleSoundVibrate(smallTime);
   		m_feedbackVibe.addSingleSoundWait(100);
   		m_feedbackVibe.addSingleSoundVibrate(smallTime);
   		m_feedbackVibe.addSingleSoundWait(500);
   		m_feedbackVibe.playDataAsync();
	}
	
	/**
	 * スマート点字のリスナ.
	 */
	private IIppitsuListener m_sbListener = new IIppitsuListener() {
		public void onInputSyntaxError(String msg) { }
		public void onUserReady() { }
		public void onInputStart() { }
		public void onLeftPassed(int left, boolean completed) { }
		public void onRightPassed(int right, boolean completed) { }
		
		public void onTopPassed(boolean left, boolean right) {
			onAny();
		}
		public void onMiddlePassed(boolean left, boolean right) {
			onAny();
		}
		public void onLowerPassed(boolean left, boolean right) {
			onAny();
		}
		public void onOneInputCompleted(int left, int right) { }
		public void onDeleteAll() {
			onAny();
		}
		public void onDeleteCurrent() {
			onAny();
		}
		
		private int m_onFlickUpCnt = 0;
		
		@Override
		public void onFlick(int direction) {
			if( direction == IIppitsuListener.DIRECTION_UP ) {
				m_onFlickUpCnt++;
				Log.d(TAG, "上向きフリックきた。連続回数=" + m_onFlickUpCnt);
				if( m_onFlickUpCnt == 2 ) {
					m_onFlickUpCnt = 0;
					// 入力完了リスナの発火
					if( m_listener != null ) {
						String str = m_sbView.getInputedString(false);
						ArrayList<Integer> arr = m_sbView.get82IntArray();
						m_listener.onInputed(arr, str);
					}
					// ダイアログを閉じる
					dismiss();
					//hide();
				}
			}
		}
		
		@Override
		public void onLongPress(int pointerCount) {
			onAny();
		}

		@Override
		public void onRelease() {
			if( m_listener != null ) {
				m_listener.onRelease();
			}
		}
		
		/**
		 * 何か入力があった.
		 */
		private void onAny() {
			m_onFlickUpCnt = 0;
		}
	};
}
/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.View;

/**
 * IPPITSU8/2Rでのイベントリスナー.
 * @author mutoh
 *
 */
public interface IIppitsuListener {
	/**
	 * 入力系エラー.
	 * @param msg
	 */
	public void onInputSyntaxError(String msg);
	
	/**
	 * ユーザーが入力準備に入った.
	 */
	public void onUserReady();
	
	/**
	 * 文字入力がはじまった.
	 */
	public void onInputStart();
	
	/**
	 * 左側入力.
	 * @param left
	 * @param completed 確定したときtrue
	 */
	public void onLeftPassed(int left, boolean completed);
	
	/**
	 * 右側入力.
	 * @param right
	 * @param completed 確定したときtrue
	 */
	public void onRightPassed(int right, boolean completed);
	
	/**
	 * 上二個入力.
	 * @param right
	 * @param left
	 */
	public void onTopPassed(boolean left, boolean right);
	
	/**
	 * 中二個入力.
	 * @param right
	 * @param left
	 */
	public void onMiddlePassed(boolean left, boolean right);
	
	/**
	 * 下二個入力.
	 * @param right
	 * @param left
	 */
	public void onLowerPassed(boolean left, boolean right);
	
	
	/**
	 * 一文字入力が完了.
	 * @param oct
	 */
	public void onOneInputCompleted(int left, int right);
	
	/**
	 * 連続した文字列の入力が完了.
	 */
	//public void onWordInputCompleted();
	
	/**
	 * 現在の文字を削除.
	 */
	public void onDeleteCurrent();
	
	/**
	 * 全削除.
	 */
	public void onDeleteAll();
	
	/**
	 * 長押し.
	 */
	public void onLongPress(int pointerCount);
	
	/**
	 * 離した.
	 */
	public void onRelease();
	
	/**
	 * フリックした向き.
	 */
	public static final int DIRECTION_UP = 1;
	public static final int DIRECTION_DOWN = 2;
	public static final int DIRECTION_LEFT = 3;
	public static final int DIRECTION_RIGHT = 4;
	public void onFlick(int direction);
}

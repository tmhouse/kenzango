/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.View;

import android.view.MotionEvent;


/**
 * ゴシゴシ動かしたことを判定するクラス.
 * @author mutoh
 *
 */
public class GosiGosiDetector {
	private float m_centerMovedAdded;
	private float m_lastCetnerPoint;
	private long m_lastCenterTime;
	private long m_centerStartTime;
	private boolean  m_bHorizontal = false;
	private int m_detectCount;
	
	private GosiGosiDetector() {
	}
	public GosiGosiDetector(boolean isHorizontal) {
		this(isHorizontal, 0);
	}
	public GosiGosiDetector(boolean isHorizontal, int detectCount) {
		m_bHorizontal = isHorizontal;
		
		// detectCountが0以下ならデフォルト値をセット
		if( detectCount <= 0 ) {
			if( m_bHorizontal == true ) {
				m_detectCount = 600;
			} else {
				m_detectCount = 400;
			}
		} else {
			m_detectCount = detectCount;
		}
	}
	
	public void setHorizontalDetect() {
		m_bHorizontal = true;
	}
	public void setVerticalDetect() {
		m_bHorizontal = false;
	}
	public void setDetectCount(int val) {
		m_detectCount = val;
	}
	
	public boolean detect(MotionEvent event) {
		boolean ret = false;
		long t = event.getEventTime();
		float p = getPoint(event);
		if( m_lastCenterTime != 0 ) {
			float dist = Math.abs(m_lastCetnerPoint - p);
			long distTime = t - m_lastCenterTime;
			float speed = dist / distTime;
			
			//m_centerMovedDistance += dist;
			//Log.d("onTouchEvent", "distTime=" + distTime + ", dist=" + dist + ", speed=" + speed);
			
			// イベントの発生間隔が300msec以上経ってたり、
			// 動き出してからゆっくりと動かしたりして時間が経ってもだめ。
			if( distTime > 300 || (t - m_centerStartTime) > 1500 ) {
				m_centerMovedAdded = 0;
				//Log.d("onTouchEvent", "time over. m_centerMovedDistance is cleared===================================");
			}
			// ゆっくりだったり、移動距離が短いのは無視
			if( speed > 0.2f && dist > 4.0f ) {
				m_centerStartTime = t;
				m_centerMovedAdded += dist;
				//Log.d("onTouchEvent", "m_centerMovedAdded=" + m_centerMovedAdded);
			}
			
			// たくさん動かしたか？
			if( m_centerMovedAdded > m_detectCount ) {
				//m_centerStartTime = 0;
				clear();
				ret = true;
			}
		}	
		m_lastCenterTime = t;
		m_lastCetnerPoint = p;
		return(ret);
	}
	
	private float getPoint(MotionEvent event) {
		float ret;
		if( m_bHorizontal ) {
			ret = event.getX();
		} else {
			ret = event.getY();
		}
		return(ret);
	}
	
	public void clear() {
		m_centerMovedAdded = 0;
		m_lastCenterTime = 0;
	}
	
}

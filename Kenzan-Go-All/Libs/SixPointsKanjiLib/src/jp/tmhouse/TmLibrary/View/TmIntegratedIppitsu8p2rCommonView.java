/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所), 長谷川貞夫（社会福祉法人 桜雲会 理事）
// 著作権は作者である「武藤　繁夫」および「長谷川　貞夫」が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.View;

import java.util.ArrayList;
import java.util.Locale;

import jp.tmhouse.SixPointsKanjiLib.R;
import jp.tmhouse.TmLibrary.Activity.SettingsActivity;
import jp.tmhouse.TmLibrary.Activity.SettingsActivity.GuideSetting;
import jp.tmhouse.TmLibrary.Utils.Data.TmBraille;
import jp.tmhouse.TmLibrary.Utils.Data.TenjiConverter.TmTenjiConverterImpl;
import jp.tmhouse.TmLibrary.Utils.Log.LogUtil;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmBrailleVibrator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

public class TmIntegratedIppitsu8p2rCommonView extends LinearLayout 
	implements TextToSpeech.OnInitListener
{
	private TmSmartBrailleView		m_brailleInputView;	// 入力エリアのViewのsuper class
	private float					m_centerCircleScaling; // 中心円の拡大縮小の率
	private TextToSpeech			m_tts;
	private boolean				m_ttsStatus = false;
	private boolean				m_ttsIgnore = false;
	private HorizontalScrollView	m_tenjiHorizonScrollV;
	private LinearLayout			m_tenjiHorizonL;
	private TmTenjiView				m_lastTenjiView;
	private TmTenjiView				m_firstTenjiView;
	
	private StringBuilder			m_sb = new StringBuilder(128);
	private TmBalloon				m_textBalloon;
	private Locale					m_locale;
	private LinearLayout.LayoutParams	m_bInputLP;
	private TmTenjiConverterImpl	m_converter;
	private String					m_wellComeSpeech;
	private IIppitsuListener		m_pubIppitsuListener;
	private String[]				m_initBrailles;
	private TmBrailleVibrator		m_tsv;
	
	// 調整
	private static boolean		m_playInputSound = true; // 入力中サウンド
	private static float 			c_BALLON_TEXT_SIZE = 50.0f;
	
    // 82スタイルの場合はtrue
    private boolean	m_is82style	= true;
    
	//////////////////////////////////////////////////////
	//
	// constructor
	//
	//////////////////////////////////////////////////////
	public TmIntegratedIppitsu8p2rCommonView(Context context) {
		super(context);
		
    	// デフォルト値で設定を保管する。最終引数がfalseだと、一生に一回だけ利く。
		SettingsActivity.GuideSetting.initDefaultValues(context);
		
		// 第二引数でサウンドファイルを指定する。0でデフォルト。
		// 設定からリソースIDを変更する。
		int soundResId = getCurrentSoundResId(context);
		m_tsv = new TmBrailleVibrator(context, soundResId);
	}
	
	public TmIntegratedIppitsu8p2rCommonView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public TmIntegratedIppitsu8p2rCommonView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs);
	}
	
	/**
	 * 設定値からサウンドファイルのリソースIDを取得する.
	 * @param ctx
	 * @return	リソースID。該当するIDが見つからない場合は0。
	 */
	private int getCurrentSoundResId(Context ctx) {
		// 設定値の取得
		int freq = SettingsActivity.GuideSetting.queryBeepSoundFreq(ctx);
		
		// リソースIDの取得
		Resources res = ctx.getResources();
		String prefix = res.getString(R.string.touchBeepFreqResIdPrefix);
		String postfix = res.getString(R.string.touchBeepFreqResIdPostfix);
		String defType = res.getString(R.string.touchBeepFreqResDefType);
		String freqName = prefix + String.valueOf(freq) + postfix;
		int resId = res.getIdentifier(freqName, defType, ctx.getPackageName());
		return(resId);
	}

	protected TmTenjiView getLastTenjiView() {
		return(m_lastTenjiView);
	}
	protected TmTenjiView getFirstTenjiView() {
		return(m_firstTenjiView);
	}
	
	// 全データをTmBrailleのArrayListで返す.
	public ArrayList<TmBraille> getTmBrailleArray() {
		TmTenjiView tv = getFirstTenjiView();
		ArrayList<TmBraille> arr = new ArrayList<TmBraille>(16);
		if( tv != null ) {
			TmBraille tb = tv.getBraille();
			for( ;tb != null; ) {
				arr.add(tb);
				tb = tb.getNext();
			}
		}
		return(arr);
	}
	
	
	// 全データを8/2式intのArrayListで返す
	public ArrayList<Integer> get82IntArray() {
		TmTenjiView tv = getFirstTenjiView();
		ArrayList<Integer> intArr = new ArrayList<Integer>(16);
		if( tv != null ) {
			TmBraille tb = tv.getBraille();
			for( ;tb != null; ) {
				// 左側を10倍して右側を足す
				intArr.add(tb.getLeft()*10 + tb.getRight());
				tb = tb.getNext();
			}
		}
		return(intArr);
	}
	
	// 全データを8/2式でString[]で返す
	public String[] getBrailleArray() {
		ArrayList<Integer> list = get82IntArray();
		
		int size = list.size();
		String[] arr = new String[size];
		int i = 0;
		for( Integer val : list ) {
			arr[i++] = String.valueOf(val.intValue());
		}
		return(arr);
	}
	
	// 8/2式String[]で入力点字をセットする
	public void setBrailleArray(String[] arr) {
		m_initBrailles = arr;
	}
	
	// 8/2式String[]で入力点字を表示する
	public void appendBrailleArray(String[] arr) throws NumberFormatException {
    	//allClear();
    	if( arr == null ) {
    		return;
    	}
    	// バイブ停止
    	m_tsv.setIgnore(true);
    	
    	// TTS停止
    	setIgnoreTTS(true);
    	
    	for( String brStr : arr ) {
    		int brNum = Integer.parseInt(brStr);
    		int brLeft = brNum / 10;
    		int brRight = brNum % 10;
    		
    		slideTenjiViews();
    		getLastTenjiView().setLeftSide(brLeft);
    		getLastTenjiView().setRightSide(brRight);
    		getLastTenjiView().onInputCompleted();
    		
    		m_ippitsuListener.onOneInputCompleted(brLeft, brRight);
    	}
    	// TTS再開
    	setIgnoreTTS(false);
    	
    	// バイブ再開
    	m_tsv.setIgnore(false);
	}
	
	public void setIgnoreTTS(boolean isIgnore) {
		m_ttsIgnore = isIgnore;
	}
	
    /**
     * 222スタイルの場合はこれを呼ぶ
     */
    public void set222Style() {
    	m_is82style = false;
    }
    
    /**
     * 82スタイルの場合はこれを呼ぶ
     */
    public void set82Style() {
    	m_is82style = true;
    }
	
	/**
	 * 全部クリア.
	 */
	public void allClear(boolean bClearInputModeStack) {
		m_lastTenjiView = null;
		m_firstTenjiView = null;
		if( m_tenjiHorizonL != null ) {
			m_tenjiHorizonL.removeAllViews();
			m_tenjiHorizonScrollV.fullScroll(View.FOCUS_RIGHT);
		}
	}
	
	/**
	 * 一文字入力する.
	 * @param oct
	 */
	protected void doNewBraille(int left, int right) {
	}
	
	/**
	 * 最後の点字を削除する.
	 */
	protected void doDeleteLast() {
	}
	
	/**
	 * 全ての点字を削除する.
	 */
	protected void doDeleteALL() {
	}
	
	/**
	 * IPPITSU8/2Rリスナ
	 */
	private IIppitsuListener m_ippitsuListener = new IIppitsuListener() {
		@Override
		public void onInputSyntaxError(String msg) {
			speechTextAppend(msg, c_WORDS_SPEECH_RATE);
			clickVibrate(false);
			
			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onInputSyntaxError(msg);
			}
		}

		@Override
		public void onUserReady() {
			Log.d("Ippitsu8p2rActivity", "UserReady");
			
			if( m_lastTenjiView == null ) {
				speechText(getContext().getString(R.string.ACTStart), c_WORDS_SPEECH_RATE);
			} else {
				speechText(getContext().getString(R.string.ACTStartToAdd), c_WORDS_SPEECH_RATE);
			}
			
			// 点字表示は全クリア
			//allClear();
			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onUserReady();
			}
		}
		
		@Override
		public void onInputStart() {
			Log.d("Ippitsu8p2rActivity", "onInputStart");
			clickVibrate(true);	
			//slideTenjiViews();
			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onInputStart();
			}
		}

		@Override
		public void onLeftPassed(int left, boolean completed) {
			// 最初にここにきたときに新しくTmTenjiViewを作る
			if( m_lastTenjiView == null || m_lastTenjiView.getBraille().isRightSetted() ) {
				slideTenjiViews();
			}
			TmTenjiView tv = m_lastTenjiView;
			
			tv.setLeftSide(left);
			tv.startBlink(true, false);
			
			clickVibrate(true);	
			String str;
			if( completed ) {	
				tv.startBlink(false, true);
				str = getContext().getString(R.string.ACTand);
			} else {
				tv.startBlink(true, false);
				str = getResources().getStringArray(R.array.SPListNumber)[left];
			}
			if( m_playInputSound ) {
				speechText(str, c_NUMBER_SPEECH_RATE);
			}
			
			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onLeftPassed(left, completed);
			}
		}

		@Override
		public void onRightPassed(int right, boolean completed) {
			// 点字表示
			TmTenjiView tv = m_lastTenjiView;
			if( tv == null ) {
				return;
			}
			tv.setRightSide(right);
			
			if( completed == false ) {
				clickVibrate(true);	
				if( m_playInputSound ) {
					String str = getResources().getStringArray(R.array.SPListNumber)[right];
					speechText(str, c_NUMBER_SPEECH_RATE);
				}
			}

			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onRightPassed(right, completed);
			}
		}

		@Override
		public void onOneInputCompleted(int left, int right) {
			// 点字表示
			TmTenjiView tv = m_lastTenjiView;
			if( tv == null ) {
				return;
			}
			tv.stopBlink();
			
			// 入力確定
			tv.onInputCompleted();
			
			// 確定時の振動
			endOfInputVibrate();
			
			doNewBraille(left, right);
			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onOneInputCompleted(left, right);
			}
		}

		@Override
		public void onDeleteCurrent() {
			clickVibrate(false);	
			
			doDeleteLast();
			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onDeleteCurrent();
			}
			
			if( m_lastTenjiView != null ) {
				m_sb.setLength(0);
				
				// 点字を振動再生
				TmBraille b = m_lastTenjiView.getBraille();
				vibrateBraille(b);

				// 点字が完成しているか否かで処理を変更
				String moji = m_lastTenjiView.getCharacterString();
				if( b.isAllSetted() && moji != null ) {
					// 点字が完成しているので文字をしゃべる
					if( moji.length() == 0 ) {
						// 該当文字が何もない場合
						moji = getContext().getString(R.string.TalkNothing);
					} else if( moji.equals(" ") || moji.equals("　") ) {
						// スペースは特定のいいまわしをする('マスアケ'とか)
						moji = getContext().getString(R.string.TalkSpace);
					}

					// 一文字消去の文言
					m_sb.append(moji);
					m_sb.append(getContext().getString(R.string.ACTwasDeleted));
				} else {
					// 点が入力されている点字番号をしゃべる
					// if  上段1,4の点が入力済みで点なし
					//  and 中段の入力済み
					//       2,5の点がない
					//     ==> "1,2,4,5の点なしを消去する"
					//  - 中段の入力がない場合
					//     ==> "1,4の点なしを消去する"
					
					boolean bIsPointNothing = false;
					String str = "";
					if( !b.isPointBlack(1) && !b.isPointBlack(4) ) {
						if( b.isMiddleSetted() ) {
							if( !b.isPointBlack(2) && !b.isPointBlack(5) ) {
								// 1,2,4,5入力済み、しかし点なし
								bIsPointNothing = true;
								str = getContext().getString(R.string.ACTdelete1245);
							}
						} else {
							// 1,4入力済み、しかし点なし
							bIsPointNothing = true;
							str = getContext().getString(R.string.ACTdelete14);
						}
					}
					
					// 点がどれかあるケース
					if( bIsPointNothing == false ) {
						for( int i = 1; i <= 6; i++ ) {
							if( b.isPointSetted(i) ) {
								if( b.isPointBlack(i) ) {
									m_sb.append(i);
									m_sb.append(' ');
								}
							}
						}
						str = m_sb.toString();
					}

					m_sb.setLength(0);
					m_sb.append(str);
					m_sb.append(getContext().getString(R.string.ACTdeleteConfirm));
				}
				
				// Viewは無条件で消す
				removeLastTenjiView();
				
				// 入力点字がゼロになったら音声でお知らせ
				if( m_lastTenjiView == null ) {
					m_sb.append(getContext().getString(R.string.ACTallWordsWereDeleted));
				}
				String msg = m_sb.toString();
				speechTextAppend(msg, c_WORDS_SPEECH_RATE);
			} else {
				// 消す文字がない
				speechTextAppend(getContext().getString(R.string.ACTnothingToDelete), c_WORDS_SPEECH_RATE);
			}
		}
		
		@Override
		public void onDeleteAll() {
			clickVibrate(false);	
			if( m_lastTenjiView != null ) {
				speechTextAppend(getContext().getString(R.string.ACTallWordsWereDeleted), c_WORDS_SPEECH_RATE);
				allClear(true);
			} else {
				speechTextAppend(getContext().getString(R.string.ACTnothingToDelete), c_WORDS_SPEECH_RATE);
			}
			
			doDeleteALL();
			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onDeleteAll();
			}
		}

		@Override
		public void onLowerPassed(boolean left, boolean right) {
			// 点字表示
			TmTenjiView tv = m_lastTenjiView;
			if( tv == null ) {
				return;
			}
			tv.setLower(left, right);
			vibrate2points(left, right);
			speechStepAction(2, left, right);
			
			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onLowerPassed(left, right);
			}
			
		}

		@Override
		public void onMiddlePassed(boolean left, boolean right) {
			// 点字表示
			TmTenjiView tv = m_lastTenjiView;
			if( tv == null ) {
				return;
			}
			tv.setMiddle(left, right);
			//clickVibrate(true);	
			vibrate2points(left, right);
			speechStepAction(1, left, right);

			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onMiddlePassed(left, right);
			}
		}

		@Override
		public void onTopPassed(boolean left, boolean right) {
			// 最初にここにきたときに新しくTmTenjiViewを作る
			if( m_lastTenjiView == null || m_lastTenjiView.getBraille().isLowSetted() ) {
				slideTenjiViews();
			}
			TmTenjiView tv = m_lastTenjiView;
			
			tv.setTop(left, right);
			//clickVibrate(true);
			vibrate2points(left, right);
			speechStepAction(0, left, right);
			
			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onTopPassed(left, right);
			}
		}
		
		@Override
		public void onFlick(int direction) {
			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onFlick(direction);
			}
		}

		@Override
		public void onLongPress(int pointerCount) {
			if( isExploreByTouchEnabled() ) {
				TmLog.d("ExploreByTouchがonなのでonLongPressでは何もしない");
			} else {
				m_tsv.clearAll();
				
				// 未完成の点字は消去
				TmTenjiView tv = m_lastTenjiView;
				if( tv != null && !tv.getBraille().isAllSetted() ) {
					removeLastTenjiView();
					m_brailleInputView.resetCurrent();
				}

				// 点字表示
				if( tv != null ) {
					tv.stopBlink();
				}
				
				// 現在表示されている点字を再生する
				ArrayList<TmBraille> brailles = getTmBrailleArray();
				if( brailles != null && brailles.size() > 0 ) {
					m_tsv.addDefaultWait();
					m_tsv.addTmBrailleArray(brailles);
					m_tsv.playDataAsync();
				}
				
				// バルーンの表示
				if( SettingsActivity.GuideSetting.
						queryEnableSpeechMainSwitch(getContext()) ) {
					String speech = getInputedString(true);
					if( speech != null ) {
						speechTextAppend(speech, c_WORDS_SPEECH_RATE);
					}
				}
				String words = getInputedString(false);
				showBalloon(words);
			
				// リスナの発火
				if( m_pubIppitsuListener != null ) {
					m_pubIppitsuListener.onLongPress(pointerCount);
				}
			}
		}

		@Override
		public void onRelease() {
			// バルーン非表示
			if( ! isExploreByTouchEnabled() ) {
				hideBalloon();
			}
			
			if( m_pubIppitsuListener != null ) {
				m_pubIppitsuListener.onRelease();
			}
		}

	};
	
	/**
	 * Accesibilityがonで、かつExploreByTouchがonのときtrueを返す.
	 * @return
	 */
	public boolean isExploreByTouchEnabled() {
		return(m_brailleInputView.isExploreByTouchEnabled());
	}
	
	/**
	 * 上中下ごとに入力状況をしゃべる.
	 * @param stepNum
	 * @param left
	 * @param right
	 */
	private void speechStepAction(int stepNum, boolean left, boolean right) {
		// フリック時の音声出力スイッチを確認
		if( !SettingsActivity.GuideSetting.queryEnableSpeechFlickNumber(getContext())) {
			return;
		}
		
		// 上、中、下ごとのリソースを取得
		String[] textArr = null;
		switch(stepNum) {
		case 0:
			textArr = getResources().getStringArray(
					R.array.speech_input_actions_upper);
			break;
		case 1:
			textArr = getResources().getStringArray(
					R.array.speech_input_actions_middle);
			break;
		case 2:
			textArr = getResources().getStringArray(
					R.array.speech_input_actions_lower);
			break;
		}
		
		// 左だけ、右だけ、両方、両方なしのリソース名
		String text = null;
		if( left ) {
			if( right ) {
				text = textArr[0];
			} else {
				text = textArr[1];
			}
		} else {
			if( right ) {
				text = textArr[2];
			} else {
				text = textArr[3];
			}
		}
		
		if( text != null ) {
			speechText(text, c_WORDS_SPEECH_RATE);
		} else {
			Log.e("speechStepAction", "no resource error");
		}
	}
	
	
	/**
	 * テキスト表示バルーンの取得.
	 * @return
	 */
    protected TmBalloon getTextBalloon() {
    	if( m_textBalloon == null ) {
    		int widthPx = getWidth();
    		float textSize = widthPx / 10;
    		SensorManager smgr = (SensorManager)getContext().getSystemService(Activity.SENSOR_SERVICE);
    		m_textBalloon = new TmBalloon(getContext(), textSize, widthPx, smgr);
    		m_textBalloon.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					speechText(getContext().getString(R.string.ACTclosed), c_WORDS_SPEECH_RATE);
					clickVibrate(true);
				}
    		});
    		m_textBalloon.setOnShowListener(new OnShowListener() {
				@Override
				public void onShow(DialogInterface dialog) {
					clickVibrate(true);
				}
    		});
    	}
    	return(m_textBalloon);
    }
    
    /**
     * テキスト表示バルーンを隠す.
     */
    private void hideBalloon() {
    	if( m_textBalloon != null && m_textBalloon.isShowing() ) {
			m_tsv.clearAll();
    		m_textBalloon.cancel();
    	}
    }
    
    /**
     * バルーン表示.
     * @param msg
     * @param bWithSound
     */
    private void showBalloon(String words) {
		getTextBalloon().setText(words);
		// ダイアログが出てなければだす
		if( getTextBalloon().isShowing() == false ) {
			// 文字列が長いときに勝手に閉じてしまうとよくないのでやめにする。
			//getTextBalloon().setAutoDismissTime(0);//0:デフォルトでよろ
			getTextBalloon().show();
		}
    }
    
	 /**
	  * 入力された文字を連結して返す.
	  * @param bIsSpeech
	  * @return
	  */
	public String getInputedString(boolean bIsSpeech) {
		return(null);
	}
	
	/**
	 * 入力しているTmTenjiViewを生成する.
	 */
	protected void slideTenjiViews() {
		TmTenjiView tv = new TmTenjiView(getContext());
		tv.setVisibility(View.VISIBLE);
		tv.setLocale(m_locale);
		tv.setConverter(m_converter);
		tv.setBackgroundColor(Color.BLACK);
		
		// 8/2形式か222形式か
		if( m_brailleInputView instanceof TmSmartBrailleView ) {
			tv.setShowNumber(false);
			tv.setDrawStyle222();
		} else {
			tv.setShowNumber(true);
			tv.setDrawStyle82();
		}
		
		if( m_bInputLP == null ) {
			int h = getHeight() - m_brailleInputView.getHeight();
			//int w = getWidth() / 2;
			int w = (int)(h * 0.8f);
			
			// Bug29 画面サイズによっては点字下の墨字が見えなくなる
			// コメントアウトする。
			//if( w < h ) {
			//	// 縦横比は1.25ぐらいがちょうどよい
			//	h = (int)(w * 1.25f);
			//}
			m_bInputLP = new LinearLayout.LayoutParams(w, h);
		}
		
		tv.setLayoutParams(m_bInputLP);
		
		// TmBrailleをリストでつなげる
		if( m_lastTenjiView != null ) {
			m_lastTenjiView.append(tv);
		}
		// 最後を覚えておく
		m_lastTenjiView = tv;
		
		// 先頭を覚えておく
		if( m_firstTenjiView == null ) {
			m_firstTenjiView = tv;
		}
		
		m_tenjiHorizonL.addView(tv);
		
		// 右までスクロール
		scrollRight();
	}
	
	/**
	 * 最後のTenjiViewを削除する.
	 */
	public void removeLastTenjiView() {
		if( m_lastTenjiView == null ) {
			return;
		}
		
		// HorizontalLayoutから削除
		m_tenjiHorizonL.removeView(m_lastTenjiView);
		m_tenjiHorizonScrollV.fullScroll(View.FOCUS_RIGHT);
		
		// TenjiViewのリストから削除
		TmTenjiView prev = m_lastTenjiView.prev();
		m_lastTenjiView.remove();
		
		// 一個前を最後にする
		m_lastTenjiView = prev;
		// 最後の一個になったら先頭もnull
		if( m_lastTenjiView == null ) {
			m_firstTenjiView = null;
		} else {
			// m_lastTenjiViewが一個前のTenjiViewになった。
			// この文字が変化する必要がある可能性があるので
			// 入力確定イベントを発行する
			m_lastTenjiView.onInputCompleted();
		}
	}
	
	// layouterが回ってなくて、最終ページがわからず、
	// 最右端までスクロールできないのでhandlerで遅延。
	private void scrollRight() {
		m_handler.sendEmptyMessage(c_SCROLL_TO_RIGHT);
	}
	
	private int c_SCROLL_TO_RIGHT = 1;
	private Handler m_handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if( msg.what == c_SCROLL_TO_RIGHT ) {
				m_tenjiHorizonScrollV.fullScroll(View.FOCUS_RIGHT);
			}
		}
	};
	
	/**
	 * TextToSpeechが利用可能になったときにしゃべる.
	 * @param speechText
	 */
	public void setWellcomeSpeech(String speechText) {
		if( m_wellComeSpeech != null && speechText != null ) {
			m_wellComeSpeech = m_wellComeSpeech + " " + speechText;
		} else {
			m_wellComeSpeech = speechText;
		}
	}
	
	/**
	 * 画面に現れたとき.
	 */
	private boolean m_beforeOnAttachedToWindow = true;
	
	@Override
	protected void onAttachedToWindow() {
		// ここでの処理は、一生に一回だけにしたい
		if( m_beforeOnAttachedToWindow == false ) {
			Log.d("TmIntegratedIppitu8p2rCommonl", "already onAttachedToWindow. return");
			return;
		}
		m_beforeOnAttachedToWindow = false;
		
    	// 自分自身の設定
    	this.setOrientation(LinearLayout.VERTICAL);
        this.setGravity(Gravity.CENTER);
        this.setBackgroundColor(Color.BLACK);
        
        Context ctx = getContext();
        
        // HorizontalScrollView
        // 点字Viewを横方向にスクロールするためのHorizontalScrollView
        m_tenjiHorizonScrollV = new HorizontalScrollView(ctx);
        m_tenjiHorizonScrollV.setLayoutParams(new LinearLayout.LayoutParams(
        		ViewGroup.LayoutParams.FILL_PARENT, 
        		ViewGroup.LayoutParams.WRAP_CONTENT,
        		1.0f)); // weightを上げることでこのViewの余白を多く取る。
        m_tenjiHorizonScrollV.setBackgroundColor(Color.BLACK);
        this.addView(m_tenjiHorizonScrollV);
        //this.addView(m_tenjiHorizonScrollV, new LinearLayout.LayoutParams(
        //		ViewGroup.LayoutParams.FILL_PARENT, 
        //		ViewGroup.LayoutParams.FILL_PARENT));
        
        // LinearLayout
        // 点字Viewを横に詰め込むLinearLayout
        m_tenjiHorizonL = new LinearLayout(ctx);
        m_tenjiHorizonL.setOrientation(LinearLayout.HORIZONTAL);
        m_tenjiHorizonL.setLayoutParams(new LinearLayout.LayoutParams(
        		ViewGroup.LayoutParams.FILL_PARENT, 
        		ViewGroup.LayoutParams.FILL_PARENT));
        m_tenjiHorizonL.setBackgroundColor(Color.BLACK);
        m_tenjiHorizonScrollV.addView(m_tenjiHorizonL);
        
        // 点字入力Viewを作る
        if( m_is82style ) {
        	//m_brailleInputView = createTmIppitsu8p2rView(ctx);
        } else {
        	m_brailleInputView = createTmSmartBrailleView(ctx);
        }
        this.addView(m_brailleInputView);
        
        m_tts = new TextToSpeech(ctx, this);
        
        // セットされている点字列を表示する
        m_handler.post(new Runnable() {
			@Override
			public void run() {
				appendBrailleArray(m_initBrailles);
			}
		});
    }
	
	/**
	 * TmIppitsu8p2rViewを作る.
	 * @param ctx
	 * @return
	 */
	private TmIppitsu8p2rView createTmIppitsu8p2rView(Context ctx) {
		TmIppitsu8p2rView i8pc = new TmIppitsu8p2rView(ctx);
        i8pc.setLayoutParams(new LinearLayout.LayoutParams(
        		ViewGroup.LayoutParams.FILL_PARENT, 
        		ViewGroup.LayoutParams.FILL_PARENT,
        		0.0f)); // weight 0.0f
        i8pc.setListener(m_ippitsuListener);
        i8pc.setChokkeiScaling(getCenterChokkei());
        return(i8pc);
	}
	
	/**
	 * TmSmartBrailleViewを作る.
	 * @param ctx
	 * @return
	 */
	private TmSmartBrailleView createTmSmartBrailleView(Context ctx) {
		TmSmartBrailleView smartBrailleView = new TmSmartBrailleView(ctx);
		
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(
        		ViewGroup.LayoutParams.FILL_PARENT, 
        		ViewGroup.LayoutParams.FILL_PARENT,
        		0.0f); // weight 0.0f
		smartBrailleView.setLayoutParams(llp);
		//smartBrailleView.setBackgroundColor(Color.YELLOW);
		smartBrailleView.setListener(m_ippitsuListener);
        return(smartBrailleView);
	}
	
	
	/**
	 * TenjiConverterのセット
	 * @param converter
	 */
	protected void setTenjiConverter(TmTenjiConverterImpl converter) {
		m_converter = converter;
	}
	
	/**
	 * 外部クラスからのIppitsuリスナの登録.
	 * @param listener
	 */
	public void setPubIppitsuListener(IIppitsuListener listener) {
		m_pubIppitsuListener = listener;
	}
	
    private void shutdownTTS() {
        // Don't forget to shutdown!
        if (m_tts != null) {
            m_tts.stop();
            m_tts.shutdown();
            m_tts = null;
        }
    }
    
    @Override
   	protected void onDetachedFromWindow() {
    	//android.os.Debug.stopMethodTracing();
    	if( m_tsv != null ) {
    		TmLog.e("m_tsv is not null.");
    	}
    	if( m_tts != null ) {
    		TmLog.e("m_tts is not null.");
    	}
        super.onDetachedFromWindow();
    }
    
    /**
     * もう使用しないとき呼ぶ.
     */
    public void finish() {
    	shutdownTTS();

    	if( m_tsv != null ) {
    		m_tsv.clearAll();
    		m_tsv.finish();
    		m_tsv = null;
    	}
    }

    
    // Implements TextToSpeech.OnInitListener.
    @Override
   	public void onInit(int status) {
    	Log.d("onInit", "status=" + status);
    	if (status == TextToSpeech.SUCCESS) {
    		m_ttsStatus = true;
    		
    		// いろいろ頑張ったが、N2TTSとPicoとでの切り替えはできなかった
    		//setTtsEngine(getLocale());
        	if( m_wellComeSpeech != null ) {
        		speechText(m_wellComeSpeech, c_WORDS_SPEECH_RATE);
        	}
        } else {
            Log.e("onInit", "Could not initialize TextToSpeech.");
            shutdownTTS();
        }
        
    }
    
	protected static SharedPreferences getAppSharedPreferences(Context ctx) {
		Context appCtx = ctx.getApplicationContext();
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(appCtx);
		return(sp);
	}
    
	/**
	 * 言語の設定.
	 * @param locale
	 */
	public void setLocale(Locale locale) {
		m_locale = locale;
		
		setConfiguration(locale);
		
		// 点字は全部クリア
		allClear(true);
	}
	
	/**
	 * 言語の取得.
	 * @return
	 */
	protected Locale getLocale() {
		return(m_locale);
	}
	
	/**
	 * Configrationの設定.
	 * @param locale
	 */
	private void setConfiguration(Locale locale) {
        Configuration conf = getResources().getConfiguration();
        conf.locale = locale;
        getResources().updateConfiguration(conf, null);
	}
	
	/**
	 * 中心円の拡大縮小率の取得.
	 * @return
	 */
	private static final String c_KEY_CIRCLE_SCALING = "centerCircleScaling";
	public float getCenterChokkei() {
		if( m_centerCircleScaling == 0.0f ) {
			SharedPreferences sp = getAppSharedPreferences(getContext());
			m_centerCircleScaling = sp.getFloat(c_KEY_CIRCLE_SCALING, 1.0f);
		}
		return(m_centerCircleScaling);
	}
	
	/**
	 * 中心円の拡大縮小率の設定.
	 * @param scale
	 */
	/***
	public void setCenterChokkei(float scale) {
		m_centerCircleScaling = scale;
		((TmIppitsu8p2rView)m_brailleInputView).setChokkeiScaling(scale);
		
		// 保管
		SharedPreferences sp = getAppSharedPreferences(getContext());
		Editor ed = sp.edit();
		ed.putFloat(c_KEY_CIRCLE_SCALING, scale);
		ed.commit();
		
	}
	***/
	
    /**
     * 中心の円の大きさを設定するダイアログの表示.
     */
    private float m_tempCenterCircleSize = 0.0f;
    public void showSetCenterCircleSizeDialog() {
    	AlertDialog.Builder dlg = new AlertDialog.Builder(getContext());
    	final String[] str_items = getResources().getStringArray(R.array.ListCenterCircleSize);
    	final float[] val_items = {1.1f, 1.0f, 0.9f, 0.8f};
    	int def_index = 1;
    	
    	// デフォルト値を探す
    	float curVal = getCenterChokkei();
    	for( int i = 0; i < val_items.length; i++ ) {
    		if( curVal == val_items[i] ) {
    			def_index = i;
    		}
    	}
    	
    	m_tempCenterCircleSize = 0.0f;
    	//dlg.setIcon(R.drawable.icon);
    	dlg.setTitle("Please Select Circle Size");
    	dlg.setSingleChoiceItems(str_items, def_index, 
    			new DialogInterface.OnClickListener(){
    				public void onClick(DialogInterface dialog, int which) {
    					if( which + 1 > val_items.length || which + 1 > str_items.length) {
    						speechText("index error", c_WORDS_SPEECH_RATE);
    						return;
    					}
    					m_tempCenterCircleSize = val_items[which];
    					Log.d("SetCenterCircleSizeDialog", "selected = " + m_tempCenterCircleSize);
    					speechText(str_items[which], c_WORDS_SPEECH_RATE);
    				}
    	});
    	
    	dlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int whichButton) {
    			/* OKボタンをクリックした時の処理 */
				Log.d("SetCenterCircleSizeDialog", "OK");
				if( m_tempCenterCircleSize > 0.0f ) {
					Log.d("SetCenterCircleSizeDialog", "center size is " + m_tempCenterCircleSize);
					//setCenterChokkei(m_tempCenterCircleSize);
					speechText(getContext().getString(R.string.ACTok), c_WORDS_SPEECH_RATE);
    			} else {
					speechText(getContext().getString(R.string.ACTokButNothingToChange), c_WORDS_SPEECH_RATE);
    			}
    		}
    	});
    	
    	dlg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int whichButton) {
    			/* Cancel ボタンをクリックした時の処理 */
				Log.d("SetCenterCircleSizeDialog", "Canceled");
				speechText(getContext().getString(R.string.ACTcancel), c_WORDS_SPEECH_RATE);
    		}
    	});
    	
    	speechText(getContext().getString(R.string.ACTcenterCircleSizeSetupOpened), c_WORDS_SPEECH_RATE);
    	dlg.show();
    }
    
    
    /**
     * 点字の左右二個を振動する.
     * @param left
     * @param right
     */
	private synchronized void vibrate2points(boolean left, boolean right) {
		m_tsv.play2PointsVibrate(left, right, true);
	}
	
	/**
	 * 点字一個を左右で鳴らす.
	 * @param left
	 * @param right
	 */
	private synchronized void vibrateBraille(TmBraille b) {
		m_tsv.addDefaultWait();
		m_tsv.addBraille(b);
		m_tsv.playDataAsync();
	}
	
	/**
	 * 一文字入力終了時振動.
	 */
	private synchronized void endOfInputVibrate() {
		m_tsv.playBreakVibrate();
	}
	
	/**
	 * BrailleSet配列を再生.
	 * @param bset
	 */
	public void playBrailleSetArr(ArrayList<BrailleSet> bset) {
		if( bset == null || bset.size() == 0 ) {
			TmLog.d("no message.");
			return;
		}
		
		m_tsv.addBrailleSetArray(bset);
		m_tsv.playDataAsync();
		
		// for debug
		if( LogUtil.isDebuggable(getContext()) ) {
			StringBuilder sb = new StringBuilder(128);
    		for( BrailleSet bs : bset ) {
    			sb.append(bs.getDebugString() + " ");
    			}
    		Log.d("playBraiile", "8/2=" + sb.toString());
		}
	}
	
    /**
     * ブルっと.
     * @param isOk
     */
	private static final int c_CLICK_VIBRATE_OK_TIME	= 100;
	private static final int c_CLICK_VIBRATE_NG_TIME	= 1600;	
	public synchronized void clickVibrate(boolean isOk) {
		boolean enableVibrate = GuideSetting.queryEnableVibrate(getContext());
		if( enableVibrate == false ) {
			return;
		}
		if( isOk ) {
			m_tsv.addSingleSoundVibrate(c_CLICK_VIBRATE_OK_TIME);
			m_tsv.addDefaultWait();
		} else {
			m_tsv.addSingleSoundVibrate(c_CLICK_VIBRATE_NG_TIME);
			m_tsv.addDefaultWait();
		}
		m_tsv.playDataAsync();
	}
	
    /**
     * しゃべる.
     * @param num
     */
	public static final float c_NUMBER_SPEECH_RATE = 1.8f;
	public static final float c_WORDS_SPEECH_RATE = 1.0f;
    public void speechText(String text, float speechRate) {
    	speechSub(text, speechRate, TextToSpeech.QUEUE_FLUSH);
    }
    public void speechTextAppend(String text, float speechRate) {
    	speechSub(text, speechRate, TextToSpeech.QUEUE_ADD);
    }
    
    private void speechSub(String text, float rate, int mode) {
    	// 音声出力メインスイッチの確認
    	if( !SettingsActivity.GuideSetting.queryEnableSpeechMainSwitch(getContext()) ) {
    		return;
    	}
    	if( m_ttsIgnore ) {
    		return;
    	}
    	if( text == null || text.length() == 0 ) {
    		return;
    	}
    	if( m_tts != null && m_ttsStatus ) {
    		if( mode == TextToSpeech.QUEUE_FLUSH ) {
    			if( m_tts.isSpeaking() ) {
    				m_tts.stop();
    			}
    		}
    		m_tts.setSpeechRate(rate);
    		
    		// ADDモードのときは、改行ごとにADDしていく。遅いから。
    		if( mode == TextToSpeech.QUEUE_ADD ) {
    			String[] arr = text.split(c_CR);
    			for( String elm : arr ) {
    				m_tts.speak(elm, mode, null);
    			}
    		} else {
    			m_tts.speak(text, mode, null);
    		}
    	} else {
    		setWellcomeSpeech(text);
    	}
    }
    private static final String	c_CR = "\n";

    ////////////////////////////////////////////////////////////////////////////
    // test code
    ////////////////////////////////////////////////////////////////////////////
    protected class Test {
    	protected class Data {
    		public Data(String name, int[][] brailles, String answer) {
    			m_name = name;
    			m_brailles = brailles;
    			m_answer = answer;
    		}
    		public String		m_name;
    		public int[][]	m_brailles;
    		public String		m_answer;
    	}
    	
    	public Test() {}
        
    	protected void addTest(Data dt) {
    		m_dataList.add(dt);
    	}
    	
    	// テストする
        protected void doTest(Data dt) {
        	allClear(true);
        	int[][] arr = dt.m_brailles;
        	for( int[] br : arr ) {
        		slideTenjiViews();
        		getLastTenjiView().setLeftSide(br[0]);
        		getLastTenjiView().setRightSide(br[1]);
        		getLastTenjiView().onInputCompleted();
        	}
        	String str = getInputedString(true);
        	
        	String answere;
        	if( dt.m_answer == null ) {
        		answere = dt.m_name;
        	} else {
        		answere = dt.m_answer;
        	}
        	
        	if( str.compareTo(answere) == 0 ) {
        		Log.d("Test OK", str);
        	} else {
        		Log.e("Test NG STR", "[" + str + "]");
        		Log.e("     ANSWER", "[" + answere + "]");
        	}
        	
        }
        
        /**
         * テストダイアログを表示する
         */
        public void openTestDialog() {
       		String[] ITEM = new String[m_dataList.size()];
       		int i = 0;
       		for( Data dt : m_dataList ) {
       			ITEM[i++] = dt.m_name;
       		}
       			
       		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
       	   			@Override
       	   			public void onClick(DialogInterface dialog, int which) {
       	   				doTest(m_dataList.get(which));
       	   			}
       			};
       		
       		new AlertDialog.Builder(getContext())
       		.setTitle("Select test")
       		.setItems(ITEM, listener)
      		.create()
      		.show();
        }
    	
    	ArrayList<Data> m_dataList = new ArrayList<Data>(50);
    }
    
    /**
     * テストダイアログを表示する.
     */
    public void openTestDialog() {
    }
}

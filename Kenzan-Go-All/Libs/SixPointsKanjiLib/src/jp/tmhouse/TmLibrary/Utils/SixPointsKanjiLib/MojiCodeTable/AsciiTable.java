/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable;

import java.util.HashMap;

import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;

public class AsciiTable extends IMojiCodeTable {
	private static HashMap<BrailleSet, String> m_brailleHash;
	private static HashMap<String, BrailleSet> m_stringHash;
	private static final String	c_MODENAME =  "英字モード";
	private static final BrailleSet c_beginFu = new BrailleSet(64, null);
	private static final BrailleSet c_endFu = new BrailleSet(46, null);
	
	@Override
	public String get(BrailleSet brailleSet, boolean bCapsLock) {
		String str = m_brailleHash.get(brailleSet);
		
		// capslockが利いてるときは大文字にする
		if( str != null && bCapsLock ) {
			str = str.toUpperCase();
		}
		return(str);
	}
	
	/**
	 * 強制的に1byteで検索する.
	 * @param moji
	 * @return
	 */
	@Override
	public BrailleSet get(String moji) {
		String s = to1Byte(moji);
		BrailleSet bs = m_stringHash.get(s);
		return(bs);
	}
	
	@Override
	public String getModeName() {
		return(c_MODENAME);
	}

	@Override
	public int getMaxLength(BrailleSet curBrailleSet) {
		return 2;
	}
	
	@Override
	public boolean isInvalid(int left, int right) {
		return(false);
	}

	/**
	 * ASCIIデータの生成.
	 * @param brailleHash
	 */
	public static void create() {
		putBrailleSet(new BrailleSet(0," "));
		putBrailleSet(new BrailleSet(4,10,"A"));
		putBrailleSet(new BrailleSet(4,30,"B"));
		putBrailleSet(new BrailleSet(4,11,"C"));
		putBrailleSet(new BrailleSet(4,13,"D"));
		putBrailleSet(new BrailleSet(4,12,"E"));
		putBrailleSet(new BrailleSet(4,31,"F"));
		putBrailleSet(new BrailleSet(4,33,"G"));
		putBrailleSet(new BrailleSet(4,32,"H"));
		putBrailleSet(new BrailleSet(4,21,"I"));
		putBrailleSet(new BrailleSet(4,23,"J"));
		putBrailleSet(new BrailleSet(4,50,"K"));
		putBrailleSet(new BrailleSet(4,70,"L"));
		putBrailleSet(new BrailleSet(4,51,"M"));
		putBrailleSet(new BrailleSet(4,53,"N"));
		putBrailleSet(new BrailleSet(4,52,"O"));
		putBrailleSet(new BrailleSet(4,71,"P"));
		putBrailleSet(new BrailleSet(4,73,"Q"));
		putBrailleSet(new BrailleSet(4,72,"R"));
		putBrailleSet(new BrailleSet(4,61,"S"));
		putBrailleSet(new BrailleSet(4,63,"T"));
		putBrailleSet(new BrailleSet(4,54,"U"));
		putBrailleSet(new BrailleSet(4,74,"V"));
		putBrailleSet(new BrailleSet(4,27,"W"));
		putBrailleSet(new BrailleSet(4,55,"X"));
		putBrailleSet(new BrailleSet(4,57,"Y"));
		putBrailleSet(new BrailleSet(4,56,"Z"));
		
		putBrailleSet(new BrailleSet(10,"a"));
		putBrailleSet(new BrailleSet(30,"b"));
		putBrailleSet(new BrailleSet(11,"c"));
		putBrailleSet(new BrailleSet(13,"d"));
		putBrailleSet(new BrailleSet(12,"e"));
		putBrailleSet(new BrailleSet(31,"f"));
		putBrailleSet(new BrailleSet(33,"g"));
		putBrailleSet(new BrailleSet(32,"h"));
		putBrailleSet(new BrailleSet(21,"i"));
		putBrailleSet(new BrailleSet(23,"j"));
		putBrailleSet(new BrailleSet(50,"k"));
		putBrailleSet(new BrailleSet(70,"l"));
		putBrailleSet(new BrailleSet(51,"m"));
		putBrailleSet(new BrailleSet(53,"n"));
		putBrailleSet(new BrailleSet(52,"o"));
		putBrailleSet(new BrailleSet(71,"p"));
		putBrailleSet(new BrailleSet(73,"q"));
		putBrailleSet(new BrailleSet(72,"r"));
		putBrailleSet(new BrailleSet(61,"s"));
		putBrailleSet(new BrailleSet(63,"t"));
		putBrailleSet(new BrailleSet(54,"u"));
		putBrailleSet(new BrailleSet(74,"v"));
		putBrailleSet(new BrailleSet(27,"w"));
		putBrailleSet(new BrailleSet(55,"x"));
		putBrailleSet(new BrailleSet(57,"y"));
		putBrailleSet(new BrailleSet(56,"z"));
		
		
		/*****
		//////////////////
		// Symbols(English Braille Grade1)
		//////////////////
		putBrailleSet(new BrailleSet(40,"'"));
		putBrailleSet(new BrailleSet(26,"."));
		putBrailleSet(new BrailleSet(20,","));
		putBrailleSet(new BrailleSet(60,";"));
		putBrailleSet(new BrailleSet(62,"!"));
		putBrailleSet(new BrailleSet(64,"?"));
		putBrailleSet(new BrailleSet(44,"-"));
		putBrailleSet(new BrailleSet(22,":"));
		putBrailleSet(new BrailleSet(41,"/"));
		putBrailleSet(new BrailleSet(1, 26,"$"));
		putBrailleSet(new BrailleSet(4, 64,"`"));
		putBrailleSet(new BrailleSet(42, 42,"*"));
		putBrailleSet(new BrailleSet(5,"."));// 小数点
		****/
		
		// ●カッコ系の処理について
		// 外国語引用符の開始終了と同じ64,46を含む符号が出てくるのが
		// 問題である。
		// - 64, 46を含む符号には何があるか？
		//   64:ビックリマーク、ダブルクオート開始、
		//      6の点前置でシングルクオート開始
		//   46:外国語引用符終了、ダブルクオート終了、
		//      6の点前置でシングルクオート終了
		// <考察>
		// 6の点前置のケースは、単にその文字を出力すればOK。
		// 解決すべき問題は３つ。
		// 問題1. ダブルクオート開始とビックリマークの区別。
		//        64+マスアケでビックリマークとするか。それ以外は
		//        ダブルクオート。
		// 問題2. ダブルクオート終了と外国語引用符の終了の区別。
		//        64が現れたら、クオートスタックを1個push。
		//        46が現れたら、クオートスタックが0ならば
		//        外国語引用符終了、そうじゃなきゃダブルクオート閉じ
		//        で、スタックをpop。
		// 問題3. シングルクオートの終了は46+40だから、46が出てきた
		//        ときは一文字先読みして40だったら消化しちゃえ。
		//        クオートの種別については、開いたときとの整合性を
		//        チェックしない。つまり、ダブルクオートが開いていた
		//        とき、シングルクオートで閉じたければ閉じられる。
		//        制約としては、開いたクオートは何らかのクオートで閉じ
		//        なければならない、こと。これは外国語引用符中のクオート
		//        系符号の定義が外国語引用符の開始終了と同じだから
		//        もたらす問題で、仕方ない。
		
		// 情報処理用点字の符号
		// http://homepage2.nifty.com/a_sato/tenji-room/fullkey/jyohou.html#anchor113422
		putBrailleSet(new BrailleSet(62, "!"));
		putBrailleSet(new BrailleSet(66, "\""));
		putBrailleSet(new BrailleSet(15, "#"));
		putBrailleSet(new BrailleSet(17, "$"));
		putBrailleSet(new BrailleSet(37, "%"));
		putBrailleSet(new BrailleSet(75, "&"));
		putBrailleSet(new BrailleSet(40, "'"));
		putBrailleSet(new BrailleSet(64, "(")); // 開始符号と同じで最悪
		putBrailleSet(new BrailleSet(46, ")")); // 終了符号と同じで最悪
		putBrailleSet(new BrailleSet(14, "*"));
		putBrailleSet(new BrailleSet(45, "+"));
		putBrailleSet(new BrailleSet(20, ","));
		putBrailleSet(new BrailleSet(44, "-"));
		putBrailleSet(new BrailleSet(26, "."));
		putBrailleSet(new BrailleSet(41, "/"));
		putBrailleSet(new BrailleSet(2, 20, ":"));
		putBrailleSet(new BrailleSet(60, ";"));
		putBrailleSet(new BrailleSet(42, 42, "<"));
		putBrailleSet(new BrailleSet(22, 22, "="));
		putBrailleSet(new BrailleSet(24, 24, ">"));
		putBrailleSet(new BrailleSet(2, 64, "?"));
		putBrailleSet(new BrailleSet(25, "@"));
		putBrailleSet(new BrailleSet(76, "["));
		putBrailleSet(new BrailleSet(35, "\\"));
		putBrailleSet(new BrailleSet(67, "]"));
		putBrailleSet(new BrailleSet(3, "^"));
		putBrailleSet(new BrailleSet(2, 44, "_"));
		putBrailleSet(new BrailleSet(2, 12, "`"));
		putBrailleSet(new BrailleSet(34, "{"));
		putBrailleSet(new BrailleSet(36, "|"));
		putBrailleSet(new BrailleSet(43, "}"));
		putBrailleSet(new BrailleSet(2, 11, "~"));
		// 行継続の意味。空白でgetしたとき当たらないように""にする。
		putBrailleSet(new BrailleSet(1, ""));
	}
	
	private static void putBrailleSet(BrailleSet bset) {
		m_brailleHash.put(bset, bset.getString());
		m_stringHash.put(bset.getString(), bset);
	}
	
	
	/**
	 * static処理
	 */
	static {
		m_brailleHash = new HashMap<BrailleSet, String>(127);
		m_stringHash = new HashMap<String, BrailleSet>(127);
		create();
	}


	@Override
	public BrailleSet getBeginFu() {
		return(c_beginFu);
	}

	@Override
	public BrailleSet getEndFu() {
		return(c_endFu);
	}

	@Override
	public boolean isBeginFu(BrailleSet bs) {
		return(getBeginFu().equals(bs));
	}

	@Override
	public boolean isEndFu(BrailleSet bs) {
		return(getEndFu().equals(bs));
	}
	
	/**
	 * 大文字符を返す.
	 * @return
	 */
	public static final BrailleSet getOomojiFu() {
		return(c_oomojiFu);
	}

	/**
	 * 大文字連続符を返す.
	 * @param bs
	 * @return
	 */
	public static final BrailleSet getOomojiRenzokuFu() {
		return(c_capsLockFu);
	}
	
	/**
	 * 大文字連続符終了符を返す.
	 * ようはマスアケ.
	 * @return
	 */
	public static final BrailleSet getOomojiRenzokuOffFu() {
		return(c_capsLockOffFu);
	}

	/**
	 * "AAa"などのように、大文字連続の後に小文字を空白なしに接続
	 * したいときに入れる符号.
	 * @return
	 */
	public static final BrailleSet getOomojiRenzokuTunagiFu() {
		return(c_oomojiRenzokuTunagiFu);
	}
	
	private static final BrailleSet	c_oomojiFu = new BrailleSet(4, null);
	private static final BrailleSet	c_capsLockFu = new BrailleSet(4,4, null);
	private static final BrailleSet	c_capsLockOffFu = new BrailleSet(0, null);
	private static final BrailleSet	c_oomojiRenzokuTunagiFu = new BrailleSet(6, null);
}

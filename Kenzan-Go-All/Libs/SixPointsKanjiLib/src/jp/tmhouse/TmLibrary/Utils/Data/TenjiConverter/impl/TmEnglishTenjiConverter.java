/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.Utils.Data.TenjiConverter.impl;

import java.util.HashMap;
import android.util.Log;
import jp.tmhouse.TmLibrary.Utils.Data.TmBraille;
import jp.tmhouse.TmLibrary.Utils.Data.TenjiConverter.TmTenjiConverterImpl;

/**
 * ８進数２個から該当する日本語を取得する.
 * @author mutoh
 *
 */
public class TmEnglishTenjiConverter extends TmTenjiConverterImpl {
	// 前置符号(SignCode)
	private static int c_SIGN_DOT5		= 1;
	private static int c_SIGN_DOT45		= 2;
	private static int c_SIGN_DOT456		= 3;
	// Final-letter Contractions
	private static int c_SIGN_DOT46		= 4;
	private static int c_SIGN_DOT56		= 5;
	private static int c_SIGN_DOT6		= 6;
	private static int c_SIGN_FINAL_CONV	= 7;//単語の後ろの縮字。本来DOT6だが、大文字符と同じなので条件で分岐
	private static int c_SIGN_NUMBER		= 8;
	private static int c_SIGN_LAST		= 9;
	
	// lowerDataの種類
	private static int c_LOWER_WORD_INNER		= 0;
	private static int c_LOWER_WORD_HEAD		= 1;
	private static int c_LOWER_WORD_TAIL		= 2;
	private static int c_LOWER_WORD_SINGLE	= 3;
	
	// 英語
	private static String[][][]	m_EN1Data = new String[c_SIGN_LAST][8][8];
	private static String[][][]	m_ENlowerData = new String[4][8][8];
	private static int[][] 		m_ENSignData = new int[8][8];
	private static boolean 		m_ENHiPriorityBralles[][] = new boolean[8][8];
	private static HashMap<String, String> m_shortFormWords;
	
	private static String c_SPACE = " ";
	
	static {
		initSignData();
		initSimpleLetters();
		initInitialLetterContractions();
		initFinalLetterContractions();
		initLargeLetters();
		initLowerWordsContractions();
		initNumbers();
		initShortFormWords();
	}
	
	private static void initSignData() {
		m_ENSignData[0][2] = c_SIGN_DOT5;
		m_ENSignData[0][3] = c_SIGN_DOT45;
		m_ENSignData[0][7] = c_SIGN_DOT456;
		
		// Final-letter Contractions
		m_ENSignData[0][5] = c_SIGN_DOT46;
		m_ENSignData[0][6] = c_SIGN_DOT56;
		m_ENSignData[0][4] = c_SIGN_DOT6;
		
		// 数符はbleと#と同じ。文脈依存。
		//m_ENSignData[4][7] = c_SIGN_NUMBER;
	}
	
	/**
	 * 単語中に現れた場合.
	 */
	private static void initSimpleLetters() {
		int z = c_SIGN_NOT_SIGN;
		m_EN1Data[z][0][0] = " ";
		m_EN1Data[z][1][0] = "a";
		m_EN1Data[z][3][0] = "b";
		m_EN1Data[z][1][1] = "c";
		m_EN1Data[z][1][3] = "d";
		m_EN1Data[z][1][2] = "e";
		m_EN1Data[z][3][1] = "f";
		m_EN1Data[z][3][3] = "g";
		m_EN1Data[z][3][2] = "h";
		m_EN1Data[z][2][1] = "i";
		m_EN1Data[z][2][3] = "j";
		m_EN1Data[z][5][0] = "k";
		m_EN1Data[z][7][0] = "l";
		m_EN1Data[z][5][1] = "m";
		m_EN1Data[z][5][3] = "n";
		m_EN1Data[z][5][2] = "o";
		m_EN1Data[z][7][1] = "p";
		m_EN1Data[z][7][3] = "q";
		m_EN1Data[z][7][2] = "r";
		m_EN1Data[z][6][1] = "s";
		m_EN1Data[z][6][3] = "t";
		m_EN1Data[z][5][4] = "u";
		m_EN1Data[z][7][4] = "v";
		m_EN1Data[z][2][7] = "w";
		m_EN1Data[z][5][5] = "x";
		m_EN1Data[z][5][7] = "y";
		m_EN1Data[z][5][6] = "z";
		
		// 優先5単語
		initHiPriorityBralleTable();
		
		m_EN1Data[z][7][5] = "and";
		m_EN1Data[z][7][7] = "for";
		m_EN1Data[z][7][6] = "of";
		m_EN1Data[z][6][5] = "the";
		m_EN1Data[z][6][7] = "with";
	}
	
	/**
	 * 優先５単語検査テーブル.
	 */
	private static void initHiPriorityBralleTable() {
		m_ENHiPriorityBralles[7][5] = true; // and
		m_ENHiPriorityBralles[7][7] = true; // for
		m_ENHiPriorityBralles[7][6] = true; // of
		m_ENHiPriorityBralles[6][5] = true; // the
		m_ENHiPriorityBralles[6][7] = true; // with
	}
	
	/**
	 * マスあけなしで現れても良い単語か.
	 * 優先5単語か、不定冠詞ならばマスあけしないで現れても良いから。
	 * @param braille
	 * @return
	 */
	private boolean isHiPriorityWord(TmBraille braille) {
		boolean ret = false;
		if( braille != null && !braille.isCleared() ) { 
			// 1.優先5単語テーブルを引いてみる
			int left = braille.getLeft();
			int right = braille.getRight();
			ret = m_ENHiPriorityBralles[left][right];
		}
		return(ret);
	}
	
	/**
	 * 不定冠詞a/anか.
	 * @param braille
	 * @return
	 */
	private boolean isIndefiniteArticle(TmBraille braille) {
		// ケース.
		// a[sp]
		// an[sp]
		// がある。
		
		if( isSpaceOrNone(braille) ) {
			return(false);
		}
		
		// 条件1. brailleは'a'であること。
		// 条件2. nextは[sp]なら、true確定。
		// 条件3. nextは'n'なら、さらにnextを調べ、[sp]ならtrue確定。
		
		// 'a'かな
		if( braille.getLeft() == 1 && braille.getRight() == 0 ) {
			TmBraille next = braille.getNext();
			if( isSpaceOrNone(next) ) {
				return(true);//条件2で確定.
			}
			// 'n'かな
			if( next.getLeft() == 5 && next.getRight() == 3 ) {
				TmBraille next_next = next.getNext();
				if( isSpaceOrNone(next_next) ) {
					return(true); //条件3で確定.
				}
			}
		}
		return(false);
	}
	
	/**
	 * 数字テーブル
	 */
	private static final void initNumbers() {
		int z = c_SIGN_NUMBER;
		m_EN1Data[z][1][0] = "1";
		m_EN1Data[z][3][0] = "2";
		m_EN1Data[z][1][1] = "3";
		m_EN1Data[z][1][3] = "4";
		m_EN1Data[z][1][2] = "5";
		m_EN1Data[z][3][1] = "6";
		m_EN1Data[z][3][3] = "7";
		m_EN1Data[z][3][2] = "8";
		m_EN1Data[z][2][1] = "9";
		m_EN1Data[z][2][3] = "0";
		
		// TODO たぶん、数符の後に続いてよい文字をここに定義しちゃえば
		// 数符の終了を決定できるのではないかなあ。
	}
	
	/**
	 * 前置符号があるケース.
	 */
	private static void initInitialLetterContractions() {
		// 5番の前置符号の後ろの場合
		int z = c_SIGN_DOT5;
		m_EN1Data[z][1][3] = "day";
		m_EN1Data[z][1][2] = "ever";
		m_EN1Data[z][3][1] = "father";
		m_EN1Data[z][3][2] = "here";
		m_EN1Data[z][5][0] = "know";
		m_EN1Data[z][7][0] = "lord";
		m_EN1Data[z][5][1] = "mother";
		m_EN1Data[z][5][3] = "name";
		m_EN1Data[z][5][2] = "one";
		m_EN1Data[z][7][1] = "part";
		m_EN1Data[z][7][3] = "question";
		m_EN1Data[z][7][2] = "right";
		m_EN1Data[z][6][1] = "some";
		m_EN1Data[z][6][3] = "time";
		m_EN1Data[z][5][4] = "under";
		m_EN1Data[z][2][7] = "work";
		m_EN1Data[z][5][7] = "young";
		m_EN1Data[z][1][4] = "character";
		m_EN1Data[z][1][7] = "through";
		m_EN1Data[z][1][6] = "where";
		m_EN1Data[z][3][6] = "ought";
		m_EN1Data[z][6][5] = "there";
		
		// 4,5番の前置符号の後ろの場合
		z = c_SIGN_DOT45;
		m_EN1Data[z][5][4] = "upon";
		m_EN1Data[z][2][7] = "word";
		m_EN1Data[z][1][7] = "those";
		m_EN1Data[z][1][6] = "whose";
		m_EN1Data[z][6][5] = "these";
		
		// 4,5,6番の前置符号の後ろの場合
		z = c_SIGN_DOT456;
		m_EN1Data[z][1][1] = "cannot";
		m_EN1Data[z][3][2] = "had";
		m_EN1Data[z][5][1] = "many";
		m_EN1Data[z][6][1] = "spirit";
		m_EN1Data[z][2][7] = "world";
		m_EN1Data[z][6][5] = "their";
	}
	
	/**
	 * 前置符号があるケースだが、単に単語の後ろの方を作るというだけ.
	 */
	private static void initFinalLetterContractions() {
		int z = c_SIGN_DOT46;
		m_EN1Data[z][1][3] = "ound";
		m_EN1Data[z][1][2] = "ance";
		m_EN1Data[z][5][3] = "sion";
		m_EN1Data[z][6][1] = "less";
		m_EN1Data[z][6][3] = "ount";
		
		z = c_SIGN_DOT56;
		m_EN1Data[z][1][2] = "ence";
		m_EN1Data[z][3][3] = "ong";
		m_EN1Data[z][7][0] = "ful";
		m_EN1Data[z][5][3] = "tion";
		m_EN1Data[z][6][1] = "ness";
		m_EN1Data[z][6][3] = "ment";
		m_EN1Data[z][5][7] = "ity";
		
		z = c_SIGN_FINAL_CONV;// 本来はc_SIGN_DOT6. 大文字の前置符号と共通。
		m_EN1Data[z][5][3] = "ation";
		m_EN1Data[z][5][7] = "ally";
	}
	
	/**
	 * 大文字
	 */
	private static void initLargeLetters() {
		int z = c_SIGN_DOT6;
		for( int i = 0; i < 8; i++ ) {
			for( int j = 0; j < 8; j++ ) {
				String word = m_EN1Data[c_SIGN_NOT_SIGN][i][j];
				if( word != null && word.length() > 0 ) {
					m_EN1Data[z][i][j] = toUpperCaseOnlyFirst(word);
				}
			}
		}
	}
	
	/**
	 * 最初の文字だけ大文字に変換する.
	 * @param str
	 * @return
	 */
	private static String toUpperCaseOnlyFirst(String str) {
		if( str != null && str.length() > 0 ) {
			str = str.substring(0, 1).toUpperCase() + str.substring(1);
		}
		return(str);
	}
	
	private static void initShortFormWords() {
		m_shortFormWords = new HashMap<String, String>(257);
		m_shortFormWords.put("ab", "about");
		m_shortFormWords.put("abv", "above");
		m_shortFormWords.put("ac", "according");
		m_shortFormWords.put("acr", "across");
		m_shortFormWords.put("af", "after");
		m_shortFormWords.put("afn", "afternoon");
		m_shortFormWords.put("afw", "afterward");
		m_shortFormWords.put("ag", "again");
		m_shortFormWords.put("agst", "against");
		m_shortFormWords.put("alm", "almost");
		m_shortFormWords.put("alr", "already");
		m_shortFormWords.put("al", "also");
		m_shortFormWords.put("alth", "although");
		m_shortFormWords.put("alt", "altogether");
		m_shortFormWords.put("alw", "always");
		m_shortFormWords.put("bec", "because");
		m_shortFormWords.put("bef", "before");
		m_shortFormWords.put("beh", "behind");
		m_shortFormWords.put("bel", "below");
		m_shortFormWords.put("ben", "beneath");
		m_shortFormWords.put("bes", "beside");
		m_shortFormWords.put("bet", "between");
		m_shortFormWords.put("bey", "beyond");
		m_shortFormWords.put("bl", "blind");
		m_shortFormWords.put("brl", "braille");
		m_shortFormWords.put("chn", "children");
		m_shortFormWords.put("fst", "first");
		m_shortFormWords.put("fr", "friend");
		m_shortFormWords.put("gd", "good");
		m_shortFormWords.put("grt", "great");
		m_shortFormWords.put("imm", "immediate");
		m_shortFormWords.put("ab", "about");
		m_shortFormWords.put("lr", "letter");
		m_shortFormWords.put("ll", "little");
		m_shortFormWords.put("mst", "must");
		m_shortFormWords.put("nec", "necessary");
		m_shortFormWords.put("o'c", "o'clock");
		m_shortFormWords.put("pd", "paid");
		m_shortFormWords.put("perh", "perhaps");
		m_shortFormWords.put("qk", "quick");
		m_shortFormWords.put("sd", "said");
		m_shortFormWords.put("tgr", "together");
		m_shortFormWords.put("cd", "could");
		m_shortFormWords.put("shd", "should");
		m_shortFormWords.put("wd", "would");
		m_shortFormWords.put("ei", "either");
		m_shortFormWords.put("nei", "neither");
		m_shortFormWords.put("mch", "much");
		m_shortFormWords.put("sch", "such");
		m_shortFormWords.put("td", "today");
		m_shortFormWords.put("tn", "tonight");
		m_shortFormWords.put("tm", "tomorrow");
		m_shortFormWords.put("hm", "him");
		m_shortFormWords.put("hmf", "himself");
		m_shortFormWords.put("xs", "its");
		m_shortFormWords.put("xf", "itself");
		m_shortFormWords.put("yr", "your");
		m_shortFormWords.put("yrf", "yourself");
		m_shortFormWords.put("yrvs", "yourselves");
		m_shortFormWords.put("herf", "herself");
		m_shortFormWords.put("myf", "myself");
		m_shortFormWords.put("onef", "oneself");
		m_shortFormWords.put("ourvs", "ourselves");
		m_shortFormWords.put("themvs", "themselves");
		m_shortFormWords.put("thyf", "thyself");
		m_shortFormWords.put("dcl", "declare");
		m_shortFormWords.put("dclg", "declaring");
		m_shortFormWords.put("rjc", "rejoice");
		m_shortFormWords.put("rjcg", "rejoicing");
		m_shortFormWords.put("concv", "conceive");
		m_shortFormWords.put("concvg", "conceiving");
		m_shortFormWords.put("dcv", "deceive");
		m_shortFormWords.put("dcvg", "deceiving");
		m_shortFormWords.put("percv", "perceive");
		m_shortFormWords.put("percvg", "perceiving");
		m_shortFormWords.put("rcv", "receive");
		m_shortFormWords.put("rcvg", "receiving");
		
		// 独自追加
		m_shortFormWords.put("dcld", "declared");
		m_shortFormWords.put("rjcd", "rejoiced");
		m_shortFormWords.put("dcvd", "deceived");
		m_shortFormWords.put("percvd", "perceived");
		m_shortFormWords.put("rcvd", "received");
		m_shortFormWords.put("rcvr", "receiver");
		
		// 一文字単体での略字
		m_shortFormWords.put("b", "but");
		m_shortFormWords.put("c", "can");
		m_shortFormWords.put("d", "do");
		m_shortFormWords.put("e", "every");
		m_shortFormWords.put("f", "from");
		m_shortFormWords.put("g", "go");
		m_shortFormWords.put("h", "have");
		m_shortFormWords.put("j", "just");
		m_shortFormWords.put("k", "knowledge");
		m_shortFormWords.put("l", "like");
		m_shortFormWords.put("m", "more");
		m_shortFormWords.put("n", "not");
		m_shortFormWords.put("p", "people");
		m_shortFormWords.put("q", "quite");
		m_shortFormWords.put("r", "rather");
		m_shortFormWords.put("s", "so");
		m_shortFormWords.put("t", "that");
		m_shortFormWords.put("u", "us");
		m_shortFormWords.put("v", "very");
		m_shortFormWords.put("w", "will");
		m_shortFormWords.put("x", "it");
		m_shortFormWords.put("y", "you");
		m_shortFormWords.put("z", "as");
	}
	
	
	// lower文字のテーブル
	
	// ※語頭と語後ろが重複して満たすときは、語頭優先。
	//         語中　　　　語頭　　　語後ろ    単独
	//{1,4}    ch          ch        ch        child
	//{1,5}    sh          sh        sh        shall
	//{1,7}    th          th        th        this
	//{1,6}    wh          wh        wh        which
	//{3,6}    ou          ou        ou        out
	//{2,7}    w           w         w         will
	//{2,0}    ea          ea        ,         ,
	//{6,0}    bb          be        be        be
	//{2,2}    cc          :         :         :
	//{2,6}    dd          dis       .         .
	//{2,4}    en          en        en        enough
	//{6,2}    ff          to        to        !
	//{6,6}    gg          (         )         were
	//{6,4}    '           ?         ?         his 
	//{4,6}    was         was       '         was
	//{4,1}    st          still     /         still
	//{4,7}    ble         ble       ble       #
	//{4,4}    -           -         com       -
	
	/**
	 * lower文字テーブルを定義する.
	 */
	private static void initLowerWordsContractions() {
		
		// 最優先の5文字は変化しないのでここではいらない。たぶん。
		//setLowerWord(m_ENlowerData, 7, 5, "and",  "and",    "and",   "and");
		//setLowerWord(m_ENlowerData, 7, 7, "for",  "for",    "for",   "for");
		//setLowerWord(m_ENlowerData, 7, 6, "of",  "of",    "of",   "of");
		
		//                                  現れる場所ごとに定義する
		//                               INNER   HEAD    TAIL     SINGLE
		setLowerWord(m_ENlowerData, 1, 4, "ch",  "ch",    "ch",   "child");
		setLowerWord(m_ENlowerData, 3, 4, "gh",  "gh",    "gh",   "gh");
		setLowerWord(m_ENlowerData, 1, 5, "sh",  "sh",    "sh",   "shall");
		setLowerWord(m_ENlowerData, 1, 7, "th",  "th",    "th",   "this");
		setLowerWord(m_ENlowerData, 1, 6, "wh",  "wh",    "wh",   "which");
		setLowerWord(m_ENlowerData, 3, 5, "ed",  "ed",    "ed",   "ed");
		setLowerWord(m_ENlowerData, 3, 7, "er",  "er",    "er",   "er");
		setLowerWord(m_ENlowerData, 3, 6, "ou",  "ou",    "ou",   "out");
		setLowerWord(m_ENlowerData, 2, 5, "ow",  "ow",    "ow",   "ow");
		setLowerWord(m_ENlowerData, 2, 7, "w",   "w",     "w",    "will");
		setLowerWord(m_ENlowerData, 2, 0, "ea",  "ea",    ",",    ",");
		setLowerWord(m_ENlowerData, 6, 0, "bb",  "be",    ";",    ";");
		setLowerWord(m_ENlowerData, 2, 2, "cc",  ":",     ":",    ":");
		setLowerWord(m_ENlowerData, 2, 6, "dd",  "dis",   ".",    ".");
		setLowerWord(m_ENlowerData, 2, 4, "en",  "en",    "en",   "enough");
		setLowerWord(m_ENlowerData, 6, 2, "ff",  "to",    "!",    "!");
		setLowerWord(m_ENlowerData, 6, 6, "gg",  "(",     ")",    "were");
		//setLowerWord(m_ENlowerData, 6, 4, "`",   "`",     "?",    "his");
		setLowerWord(m_ENlowerData, 6, 4, "?",   "`",     "?",    "his");
		setLowerWord(m_ENlowerData, 4, 2, "in",  "in",    "in",   "in");
		//setLowerWord(m_ENlowerData, 4, 6, "was", "was",   "'",    "was");
		setLowerWord(m_ENlowerData, 4, 6, "'",   "'",   "'",    "was");
		//setLowerWord(m_ENlowerData, 4, 1, "st",  "still", "/",    "still");
		setLowerWord(m_ENlowerData, 4, 1, "st",  "st",    "st",   "still");// '/'はどのケースよ？
		setLowerWord(m_ENlowerData, 4, 5, "ing", "ing",   "ing",  "ing");
		setLowerWord(m_ENlowerData, 4, 7, "ble", "ble",   "ble",  "#");
		setLowerWord(m_ENlowerData, 4, 3, "ar",  "ar",    "ar",   "ar");
		setLowerWord(m_ENlowerData, 4, 0, "'",   "'",     "'",    "'");
		setLowerWord(m_ENlowerData, 4, 4, "com", "com",   "com",  "-");
	}
	
	// lower文字の代入
	private static void setLowerWord(String[][][] arr, int left, int right,
			String strAsIn, String strAsHead, String strAsTail, String strAsSingle)
	{
		arr[c_LOWER_WORD_INNER ][left][right] = strAsIn;
		arr[c_LOWER_WORD_HEAD  ][left][right] = strAsHead;
		arr[c_LOWER_WORD_TAIL  ][left][right] = strAsTail;
		arr[c_LOWER_WORD_SINGLE][left][right] = strAsSingle;
	}
	
	/**
	 * lower文字の縮字を得る
	 */
	private static String getLowerWordsContractions(int type, TmBraille braille) {
		if( !(type == c_LOWER_WORD_INNER || type == c_LOWER_WORD_HEAD || 
			type == c_LOWER_WORD_TAIL || type == c_LOWER_WORD_SINGLE )) {
			Log.e("getLowerWordsContractions", "bad type args");
			return(null);
		}
		int left = braille.getLeft();
		int right = braille.getRight();
		if( left < 0 || left > 7 || right < 0 || right > 7 ) {
			Log.e("getLowerWordsContractions", "bad left or right args");
			return(null);
		}
		
		String ret = m_ENlowerData[type][left][right];
		return(ret);
	}
	
	/**
	 * マスあけしない低下文字か.
	 * @param type
	 * @param braille
	 * @return
	 */
	private static boolean isNoMasuAkeLowerWords(int type, TmBraille braille) {
		if( isSpaceOrNone(braille) ) {
			return(false);
		}
		// 先頭にあって、特定の点字はマスアケしない
		if( type == c_LOWER_WORD_HEAD ) {
			int left = braille.getLeft();
			int right = braille.getRight();
			if( (left == 6 && right == 0) || // be
				(left == 6 && right == 2)    // to
			) {
				return(true);
			}
		}
		return(false);
	}
	
	/**
	 * 前置符号か否か.
	 * @param braille
	 * @return
	 */
	private boolean isSignCode(TmBraille braille) {
		if( braille != null && !braille.isCleared() ) {
			int left = braille.getLeft();
			int right = braille.getRight();
			if( left == 0 && right >= 2 && right <= 7 ) {
				// 何か前置符号である
				return(true);
			}
		}
		return(false);
	}
	
	/**
	 * 空白文字か.
	 */
	@Override
	protected boolean isWhitespace(TmBraille braille) {
		if( super.isWhitespace(braille) ) {
			return(true);
		} else {
			if( isSignCode(braille) ) {
				return(false);
			}
			// TODO 他の文字もしらべる
			// .とか,とか
			// こりゃ表にしなきゃだめだ
			
			int left = braille.getLeft();
			int right = braille.getRight();
			
			// ?" としたいときに、?がwhitespaceだと、"が単独出現になって、wasに解釈される。
			if( 
				(left == 6 && right == 4)  // ?
			) {
				return(false);
			}
			
			// 何か記号は空白文字ということにしたいわけだが。。。。
			if( 
				(left == 6 && right == 0) ||  // ,
				(left == 4 && right == 0)     // '
			) {
				return(true);
			}
			
			// '.'カンマの場合は次のケースがありえる。
			// *[2,6][none/sp] : カンマ(この場合だけwhitespaceと判定する)
			// *[2,6]*         : dd
			// [none/sp][2,6]* : dis
			if( (left == 2 && right == 6)  // .
			) {
				TmBraille next = braille.getNext();
				TmBraille prev = braille.getPrev();
				if( isSpaceOrNone(next) && !isSpaceOrNone(prev)	) {
					return(true);
				}
			}
			
			// 低下文字だからといって、空白文字にできないよ。
			// 例)bi[gg][er]みたいなケースでは、[gg]がwereとみなされてしまう。
			//String lowerSignStr = m_ENlowerData[c_LOWER_WORD_TAIL][left][right];
			//if( lowerSignStr != null ) {
			//	// 何か低下文字なので空白文字とする
			//	return(true);
			//}
			
			return(false);
		}
	}
	
	/**
	 * 単語の先頭か.
	 */
	protected boolean isFirstOfWord(TmBraille braille) {
		if( braille == null ) {
			// 微妙
			return(false);
		}
		
		TmBraille prev = braille.getPrev();
		
		// prevが前置符号の場合はもう一個prevを見る
		if( isSignCode(prev) ) { 
			prev = prev.getPrev();
			// 念のためもう一個ぐらいみておく。てきとー。
			if( prev != null && isSignCode(prev) ) { 
				prev = prev.getPrev();
			}
		}
		
		// prevが[0,0]かnullならbrailleは単語の最初
		if( isSpaceOrNone(prev) ) {
			return(true);
		}
		return(false);
	}
	
	// スペース[0,0]または何もないならtrue.
	private static boolean isSpaceOrNone(TmBraille br) {
		if( br == null ) {
			return(true);
		}
		if( br.getLeft() == 0 && br.getRight() == 0 ) {
			return(true);
		}
		return(false);
	}
	
	@Override
	public boolean isPunctuation(String str) {
		if( str == null ) {
			// 文字がなければtrueとする。
			return(true);
		}
		if( str.length() != 1 ) {
			// 長さが1じゃなきゃfalse
			return(false);
		}
		
		char c = str.charAt(0);
		//if( c == '\'' || c == '?' ) {
			// ***?" で"がwasになっちゃうから。
		if( c == '\'' ) {
			// don't でtがthatに変換されるから。
			return(false);
		}
		
		if( !Character.isLetter(c) ) {
			// letterじゃなければtrue
			return(true);
		}
		return(false);
	}
	
	/**
	 * 前置符号を取得
	 * @param left
	 * @param right
	 * @return
	 */
	@Override
	public int getSignCode(TmBraille braille) {
		int signCode = getSignCodeImpl(m_ENSignData, braille);
		
		// c_SIGN_DOT6は大文字符だが、prevもnextも文字の場合、
		// 後ろ縮字の可能性あり。なければただの大文字符。
		if( signCode == c_SIGN_DOT6 ) {
			TmBraille prevBraille = braille != null ? braille.getPrev() : null;
			TmBraille nextBraille = braille != null ? braille.getNext() : null;	
			if( !isWhitespace(prevBraille) && !isWhitespace(nextBraille)) {
				String finalConvStr = convertWithPrevSign(m_EN1Data, nextBraille, c_SIGN_FINAL_CONV);
				if( finalConvStr != null ) {
					// 何か後ろ縮字が存在する
					signCode = c_SIGN_FINAL_CONV;
				}
			}
		}
		return(signCode);
	}
	
	/**
	 * さかのぼって数符を探す.
	 * @param braille
	 * @return
	 */
	private boolean detectNumberSign(TmBraille braille) {
		boolean ret = false;
		
		// 後ろへ検索する
		for( TmBraille br = braille; br != null; br = br.getPrev() ) {
			int left = br.getLeft();
			int right = br.getRight();
			
			//{2,0}はカンマなんで無視
			if( left == 2 && right == 0 ) {
				continue;
			}
			
			// 数符か？
			if( left == 4 && right == 7 ) {
				// brは数符と思われるので、brの次の符号が0-9であるかを見て判定。
				TmBraille next = br.getNext();
				String numStr = convertWithPrevSign(m_EN1Data, next, c_SIGN_NUMBER);
				if( numStr != null && numStr.length() > 0 ) {
					ret = true;
				}
				break;
			}
			
			// brが数値でなくなったら抜ける
			String numStr = convertWithPrevSign(m_EN1Data, br, c_SIGN_NUMBER);
			if( numStr == null || numStr.length() == 0 ) {
				break;
			}
		}
		return(ret);
	}
	
	/**
	 * 大文字前置符号を探す.
	 * @param braille
	 * @return
	 */
	private boolean detectLargeSign(TmBraille braille) {
		boolean ret = false;
		
		if( braille != null && !braille.isCleared() ) {
			// 自分自身と、後ろへ1個だけ検索する
			if( braille.getLeft() == 0 && braille.getRight() == 4 ) {
				ret = true;
			} else {
				TmBraille back = braille.getPrev();
				if( back != null ) {
					if( back.getLeft() == 0 && back.getRight() == 4 ) {
						ret = true;
					}
				}
			}
		}
		return(ret);
	}
	
	/**
	 * 点字から墨字へ一文字変換.
	 * @param left
	 * @param right
	 * @param locale
	 * @return
	 */
	@Override
	public String convert(TmBraille braille) {
		// 優先順位
		// 1. prev_signが何かあったらもうそれで引くだけ。
		// 2. prev_signがない場合は、[語の先頭|語の中|語の終わり|単独出現]のどれか.
		// ※空白文字には文字が存在しない場合も含む。
		// -------------------------------------------------------------------
		//        語の先頭　　　  語の中　　  語の後ろ      単独出現
		//        である条件　　である条件　　である条件　　である条件
		// -------------------------------------------------------------------
		// prev   空白文字        文字        *             空白文字
		// next   *               文字        空白文字      空白文字
		
		if( braille == null || braille.isCleared() ) {
			return(c_NULL_STR);
		}
		
		String ret = null;
		TmBraille prevBraille = braille != null ? braille.getPrev() : null;
		TmBraille nextBraille = braille != null ? braille.getNext() : null;
		int prev_sign = getSignCode(prevBraille);
		//int next_sign = getSignCode(nextBraille);
		boolean isFirstOfWord = isFirstOfWord(braille);
		boolean isLastOfWord = isWhitespace(nextBraille);
		
		// 数符判定
		if( prev_sign == c_SIGN_NOT_SIGN ) {
			if( detectNumberSign(braille) ) {
				prev_sign = c_SIGN_NUMBER;
			}
		}
		
		// 大文字判定
		boolean large_sign = (prev_sign == c_SIGN_DOT6 ) ? true : false;
		if( prev_sign != c_SIGN_DOT6 && prev_sign != c_SIGN_NOT_SIGN ) {
			// 大文字前置符号以外の何か符号が直前にある
			large_sign = detectLargeSign(prevBraille);
		}
		
		if( prev_sign != c_SIGN_NOT_SIGN && prev_sign != c_SIGN_DOT6 ) {
			// 大文字符号ではない前置符号がある
			ret = convertWithPrevSign(m_EN1Data, braille, prev_sign);
		} else if( isFirstOfWord && isLastOfWord ) {
			// 単独出現
			ret = convertWithSituation(braille, prev_sign, c_LOWER_WORD_SINGLE);
			//if( ret == null ) {
				// 縮語だった場合はその展開
				String expanded = expandShortFormWords(ret);	
				if( expanded != null ) {
					ret = expanded;
				}
			//}
		} else if( !isFirstOfWord && !isLastOfWord ) {
			// 語の中
			ret = convertWithSituation(braille, prev_sign, c_LOWER_WORD_INNER);
		} else if( isFirstOfWord ) {
			// 語の先頭
			ret = convertWithSituation(braille, prev_sign, c_LOWER_WORD_HEAD);
		} else {
			// 語の後ろ
			ret = convertWithSituation(braille, prev_sign, c_LOWER_WORD_TAIL);
		}
		
		// 最終処理
		if( ret != null ) {
			// 大文字処理
			if( large_sign ) {
				ret = toUpperCaseOnlyFirst(ret);
			}
		
			// 優先5単語 + 不定冠詞のマスあけなかったケースではないか？
			if( isHiPriorityWord(prevBraille) ) {
				if( isIndefiniteArticle(braille) || isHiPriorityWord(braille) ) {
					ret = c_SPACE + ret;
				}
			}
			
			// 次文字が何か文字で、特定のマスアケしない低下文字なら後ろに空白をねじ込む
			if( !isSpaceOrNone(nextBraille) && isNoMasuAkeLowerWords(c_LOWER_WORD_HEAD, braille) ) {
				ret = ret + c_SPACE;
			}
		}
		return(ret);
	}
	
	/**
	 * 
	 * @param braille
	 * @param prev_sign
	 * @param situation
	 * @return
	 */
	private String convertWithSituation(TmBraille braille , int prev_sign, int situation) {
		String ret = null;
		if( situation == c_LOWER_WORD_SINGLE || situation == c_LOWER_WORD_TAIL || 
			situation == c_LOWER_WORD_HEAD || situation == c_LOWER_WORD_INNER )
		{
			ret = getLowerWordsContractions(situation, braille);
			if( ret == null ) {
				ret = convertWithPrevSign(m_EN1Data, braille, prev_sign);
			}
		}
		return(ret);
	}
	
	/**
	 * 英語のSpeechText用に一文字変換.
	 * @param left
	 * @param right
	 * @return
	 */
	@Override
	public String convertSpeechText(TmBraille bralle) {
		return(convert(bralle));
	}
	
	
	/**
	 * 略語の展開.
	 * ハズレの場合はnullを返す.
	 * @param shortForm
	 * @return
	 */
	@Override
	public String expandShortFormWords(String shortForm) {
		if( shortForm == null || shortForm.length() == 0 ) {
			return(null);
		}
		
		// 大文字対策
		char topC = shortForm.charAt(0);
		boolean isFirstLarge = false;
		if( 'A' <= topC && topC <= 'Z') {
			shortForm = shortForm.toLowerCase();
			isFirstLarge = true;
		}
		
		// hash tableを引く
		String expaned = m_shortFormWords.get(shortForm);
		
		// 大文字だった場合は返す文字列も大文字にして返す.
		// ただし先頭だけ。
		if( expaned != null && isFirstLarge ) {
			expaned = toUpperCaseOnlyFirst(expaned);
		}
		return(expaned);
	}
}

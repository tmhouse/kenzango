/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib;

import java.util.ArrayList;

import jp.tmhouse.SixPointsKanjiLib.R;
import jp.tmhouse.TmLibrary.Activity.SettingsActivity.GuideSetting;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmBrailleVibrator.SingleKind;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Vibrator;

public class TmSoundVibrator {
	protected Context		m_ctx;
	private Handler			m_handler = new Handler();

	// 一点式用
    private Thread				m_singleVibeThread;
    private SoundVibrateRun		m_singleVibeRun;
    
    // 二点式用
    private Thread				m_dualSoundThread;
    private SoundVibrateRun		m_dualSoundRun;
    
    private int	c_defaultResId = R.raw._sin_200hz_1000ms_16bit44k_gain20db;
    
    // 鳴らさない時間がある程度経過した後、最初の音が鳴らない
    // 問題に対処するため、一定間隔で無音を鳴らす
	private IntervalRun		m_intervalRun;
    
	// 両チャンネル同時に再生したとき、音量が大きく感じるのでゲイン調整
	private static final float c_bothChannelGain = 0.80f;

	private AudioManager	m_am;

	/**
	 * コンストラクタ.
	 * @param ctx
	 * @param SoundResId
	 */
	public TmSoundVibrator(Context ctx, int soundResId) {
		m_ctx = ctx;
		
        m_am = (AudioManager)ctx.getSystemService(Context.AUDIO_SERVICE);

		// スレッドを２個に分ける。
		// 一点式振動は端末での振動を司り、かつ一点式音声出力も司る
		// ため、スレッド１本を用いることにより振動を同期させる。
		// 二点式は音声振動のみを司るため、一点式とは別のスレッドにする。
		
		// 一点式振動とビープ振動を行うスレッドを生成
		m_singleVibeRun = new SoundVibrateRun(ctx, 1);
		m_singleVibeRun.setSoundResId(soundResId);
		m_singleVibeThread = new Thread(m_singleVibeRun);
		m_singleVibeThread.setName("SingleVibeThread");
		m_singleVibeThread.setPriority(Thread.MAX_PRIORITY);
		m_singleVibeThread.start();
		
		// 二点式音声振動を生む。Vibraterはオフにする
		m_dualSoundRun = new SoundVibrateRun(ctx, 2);
		m_dualSoundRun.setVibratorEnable(false);
		m_dualSoundRun.setSoundResId(soundResId);
		m_dualSoundThread = new Thread(m_dualSoundRun);
		m_dualSoundThread.setName("DualVibeThread");
		m_dualSoundThread.setPriority(Thread.MAX_PRIORITY);
		m_dualSoundThread.start();
	}
    
    /**
     * 終了.
     */
    public void finish() {
    	if( m_intervalRun != null ) {
    		m_intervalRun.stop();
    		m_intervalRun = null;
    	}
    	synchronized(m_singleVibeRun) {
    		m_singleVibeRun.stop();
    		m_singleVibeRun.notify();
    	}
    	synchronized(m_dualSoundRun) {
    		m_dualSoundRun.stop();
    		m_dualSoundRun.notify();
    	}
    	
    	if( m_singleVibeThread != null ) {
    		m_singleVibeThread.interrupt();
    		m_singleVibeThread = null;
    	}
    	if( m_dualSoundThread != null ) {
    		m_dualSoundThread.interrupt();
    		m_dualSoundThread = null;
    	}
    }
	
	/**
	 * しばらく放置された後、最初に鳴らす音がでないので、
	 * 定期的に無音を鳴らす。
	 * @param ctx
	 * @param intervalMSec
	 */
	public void playMuteContinuos(Context ctx, long intervalMSec) {
		if( m_intervalRun == null ) {
			int mute_sound_resid = R.raw._sin_18000hz_1000ms_16bit44k_gain1db;
			m_intervalRun = new IntervalRun(ctx, mute_sound_resid, intervalMSec);
			m_intervalRun.start();
		}
	}
	
    /**
     * マスターボリュームを設定する.
     */
    protected void setupMasterVolume() {
    	if( GuideSetting.queryVolumeMaxMode(m_ctx) ) {
    		// 最大にする
    		int maxVol = m_am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    		m_am.setStreamVolume(AudioManager.STREAM_MUSIC, maxVol, 0);
    		TmLog.d("set max volume. max=" + maxVol);
    	} else {
    		TmLog.d("volume not changed");
    	}
    }
	
	/**
	 * 定期的に無音再生するクラス.
	 * @author mutoh
	 *
	 */
	private class IntervalRun implements Runnable {
		private MutePlayer		m_player;
		private long			m_interval;
		
		public IntervalRun(Context ctx, int resId, long intervalTime) {
			m_player = new MutePlayer(ctx, resId);
			m_interval = intervalTime;
		}
		
		public void start() {
			m_handler.postDelayed(this, m_interval);
		}
		public void stop() {
			m_handler.removeCallbacks(this);
			if( m_player != null ) {
				m_player.finish();
				m_player = null;
			}
		}
		
		@Override
		public void run() {
			m_player.play();
			m_player.stop();
			start(); // 繰り返し
		}
	}
	
	/**
	 * 無音再生するクラス.
	 * @author mutoh
	 *
	 */
	private class MutePlayer {
		private SoundPool		m_sounds;
		private int			m_beepId;
		//private static final int c_LOOP_FOREVER = -1;
		private static final int c_NOLOOP = 0;
		private static final int c_LOW_PRIORITY = 0;
		private static final float c_LOW_VOLUME = 0.01f;
		
		public MutePlayer(Context ctx, int resId) {
    		m_sounds = new SoundPool( 1, AudioManager.STREAM_MUSIC, 0 );
    		m_beepId = m_sounds.load(ctx, resId, 1 );
    		
		}
		public void play() {
			if( m_beepId != 0 ) {
				//Log.d("MutePlayer", "play");
				m_sounds.play(m_beepId, 
						c_LOW_VOLUME, c_LOW_VOLUME,
						c_LOW_PRIORITY, 
						c_NOLOOP, 
						1.0F);
			}
		}
		public void stop() {
			if( m_beepId > 0 ) {
				m_sounds.stop(m_beepId);
			}
		}
		
		public void finish() {
			m_sounds.release();
			m_sounds = null;
		}
	};
	
	/**
	 * サウンドリソースIDのセット.
	 * @param ctx
	 * @param soundResId
	 */
	public synchronized void setSoundResId(int soundResId) {
		m_singleVibeRun.setSoundResId(soundResId);
		m_dualSoundRun.setSoundResId(soundResId);
	}
	
	public synchronized void setVibratorEnable(boolean bEnable) {
		m_singleVibeRun.setVibratorEnable(bEnable);
		//m_dualSoundRun.setVibratorEnable(bEnable);
	}
	
	public synchronized void setIgnore(boolean bIgnore) {
		m_singleVibeRun.setIgnore(bIgnore);
		m_dualSoundRun.setIgnore(bIgnore);
	}
	
	
	/**
	 * レシオ考慮済み振動時間を返す.
	 * @param msec
	 * @return
	 */
	private long calcVibrateTime(long msec) {
		float ratio = GuideSetting.queryVibrateSpeedValue(m_ctx);
		return (long)((float)msec * ratio);
	}
    
    /**
     * ２チャンネル同時の振動を出す.
     * @param msec				振動の時間
     */
    public void addSingleSoundVibrate(long msec) {
    	msec = calcVibrateTime(msec);
    	if( ! m_singleVibeRun.getIgnore() ) {
    		float ratio = GuideSetting.queryBeepGuideVolume(m_ctx);
    		boolean bWithSound = GuideSetting.queryEnableBeepGuide(m_ctx);
    		float vol = bWithSound ? ratio : 0;
    		m_singleVibeRun.add(vol, vol, msec, 0, false);
    	}
    }
    
    /**
     * ２チャンネル鳴らしわけの振動を出す.
     * @param msec
     * @param volRatio
     * @param left
     * @param right
     */
    public void addDualSoundVibrate(
    		long msec, boolean left, boolean right)
    {
    	msec = calcVibrateTime(msec);
    	if( ! m_dualSoundRun.getIgnore() ) {
    		// 左右鳴らすのはどれなのかのセット
    		float ratio = GuideSetting.queryBeepGuideVolume(m_ctx);
    		float leftVol = left ? ratio : 0;
    		float rightVol = right ? ratio : 0;
    		m_dualSoundRun.add(leftVol, rightVol, msec, 0, false);
    	}
    }
    
    /**
     * SingleSoundに何もしない待ちを入れる.
     * @param waitTime			待つ時間
     */
    public void addSingleSoundWait(long waitTime) {
    	if( m_singleVibeRun.getIgnore() ) { return; }
    	//waitTime = calcVibrateTime(waitTime);
    	m_singleVibeRun.add(0, 0, 0, waitTime, false);
    }
    
    /**
     * DualSoundに何もしない待ちを入れる.
     * @param waitTime			待つ時間
     */
    public void addDualSoundWait(long waitTime) {
    	if( waitTime <= 0 ) {
    		return;
    	}
    	if( m_dualSoundRun.getIgnore() ) { return; }
    	//waitTime = calcVibrateTime(waitTime);
    	m_dualSoundRun.add(0, 0, 0, waitTime, false);
    }
    
    /**
     * デフォルトの文字間待ちを入れる.
     */
    public void addDefaultWait() {
    	long wait = GuideSetting.queryRecvVibrateEachSpaceTimeValue(m_ctx);
    	if( isConnectingHeadset() ) {
    		addDualSoundWait(wait);
    	} else {
    		addSingleSoundWait(wait);
    	}
    }
    
    public void addDualSoundSeparator() {
    	if( m_dualSoundRun.getIgnore() ) { return; }
    	m_dualSoundRun.addSeparator();
    }
    
    public void addSingleSeparator() {
    	if( m_singleVibeRun.getIgnore() ) { return; }
    	m_singleVibeRun.addSeparator();
    }

    /**
     * 再生中であってもqueをクリアする.
     */
    public synchronized void clearAll() {
    	m_singleVibeRun.clear();
    	m_dualSoundRun.clear();
    }
    
    /**
     * queのサイズを返す.
     * @return
     */
    public int getQueSize() {
    	// TODO 本来threadを一本にすべきだったが、
    	// とりあえずdualを優先で逃げる。
    	int size =  m_dualSoundRun.getQueSize();
    	if( size > 0 ) {
    		return(size);
    	}
    	return(m_singleVibeRun.getQueSize());
    }
    
    /**
     * queの位置を返す.
     * @return
     */
    public int getQuePos() {
    	// TODO 本来threadを一本にすべきだったが、
    	// とりあえずdualを優先で逃げる。
    	int pos = m_dualSoundRun.getQuePos();
    	if( pos > 0 ) {
    		return(pos);
    	}
    	return(m_singleVibeRun.getQuePos());
    }
    
    /**
     * SingleSoundのqueのサイズを返す.
     * @return
     */
    public int getSingleSoundQueSize() {
    	return(m_singleVibeRun.getQueSize());
    }
    
    /**
     * 非同期再生.
     */
    public void playDataAsync() {
    	synchronized(m_singleVibeRun) {
    		m_singleVibeRun.notify();
    	}
    	synchronized(m_dualSoundRun) {
    		m_dualSoundRun.notify();
    	}
    }
    
    /**
     * 再生の合間で呼ばれるcallback i/f.
     * @author mutoh
     *
     */
    public interface IOnProgressUpdate {
    	/**
    	 * 再生の合間合間で呼ばれる.
    	 * @return 停止するときtrue.
    	 */
    	public boolean onProgressUpdate();
    }
    
    /**
     * 振動再生の一回ごとの経過をcallbackするi/f.
     * @author mutoh
     *
     */
    public interface IOnPlayBrailleHandler {
    	/**
    	 * 再生開始.
    	 */
    	public void onVibrateStarted();

    	/**
    	 * 再生全て終了.
    	 */
    	public void onVibrateEnded();

    	/**
    	 * 停止.
    	 */
    	public void onVibratePaused(PauseKind pauseKind);

    	/**
    	 * 点字一つの中の再生開始.
    	 * brailleNum
    	 * @param position		点字列の中の番号. > 0
    	 * @param brailleNum1	再生しようとしている点字番号1.
    	 * @param brailleNum2	再生しようとしている点字番号2.(同時再生ではないとき0が入る）
    	 */
    	public void onBrailleViberateStarted(int position, int brailleNum1, int brailleNum2);
    	/**
    	 * 点字一つの中の再生停止.
    	 * @param position		点字列の中の番号. > 0
    	 */
    	public void onBrailleViberateEnded(int position);
    }

    public void playDualSoundOnThread(IOnProgressUpdate cb) {
    	//synchronized(m_dualSoundRun) {
    		m_dualSoundRun.setProgressUpdate(cb);
    		m_dualSoundRun.playSync();
    	//}
    }
    
    public void setOnPlayBrailleHandler(IOnPlayBrailleHandler cb) {
    	m_singleVibeRun.setOnPlayBrailleHandler(cb);
    	m_dualSoundRun.setOnPlayBrailleHandler(cb);
    }
    
    /**
     * dual soundの再生完了を待つnotifyerをセットする.
     * @param notifyer
     */
    public void setDualSoundNotifyer(Object notifyer) {
    	m_dualSoundRun.setNotifier(notifyer);
    }
    
    /**
     * Queを一時停止.
     * @param bImmediate trueのとき直ちに停止. falseのとき点字の区切りで停止.
     */
    public void pause() {
   		m_singleVibeRun.pause(true);
   		m_dualSoundRun.pause(true);
    }

    /**
     * Queを一時停止(再生中の点字の終わりで停止).
     */
    public void pauseAtEndOfBraille() {
   		m_singleVibeRun.pause(false);
   		m_dualSoundRun.pause(false);
    }
    
    /**
     * Queの一時停止を解除.
     */
    public void unpause() {
    	if( m_singleVibeRun.getQueSize() > 0 && m_dualSoundRun.getQueSize() > 0 ) {
    		m_singleVibeRun.clear();
    		m_dualSoundRun.clear();
    		TmLog.w("single and dual have que both.");
    	}
    	if( m_singleVibeRun.getQueSize() > 0 ) {
    		m_singleVibeRun.unpause();
    	} else if( m_dualSoundRun.getQueSize() > 0 ) {
    		m_dualSoundRun.unpause();
    	}
    }
    
    /**
     * セパレータへ移動する.
     * @param separatorNum ＋値で進む、－値で戻る.
     */
    public void moveToSeparator(int separatorNum) {
    	// TODO とりあえず
    	synchronized(m_dualSoundRun) {
    		if( m_dualSoundRun.getQuePos() > 0 ) {
    			m_dualSoundRun.move(separatorNum);
    		}
    	}
    	synchronized(m_singleVibeRun) {
    		if( m_singleVibeRun.getQuePos() > 0 ) {
    			m_singleVibeRun.move(separatorNum);
    		}
    	}
    }

	
	@SuppressWarnings("deprecation")
	public boolean isConnectingHeadset() {
		return(m_am.isBluetoothA2dpOn() || m_am.isWiredHeadsetOn() );
	}
    
    
    /**
     * 再生データのフォルダ.
     * @author mutoh
     *
     */
    private class VibrateData {
    	protected long m_playTime;
    	protected long m_afterWaitTime;
    	protected float m_leftVol;
    	protected float m_rightVol;
    	protected boolean m_bWithVibrate;
    	protected boolean m_bSeparator = false; // 文字区切りを表す
    	
    	public void debugPrint(SoundVibrateRun run) {
    		if( false == TmLog.isDebugBuild(m_ctx) ) {
    			return;
    		}
    		Thread t = Thread.currentThread();
    		StringBuilder sb = new StringBuilder(256);
    		sb.append("<<<VibrateData>>>");
    		sb.append(t.getName());
    		sb.append("(tid="); sb.append(t.getId());sb.append(")");
    			
    		sb.append(", vibe="); sb.append(run.getVibratorEnable());
    		sb.append(", headset="); sb.append(isConnectingHeadset());sb.append("\n");
    			
    		sb.append("##data##\n");
    		sb.append("m_afterWaitTime="); sb.append(m_afterWaitTime); sb.append(", ");
    		sb.append("m_bSeparator="); sb.append(m_bSeparator); sb.append(", ");
    		sb.append("m_bWithVibrate="); sb.append(m_bWithVibrate); sb.append(", ");
    		sb.append("m_leftVol="); sb.append(m_leftVol); sb.append(", ");
    		sb.append("m_rightVol="); sb.append(m_rightVol); sb.append(", ");
    		sb.append("m_playTime="); sb.append(m_playTime); 
    		sb.append("\n");
    		TmLog.i(sb.toString());
    	}
    	
    	public VibrateData() {
    	}
    	
    	public VibrateData(float leftVol, float rightVol, 
    			long playTime, long afterWaitTime, boolean bWithVibrate) {
    		m_playTime= playTime;
    		m_afterWaitTime = afterWaitTime;
    		m_leftVol = leftVol;
    		m_rightVol = rightVol;
    		m_bWithVibrate = bWithVibrate;
    	}
    	
    	/**
    	 * 文字区切りフラグのセット.
    	 * @param bSeparator
    	 */
    	public void setSeparator(boolean bSeparator) {
    		m_bSeparator = bSeparator;
    	}
    	
    	/**
    	 * 文字区切りか.
    	 * @return
    	 */
    	public boolean isSeparator() {
    		return(m_bSeparator);
    	}
    }
    
    /**
     * 中断したときの理由の種別.
     * @author mutoh
     *
     */
    public enum PauseKind {
    	NOT_PAUSED,			// pauseされていない
    	PAUSED_IMMEDIATELY,		// 即停止
    	PAUSED_AT_ENDOF_BRAILLE	// 点字一個を再生し終わったら停止
    }

	/**
	 * 振動と音を鳴らすRunnable.
	 * @author mutoh
	 *
	 */
    private class SoundVibrateRun implements Runnable {
    	private Vibrator		m_vibrator;
        private ArrayList<VibrateData> m_que = new ArrayList<VibrateData>(256);
        private Object			m_blocking = new Object();
        private boolean		m_stop = false;
        private Context		m_ctx;
        private SoundPool	m_sounds;
        private int			m_beepId;
        private boolean		m_vibratorEnable = true;
        private boolean		m_ignore = false;
        private int			m_soundResId = Integer.MAX_VALUE;
		private int	m_mute_sound_resid = R.raw._sin_18000hz_1000ms_16bit44k_gain1db;
		private int			m_muteId;
		private PauseKind	m_pauseKind = PauseKind.NOT_PAUSED;
		private Object			m_notifier;
		private IOnProgressUpdate		m_onProgressUpdCb;
		private IOnPlayBrailleHandler	m_onPlayBrailleHandler;
		private int			m_channel = 0;
		private SingleKind	m_singleKind;
		private final int[] c_142536_ARR = {0, 1, 4, 2, 5, 3, 6};
		private	int			m_separatorCount = 0;

        public SoundVibrateRun(Context ctx, int channel) {
        	super();
        	m_channel = channel;
        	m_ctx = ctx;
        	m_vibrator = ((Vibrator)ctx.getSystemService(Context.VIBRATOR_SERVICE));
        }
        public synchronized void setNotifier(Object obj) {
        	m_notifier = obj;
        }
        private synchronized Object getNotifier() {
        	return(m_notifier);
        }
        
        public synchronized void setOnPlayBrailleHandler(IOnPlayBrailleHandler cb) {
        	m_onPlayBrailleHandler = cb;
        }
        
        public synchronized void setProgressUpdate(IOnProgressUpdate cb) {
        	m_onProgressUpdCb = cb;
        }
        public synchronized IOnProgressUpdate getProgressUpdate() {
        	return(m_onProgressUpdCb);
        }
        
        private boolean callOnProgressUpdate() {
        	IOnProgressUpdate cb = getProgressUpdate();
        	if( cb != null ) {
        		return(cb.onProgressUpdate());
        	}
        	return(false);
        }
        
    	public synchronized void setSoundResId(int soundResId) {
    		if( m_soundResId == soundResId ) {
    			// 同じなので何もしない
    			return;
    		}
    		if( soundResId <= 0 ) {
    			// デフォルトのサウンドファイル
    			soundResId = c_defaultResId;
    		}
    		m_sounds = new SoundPool( 5, AudioManager.STREAM_MUSIC, 0 );
    		m_beepId = m_sounds.load(m_ctx, soundResId, 1 );
    		m_muteId = m_sounds.load(m_ctx, m_mute_sound_resid, 1 );
    		m_soundResId = soundResId;
    	}
    	
    	public synchronized void setVibratorEnable(boolean bEnable) {
    		m_vibratorEnable = bEnable;
    	}
    	
    	public synchronized boolean getVibratorEnable() {
    		return(m_vibratorEnable);
    	}
    	
    	public synchronized void setIgnore(boolean bIgnore) {
    		m_ignore = bIgnore;
    	}
    	public synchronized boolean getIgnore() {
    		return(m_ignore);
    	}
		
        /**
         * run
         */
		@Override
		public void run() {
    		while(true) {
    			synchronized(this) {
    				try {
						wait();
					} catch (InterruptedException e) {
						//e.printStackTrace();
						return;
					}
    			}
    			// 再生
    			if( getQueSize() > 0 ) {
    				playSync();

    				// notifierが設定されているときは取り出してnofityする
    				Object notifier = getNotifier();
    				if( notifier != null ) {
    					synchronized (notifier) {
    						notifier.notify();
    					}
    				}
    			} else {
    				TmLog.i("no que");
    				releaseSoundPool();
    			}
    		}
    	}
		
		/**
		 * 同期再生.
		 */
		public void playSync() {
			TmLog.i("playSync start:" + Thread.currentThread().getName());
			callOnStartCb();
			
			// 一点式の場合の再生順序を覚えておく
			m_singleKind = GuideSetting.queryRecvSingleVibeKind(m_ctx);
					
			VibrateData data = null;
			// セパレータの位置
			int separatorPos = getQuePos();

   			// 一つの点字の中(セパレータが現れるまで)で振動した回数
   			int vibrateCount = 0;
   			PauseKind pauseKind = PauseKind.NOT_PAUSED;
    		do {
    			if( isStopped() ) {
    				TmLog.d("thread end");
    				return;
    			}
    			pauseKind = getPauseKind();
    			if( pauseKind == PauseKind.PAUSED_IMMEDIATELY ) {
    				TmLog.d("pause immediately");
    				break;
    			}

    			// 一個queから取り出す
    			data = getNext();
    			if( data != null ) {
    				//data.debugPrint(this);
    				// 文字セパレータなら何もしない
    				if( data.isSeparator() ) {
   						// 停止要求が来ているなら、前回セパレータ位置へ移動してから抜ける
    					if( pauseKind == PauseKind.PAUSED_AT_ENDOF_BRAILLE ) {
    						TmLog.d("pause at end of braille. separaterPos=" + separatorPos);
    						setQuePos(separatorPos);
    						break;
    					}
    					// セパレータ位置の更新
    					separatorPos = getQuePos();
    					// セパレータ数更新
    					m_separatorCount++;
    					// 振動した回数をリセット
    					vibrateCount = 0;
    					//TmLog.i("separatorPos = " + separatorPos + ", separatorCount = " + separatorCount);
    					continue;
    				}
    				
    				// バイブレータの振動
	    			if( m_vibrator != null && data.m_playTime > 0 ) {
	    				// bWithVibrateがtrueなら強制的に振動させる
	    				if( data.m_bWithVibrate || getVibratorEnable() ) {
	    					m_vibrator.vibrate(data.m_playTime);
	    				}
	    			}
	    			
	    			// 音の振動
    				int soundId = 0;
	    			if( data.m_playTime > 0 && (data.m_leftVol > 0 || data.m_rightVol > 0) ) {
	    				soundId = playBeep(data);
	    			}
	    			
	    			// 再生時間完了まで待つ
					if( data.m_playTime > 0 ) {
						// 振動した回数を更新
						vibrateCount++;

						// 振動が開始したことをCBする
						callOnVibrateStartCb(m_separatorCount, vibrateCount, data);

						synchronized(m_blocking) {
							try {
								m_blocking.wait(data.m_playTime);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						// 振動が終了したことをCBする
						callOnBrailleVibrateEndCb(m_separatorCount);
					}
					// 音の停止
					if( soundId > 0 ) {
						stopBeep(soundId);
					}

	    			if( callOnProgressUpdate() ) {
	    				break;
	    			}
					
					// 次の再生まで少しあける
					// Queがまだあるときだけ
					if( getQueSize() > 0 && data.m_afterWaitTime > 0 ) {
						synchronized(m_blocking) {
							try {
								m_blocking.wait(data.m_afterWaitTime);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						if( callOnProgressUpdate() ) {
							break;
						}
					}
    			}
    		} while (data != null);

			if( pauseKind == PauseKind.NOT_PAUSED ) {
				TmLog.i("playSync end:" + Thread.currentThread().getName());
				callOnEndCb();
			} else {
				TmLog.i("playSync paused:" + pauseKind);
				callOnPauseCb(pauseKind);
			}
		}
		
		/**
		 * onStartを呼ぶ.
		 * @param sepaCnt
		 */
		private void callOnStartCb() {
			if( m_onPlayBrailleHandler == null ) {
				return;
			}
			
			m_handler.post(new Runnable() {
				@Override
				public void run() {
					m_onPlayBrailleHandler.onVibrateStarted();
				}
			});
		}
		/**
		 * onEndを呼ぶ.
		 * @param sepaCnt
		 */
		private void callOnEndCb() {
			if( m_onPlayBrailleHandler == null ) {
				return;
			}
			
			m_handler.post(new Runnable() {
				@Override
				public void run() {
					m_onPlayBrailleHandler.onVibrateEnded();
				}
			});
		}
		/**
		 * onPauseを呼ぶ.
		 * @param sepaCnt
		 */
		private void callOnPauseCb(final PauseKind pauseKind) {
			if( m_onPlayBrailleHandler == null ) {
				return;
			}
			
			m_handler.post(new Runnable() {
				@Override
				public void run() {
					m_onPlayBrailleHandler.onVibratePaused(pauseKind);
				}
			});
		}	
		
		/**
		 * onViberateStartを呼ぶ.
		 * @param sepaCnt
		 * @param vibeCnt
		 * @param data
		 */
		private void callOnVibrateStartCb(
				int sepaCnt, int vibeCnt, VibrateData data) {
			if( m_onPlayBrailleHandler == null ) {
				return;
			}
			
			if( m_channel == 1 ) {
				// 1channelの場合は、点字再生順序(142536 or 123456)で最大6回。
				if( m_singleKind == SingleKind.USE_SINGLE_123456 ) {
					postOnBrailleVibrateStart(sepaCnt, vibeCnt, 0);
				} else if (m_singleKind == SingleKind.USE_SINGLE_142536 ) {
					int val = c_142536_ARR[vibeCnt];
					postOnBrailleVibrateStart(sepaCnt, val, 0);
				} else {
					throw new RuntimeException();
				}
			} else if ( m_channel == 2 ) {
				// 2channelの場合は、14, 25, 36の最大3回で、左右どちらか or 両方
				int val1 = data.m_leftVol > 0.0f ? vibeCnt : 0;
				int val2 = data.m_rightVol > 0.0f ? 3 + vibeCnt : 0; 
				postOnBrailleVibrateStart(sepaCnt, val1, val2);
			}
		}
		
		private void postOnBrailleVibrateStart(
				final int sepaCnt, final int val1, final int val2) {
			if( sepaCnt == 0 || m_onPlayBrailleHandler == null ) {
				return;
			}
			m_handler.postAtFrontOfQueue(new Runnable() {
				@Override
				public void run() {
					m_onPlayBrailleHandler.onBrailleViberateStarted(
							sepaCnt, val1, val2);
				}
			});	
		}

		/**
		 * onViberateEndを呼ぶ.
		 * @param sepaCnt
		 */
		private void callOnBrailleVibrateEndCb(final int sepaCnt) {
			if( m_onPlayBrailleHandler == null ) {
				return;
			}
			
			m_handler.post(new Runnable() {
				@Override
				public void run() {
					m_onPlayBrailleHandler.onBrailleViberateEnded(sepaCnt);
				}
			});	
		}
		
		public synchronized void stop() {
			m_stop = true;
			clear();
		}
		public synchronized boolean isStopped() {
			return(m_stop);
		}
		
		public synchronized void pause(boolean bImmediate) {
			if( bImmediate ) {
				m_pauseKind = PauseKind.PAUSED_IMMEDIATELY;
			} else {
				m_pauseKind = PauseKind.PAUSED_AT_ENDOF_BRAILLE;
			}
		}
		public synchronized void unpause() {
			m_pauseKind = PauseKind.NOT_PAUSED;
			this.notify();
		}
		public synchronized PauseKind getPauseKind() {
			return(m_pauseKind);
		}
		/***
		private synchronized boolean isPaused() {
			return(m_pauseKind == PauseKind.PAUSED_IMMEDIATELY);
		}
		private synchronized boolean isPausedAtEndOfBraille() {
			return(m_pauseKind == PauseKind.PAUSED_AT_ENDOF_BRAILLE);
		}
		***/
		
	    /**
	     * データを投入する.
	     * @param data			バイト列
	     * @param afterWaitSec	再生開始からブロックする時間(秒)
	     * @param forceStart	再生開始前に以前の再生が続いていた場合は強制停止するか
	     */
		public void add(float leftVol, float rightVol, long playTime, 
				long afterWaitTime, boolean bWithVibrate) {
			synchronized(m_que) {
				m_que.add(new VibrateData(
						leftVol, rightVol, playTime, afterWaitTime, bWithVibrate));
			}
		}
		
		/**
		 * 文字区切りを投入する.
		 */
		public void addSeparator() {
			synchronized(m_que) {
				VibrateData vd = new VibrateData();
				vd.setSeparator(true);
				m_que.add(vd);
			}
		}
		
		// queの中の「これから再生する位置」
		private int m_quePos;

		private VibrateData getNext() {
			synchronized(m_que) {
				if( m_que.size() > m_quePos ) {
					return(m_que.get(m_quePos++));
				}
				return(null);
			}
		}
		
		/**
		 * 再生位置の移動.
		 * できればpuase/unpauseを使って制御すること.
		 * 
		 * @param moveCount
		 */
		public void move(int moveCount) {
			int size = m_que.size();
			if( moveCount == 0 || size == 0 ){
				return;
			}
			
			int moveAbsCount = Math.abs(moveCount);
			int initialMoveCount = moveCount > 0 ? 1 : -1;
			synchronized(m_que) {
				int sepaCount = 0;
				int quePos = m_quePos + initialMoveCount;
				while( quePos > 0 && quePos < size ) {
					VibrateData vd = m_que.get(quePos);
					// セパレータがあった
					if( vd.isSeparator() ) {
						sepaCount++;
						if( moveAbsCount == sepaCount ) {
							m_quePos = quePos;
							return;
						}
					}
					if( moveCount > 0 ) {
						// 前進方向に移動
						quePos++;
					} else {
						// 後退方向に移動
						quePos--;
					}
				}
				// セパレータが見つからなかった
				if( moveCount > 0 ) {
					// 前進方向にセパレータがない場合は
					// 一番後ろを指す
					m_quePos = size -1;
				} else {
					// 後退方向では何もしない
				}
			}
		}
		
		private int getQuePos() {
			synchronized(m_que) {
				return(m_quePos);
			}
		}
		private void setQuePos(int quePos) {
			synchronized(m_que) {
				m_quePos = quePos;
			}
		}
		
		public void clear() {
			synchronized(m_que) {
				m_que.clear();
				m_quePos = 0;
				m_separatorCount = 0;
			}
			
			// waitしていた場合はnofityして先に行かせる
			synchronized(m_blocking) {
				m_blocking.notify();
			}
		}
		
		private int getQueSize() {
			int size = 0;
			synchronized(m_que) {
				size = m_que.size();
			}
			return(size);
		}
		
		private int playBeep(VibrateData data) {
			/***
			Log.d("playBeep", 
					"leftVol  = " + data.m_leftVol +
					"rightVol = " + data.m_rightVol +
					"playTime = " + data.m_playTime +
					"afterWait = " + data.m_afterWaitTime
					);
			***/
			if( data.m_leftVol > 1.0f || data.m_rightVol > 1.0f ) {
				throw new RuntimeException("volume is too big.");
			}
			
			// 両チャンネル再生時のボリュームを補正
			float leftVol = data.m_leftVol;
			float rightVol = data.m_rightVol;
			if( leftVol > 0 && rightVol > 0 ) {
				leftVol = leftVol * c_bothChannelGain;
				rightVol = rightVol * c_bothChannelGain;
			}
			
			int id = m_sounds.play(m_beepId, leftVol, rightVol, 100, -1, 1.0F);
			return(id);
		}
		
		private void stopBeep(int id) {
			if( m_sounds == null ) {
				return;
			}
			// pauseすると遅くて止まらないときがある
			//m_sounds.pause(id);
			m_sounds.stop(id);
		}
		
		/**
		 * 何もない音を鳴らす。主にBluetoothなどを起すため。
		 */
		
/***やっぱり定期的に無音を鳴らし続けることにする。
		// この時間以上経ってたら無音を鳴らす
		private static final long		c_kankakuTime = 5000;
		// ボリューム
		private static final float	c_muteVol = 0.01f;
		// 鳴らした後待つ時間(ms)
		private static final long		c_waitTime = 1500;
		private void playMuteSound() {
			long curTime = System.currentTimeMillis();
			if( m_lastFireTime == 0 || m_lastFireTime + c_kankakuTime < curTime ) {
				Log.d("playMuteSound", "play mute. wait ms=" + c_waitTime);
				m_sounds.play(m_muteId, c_muteVol, c_muteVol, 100, 0, 1.0F);
				
				synchronized(m_blocking) {
					try {
						m_blocking.wait(c_waitTime);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			m_lastFireTime = curTime;
		}
***/
		
		private void releaseSoundPool() {
			if( m_sounds == null ) {
				return;
			}
			if( m_beepId > 0 ) {
				m_sounds.unload(m_beepId);
			}
			if( m_muteId > 0 ) {
				m_sounds.unload(m_muteId);
			}
			m_sounds.release();
			m_sounds = null;
		}
    }

    /**
     * 
     * @param F
     * @param msec
     * @param volRatio 0.0 to 1.0
     * @return
     */
    /***
    private static byte[] create8bitMonoWaveData(int F, long playMSec, float volRatio) {
    	int BlockAlign = s_CHANNELS * s_BITSPERSAMPLE/8;
    	int AvgBytesPerSec = s_SRATE * BlockAlign;
    	
    	float msec = ((float)playMSec) / 1000.0f;
    	int dataSize = (int)((float)AvgBytesPerSec * msec);
    	byte[] data = new byte[dataSize];
    	
    	int len = s_SRATE / F;    //波長
    	int vol = volRatio < 1.0f ? (int)(64.0f * Math.abs(volRatio)) : 64; // max 64
    	for( int i = 0; i < dataSize; i++ ){
    	    if( i % len < len / 2) {
    	    	data[i] = (byte)(128 + vol);
    	    } else {
    	    	data[i] = (byte)(128 - vol);
    	    }
    	}
    	//SinOsc so = new SinOsc(F, 1);
    	//so.sinWave(data);
    	return(data);
    }
    
    public static byte[] byteArrayConcat(byte[] ... args) {
    	// バッファサイズを知る
    	int bufSize = 0;
    	for( byte[] arr : args ) {
    		bufSize += arr.length;
    	}
    	
    	ByteBuffer byteBuf = ByteBuffer.allocate(bufSize);
    	for( byte[] arr : args ) {
    		byteBuf.put(arr);
    	}
    	byte[] c = byteBuf.array();
    	return(c);
    }
    ***/
}

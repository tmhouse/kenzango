/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable;

import java.text.Normalizer;

import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;

public abstract class IMojiCodeTable {
	/**
	 * 検索.
	 * @param brailleSet
	 * @return
	 */
	abstract public String get(BrailleSet brailleSet, boolean bCapsLock);
	
	abstract public BrailleSet get(String moji);

	abstract public String getModeName();
	
	abstract public int getMaxLength(BrailleSet curBrailleSet);
	
	// 入力不可な符号なときtrue
	abstract public boolean isInvalid(int left, int right);
	
	/**
	 * 開始の意味の前置符号を返す.
	 * @return 
	 */
	public BrailleSet getBeginFu() { return(null);}
	
	/**
	 * 終了の意味の後置符号を返す.
	 * @param typeNumber
	 * @return
	 */
	public BrailleSet getEndFu() { return(null);}
	
	
	/**
	 * 開始符か否か.
	 * @param bs
	 * @return
	 */
	public boolean isBeginFu(BrailleSet bs) { return(false); }

	/**
	 * 終了が確定する符合か否か.
	 * 終了符として定義されていなくても、終了が確定する符号であればtrue.
	 * @param bs
	 * @return
	 */
	public boolean isEndFu(BrailleSet bs) { return(false); }
	
	/**
	 * 2バイト文字へ変換する.
	 * @param c
	 * @return
	 */
	public static char to2Byte(char c) {
		if( c == ' ' ) {
			return('　');
		}
		if( c > ' ' && c <= '~' ) {
			int dist = 'ａ' - 'a';
			return((char)(c + dist));
		}
		return(c);
	}
	
	/**
	 * 1バイト文字へ変換する.
	 * @param c
	 * @return
	 */
	public static char to1Byte(char c) {
		if( c == '　' ) {
			return(' ');
		}
		String s = String.valueOf(c);
		if( s.getBytes().length >= 2 ) {
			int dist = 'ａ' - 'a';
			char ret = (char)(c - dist);
			return(ret);
		}
		return(c);
	}
	
	public static String to1Byte(CharSequence s) {
		//String result = Normalizer.normalize(s, Normalizer.Form.NFKC);
		//return(result);
		char c = s.charAt(0);
		return(String.valueOf(to1Byte(c)));
	}
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib;

import java.util.ArrayList;

import jp.tmhouse.TmLibrary.Activity.SettingsActivity.GuideSetting;
import jp.tmhouse.TmLibrary.Utils.Data.TmBraille;
import jp.tmhouse.TmLibrary.Utils.Log.LogUtil;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.media.AudioManager;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * 点字入力用体表点字振動制御.
 * @author mutoh
 *
 */
public class TmBrailleVibrator extends TmSoundVibrator {
    /*
     * 振動制御.
     * 
     */
    private static final long	c_SHORT_VIBE_TIME = 100; // 単振動の時間(ms)
	private int	m_shortLongRatio = 5;  // 短振動と長振動の比率
	private int	m_waitRatio = 4; // 振動した後の待ち時間の比率
	private boolean m_bMatubiSyoryaku = true; // 末尾省略
	private boolean m_bMutenjiMigiTanTikan = false; // 無点時右短振動置換
	private int[]	m_opcode = new int[6];
	private MasuakeVibeKind		m_masuakeVibeKind = MasuakeVibeKind.SHORT_3;
    
    private static final long	c_BRAILLE_VIBE_TIME_ENDOFINPUT = 800;
    private static final long	c_BRAILLE_VIBE_TIME_WAIT_AFTER_ENDOFINPUT = 700;
    
	/**
	 * マスアケの振動設定の種類.
	 * @author mutoh
	 *
	 */
	public static enum MasuakeVibeKind {
		SHORT_3, // = 3,
		SHORT_1, // = 1,
		LONG_WAIT // = 0,
	}
	
	/**
	 * コンストラクタ.
	 * @param ctx
	 * @param soundResId
	 */
	public TmBrailleVibrator(Context ctx, int soundResId) {
		super(ctx, soundResId);
		loadPreferences();
	}

    /**
     * 設定値の読み込み.
     * @param msg
     */
	private void loadPreferences() {
		//setSoundEnable(GuideSetting.queryEnableBeepGuide(m_ctx));
		setVibratorEnable(GuideSetting.queryEnableVibrate(m_ctx));
		
		// 短振動と長振動の比率
		int shortLongRatio = GuideSetting.queryRecvVibrateShortLongRatioValue(m_ctx);
		setShortLongRatio(shortLongRatio);
		
		// 再生速度セット
		//setVibrteRatio(GuideSetting.queryRecvVibrateSpeedValue(m_ctx));
		
		// 末尾省略モード
		setMatubiSyoryaku(GuideSetting.queryRecvOmissionEndMode(m_ctx));

		// 無点時右短振動置換
		setMutenjiMigiTanTikan(GuideSetting.queryRecvReplaceNone2ShortRightMode(m_ctx));

		// マスアケの振動設定
		setMasuakeVibeKind(GuideSetting.queryRecvMasuakeVibeKind(m_ctx));
		
		// マスターボリューム調整
		setupMasterVolume();
    }

	
	/**
	 * 振動時間を返す.
	 * @param bIsLong    長振動のときtrue
	 * @return
	 */
	private long getVibeTime(boolean bIsLong) {
		if( bIsLong ) {
			return(c_SHORT_VIBE_TIME * m_shortLongRatio);
		}
		return(c_SHORT_VIBE_TIME);
	}
	
	/**
	 * 振動した後の待ち時間.
	 * @return
	 */
	private long getAfterVibeWaitTime() {
		return(c_SHORT_VIBE_TIME * m_waitRatio);
	}
	
	/**
	 * マスアケの振動設定.
	 * @param vibeKind
	 */
	public void setMasuakeVibeKind(MasuakeVibeKind vibeKind) {
		m_masuakeVibeKind = vibeKind;
	}
	
	/**
	 * 短振動と長振動の比率.
	 * @param ratio
	 */
	public void setShortLongRatio(int ratio) {
		if( ratio < 0 ) {
			throw new RuntimeException("bad argument");
		}
		m_shortLongRatio = ratio;
	}
	
	/**
	 * 末尾省略するか.
	 * @param bSyoryaku
	 */
	public void setMatubiSyoryaku(boolean bSyoryaku) {
		m_bMatubiSyoryaku = bSyoryaku;
	}

	/**
	 * 無点字右短振動置換.
	 * @param bTikan
	 */
	public void setMutenjiMigiTanTikan(boolean bTikan) {
		m_bMutenjiMigiTanTikan = bTikan;
	}
	
	/**
	 * 一点式振動順序.
	 */
	public enum SingleKind {
		USE_UNKNOWN,
		USE_SINGLE_123456,
		USE_SINGLE_142536,
		//USE_STEREO,
	}
	
	/**
	 * 区切りの振動を行う.
	 */
	public void playBreakVibrate() {
		long vibeTime = c_BRAILLE_VIBE_TIME_ENDOFINPUT;
		long afterWaitTime = c_BRAILLE_VIBE_TIME_WAIT_AFTER_ENDOFINPUT;
		
		// 一点式
		//if( m_soundChanel == SoundChannel.USE_SINGLE_123456 ||
		//		m_soundChanel == SoundChannel.USE_SINGLE_142536 )
		{
			// 1chだけだったときは端末の音を鳴らす
			addSingleSoundVibrate(vibeTime);
			addSingleSoundWait(afterWaitTime);
		}
		
		/*** 入力時振動は一点式のみサポート
		// 二点式
		if( isConnectingHeadset() ) {
			// 右、左を一回ずつ鳴らす
			addDualSoundVibrate(vibeTime/2, true, false);
			addDualSoundWait(50);//連続しすぎてわかりずらいから
			addDualSoundVibrate(vibeTime/2, false, true);
			addDualSoundWait(afterWaitTime);
		}
		****/
		
		playDataAsync();
	}
	
	/**
	 * 左右の点字に応じてブザー音で振動させる.
	 * @param left
	 * @param right
	 */
	public void play2PointsVibrate(boolean left, boolean right, boolean bCancelCurrent) {
		// 現在再生中ならば止める場合、間隔をあける
		if( bCancelCurrent ) {
			clearAll();
			addDualSoundWait(10);
			addSingleSoundWait(10);
		}

		// 一点式(と端末振動)
		//if( m_soundChanel == SoundChannel.USE_SINGLE_123456 ||
		//	m_soundChanel == SoundChannel.USE_SINGLE_142536 )
		{
			// 1chだけだったときは端末の音を鳴らす
			addSingleSoundVibrate(getVibeTime(left));
			addSingleSoundWait(getAfterVibeWaitTime());
			addSingleSoundVibrate(getVibeTime(right));
			addSingleSoundWait(getAfterVibeWaitTime());
		}

		/*** 入力時振動は一点式のみサポート
		// 二点式(音声のみ)
		if( isConnectingHeadset() ) {
			long vibeTime = 0;
			if( !left && !right ) {
				// 両方点がないときは、短い振動を両方同時に鳴らす
				vibeTime = getVibeTime(false);
				left = true;
				right = true;
			} else {
				// 両方点ががる、または片方点があるときは、長い振動を
				// 両方または片方鳴らす
				vibeTime = getVibeTime(true);
			}
			addDualSoundVibrate(vibeTime, left, right);
			addDualSoundWait(getAfterVibeWaitTime());
		}
***/
		playDataAsync();
	}

	
	/**
	 * opcode配列をクリアする.
	 */
	private void clearOpcode(int[] oparr) {
		for( int i = 0; i < oparr.length; i++ ) {
			oparr[i] = c_OP_NOP;
		}
	}
	
	//////////////////////////////////////////////////
	// ●振動パターンの命令列ようなものを作り加工して対処する。
	//////////////////////////////////////////////////
	
	/**
	 * 一文字の点字を一点式123456で端末振動で再生する.
	 * @param b
	 * @param waitTime
	 */
	private void addBrailleForSingle(
			TmBraille b, short waitTime, int[] oparr,
			SingleKind channel)
	{
		// 1. opcodeを生成
		// マスアケ時はマスアケ振動設定に従う.
		if( b.isSpace() ) {
			createMasuakeVibeOpSingle(m_masuakeVibeKind, waitTime, oparr);
		} else {
			// single用1個の点字の標準振動opcode生成
			if( channel == SingleKind.USE_SINGLE_123456 ) {
				// 123456版
				for( int i = 0; i < 6; i++ ) {
					if( b.isPointSetted(i + 1) ) {
						oparr[i] = b.isPointBlack(i + 1) ? c_OP_S_LONG : c_OP_S_SHORT;
					}
				}
			} else if( channel == SingleKind.USE_SINGLE_142536 ){
				// 142536版
				int i = 0;
				if( b.isPointSetted(1) ) {
					oparr[i++] = b.isPointBlack(1) ? c_OP_S_LONG : c_OP_S_SHORT;
				}
				if( b.isPointSetted(4) ) {
					oparr[i++] = b.isPointBlack(4) ? c_OP_S_LONG : c_OP_S_SHORT;
				}
				if( b.isPointSetted(2) ) {
					oparr[i++] = b.isPointBlack(2) ? c_OP_S_LONG : c_OP_S_SHORT;
				}
				if( b.isPointSetted(5) ) {
					oparr[i++] = b.isPointBlack(5) ? c_OP_S_LONG : c_OP_S_SHORT;
				}
				if( b.isPointSetted(3) ) {
					oparr[i++] = b.isPointBlack(3) ? c_OP_S_LONG : c_OP_S_SHORT;
				}
				if( b.isPointSetted(6) ) {
					oparr[i++] = b.isPointBlack(6) ? c_OP_S_LONG : c_OP_S_SHORT;
				}
			} else {
				throw new RuntimeException("invalid sound channel");
			}
		}
		
		// 2. 末尾省略処理
		if( m_bMatubiSyoryaku && !b.isSpace() ) {
			matubiSyoryakuSingle(oparr);
		}
		
		// 3. 無点時右短振動置換処理
		// 一点式なので関係ない。
		
		// 点字一字ごとの区切りの意味のデータを入れて、
		// 後で再生位置を変更するときに使うことにする。
		addSingleSeparator();
				
		// 4. データを上から参照して命令どおりに再生する
		execOpcode(oparr);

		addSingleSoundWait(waitTime);
	}
	
	/**
	 * 一文字の点字を二点式で音声で再生する.
	 * @param b
	 * @param waitTime
	 */
	private void addBrailleForDual(TmBraille b, short waitTime, int[] oparr) {
		// 1. 単純に点字データから振動パターンのコードを生成する
		//   データ1個には、{左右長い, 右だけ長い, 左だけ長い, 左右短い}の4通りがある。
		//   常に上中下の3個生成する。
		// 
		// 2. 末尾省略処理
		//   マスアケでないとき、下段方向から「左右短い」が続く限り、その命令を削除する。
		// 
		// 3. 無点時右短振動置換処理
		//   データの中に「左右短い」があるとき、「右だけ短い」の命令に変更する。
		//
		// 4. データを上から参照して命令どおりに再生する。
		
		// 1. opcodeを生成
		// マスアケ時はマスアケ振動設定に従う.
		if( b.isSpace() ) {
			createMasuakeVibeOpDual(m_masuakeVibeKind, waitTime, oparr);
		} else {
			createVibeOpDual(b, oparr);
		}
		
		// 2. 末尾省略処理
		if( m_bMatubiSyoryaku && !b.isSpace() ) {
			matubiSyoryakuDual(oparr);
		}
		
		// 3. 無点時右短振動置換処理
		if( m_bMutenjiMigiTanTikan ) {
			mutenjiMigiTanTikanDual(oparr);
		}
		
		// 点字一字ごとの区切りの意味のデータを入れて、
		// 後で再生位置を変更するときに使うことにする。
		addDualSoundSeparator();
		
		// 4. データを上から参照して命令どおりに再生する
		execOpcode(oparr);

		addDualSoundWait(waitTime);
	}
	
	/**
	 * BrailleSet配列を再生する.
	 * @param brailleArr
	 */
	public void addBrailleSetArray(
			ArrayList<BrailleSet> brailleArr)
	{
		if( brailleArr == null ) {
			return;
		}

		// マスターボリューム調整
		setupMasterVolume();
		
    	TmBraille b = new TmBraille();
    	for( BrailleSet bs : brailleArr ) {
    		for( int pos = 0; pos < bs.length(); pos++ ) {
    			b.clear();
    			b.set(bs.get(pos));
    			addBraille(b);
    		}
    	}
    	//playData();
	}
	
	/**
	 * TmBraille配列を追加する.
	 * @param brailleArr
	 */
	public void addTmBrailleArray(ArrayList<TmBraille> brailleArr) {
		if( brailleArr == null || brailleArr.size() == 0 ) {
			return;
		}
		
		// マスターボリューム調整
		setupMasterVolume();
		
		for( TmBraille b : brailleArr ) {
    		addBraille(b);
		}
	}
	
	/**
	 * 点字ひとつを再生.
	 * @param b
	 */
	public void addBraille(TmBraille b) {
		short waitTime = (short)GuideSetting.queryRecvVibrateEachSpaceTimeValue(m_ctx);
		
		clearOpcode(m_opcode);
		if( isConnectingHeadset() ) {
			addBrailleForDual(b, waitTime, m_opcode);
		} else {
			SingleKind kind = GuideSetting.queryRecvSingleVibeKind(m_ctx);
			addBrailleForSingle(b, waitTime, m_opcode, kind);
		}
	}

	
	// opcode種類。下16bitは引数を埋められる。
	private static final int c_OPCODE_BITS	= 16;
	private static final int c_OP_VALUE_MASK = 0xffff; // 16bit value
	private static final int c_OP_NOP			= 0;
	private static final int c_OP_D_BOTH_LONG		= 1 << c_OPCODE_BITS;
	private static final int c_OP_D_BOTH_SHORT	= 2 << c_OPCODE_BITS;
	private static final int c_OP_D_LEFT_LONG		= 3 << c_OPCODE_BITS;
	private static final int c_OP_D_LEFT_SHORT	= 4 << c_OPCODE_BITS;
	private static final int c_OP_D_RIGHT_LONG	= 5 << c_OPCODE_BITS;
	private static final int c_OP_D_RIGHT_SHORT	= 6 << c_OPCODE_BITS;
	private static final int c_OP_S_SHORT			= 7 << c_OPCODE_BITS;
	private static final int c_OP_S_LONG			= 8 << c_OPCODE_BITS;
	private static final int c_OP_D_WAIT		= 14 << c_OPCODE_BITS;
	private static final int c_OP_S_WAIT		= 15 << c_OPCODE_BITS;
	
	// valueを消してopcodeを返す.
	private int getOp(int opcode) {
		return(opcode & ~c_OP_VALUE_MASK);
	}
	// opcodeを消してvalueを返す.
	private short getVal(int opcode) {
		return((short)(opcode & c_OP_VALUE_MASK));
	}
	// opcodeとvalueをセットする
	private int setOp(int op, short value) {
		if( value >= c_OP_VALUE_MASK ) {
			throw new RuntimeException("bad arg");
		}
		return(op | value);
	}
	
	// dual用1個の点字の標準振動opcode生成
	private void createVibeOpDual(TmBraille b, int[] oparr) {
		if( b.isSpace() ) {
			// マスアケ時
			throw new RuntimeException("bad args");
		}
		oparr[0] = opCodeDual(b.isPointBlack(1), b.isPointBlack(4));
		oparr[1] = opCodeDual(b.isPointBlack(2), b.isPointBlack(5));
		oparr[2] = opCodeDual(b.isPointBlack(3), b.isPointBlack(6));
	}
	
	
	private void createMasuakeVibeOpDual(
			MasuakeVibeKind masuakeKind, short waitTime, int[] oparr) {
		//if( waitTime >= c_OP_VALUE_MASK ) {
		//	throw new RuntimeException("bad args. wait time is too big.");
		//}

		// マスアケ時
		switch(masuakeKind) {
		case SHORT_1:
			oparr[0] = c_OP_D_BOTH_SHORT;
			oparr[1] = c_OP_NOP;
			oparr[2] = c_OP_NOP;
			break;
		case SHORT_3:
			oparr[0] = c_OP_D_BOTH_SHORT;
			oparr[1] = c_OP_D_BOTH_SHORT;
			oparr[2] = c_OP_D_BOTH_SHORT;
			break;
		case LONG_WAIT:
			oparr[0] = setOp(c_OP_D_WAIT, waitTime);
			oparr[1] = c_OP_NOP;
			oparr[2] = c_OP_NOP;
			break;
		default:
			throw new RuntimeException("not impl");
		}
	}
	
	private void createMasuakeVibeOpSingle(
			MasuakeVibeKind masuakeKind, short waitTime, int[] oparr) {
		// マスアケ時
		switch(masuakeKind) {
		case SHORT_1:
			oparr[0] = c_OP_S_SHORT;
			oparr[1] = c_OP_NOP;
			oparr[2] = c_OP_NOP;
			oparr[3] = c_OP_NOP;
			oparr[4] = c_OP_NOP;
			oparr[5] = c_OP_NOP;
			break;
		case SHORT_3:
			oparr[0] = c_OP_S_SHORT;
			oparr[1] = c_OP_S_SHORT;
			oparr[2] = c_OP_S_SHORT;
			oparr[3] = c_OP_S_SHORT;
			oparr[4] = c_OP_S_SHORT;
			oparr[5] = c_OP_S_SHORT;
			break;
		case LONG_WAIT:
			oparr[0] = setOp(c_OP_S_WAIT, waitTime);
			oparr[1] = c_OP_NOP;
			oparr[2] = c_OP_NOP;
			oparr[3] = c_OP_NOP;
			oparr[4] = c_OP_NOP;
			oparr[5] = c_OP_NOP;
			break;
		default:
			throw new RuntimeException("not impl");
		}
	}
	
	/**
	 * 左右の点から1個のopcodeを生成
	 * @param left
	 * @param right
	 * @return
	 */
	private int opCodeDual(boolean left, boolean right) {
		if( left && right ) {
			return(c_OP_D_BOTH_LONG);
		}
		if( !left && !right ) {
			return(c_OP_D_BOTH_SHORT);
		}
		if( left && !right ) {
			return(c_OP_D_LEFT_LONG);
		}
		if( !left && right ) {
			return(c_OP_D_RIGHT_LONG);
		}
		throw new RuntimeException("bad args");
	}
	
	// 末尾省略
	private void matubiSyoryakuDual(int[] opcode) {
		if( opcode[2] == c_OP_D_BOTH_SHORT ) {
			opcode[2] = c_OP_NOP;
			if( opcode[1] == c_OP_D_BOTH_SHORT ) {
				opcode[1] = c_OP_NOP;
			}
		}
	}
	
	private void matubiSyoryakuSingle(int[] opcode) {
		for( int i = 5; i >= 0; i-- ) {
			if( opcode[i] == c_OP_S_SHORT ) {
				opcode[i] = c_OP_NOP;
			} else {
				break;
			}
		}
	}
	
	
	// 無点時右単振動置換処理
	private void mutenjiMigiTanTikanDual(int[] opcode) {
		if( opcode[0] == c_OP_D_BOTH_SHORT ) {
			opcode[0] = c_OP_D_RIGHT_SHORT;
		}
		if( opcode[1] == c_OP_D_BOTH_SHORT ) {
			opcode[1] = c_OP_D_RIGHT_SHORT;
		}
		if( opcode[2] == c_OP_D_BOTH_SHORT ) {
			opcode[2] = c_OP_D_RIGHT_SHORT;
		}
	}
	
	// opcodeの実行
	private void execOpcode(int[] opcodeArr) {
		long longTime = getVibeTime(true);
		long shortTime = getVibeTime(false);
		for( int opcode : opcodeArr ) {
			switch(getOp(opcode)) {
			case c_OP_NOP:
				continue;
				
			/**
			 * For Dual
			 */
			case c_OP_D_BOTH_LONG:
				addDualSoundVibrate(longTime, true, true);
				addDualSoundWait(getAfterVibeWaitTime());
				break;
			case c_OP_D_BOTH_SHORT:
				addDualSoundVibrate(shortTime, true, true);
				addDualSoundWait(getAfterVibeWaitTime());
				break;
			case c_OP_D_LEFT_LONG:
				addDualSoundVibrate(longTime, true, false);
				addDualSoundWait(getAfterVibeWaitTime());
				break;
			case c_OP_D_LEFT_SHORT:
				addDualSoundVibrate(shortTime, true, false);
				addDualSoundWait(getAfterVibeWaitTime());
				break;
			case c_OP_D_RIGHT_LONG:
				addDualSoundVibrate(longTime, false, true);
				addDualSoundWait(getAfterVibeWaitTime());
				break;
			case c_OP_D_RIGHT_SHORT:
				addDualSoundVibrate(shortTime, false, true);
				addDualSoundWait(getAfterVibeWaitTime());
				break;
			case c_OP_D_WAIT:
				short waitTime = getVal(opcode);
				addDualSoundWait(waitTime + getAfterVibeWaitTime());
				break;
				
			/*
			 * For Single
			 */
			case c_OP_S_SHORT:
				addSingleSoundVibrate(shortTime);
				addSingleSoundWait(getAfterVibeWaitTime());
				break;
			case c_OP_S_LONG:
				addSingleSoundVibrate(longTime);
				addSingleSoundWait(getAfterVibeWaitTime());
				break;
			case c_OP_S_WAIT:
				short w = getVal(opcode);
				addSingleSoundWait(w + getAfterVibeWaitTime());
				break;

			default:
				throw new RuntimeException("bad opcode");
			}
		}
	}
}
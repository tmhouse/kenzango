/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.Utils.Data.TenjiConverter.impl;

import java.util.Locale;
import java.util.Vector;

import jp.tmhouse.TmLibrary.Utils.Data.TmBraille;
import jp.tmhouse.TmLibrary.Utils.Data.TenjiConverter.TmTenjiConverterImpl;

import android.util.Log;

/**
 * ８進数２個から該当する日本語を取得する.
 * @author mutoh
 *
 */
public class TmJapaneseTenjiConverter extends TmTenjiConverterImpl {
	
	// 日本語
	private static String[][][] 	m_JPData = new String[10][8][8];
	private static String[][][] 	m_JPEngSpeechData = new String[10][8][8];
	private static int[][] 		m_JPFugoData = new int[8][8];
	
	// かな系前置符号
	
	// 該当する点字番号だけビットを立てる。
	// なので一番右のビットは使用していない。常に0。
	
	// 日本語前置符号
	// 濁音(5):「がぎぐげご」など
	private static final int	c_SIGN_DAKUON		= 1;
	// 半濁音(6):「ぱぴぷぺぽ」など
	private static final int	c_SIGN_HANDAKUON	= 2;
	// 拗音・開拗音(4)：「きゃきゅきょ」など
	private static final int	c_SIGN_YOUON		= 3;
	// 拗濁音(45)：「ぎゃぎゅぎょ」
	private static final int	c_SIGN_YOUDAKUON	= 4;
	// 拗半濁音(46)：「ぴゃぴゅぴょ」
	private static final int	c_SIGN_YOUHANDAKU	= 5;
	
	//-特殊音系(外来音)前置符号
	
	// 開拗音(4)：「イェキェシェチェニェヒェ」など
	//public static final int	c_KATA_KAIYOUON_FU	= ;
	// 合拗音(26)：「ウィウェウォ」など
	//public static final int	c_KATA_GOUYOUON_FU	= 6;
	// 合拗音(246)：「グァグィグェグォ」など
	//public static final int	c_KATA_GOUYOUON_FU2	= 7;
	
	// 数字符(3456)
	private static final int	c_SUUJI_FU			= 8;
	
	static {
		initJapanese();
		initJapaneseEnglishSpeech();
		initJPFugoData();
		
		/**
		//漢字対応でコードを生成したかった
		StringBuilder sb = new StringBuilder(2000);
		String head = "putMojiBrailleSet(new BrailleSet";
		for( int i = 0; i < 10; i++ ) {
			for( int left = 0; left < 8; left++ ) {
				for( int right = 0; right < 8; right++ ) {
					String str = m_JPData[i][left][right];
					if( str != null ) {
						sb.append(head);
						sb.append('(');
						if( i == c_SIGN_DAKUON ) {
							sb.append('2'); sb.append(',');
						}
						if( i == c_SIGN_HANDAKUON ) {
							sb.append('4'); sb.append(',');
						}
						if( i == c_SIGN_YOUON ) {
							sb.append('1'); sb.append(',');
						}
						if( i == c_SIGN_YOUDAKUON ) {
							sb.append('3'); sb.append(',');
						}
						if( i == c_SIGN_YOUHANDAKU ) {
							sb.append('5'); sb.append(',');
						}
						if( i == c_SUUJI_FU ) {
							sb.append("47"); sb.append(',');
						}
						int fugo = (left * 10) + right;
						sb.append(fugo);
						sb.append(',');
						sb.append('\"');
						sb.append(str);
						sb.append('\"');
						sb.append(')');
						sb.append(')');
						sb.append(';');
						sb.append('\n');
					}
				}
			}
		}
		m_data = sb.toString();
		**/
	}
	//public static String m_data;
	
	private static void initJPFugoData() {
		m_JPFugoData[0][2] = c_SIGN_DAKUON;
		m_JPFugoData[0][4] = c_SIGN_HANDAKUON;
		m_JPFugoData[0][1] = c_SIGN_YOUON;
		m_JPFugoData[0][3] = c_SIGN_YOUDAKUON;
		m_JPFugoData[0][5] = c_SIGN_YOUHANDAKU;
		//m_JPFugoData[2][4] = c_KATA_GOUYOUON_FU;	// これ「？」と同じだ。文脈依存か。
		//m_JPFugoData[2][6] = c_KATA_GOUYOUON_FU2;
		m_JPFugoData[4][7] = c_SUUJI_FU;
	}
	
	private static void initJapanese() {
		int aL0 = 1, aR0 = 0;//あ
		int aL1 = 3, aR1 = 0;//い
		int aL2 = 1, aR2 = 1;//う
		int aL3 = 3, aR3 = 1;//え
		int aL4 = 2, aR4 = 1;//お
		
		//int ten1_add_li = 1;
		//int ten2_add_li = 2;
		int ten3_add_li = 4;
		//int ten4_add_rj = 1;
		int ten5_add_rj = 2;
		int ten6_add_rj = 4;
		
		int li = 0;
		int rj = 0;
		
		int z = TmTenjiConverterImpl.c_SIGN_NOT_SIGN;
		m_JPData[z][0][0] = "　";	// 空白文字
		
		// あ行
		m_JPData[z][aL0 + li][aR0 + rj] = "あ";
		m_JPData[z][aL1 + li][aR1 + rj] = "い";
		m_JPData[z][aL2 + li][aR2 + rj] = "う";
		m_JPData[z][aL3 + li][aR3 + rj] = "え";
		m_JPData[z][aL4 + li][aR4 + rj] = "お";
		
		// か行：母音に６の点を加える
		li = 0; rj = ten6_add_rj;
		m_JPData[z][aL0 + li][aR0 + rj] = "か";
		m_JPData[z][aL1 + li][aR1 + rj] = "き";
		m_JPData[z][aL2 + li][aR2 + rj] = "く";
		m_JPData[z][aL3 + li][aR3 + rj] = "け";
		m_JPData[z][aL4 + li][aR4 + rj] = "こ";
		
		// さ行：母音に５と６の点を加える
		li = 0; rj = ten5_add_rj + ten6_add_rj;
		m_JPData[z][aL0 + li][aR0 + rj] = "さ";
		m_JPData[z][aL1 + li][aR1 + rj] = "し";
		m_JPData[z][aL2 + li][aR2 + rj] = "す";
		m_JPData[z][aL3 + li][aR3 + rj] = "せ";
		m_JPData[z][aL4 + li][aR4 + rj] = "そ";
		
		// た行：母音に３と５の点を加える
		li = ten3_add_li; rj = ten5_add_rj;
		m_JPData[z][aL0 + li][aR0 + rj] = "た";
		m_JPData[z][aL1 + li][aR1 + rj] = "ち";
		m_JPData[z][aL2 + li][aR2 + rj] = "つ";
		m_JPData[z][aL3 + li][aR3 + rj] = "て";
		m_JPData[z][aL4 + li][aR4 + rj] = "と";
		
		// な行：母音に３の点を加える
		li = ten3_add_li; rj = 0;
		m_JPData[z][aL0 + li][aR0 + rj] = "な";
		m_JPData[z][aL1 + li][aR1 + rj] = "に";
		m_JPData[z][aL2 + li][aR2 + rj] = "ぬ";
		m_JPData[z][aL3 + li][aR3 + rj] = "ね";
		m_JPData[z][aL4 + li][aR4 + rj] = "の";
		
		// な行：母音に３と６の点を加える
		li = ten3_add_li; rj = ten6_add_rj;
		m_JPData[z][aL0 + li][aR0 + rj] = "は";
		m_JPData[z][aL1 + li][aR1 + rj] = "ひ";
		m_JPData[z][aL2 + li][aR2 + rj] = "ふ";
		m_JPData[z][aL3 + li][aR3 + rj] = "へ";
		m_JPData[z][aL4 + li][aR4 + rj] = "ほ";
		
		// ま行：母音に３と５と６の点を加える
		li = ten3_add_li; rj = ten5_add_rj + ten6_add_rj;
		m_JPData[z][aL0 + li][aR0 + rj] = "ま";
		m_JPData[z][aL1 + li][aR1 + rj] = "み";
		m_JPData[z][aL2 + li][aR2 + rj] = "む";
		m_JPData[z][aL3 + li][aR3 + rj] = "め";
		m_JPData[z][aL4 + li][aR4 + rj] = "も";
		
		// や行：
		m_JPData[z][4][1] = "や";
		m_JPData[z][4][5] = "ゆ";
		m_JPData[z][4][3] = "よ";
		
		// ら行：母音に５の点を加える
		li = 0; rj = ten5_add_rj;
		m_JPData[z][aL0 + li][aR0 + rj] = "ら";
		m_JPData[z][aL1 + li][aR1 + rj] = "り";
		m_JPData[z][aL2 + li][aR2 + rj] = "る";
		m_JPData[z][aL3 + li][aR3 + rj] = "れ";
		m_JPData[z][aL4 + li][aR4 + rj] = "ろ";
		
		// わ行
		m_JPData[z][4][0] = "わ";
		m_JPData[z][6][0] = "ゐ";
		m_JPData[z][6][2] = "ゑ";
		m_JPData[z][4][2] = "を";
		
		// 記号類
		m_JPData[z][4][6] = "ん";
		m_JPData[z][2][0] = "っ";
		m_JPData[z][2][2] = "ー";
		m_JPData[z][0][6] = "、";
		m_JPData[z][2][4] = "？";
		m_JPData[z][6][2] = "！";// "ゑ"と一緒じゃん。
		m_JPData[z][0][2] = "・";
		m_JPData[z][2][6] = "。";
		
		// 濁音、半濁音、拗音
		m_JPData[z][0][2] = "濁音符";
		m_JPData[z][0][4] = "半濁音符";
		m_JPData[z][0][3] = "濁拗音符";
		m_JPData[z][0][5] = "半拗音符";
		m_JPData[z][0][1] = "拗音符";
		m_JPData[z][4][7] = "数符";
		
		
		// 濁音系
		z = c_SIGN_DAKUON;
		m_JPData[z][1][4] = "が";
		m_JPData[z][3][4] = "ぎ";
		m_JPData[z][1][5] = "ぐ";
		m_JPData[z][3][5] = "げ";
		m_JPData[z][2][5] = "ご";
		m_JPData[z][1][6] = "ざ";
		m_JPData[z][3][6] = "じ";
		m_JPData[z][1][7] = "ず";
		m_JPData[z][3][7] = "ぜ";
		m_JPData[z][2][7] = "ぞ";
		m_JPData[z][5][2] = "だ";
		m_JPData[z][7][2] = "ぢ";
		m_JPData[z][5][3] = "づ";
		m_JPData[z][7][3] = "で";
		m_JPData[z][6][3] = "ど";
		m_JPData[z][5][4] = "ば";
		m_JPData[z][7][4] = "び";
		m_JPData[z][5][5] = "ぶ";
		m_JPData[z][7][5] = "べ";
		m_JPData[z][6][5] = "ぼ";
		
		// 半濁音
		z = c_SIGN_HANDAKUON;
		m_JPData[z][5][4] = "ぱ";
		m_JPData[z][7][4] = "ぴ";
		m_JPData[z][5][5] = "ぷ";
		m_JPData[z][7][5] = "ぺ";
		m_JPData[z][6][5] = "ぽ";
		
		// 拗音符
		z = c_SIGN_YOUON;
		m_JPData[z][1][4] = "きゃ";
		m_JPData[z][1][5] = "きゅ";
		m_JPData[z][2][5] = "きょ";
		m_JPData[z][1][6] = "しゃ";
		m_JPData[z][1][7] = "しゅ";
		m_JPData[z][2][7] = "しょ";
		m_JPData[z][5][2] = "ちゃ";
		m_JPData[z][5][3] = "ちゅ";
		m_JPData[z][6][3] = "ちょ";
		m_JPData[z][5][0] = "にゃ";
		m_JPData[z][5][1] = "にゅ";
		m_JPData[z][6][1] = "にょ";
		m_JPData[z][5][4] = "ひゃ";
		m_JPData[z][5][5] = "ひゅ";
		m_JPData[z][6][5] = "ひょ";
		m_JPData[z][5][6] = "みゃ";
		m_JPData[z][5][7] = "みゅ";
		m_JPData[z][6][7] = "みょ";
		m_JPData[z][1][2] = "りゃ";
		m_JPData[z][1][3] = "りゅ";
		m_JPData[z][2][3] = "りょ";
		
		// 拗濁音
		z = c_SIGN_YOUDAKUON;
		m_JPData[z][1][4] = "ぎゃ";
		m_JPData[z][1][5] = "ぎゅ";
		m_JPData[z][2][5] = "ぎょ";
		m_JPData[z][1][6] = "じゃ";
		m_JPData[z][1][7] = "じゅ";
		m_JPData[z][2][7] = "じょ";
		m_JPData[z][5][2] = "ぢゃ";
		m_JPData[z][5][3] = "ぢゅ";
		m_JPData[z][6][3] = "ぢょ";
		m_JPData[z][5][4] = "びゃ";
		m_JPData[z][5][5] = "びゅ";
		m_JPData[z][6][5] = "びょ";
		
		// 拗半濁音
		z = c_SIGN_YOUHANDAKU;
		m_JPData[z][5][4] = "ぴゃ";
		m_JPData[z][5][5] = "ぴゅ";
		m_JPData[z][6][5] = "ぴょ";
		
		// 数値
		z = c_SUUJI_FU;
		m_JPData[z][2][3] = "０";
		m_JPData[z][1][0] = "１";
		m_JPData[z][3][0] = "２";
		m_JPData[z][1][1] = "３";
		m_JPData[z][1][3] = "４";
		m_JPData[z][1][2] = "５";
		m_JPData[z][3][1] = "６";
		m_JPData[z][3][3] = "７";
		m_JPData[z][3][2] = "８";
		m_JPData[z][2][1] = "９";
		

	}
	
	private static void initJapaneseEnglishSpeech() {
		int aL0 = 1, aR0 = 0;//あ
		int aL1 = 3, aR1 = 0;//い
		int aL2 = 1, aR2 = 1;//う
		int aL3 = 3, aR3 = 1;//え
		int aL4 = 2, aR4 = 1;//お
		
		//int ten1_add_li = 1;
		//int ten2_add_li = 2;
		int ten3_add_li = 4;
		//int ten4_add_rj = 1;
		int ten5_add_rj = 2;
		int ten6_add_rj = 4;
		
		int li = 0;
		int rj = 0;
		
		int z = TmTenjiConverterImpl.c_SIGN_NOT_SIGN;
		m_JPEngSpeechData[z][0][0] = " "; // 空白文字
		
		// あ行
		m_JPEngSpeechData[z][aL0 + li][aR0 + rj] = "a";
		m_JPEngSpeechData[z][aL1 + li][aR1 + rj] = "e";
		m_JPEngSpeechData[z][aL2 + li][aR2 + rj] = "wo";
		m_JPEngSpeechData[z][aL3 + li][aR3 + rj] = "ae";
		m_JPEngSpeechData[z][aL4 + li][aR4 + rj] = "o";
		
		// か行：母音に６の点を加える
		li = 0; rj = ten6_add_rj;
		m_JPEngSpeechData[z][aL0 + li][aR0 + rj] = "ka";
		m_JPEngSpeechData[z][aL1 + li][aR1 + rj] = "key";
		m_JPEngSpeechData[z][aL2 + li][aR2 + rj] = "ku";
		m_JPEngSpeechData[z][aL3 + li][aR3 + rj] = "ke";
		m_JPEngSpeechData[z][aL4 + li][aR4 + rj] = "ko";
		
		// さ行：母音に５と６の点を加える
		li = 0; rj = ten5_add_rj + ten6_add_rj;
		m_JPEngSpeechData[z][aL0 + li][aR0 + rj] = "sa";
		m_JPEngSpeechData[z][aL1 + li][aR1 + rj] = "see";
		m_JPEngSpeechData[z][aL2 + li][aR2 + rj] = "su";
		m_JPEngSpeechData[z][aL3 + li][aR3 + rj] = "sei";
		m_JPEngSpeechData[z][aL4 + li][aR4 + rj] = "so";
		
		// た行：母音に３と５の点を加える
		li = ten3_add_li; rj = ten5_add_rj;
		m_JPEngSpeechData[z][aL0 + li][aR0 + rj] = "ta";
		m_JPEngSpeechData[z][aL1 + li][aR1 + rj] = "chi";
		m_JPEngSpeechData[z][aL2 + li][aR2 + rj] = "tsu";
		m_JPEngSpeechData[z][aL3 + li][aR3 + rj] = "t";
		m_JPEngSpeechData[z][aL4 + li][aR4 + rj] = "toh";
		
		// な行：母音に３の点を加える
		li = ten3_add_li; rj = 0;
		m_JPEngSpeechData[z][aL0 + li][aR0 + rj] = "na";
		m_JPEngSpeechData[z][aL1 + li][aR1 + rj] = "knee";
		m_JPEngSpeechData[z][aL2 + li][aR2 + rj] = "nuu";
		m_JPEngSpeechData[z][aL3 + li][aR3 + rj] = "ne";
		m_JPEngSpeechData[z][aL4 + li][aR4 + rj] = "no";
		
		// は行：母音に３と６の点を加える
		li = ten3_add_li; rj = ten6_add_rj;
		m_JPEngSpeechData[z][aL0 + li][aR0 + rj] = "ha";
		m_JPEngSpeechData[z][aL1 + li][aR1 + rj] = "he";
		m_JPEngSpeechData[z][aL2 + li][aR2 + rj] = "who";
		m_JPEngSpeechData[z][aL3 + li][aR3 + rj] = "hey";
		m_JPEngSpeechData[z][aL4 + li][aR4 + rj] = "fo";
		
		// ま行：母音に３と５と６の点を加える
		li = ten3_add_li; rj = ten5_add_rj + ten6_add_rj;
		m_JPEngSpeechData[z][aL0 + li][aR0 + rj] = "ma";
		m_JPEngSpeechData[z][aL1 + li][aR1 + rj] = "me";
		m_JPEngSpeechData[z][aL2 + li][aR2 + rj] = "mu";
		m_JPEngSpeechData[z][aL3 + li][aR3 + rj] = "may";
		m_JPEngSpeechData[z][aL4 + li][aR4 + rj] = "mo";
		
		// や行：
		m_JPEngSpeechData[z][4][1] = "ya";
		m_JPEngSpeechData[z][4][5] = "yu";
		m_JPEngSpeechData[z][4][3] = "yo";
		
		// ら行：母音に５の点を加える
		li = 0; rj = ten5_add_rj;
		m_JPEngSpeechData[z][aL0 + li][aR0 + rj] = "ra";
		m_JPEngSpeechData[z][aL1 + li][aR1 + rj] = "lee";
		m_JPEngSpeechData[z][aL2 + li][aR2 + rj] = "ru";
		m_JPEngSpeechData[z][aL3 + li][aR3 + rj] = "re";
		m_JPEngSpeechData[z][aL4 + li][aR4 + rj] = "ro";
		
		// わ行
		m_JPEngSpeechData[z][4][0] = "wa";
		m_JPEngSpeechData[z][6][0] = "wo";
		m_JPEngSpeechData[z][6][2] = "e";
		m_JPEngSpeechData[z][4][2] = "wo";
		
		// 記号類
		m_JPEngSpeechData[z][4][6] = "nnn";//ん
		m_JPEngSpeechData[z][2][0] = "";//っ
		m_JPEngSpeechData[z][2][2] = "";//ー
		m_JPEngSpeechData[z][0][6] = "、";
		m_JPEngSpeechData[z][2][4] = "?";
		m_JPEngSpeechData[z][6][2] = "!";// "ゑ"と一緒じゃん。
		//m_JPEngSpeechData[z][0][2] = "ten";//「・」これ濁音符と一緒じゃん。
		m_JPEngSpeechData[z][2][6] = "maru";//「。」
		
		// 濁音、半濁音、拗音
		m_JPEngSpeechData[z][0][2] = "";
		m_JPEngSpeechData[z][0][4] = "";
		m_JPEngSpeechData[z][0][3] = "";
		m_JPEngSpeechData[z][0][5] = "";
		
		// 濁音系
		z = c_SIGN_DAKUON;
		m_JPEngSpeechData[z][1][4] = "gaa";//が
		m_JPEngSpeechData[z][3][4] = "gee";//ぎ
		m_JPEngSpeechData[z][1][5] = "goo";
		m_JPEngSpeechData[z][3][5] = "gay";
		m_JPEngSpeechData[z][2][5] = "go";
		m_JPEngSpeechData[z][1][6] = "za";
		m_JPEngSpeechData[z][3][6] = "g"; 	//じ
		m_JPEngSpeechData[z][1][7] = "zoo";	//ず
		m_JPEngSpeechData[z][3][7] = "they";
		m_JPEngSpeechData[z][2][7] = "zo";
		m_JPEngSpeechData[z][5][2] = "da";
		m_JPEngSpeechData[z][7][2] = "gi";	//ぢ
		m_JPEngSpeechData[z][5][3] = "zoo";	//づ
		m_JPEngSpeechData[z][7][3] = "de";	//で
		m_JPEngSpeechData[z][6][3] = "doh";	//ど
		m_JPEngSpeechData[z][5][4] = "ba";
		m_JPEngSpeechData[z][7][4] = "be";
		m_JPEngSpeechData[z][5][5] = "bu";
		m_JPEngSpeechData[z][7][5] = "beh";
		m_JPEngSpeechData[z][6][5] = "bo";
		
		// 半濁音
		z = c_SIGN_HANDAKUON;
		m_JPEngSpeechData[z][5][4] = "pa";
		m_JPEngSpeechData[z][7][4] = "p";
		m_JPEngSpeechData[z][5][5] = "pu";
		m_JPEngSpeechData[z][7][5] = "pe";
		m_JPEngSpeechData[z][6][5] = "po";
		
		// 拗音符
		z = c_SIGN_YOUON;
		m_JPEngSpeechData[z][1][4] = "kya";//きゃ:(kya=kiya)
		m_JPEngSpeechData[z][1][5] = "q";
		m_JPEngSpeechData[z][2][5] = "kyo";
		m_JPEngSpeechData[z][1][6] = "sha"; // しゃ
		m_JPEngSpeechData[z][1][7] = "shu";
		m_JPEngSpeechData[z][2][7] = "sho";
		m_JPEngSpeechData[z][5][2] = "cha";
		m_JPEngSpeechData[z][5][3] = "chu";
		m_JPEngSpeechData[z][6][3] = "cho";
		m_JPEngSpeechData[z][5][0] = "neer";
		m_JPEngSpeechData[z][5][1] = "new";
		m_JPEngSpeechData[z][6][1] = "kneyo"; // にょ
		m_JPEngSpeechData[z][5][4] = "heeya";//ひゃ:(hya:ha-ya)
		m_JPEngSpeechData[z][5][5] = "heeyu";//ひゅ(hyu:ha-yu)
		m_JPEngSpeechData[z][6][5] = "heeyo";//ひょ
		m_JPEngSpeechData[z][5][6] = "meeya";//みゃ
		m_JPEngSpeechData[z][5][7] = "meeyu";//みゅ
		m_JPEngSpeechData[z][6][7] = "meeyo";//みょ
		m_JPEngSpeechData[z][1][2] = "leeya";
		m_JPEngSpeechData[z][1][3] = "leeyu";
		m_JPEngSpeechData[z][2][3] = "leeyo";
		
		// 拗濁音
		z = c_SIGN_YOUDAKUON;
		m_JPEngSpeechData[z][1][4] = "geeya";//ぎゃ
		m_JPEngSpeechData[z][1][5] = "geeyu";//ぎゅ
		m_JPEngSpeechData[z][2][5] = "geeyo";//ぎょ
		m_JPEngSpeechData[z][1][6] = "jar";
		m_JPEngSpeechData[z][1][7] = "ju";
		m_JPEngSpeechData[z][2][7] = "jo";
		m_JPEngSpeechData[z][5][2] = "jar";
		m_JPEngSpeechData[z][5][3] = "ju";
		m_JPEngSpeechData[z][6][3] = "jo";
		m_JPEngSpeechData[z][5][4] = "beeya";//びゃ
		m_JPEngSpeechData[z][5][5] = "view";
		m_JPEngSpeechData[z][6][5] = "viow";
		
		// 拗半濁音
		z = c_SIGN_YOUHANDAKU;
		m_JPEngSpeechData[z][5][4] = "pya";//ぴゃ
		m_JPEngSpeechData[z][5][5] = "pyu";//ぴゅ
		m_JPEngSpeechData[z][6][5] = "p yo";//ぴょ
		
		// 数値
		z = c_SUUJI_FU;
		m_JPEngSpeechData[z][2][3] = "zero";
		m_JPEngSpeechData[z][1][0] = "ichi";
		m_JPEngSpeechData[z][3][0] = "knee";
		m_JPEngSpeechData[z][1][1] = "sun";
		m_JPEngSpeechData[z][1][3] = "yon";
		m_JPEngSpeechData[z][1][2] = "go";
		m_JPEngSpeechData[z][3][1] = "roku";
		m_JPEngSpeechData[z][3][3] = "nana";
		m_JPEngSpeechData[z][3][2] = "hatchi";
		m_JPEngSpeechData[z][2][1] = "q";
	}
	
	/**
	 * 前置符号を取得
	 * @param left
	 * @param right
	 * @return
	 */
	@Override
	public int getSignCode(TmBraille braille) {
		return(getSignCodeImpl(m_JPFugoData, braille));
	}
	
	/**
	 * 点字から墨字へ一文字変換.
	 * @param left
	 * @param right
	 * @param locale
	 * @return
	 */
	@Override
	public String convert(TmBraille braille) {
		TmBraille prevBraille = braille != null ? braille.getPrev() : null;
		int prevSign = getSignCode(prevBraille);
		String ret = convertWithPrevSign(m_JPData, braille, prevSign);
		return(ret);
	}
	
	/**
	 * 英語のSpeechText用に一文字変換.
	 * @param left
	 * @param right
	 * @return
	 */
	@Override
	public String convertSpeechText(TmBraille braille) {
		TmBraille prevBraille = braille != null ? braille.getPrev() : null;
		int prevSign = getSignCode(prevBraille);
		String ret = convertWithPrevSign(m_JPEngSpeechData, braille, prevSign);
		return(ret);
	}
}

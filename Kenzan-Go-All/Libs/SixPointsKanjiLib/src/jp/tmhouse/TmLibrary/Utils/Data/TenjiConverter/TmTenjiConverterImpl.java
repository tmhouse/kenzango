/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所)
// 著作権は作者である「武藤　繁夫」　が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.Utils.Data.TenjiConverter;

import java.util.Locale;

import jp.tmhouse.TmLibrary.Utils.Data.TmBraille;


/**
 * ８進数２個から該当する語を取得する.
 * @author mutoh
 *
 */
public class TmTenjiConverterImpl {
	// 前置符号ではない符号を表す
	public static final int	c_SIGN_NOT_SIGN	= 0;
	
	protected static final String c_NULL_STR = "";
			
	/**
	 * 空白文字か.
	 */
	protected boolean isWhitespace(TmBraille braille) {
		if( braille == null ) {
			return(true);
		}
		if( braille.isSpace() || braille.isCleared() ) {
			return(true);
		}
		return(false);
	}
	
	/**
	 * 区切り文字か.
	 * @param str
	 * @return
	 */
	public boolean isPunctuation(String str) {
		return(false);
	}
	
	/**
	 * 前置符号を取得
	 * @param left
	 * @param right
	 * @return
	 */
	public int getSignCode(TmBraille braille) {
		return(TmTenjiConverterImpl.c_SIGN_NOT_SIGN);
	}
	
	protected int getSignCodeImpl(int[][] arr, TmBraille braille) {
		if( braille != null ) {
			int left = braille.getLeft();
			int right = braille.getRight();
			if( 0 <= left && left <= 7 && 0 <= right && right <= 7 ) {
				int sign = arr[left][right];
				if( sign != 0 ) {
					// 何か前置符号である
					return(sign);
				}
			}
		}
		return(TmTenjiConverterImpl.c_SIGN_NOT_SIGN);
	}
	
	/**
	 * 点字から墨字へ一文字変換.
	 * @param left
	 * @param right
	 * @param locale
	 * @return
	 */
	public String convert(TmBraille braille) {
		return(c_NULL_STR);
	}
	
	/**
	 * 英語のSpeechText用に一文字変換.
	 * @param left
	 * @param right
	 * @return
	 */
	public String convertSpeechText(TmBraille braille) {
		return(c_NULL_STR);
	}
	
	/**
	 * 日本語localeか.
	 * @param locale
	 * @return
	 */
	public static boolean isJapaneseLocale(Locale locale) {
		if( locale != null && (locale.equals(Locale.JAPAN) || locale.equals(Locale.JAPANESE) )) {
			return(true);
		} else {
			return(false);
		}
	}
	
	/**
	 * 点字テーブルからleft, right, prev_signの文字を引く.
	 * @param table
	 * @param braille
	 * @param prev_sign
	 * @return
	 */
	protected String convertWithPrevSign(
			String[][][] table, TmBraille braille, int prev_sign)
	{
		String ret = null;
		if( table == null || braille == null ) {
			return(c_NULL_STR);
		}
		int left = braille.getLeft();
		int right = braille.getRight();
		if( 0 <= left && left <= 7 && 0 <= right && right <= 7 )
		{
			ret = table[prev_sign][left][right];
		}
		if( ret == null ) {
			ret = c_NULL_STR;
		}
		return(ret);
	}
	
	/**
	 * 略語の展開.
	 * ハズレの場合はnullを返す.
	 * @param shortForm
	 * @return
	 */
	public String expandShortFormWords(String shortForm) {
		return(null);
	}
}

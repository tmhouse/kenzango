/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable;

import java.util.HashMap;

import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;

public class SujiTable extends IMojiCodeTable {
	private HashMap<BrailleSet, String> m_sujiHash;
	private HashMap<String, BrailleSet> m_mojiHash;
	private String					m_modeName;
	private boolean				m_bIsKanSuji = false;
	private static final BrailleSet c_beginKanSujiFu = new BrailleSet(2, 47, null);
	private static final BrailleSet c_beginSanyoFu = new BrailleSet(47, null);
	
	/**
	 * デフォルトコンストラクタは使わない
	 */
	@SuppressWarnings("unused")
	private SujiTable() {}
	
	public SujiTable(boolean isKansuji) {
		m_bIsKanSuji = isKansuji;
		m_sujiHash = new HashMap<BrailleSet, String>(53);
		m_mojiHash = new HashMap<String, BrailleSet>(53);
		if( isKansuji ) {
			createKanSuji();
			m_modeName = "漢数字モード";
		} else {
			createSanyoSuji();
			m_modeName = "算用数字モード";
		}
	}

	@Override
	public String get(BrailleSet brailleSet, boolean bCapsLock) {
		String str = m_sujiHash.get(brailleSet);
		return(str);
	}
	
	@Override
	public BrailleSet get(String moji) {
		BrailleSet bs = m_mojiHash.get(moji);
		return(bs);
	}
	
	@Override
	public String getModeName() {
		return(m_modeName);
	}
	
	@Override
	public boolean isInvalid(int left, int right) {
		return(false);
	}
	
	/**
	 * 算用数字データの生成.
	 */
	private void createSanyoSuji() {
		putBrailleSet(new BrailleSet(23, "０"), true);
		putBrailleSet(new BrailleSet(10, "１"), true);
		putBrailleSet(new BrailleSet(30, "２"), true);
		putBrailleSet(new BrailleSet(11, "３"), true);
		putBrailleSet(new BrailleSet(13, "４"), true);
		putBrailleSet(new BrailleSet(12, "５"), true);
		putBrailleSet(new BrailleSet(31, "６"), true);
		putBrailleSet(new BrailleSet(33, "７"), true);
		putBrailleSet(new BrailleSet(32, "８"), true);
		putBrailleSet(new BrailleSet(21, "９"), true);
		putBrailleSet(new BrailleSet(20, "．"), true);
		putBrailleSet(new BrailleSet(40, "，"), true);
	}
	/**
	 * 漢数字データの生成.
	 */
	private void createKanSuji() {
		putBrailleSet(new BrailleSet(23, "〇"), false);
		putBrailleSet(new BrailleSet(10, "一"), false);
		putBrailleSet(new BrailleSet(30, "二"), false);
		putBrailleSet(new BrailleSet(11, "三"), false);
		putBrailleSet(new BrailleSet(13, "四"), false);
		putBrailleSet(new BrailleSet(12, "五"), false);
		putBrailleSet(new BrailleSet(31, "六"), false);
		putBrailleSet(new BrailleSet(33, "七"), false);
		putBrailleSet(new BrailleSet(32, "八"), false);
		putBrailleSet(new BrailleSet(21, "九"), false);
		putBrailleSet(new BrailleSet(20, "．"), false);
		putBrailleSet(new BrailleSet(40, "，"), false);
		putBrailleSet(new BrailleSet(46, "十"), false);
	}
	
	/**
	 * 文字の登録.
	 * @param bs
	 * @param bWithHalf   文字から引くテーブルに半角文字を同時登録
	 */
	private void putBrailleSet(BrailleSet bs, boolean bWithHalf) {
		m_sujiHash.put(bs, bs.getString());
		m_mojiHash.put(bs.getString(), bs);
		
		// 半角文字も同時登録するか
		if( bWithHalf ) {
			String s_org = bs.getString();
			String s = IMojiCodeTable.to1Byte(s_org);
			m_mojiHash.put(s, bs);
		}
	}

	@Override
	public int getMaxLength(BrailleSet curBrailleSet) {
		return 1;
	}


	@Override
	public BrailleSet getBeginFu() {
		if( m_bIsKanSuji ) {
			return(c_beginKanSujiFu);
		}
		return(c_beginSanyoFu);
	}

	@Override
	public BrailleSet getEndFu() {
		// tableから引けなかったら数符の終わりなので
		// 終わり符号は特にない
		return null;
	}

	@Override
	public boolean isBeginFu(BrailleSet bs) {
		return(getBeginFu().equals(bs));
	}

}

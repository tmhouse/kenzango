/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable;

import java.util.HashMap;

import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;

public class KatakanaTable extends IMojiCodeTable {
	private static HashMap<BrailleSet, String> m_brailleHash;
	private static HashMap<String, BrailleSet> m_stringBrailleHash;
	// 開始符タイプ0 : [6, 36]の点
	private static final BrailleSet c_beginFu0 = new BrailleSet(4, 44, null);
	// 開始符タイプ1 : [1, 14]の点
	private static final BrailleSet c_beginFu1 = new BrailleSet(1, 11, null);

	private static final BrailleSet c_endFu = new BrailleSet(60, null);

	private static final String	c_MODENAME =  "カタカナモード";

	@Override
	public String get(BrailleSet brailleSet, boolean bCapsLock) {
		String str = m_brailleHash.get(brailleSet);
		return(str);
	}
	
	@Override
	public BrailleSet get(String moji) {
		BrailleSet set = m_stringBrailleHash.get(moji);
		return(set);
	}
	
	@Override
	public String getModeName() {
		return(c_MODENAME);
	}
	
	@Override
	public int getMaxLength(BrailleSet curBrailleSet) {
		return 2;
	}
	
	@Override
	public boolean isInvalid(int left, int right) {
		return(false);
	}
	

	/**
	 * カタカナデータの生成.
	 * @param brailleHash
	 */
	public static void createKatakana(
			HashMap<BrailleSet, String> brailleHash)
	{
		putBrailleSet(new BrailleSet(60, null)); // 囲み終了符
		
		//カ		hash.put( 444013260L, "ヽ");	// 6,36,4,125,23	片仮名繰返し記号
		//カ		hash.put( 444023260L, "ヾ");	// 6,36,5,125,23	片仮名繰返し記号（濁点）／片仮名繰返し記号におなじ
		putBrailleSet(new BrailleSet(1,32, "ヽ"));	// 6,36,4,125,23	片仮名繰返し記号
		putBrailleSet(new BrailleSet(2,32, "ヾ"));	// 6,36,5,125,23	片仮名繰返し記号（濁点）／片仮名繰返し記号におなじ
		
		putBrailleSet(new BrailleSet(0,"　"));
		putBrailleSet(new BrailleSet(6,0,"、"));
		putBrailleSet(new BrailleSet(2,0,"・"));
		
		putBrailleSet(new BrailleSet(3,10,"ァ"));
		putBrailleSet(new BrailleSet(3,30,"ィ"));
		putBrailleSet(new BrailleSet(3,11,"ゥ"));
		putBrailleSet(new BrailleSet(3,31,"ェ"));
		putBrailleSet(new BrailleSet(3,21,"ォ"));
		putBrailleSet(new BrailleSet(3,40,"ヮ"));
		putBrailleSet(new BrailleSet(3,41,"ャ"));
		putBrailleSet(new BrailleSet(3,45,"ュ"));
		putBrailleSet(new BrailleSet(3,43,"ョ"));
		
		putBrailleSet(new BrailleSet(10,"ア"));
		putBrailleSet(new BrailleSet(11,"ウ"));
		putBrailleSet(new BrailleSet(12,"ラ"));
		putBrailleSet(new BrailleSet(13,"ル"));
		putBrailleSet(new BrailleSet(14,"カ"));
		putBrailleSet(new BrailleSet(15,"ク"));
		putBrailleSet(new BrailleSet(16,"サ"));
		putBrailleSet(new BrailleSet(17,"ス"));
		putBrailleSet(new BrailleSet(20,"ッ"));
		putBrailleSet(new BrailleSet(21,"オ"));
		putBrailleSet(new BrailleSet(22,"ー"));
		putBrailleSet(new BrailleSet(23,"ロ"));
		putBrailleSet(new BrailleSet(24,0,"？"));
		putBrailleSet(new BrailleSet(25,"コ"));
		putBrailleSet(new BrailleSet(26,0,"。"));
		putBrailleSet(new BrailleSet(27,"ソ"));
		putBrailleSet(new BrailleSet(30,"イ"));
		putBrailleSet(new BrailleSet(31,"エ"));
		putBrailleSet(new BrailleSet(32,"リ"));
		putBrailleSet(new BrailleSet(33,"レ"));
		putBrailleSet(new BrailleSet(34,"キ"));
		putBrailleSet(new BrailleSet(35,"ケ"));
		putBrailleSet(new BrailleSet(36,"シ"));
		putBrailleSet(new BrailleSet(37,"セ"));
		putBrailleSet(new BrailleSet(40,"ワ"));
		putBrailleSet(new BrailleSet(41,"ヤ"));
		putBrailleSet(new BrailleSet(42,"ヲ"));
		putBrailleSet(new BrailleSet(43,"ヨ"));
		putBrailleSet(new BrailleSet(45,"ユ"));
		putBrailleSet(new BrailleSet(46,"ン"));
		putBrailleSet(new BrailleSet(50,"ナ"));
		putBrailleSet(new BrailleSet(51,"ヌ"));
		putBrailleSet(new BrailleSet(52,"タ"));
		putBrailleSet(new BrailleSet(53,"ツ"));
		putBrailleSet(new BrailleSet(54,"ハ"));
		putBrailleSet(new BrailleSet(55,"フ"));
		putBrailleSet(new BrailleSet(56,"マ"));
		putBrailleSet(new BrailleSet(57,"ム"));
		putBrailleSet(new BrailleSet(1,60,"ヰ"));
		putBrailleSet(new BrailleSet(1,62,"ヱ"));
		putBrailleSet(new BrailleSet(61,"ノ"));
		putBrailleSet(new BrailleSet(62,0,"！"));
		putBrailleSet(new BrailleSet(63,"ト"));
		putBrailleSet(new BrailleSet(65,"ホ"));
		putBrailleSet(new BrailleSet(67,"モ"));
		putBrailleSet(new BrailleSet(70,"ニ"));
		putBrailleSet(new BrailleSet(71,"ネ"));
		putBrailleSet(new BrailleSet(72,"チ"));
		putBrailleSet(new BrailleSet(73,"テ"));
		putBrailleSet(new BrailleSet(74,"ヒ"));
		putBrailleSet(new BrailleSet(75,"ヘ"));
		putBrailleSet(new BrailleSet(76,"ミ"));
		putBrailleSet(new BrailleSet(77,"メ"));
		putBrailleSet(new BrailleSet(2,14,"ガ"));
		putBrailleSet(new BrailleSet(2,15,"グ"));
		putBrailleSet(new BrailleSet(2,16,"ザ"));
		putBrailleSet(new BrailleSet(2,17,"ズ"));
		putBrailleSet(new BrailleSet(2,25,"ゴ"));
		putBrailleSet(new BrailleSet(2,27,"ゾ"));
		putBrailleSet(new BrailleSet(2,34,"ギ"));
		putBrailleSet(new BrailleSet(2,35,"ゲ"));
		putBrailleSet(new BrailleSet(2,36,"ジ"));
		putBrailleSet(new BrailleSet(2,37,"ゼ"));
		putBrailleSet(new BrailleSet(2,52,"ダ"));
		putBrailleSet(new BrailleSet(2,53,"ヅ"));
		putBrailleSet(new BrailleSet(2,54,"バ"));
		putBrailleSet(new BrailleSet(2,55,"ブ"));
		putBrailleSet(new BrailleSet(2,63,"ド"));
		putBrailleSet(new BrailleSet(2,65,"ボ"));
		putBrailleSet(new BrailleSet(2,72,"ヂ"));
		putBrailleSet(new BrailleSet(2,73,"デ"));
		putBrailleSet(new BrailleSet(2,74,"ビ"));
		putBrailleSet(new BrailleSet(2,75,"ベ"));
		putBrailleSet(new BrailleSet(4,54,"パ"));
		putBrailleSet(new BrailleSet(4,55,"プ"));
		putBrailleSet(new BrailleSet(4,65,"ポ"));
		putBrailleSet(new BrailleSet(4,74,"ピ"));
		putBrailleSet(new BrailleSet(4,75,"ペ"));
		putBrailleSet(new BrailleSet(1,12,"リャ"));
		putBrailleSet(new BrailleSet(1,13,"リュ"));
		putBrailleSet(new BrailleSet(1,14,"キャ"));
		putBrailleSet(new BrailleSet(1,15,"キュ"));
		putBrailleSet(new BrailleSet(1,16,"シャ"));
		putBrailleSet(new BrailleSet(1,17,"シュ"));
		putBrailleSet(new BrailleSet(1,23,"リョ"));
		putBrailleSet(new BrailleSet(1,25,"キョ"));
		putBrailleSet(new BrailleSet(1,27,"ショ"));
		putBrailleSet(new BrailleSet(1,50,"ニャ"));
		putBrailleSet(new BrailleSet(1,51,"ニュ"));
		putBrailleSet(new BrailleSet(1,52,"チャ"));
		putBrailleSet(new BrailleSet(1,53,"チュ"));
		putBrailleSet(new BrailleSet(1,54,"ヒャ"));
		putBrailleSet(new BrailleSet(1,55,"ヒュ"));
		putBrailleSet(new BrailleSet(1,56,"ミャ"));
		putBrailleSet(new BrailleSet(1,57,"ミュ"));
		putBrailleSet(new BrailleSet(1,61,"ニョ"));
		putBrailleSet(new BrailleSet(1,63,"チョ"));
		putBrailleSet(new BrailleSet(1,65,"ヒョ"));
		putBrailleSet(new BrailleSet(1,67,"ミョ"));
		putBrailleSet(new BrailleSet(3,14,"ギャ"));
		putBrailleSet(new BrailleSet(3,15,"ギュ"));
		putBrailleSet(new BrailleSet(3,16,"ジャ"));
		putBrailleSet(new BrailleSet(3,17,"ジュ"));
		putBrailleSet(new BrailleSet(3,25,"ギョ"));
		putBrailleSet(new BrailleSet(3,27,"ジョ"));
		putBrailleSet(new BrailleSet(3,52,"ヂャ"));
		putBrailleSet(new BrailleSet(3,53,"ヂュ"));
		putBrailleSet(new BrailleSet(3,54,"ビャ"));
		putBrailleSet(new BrailleSet(3,55,"ビュ"));
		putBrailleSet(new BrailleSet(3,63,"ヂョ"));
		putBrailleSet(new BrailleSet(3,65,"ビョ"));
		putBrailleSet(new BrailleSet(5,54,"ピャ"));
		putBrailleSet(new BrailleSet(5,55,"ピュ"));
		putBrailleSet(new BrailleSet(5,65,"ピョ"));
		
		// 特殊音
		// (1)開拗音系
		putBrailleSet(new BrailleSet(1,31,"イェ"));
		putBrailleSet(new BrailleSet(1,35,"キェ"));
		putBrailleSet(new BrailleSet(1,37,"シェ"));
		putBrailleSet(new BrailleSet(1,73,"チェ"));
		putBrailleSet(new BrailleSet(1,71,"ニェ"));
		putBrailleSet(new BrailleSet(1,75,"ヒェ"));
		putBrailleSet(new BrailleSet(3,37,"ジェ"));
		// (2)合拗音系
		putBrailleSet(new BrailleSet(24,30,"ウィ"));
		putBrailleSet(new BrailleSet(24,31,"ウェ"));
		putBrailleSet(new BrailleSet(24,21,"ウォ"));
		putBrailleSet(new BrailleSet(24,14,"クァ"));
		putBrailleSet(new BrailleSet(24,34,"クィ"));
		putBrailleSet(new BrailleSet(24,35,"クェ"));
		putBrailleSet(new BrailleSet(24,25,"クォ"));
		putBrailleSet(new BrailleSet(26,14,"グァ"));
		putBrailleSet(new BrailleSet(26,34,"グィ"));
		putBrailleSet(new BrailleSet(26,35,"グェ"));
		putBrailleSet(new BrailleSet(26,25,"グォ"));
		putBrailleSet(new BrailleSet(24,52,"ツァ"));
		putBrailleSet(new BrailleSet(24,72,"ツィ"));
		putBrailleSet(new BrailleSet(24,73,"ツェ"));
		putBrailleSet(new BrailleSet(24,63,"ツォ"));
		putBrailleSet(new BrailleSet(24,54,"ファ"));
		putBrailleSet(new BrailleSet(24,74,"フィ"));
		putBrailleSet(new BrailleSet(24,75,"フェ"));
		putBrailleSet(new BrailleSet(24,65,"フォ"));
		putBrailleSet(new BrailleSet(26,54,"ヴァ"));
		putBrailleSet(new BrailleSet(26,74,"ヴィ"));
		putBrailleSet(new BrailleSet(26,75,"ヴェ"));
		putBrailleSet(new BrailleSet(26,65,"ヴォ"));
		// (3)その他
		putBrailleSet(new BrailleSet(1,36,"スィ"));
		putBrailleSet(new BrailleSet(3,36,"ズィ"));
		putBrailleSet(new BrailleSet(1,72,"ティ"));
		putBrailleSet(new BrailleSet(3,72,"ディ"));
		putBrailleSet(new BrailleSet(24,53,"トゥ"));
		putBrailleSet(new BrailleSet(26,53,"ドゥ"));
		putBrailleSet(new BrailleSet(5,53,"テュ"));
		putBrailleSet(new BrailleSet(7,53,"デュ"));
		putBrailleSet(new BrailleSet(5,45,"フュ"));
		putBrailleSet(new BrailleSet(7,45,"ヴュ"));
		putBrailleSet(new BrailleSet(5,43,"フョ"));
		putBrailleSet(new BrailleSet(7,43,"ヴョ"));
		putBrailleSet(new BrailleSet(2,11,"ヴ"));
	}
	
	private static void putBrailleSet(BrailleSet bset) {
		m_brailleHash.put(bset, bset.getString());
		m_stringBrailleHash.put(bset.getString(), bset);
	}
	
	
	/**
	 * static処理
	 */
	static {
		m_brailleHash = new HashMap<BrailleSet, String>(337);
		m_stringBrailleHash = new HashMap<String, BrailleSet>(337);
		createKatakana(m_brailleHash);
	}

	/**
	 * 開始の意味の前置符号を返す.
	 * @param typeNumber	複数の前置符号を持つ場合、番号で選択.
	 * @return
	 */
	@Override
	public final BrailleSet getBeginFu() {
		// タイプ0
		return(c_beginFu0);
	}
	
	/**
	 * 開始の意味のサブの前置符号を返す.
	 * カタカナは二つ開始符合がある。
	 */
	public final BrailleSet getBeginSubFu() {
		// タイプ1
		return(c_beginFu1);
	}

	/**
	 * 終了の意味の後置符号を返す.
	 * @param typeNumber
	 * @return
	 */
	@Override
	public final BrailleSet getEndFu() {
		return(c_endFu);
	}
	
	@Override
	public boolean isBeginFu(BrailleSet bs) {
		return(getBeginFu().equals(bs) || getBeginSubFu().equals(bs));
	}

	@Override
	public boolean isEndFu(BrailleSet bs) {
		return(getEndFu().equals(bs));
	}

}

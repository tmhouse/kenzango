/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib;

import java.util.ArrayList;

import android.util.Log;
import jp.tmhouse.TmLibrary.Utils.Log.TmLog;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable.AsciiTable;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable.HiraganaKanjiTable;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable.IMojiCodeTable;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable.KatakanaTable;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable.SujiTable;

/**
 * かなを点字データに変換する.
 * @author mutoh
 *
 */
public class TmKana2Braille {
	private HiraganaKanjiTable	m_hiraKanjiTable = new HiraganaKanjiTable();
	private KatakanaTable	m_katakanaTable = new KatakanaTable();
	private SujiTable		m_sujiTable = new SujiTable(false);
	private SujiTable		m_kansujiTable = new SujiTable(true);
	private AsciiTable		m_alphaTable = new AsciiTable();
	private boolean		m_bUseBeginFu114 = false; // カタカナ開始符合1,14を使用するか
	private Mode			m_mode;
	private IMojiCodeTable	m_curTbl;

	// 大文字小文字モード
	private enum AlphaLargeMode {
		SMALL,		// 英小文字モード
		CONT_LARGE	// 英大文字連続モード
	};

	/**
	 * 文字モード.
	 * @author mutoh
	 *
	 */
	public static enum Mode {
		KANA,
		KATAKANA,
		NUMBER,
		KAN_SUJI,
		ALPHA,
		UNKNOWN,
	}
	
	public TmKana2Braille() {
		// デフォルトは「かな」モード
		m_mode = Mode.KANA;
		m_curTbl = m_hiraKanjiTable;
	}
	
	/**
	 * カタカナ前置符号を設定する.
	 * @param bIgnore
	 */
	public void setKatakanaBegin114Type(boolean bUse114Type) {
		m_bUseBeginFu114 = bUse114Type;
	}
	
	/////////////////////////////////////////////////
	// 変換方針
	// - 一文字読む
	//　　- 文字種別判定
	//    - 文字種別変更か判定
	//      - 変更あり：文字種別変更処理
	//  - 文字種別ごとにhash引き
	//    - 先読みが必要な文字種のときは先読みする
	//    - 該当文字あり：追加
	//    - 同　　　　なし：マスアケでも入れとく
	//  - continue
	// 
	// ------------------------------------------------------
	// 種類　　　　先読み数               モード
	// ------------------------------------------------------
	// 数字　　　　0                       NUMBER
	// 英字　　　　0                       ALPHA
	// ひらがな　　1([きゃ、ぎゃ、...])    KANA
	// カタカナ　　1([キャ、ギャ、...])    ?????(あとで考える)
	// 漢字　　　　0                       KANA
	// 記号　　　　0                       KANA(たぶん漢字と一緒)
	/////////////////////////////////////////////////

	/**
	 * 墨字->点字変換.
	 * @param kana
	 * @return
	 */
	public ArrayList<BrailleSet> convert(String kana) {
		ArrayList<BrailleSet> brailleSetArr = new ArrayList<BrailleSet>(kana.length());
		CharTokenizer ct = new CharTokenizer(kana);

		// 最初は平仮名モード
		m_mode = Mode.KANA;
		
		// 英大文字小文字モードは小文字
		AlphaLargeMode alphaLargeMode = AlphaLargeMode.SMALL;
		
		while( ct.hasMoreTokens() ) {
			// 一文字読み込む
			char c_org = ct.nextToken();

			// モード変更兼テーブル取得処理
			setMode(getMode(c_org), brailleSetArr);

			char c_2byte = IMojiCodeTable.to2Byte(c_org);
			String s_2byte = String.valueOf(c_2byte);

			switch(m_mode) {
			case KATAKANA:
			case KANA:
				// ひらがな or カタカナ
				BrailleSet bs = null;
				if( ct.hasMoreTokens() ) {
					char c1 = IMojiCodeTable.to2Byte(ct.nextToken());
					// 二文字で平仮名、漢字などを探す
					bs = m_curTbl.get(s_2byte + String.valueOf(c1));
					if( bs == null ) {
						ct.pushBack(); // 戻す
					}
				}

				// 一文字で平仮名、漢字、カタカナなどを探す
				if( bs == null ) {
					bs = m_curTbl.get(s_2byte);
				}
				
				if( bs != null ) {
					//TmLog.d("kana or katakana or kanji:" + s_2byte);
					brailleSetArr.add(bs);
				}
				break;
			case NUMBER:
			case KAN_SUJI:
				// 数字
				//TmLog.d("digit:" + c_org);
				brailleSetArr.add(m_curTbl.get(s_2byte));
				
				if( ct.hasMoreTokens() ) {
					// 数字つなぎ符が必要であれば付加
					char c_n = IMojiCodeTable.to2Byte(ct.nextToken());
					if( ((c_n >= 'あ' && c_n <= 'お') || (c_n >= 'ら' && c_n <= 'ろ')) ) {
						// 数字つなぎ符
						brailleSetArr.add(new BrailleSet(44, ""));
					}
					ct.pushBack();
				}
				break;
			case ALPHA:
				// 英字
				//TmLog.d("alpha:" + c_org);
				char c_1byte = IMojiCodeTable.to1Byte(c_org);
				if( Character.isUpperCase(c_1byte) ) {
					// 英大文字
					if( alphaLargeMode == AlphaLargeMode.SMALL ) {
						// 次の文字を調べる
						if( ct.hasMoreTokens() ) {
							char c_next_1byte = IMojiCodeTable.to1Byte(ct.nextToken());
							ct.pushBack(); // 戻す
							if( Character.isUpperCase(c_next_1byte) ) {
								// 次も大文字
								alphaLargeMode = AlphaLargeMode.CONT_LARGE;
							} 
						} else {
							// 最後の文字なので二重大文字符は不要
						}
						
						if( alphaLargeMode == AlphaLargeMode.CONT_LARGE ) {
							// 二重大文字符
							brailleSetArr.add(AsciiTable.getOomojiRenzokuFu());
						} else {
							// 単体の大文字符
							brailleSetArr.add(AsciiTable.getOomojiFu());
						}
					}	
					// 英字追加
					char c_lower_1byte = Character.toLowerCase(c_1byte);
					brailleSetArr.add(m_curTbl.get(String.valueOf(c_lower_1byte)));
				} else if( Character.isLowerCase(c_1byte) ) {
					// 二重大文字中の小文字接続
					if( alphaLargeMode == AlphaLargeMode.CONT_LARGE ) {
						// 小文字モードへ
						alphaLargeMode = AlphaLargeMode.SMALL;
						brailleSetArr.add(AsciiTable.getOomojiRenzokuTunagiFu());
					}
					
					// 英小文字
					brailleSetArr.add(m_curTbl.get(s_2byte));
				} else if( m_curTbl.get(s_2byte) != null ) {
					// 二重大文字符の自動終了
					alphaLargeMode = AlphaLargeMode.SMALL;
					
					// 記号、スペース
					brailleSetArr.add(m_curTbl.get(s_2byte));
				} else {
					// 英字またはスペースではない
					TmLog.e("unknown char:" + c_org);
				}
				break;
			default:
				TmLog.w("unknown:" + c_org);
			}
		}
		
		// モードを元に戻す処理
		setMode(Mode.KANA, brailleSetArr);
				
		return(brailleSetArr);
	}
	
	/**
	 * cの文字モードを返す.
	 * @param c
	 * @return
	 */
	private Mode getMode(char c) {
		
		//片っ端から文字テーブルを引いて該当文字モードを決めるのが楽かなあ。
		//英字は大文字小文字に注意
		
		String s = String.valueOf(c);

		// 現在の文字テーブルから引ける場合は文字モード変更なし
		BrailleSet bs = m_curTbl.get(s);
		if( bs != null ) {
			return(m_mode);
		}
		
		if( m_hiraKanjiTable.get(s) != null ) {
			return(Mode.KANA);
		}
		if( m_sujiTable.get(s) != null ) {
			return(Mode.NUMBER);
		}
		if( m_kansujiTable.get(s) != null ) {
			return(Mode.KAN_SUJI);
		}
		if( m_alphaTable.get(s) != null ) {
			return(Mode.ALPHA);
		}
		if( m_katakanaTable.get(s) != null ) {
			return(Mode.KATAKANA);
		}
		return(Mode.UNKNOWN);
	}

	
	private void setMode(Mode newMode, ArrayList<BrailleSet> bsArray) {
		////////////////
		// 1. 閉じる処理
		////////////////
		
		// 数字または漢数字が終わる場合
		if( (m_mode == Mode.NUMBER && newMode != Mode.NUMBER) ||
			(m_mode == Mode.KAN_SUJI && newMode != Mode.KAN_SUJI) )
		{
			// 数字符閉じる
			// つなぎ符号44は必要な場合とそうでない場合がある。
			// すでに44が必要であれば付加されているのでここでは何もしない。
			//bsArray.add(new BrailleSet(44, "　"));
			m_curTbl = m_hiraKanjiTable;
		}

		// 英字が終わる場合
		if( m_mode == Mode.ALPHA && newMode != Mode.ALPHA ) {
			// 外国語引用符閉じる
			bsArray.add(m_alphaTable.getEndFu());
			m_curTbl = m_hiraKanjiTable;
		}
		
		// カタカナが終わる処理
		if( m_mode == Mode.KATAKANA && newMode != Mode.KATAKANA ) {
			// カタカナ符号を選択
			bsArray.add(m_katakanaTable.getEndFu());
			m_curTbl = m_hiraKanjiTable;
		}
		
		////////////////
		// 2. 開く処理
		////////////////

		// 数字が開始される場合
		if( m_mode != Mode.NUMBER && newMode == Mode.NUMBER ) {
			// 数符
			bsArray.add( m_sujiTable.getBeginFu() );
			m_curTbl = m_sujiTable;
		}
		if( m_mode != Mode.KAN_SUJI && newMode == Mode.KAN_SUJI ) {
			// 漢数字の数符
			bsArray.add( m_kansujiTable.getBeginFu() );
			m_curTbl = m_kansujiTable;
		}

		// 連続した英字が開始される場合
		if( m_mode != Mode.ALPHA && newMode == Mode.ALPHA ) {
			// 外国語引用符(後で閉じること)
			bsArray.add( m_alphaTable.getBeginFu() );
			m_curTbl = m_alphaTable;
		}
		
		// カタカナ開始
		if( m_mode != Mode.KATAKANA && newMode == Mode.KATAKANA ) {
			// カタカナ符号を追加する設定のときだけ
			bsArray.add( m_bUseBeginFu114 ?
					m_katakanaTable.getBeginSubFu() : 
					m_katakanaTable.getBeginFu());
			m_curTbl = m_katakanaTable;
		}
		
		m_mode = newMode;
	}
	
	/**
	 * 一文字ずつ処理するtokenizer.
	 * @author mutoh
	 *
	 */
	public class CharTokenizer {
		private String	m_input;
		private int	m_cnt;
		private int	m_inputLen;
		
		public CharTokenizer(String input) {
			m_input = input;
			m_inputLen = input.length();
		}
		
		public char nextToken() {
			if( hasMoreTokens() ) {
				char c = m_input.charAt(m_cnt);
				m_cnt++;
				return(c);
			}
			return(0);
		}
		public void pushBack() {
			if( m_cnt > 0 ) {
				m_cnt--;
			} else {
				throw new RuntimeException("too many pushBack.");
			}
		}
		
		public boolean hasMoreTokens() {
			if( m_inputLen > m_cnt ) {
				return(true);
			}
			return(false);
		}
	}
}

/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable;
import java.util.HashMap;

import android.util.SparseArray;

import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;
public class StaticKigoTable {
	private static final boolean c_USE_4MASU_KIGO = false;
	
	// 記号hash
	private static SparseArray<String>			m_brailleKigoHash;
	private static HashMap<String, Integer>	m_kigoBrailleHash;

	private static void initKigoHash() {
		
		putKigoHash( 132, "ゝ");	// 4,125	平仮名繰返し記号／片仮名繰返し記号におなじ
		putKigoHash( 232, "ゞ");	// 5,125	平仮名繰返し記号（濁点）／片仮名繰返し記号におなじ
		putKigoHash( 50442, "〃");	// 46,6,35	同じく記号
		putKigoHash( 50422, "仝");	// 46,6,25	同上記号
		putKigoHash( 532, "々");	// 46,125	おどり字／片仮名繰返し記号におなじ
		if( c_USE_4MASU_KIGO ) {
		putKigoHash( 2223677, "〆");	// 5,25,1256,123456	シメ
		}
		putKigoHash( 24723, "〇");	// 5,3456,245	漢数字ゼロ
		putKigoHash(440404, "‐");	// 36,6,6	ハイフン（四分）
		putKigoHash(4444, "〜");	// 36,36	波線（波ダッシュ）
		if( c_USE_4MASU_KIGO ) {
		putKigoHash( 2220606, "‖");	// 5,25,56,56	縦平行（双柱）
		}
		putKigoHash( 62022, "…");	// 56,2,25	三点リーダ
		putKigoHash( 62020, "‥");	// 56,2,2	二点リーダ
		putKigoHash( 266, "（");	// 5,2356	丸カッコ開き/始め丸括弧
		putKigoHash(6620, "）");	// 2356,2	丸カッコ閉じ/終わり丸括弧
		putKigoHash( 66440, "〔");	// 56,236,3	きっこうカッコ開き（始めきっこう（亀甲）括弧）
		putKigoHash( 44660, "〕");	// 6,356,23	きっこうカッコ閉じ（終わりきっこう（亀甲）括弧）
		putKigoHash( 666, "［");	// 56,2356	角カッコ開き（始め大括弧/始め角括弧）
		putKigoHash(6660, "］");	// 2356,23	角カッコ閉じ（終わり大括弧/終わり角括弧）
		putKigoHash( 62440, "〈");	// 56,26,3	山カッコ開き（始め山括弧）
		putKigoHash( 44260, "〉");	// 6,35,23	山カッコ閉じ（終り山括弧）
		putKigoHash( 62460, "《");	// 56,26,23	二重山カッコ開き（始め二重山括弧）
		putKigoHash( 64260, "》");	// 56,35,23	二重山カッコ閉じ（終わり二重山括弧）
		putKigoHash( 640, "「");	// 56,3	カギ開き（始めかぎ括弧）
		putKigoHash( 460, "」");	// 6,23	カギ閉じ（終わりかぎ括弧）
		putKigoHash( 644, "『");	// 56,36	二重カギ開き（始め二重かぎ括弧）
		putKigoHash(4460, "』");	// 36,23	二重カギ閉じ（終わり二重かぎ括弧）
		putKigoHash( 66460, "【");	// 56,236,23	すみ付きカッコ開き（始めすみ付き括弧）
		putKigoHash( 64660, "】");	// 56,356,23	すみ付きカッコ閉じ（終わりすみ付き括弧）
		putKigoHash( 42442, "±");	// 6,26,35	プラス,マイナス
		putKigoHash( 61420, "×");	// 56,16,2	かける（乗算記号）
		putKigoHash( 64120, "÷");	// 56,34,2	割る（除算記号）
		putKigoHash( 41320, "°");	// 6,145,2	度
		putKigoHash( 41360, "′");	// 6,145,23	分
		putKigoHash( 41322, "″");	// 6,145,25	秒
		putKigoHash( 41120, "℃");	// 6,14,2	度Ｃ（セ氏度記号）
		putKigoHash( 41126, "￠");	// 6,14,256	セント記号
		putKigoHash( 47126, "￡");	// 6,1234,256	ポンド記号
		putKigoHash( 66520, "☆");	// 56,2346,2	白星
		putKigoHash( 66560, "★");	// 56,2346,23	黒星
		putKigoHash( 45620, "○");	// 6,1356,2	丸印/白丸
		putKigoHash( 45660, "●");	// 6,1356,23	黒丸
		putKigoHash( 45626, "◎");	// 6,1356,256	二重丸
		putKigoHash( 57420, "◇");	// 46,1236,2	ヒシ形/ダイヤ（=ダイヤモンド）/斜め正方形
		putKigoHash( 57460, "◆");	// 46,1236,23	黒ヒシ形
		putKigoHash( 43620, "□");	// 6,1256,2	四角
		putKigoHash( 43660, "■");	// 6,1256,23	黒四角
		putKigoHash( 41620, "△");	// 6,156,2	三角
		putKigoHash( 41660, "▲");	// 6,156,23	黒三角
		putKigoHash( 61620, "▽");	// 56,156,2	逆三角
		putKigoHash( 61660, "▼");	// 56,156,23	黒逆三角（逆黒三角）
		putKigoHash( 42520, "※");	// 6,246,2	米印
		putKigoHash( 44520, "〒");	// 6,346,2	郵便記号
		if( c_USE_4MASU_KIGO ) {
		putKigoHash( 2222252, "→");	// 5,25,25,135	右向矢印
		putKigoHash( 2222225, "←");	// 5,25,25,246	左向矢印
		putKigoHash( 2225225, "↔");	// 5,25,135,246	両向矢印
		putKigoHash( 2222247, "↑");	// 5,25,25,3456	上向矢印
		putKigoHash( 2222217, "↓");	// 5,25,25,1456	下向矢印
		}
//無理		putKigoHash(64212146, "ⅰ");	// 236,24,24,356	ローマ数字1小文字
//無理		putKigoHash(64212146, "ⅱ");	// 236,24,24,356	ローマ数字2小文字
//無理		putKigoHash(6421212146, "ⅲ");	// 236,24,24,24,356	ローマ数字3小文字
//無理		putKigoHash(64217446, "ⅳ");	// 236,24,1236,356	ローマ数字4小文字
//無理		putKigoHash(647446, "ⅴ");	// 236,1236,356	ローマ数字5小文字
//無理		putKigoHash(64742146, "ⅵ");	// 236,1236,24,356	ローマ数字6小文字
//無理		putKigoHash(6474212146, "ⅶ");	// 236,1236,24,24,356	ローマ数字7小文字
//無理		putKigoHash(6474212146, "ⅷ");	// 236,1236,24,24,24,356	ローマ数字8小文字
//無理		putKigoHash(64215546, "ⅸ");	// 236,24,1346,356	ローマ数字9小文字
//無理		putKigoHash(645546, "ⅹ");	// 236,1346,356	ローマ数字10小文字
//無理		putKigoHash(64552146, "ⅺ");	// 236,1346,24,356	ローマ数字11小文字
//無理		putKigoHash(6455212146, "ⅻ");	// 236,1346,24,24,356	ローマ数字12小文字
		
		/////////////////////////////////
		// NEC固有非漢字
		/////////////////////////////////
/***Bug21 機種依存文字は非サポートとする
		putKigoHash( 34710, "①");	// 45,3456,1	丸1
		putKigoHash( 34730, "②");	// 45,3456,12	丸2
		putKigoHash( 34711, "③");	// 45,3456,14	丸3
		putKigoHash( 34713, "④");	// 45,3456,145	丸4
		putKigoHash( 34712, "⑤");	// 45,3456,15	丸5
		putKigoHash( 34731, "⑥");	// 45,3456,124	丸6
		putKigoHash( 34733, "⑦");	// 45,3456,1245	丸7
		putKigoHash( 34732, "⑧");	// 45,3456,125	丸8
		putKigoHash( 34721, "⑨");	// 45,3456,24	丸9
		putKigoHash( 3471023, "⑩");	// 45,3456,1,245	丸10
		putKigoHash( 3471010, "⑪");	// 45,3456,1,1	丸11
		putKigoHash( 3471030, "⑫");	// 45,3456,1,12	丸12
		putKigoHash( 3471011, "⑬");	// 45,3456,1,14	丸13
		putKigoHash( 3471013, "⑭");	// 45,3456,1,145	丸14
		putKigoHash( 3471012, "⑮");	// 45,3456,1,15	丸15
		putKigoHash( 3471031, "⑯");	// 45,3456,1,124	丸16
		putKigoHash( 3471033, "⑰");	// 45,3456,1,1245	丸17
		putKigoHash( 3471032, "⑱");	// 45,3456,1,125	丸18
		putKigoHash( 3471021, "⑲");	// 45,3456,1,24	丸19
		putKigoHash( 3472023, "⑳");	// 45,3456,2,245	丸20
//無理		putKigoHash(6404212146, "Ⅰ");	// 236,6,24,24,356	ローマ数字1大文字
//無理		putKigoHash(6404212146, "Ⅱ");	// 236,6,6,24,24,356	ローマ数字2大文字
//無理		putKigoHash(6404212146, "Ⅲ");	// 236,6,6,24,24,24,356	ローマ数字3大文字
//無理		putKigoHash(6404212146, "Ⅳ");	// 236,6,6,24,1236,356	ローマ数字4大文字
//無理		putKigoHash(64047446, "Ⅴ");	// 236,6,1236,356	ローマ数字5大文字
//無理		putKigoHash(64047446, "Ⅵ");	// 236,6,6,1236,24,356	ローマ数字6大文字
//無理		putKigoHash(64047446, "Ⅶ");	// 236,6,6,1236,24,24,356	ローマ数字7大文字
//無理		putKigoHash(64047446, "Ⅷ");	// 236,6,6,1236,24,24,24,356	ローマ数字8大文字
//無理		putKigoHash(64047446, "Ⅸ");	// 236,6,6,24,1346,356	ローマ数字9大文字
//無理		putKigoHash(64045546, "Ⅹ");	// 236,6,1346,356	ローマ数字10大文字
//無理		putKigoHash(64045546, "Ⅺ");	// 236,6,6,1346,24,356	ローマ数字11大文字
		putKigoHash( 2227632, "㍉");	// 5,25,12356,125	（全角）ミリ
		putKigoHash( 2223461, "㌔");	// 5,25,126,234	（全角）キロ
		putKigoHash( 2223746, "㌢");	// 5,25,12456,356	（全角）センチ
		putKigoHash( 2227722, "㍍");	// 5,25,123456,25	（全角）メートル
		putKigoHash( 2220215, "㌘");	// 5,25,5,146	（全角）グラム
		putKigoHash( 2226346, "㌧");	// 5,25,2345,356	（全角）トン
		putKigoHash( 2221022, "㌃");	// 5,25,1,25	（全角）アール
		putKigoHash( 2227515, "㌶");	// 5,25,12346,146	（全角）ヘクタール
		putKigoHash( 2223263, "㍑");	// 5,25,125,2345	（全角）リットル
		putKigoHash( 2220463, "㍗");	// 5,25,6,2345	（全角）ワット
		putKigoHash( 2221423, "㌍");	// 5,25,16,245	（全角）カロリー
		putKigoHash( 2226313, "㌦");	// 5,25,2345,145	（全角）ドル
		putKigoHash( 2223763, "㌣");	// 5,25,12456,2345	（全角）セント
		putKigoHash( 2220454, "㌫");	// 5,25,6,136	（全角）パーセント
		putKigoHash( 2227613, "㍊");	// 5,25,12356,145	（全角）ミリバール
		putKigoHash( 2220475, "㌻");	// 5,25,6,12346	（全角）ページ
		putKigoHash( 2225151, "㎜");	// 5,25,134,134	（全角）MM
		putKigoHash( 2221151, "㎝");	// 5,25,14,134	（全角）CM
		putKigoHash( 2225051, "㎞");	// 5,25,13,134	（全角）KM
		putKigoHash( 2225133, "㎎");	// 5,25,134,1245	（全角）MG
		putKigoHash( 2225033, "㎏");	// 5,25,13,1245	（全角）KG
		putKigoHash( 2221111, "㏄");	// 5,25,14,14	（全角）CC
		putKigoHash( 2225134, "㎡");	// 5,25,134,126	（全角）M2
		putKigoHash( 2223271, "㍱");	// 5,25,125,1234	圧力の単位ヘクトパスカル
		putKigoHash( 2227530, "㍻");	// 5,25,12346,12	平成（全角元号平成）
		putKigoHash( 2225352, "№");	// 5,25,1345,135	ナンバー（全角NO）
		putKigoHash( 2225050, "㏍");	// 5,25,13,13	株式会社（全角KK）
		putKigoHash( 2226312, "℡");	// 5,25,2345,15	テレフォン（全角TEL）
		putKigoHash( 2225031, "㊤");	// 5,25,13,124	丸上（丸付き上）
		putKigoHash( 2225014, "㊥");	// 5,25,13,16	丸中（丸付き中）
		putKigoHash( 2223652, "㊦");	// 5,25,1256,135	丸下（丸付き下）
		putKigoHash( 2227452, "㊧");	// 5,25,1236,135	丸左（丸付き左）
		putKigoHash( 2227634, "㊨");	// 5,25,12356,126	丸右（丸付き右）
		putKigoHash( 2221455, "㈱");	// 5,25,16,1346	カッコ株（全角括弧付き株）
		putKigoHash( 2224511, "㈲");	// 5,25,346,14	カッコ有限（全角括弧付き有）
		putKigoHash( 2220252, "㈹");	// 5,25,5,135	カッコ代表（全角括弧付き代）
		putKigoHash( 2227730, "㍾");	// 5,25,123456,12	明治（全角元号明治）
		putKigoHash( 2225230, "㍽");	// 5,25,135,12	大正（全角元号大正）
		putKigoHash( 2220127, "㍼");	// 5,25,4,2456	昭和（全角元号昭和）
//コードナシ		putKigoHash( 62222, "₋");	// 56,25,25	棒線一文字用
//コードナシ		putKigoHash( 6222220, "—");	// 56,25,25,2	棒線二文字用
//コードナシ		putKigoHash( 6222220, "/");	// 456　□	詩行符
		putKigoHash( 707, "∥");	// 456,456　□	二重詩行符（平行）
Bug21 ****/
	};
	
	/**
	 * 記号をhashテーブルに突っ込む.
	 * @param key
	 * @param moji
	 */
	private static void putKigoHash(int key, String moji) {
		if( key <= 0 || moji == null ) {
			throw new RuntimeException("invalid argument.");
		}
		m_brailleKigoHash.put(key, moji);
		m_kigoBrailleHash.put(moji, key);
	}
	
	public static String getKigo(BrailleSet bset) {
		int key = bset.toInteger();
		return(m_brailleKigoHash.get(key));
	}
	
	public static BrailleSet getKigo(String kigo) {
		Integer val = m_kigoBrailleHash.get(kigo);
		if( val == null ) {
			return(null);
		}
		
		// いつも作るともったいないから、String,BrailleSetの
		// hashは作っても良いか。
		BrailleSet bs = new BrailleSet(val, kigo, false);
		return(bs);

	}
	
	static {
		m_brailleKigoHash = new SparseArray<String>(777);
		m_kigoBrailleHash = new HashMap<String, Integer>(777);
		initKigoHash();
	}
}

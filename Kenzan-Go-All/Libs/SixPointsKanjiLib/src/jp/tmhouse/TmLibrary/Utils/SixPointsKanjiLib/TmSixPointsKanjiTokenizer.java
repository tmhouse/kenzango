/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib;

import java.util.ArrayList;

import jp.tmhouse.TmLibrary.Utils.Data.TmBraille;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable.AsciiTable;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable.HiraganaKanjiTable;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable.IMojiCodeTable;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable.JisCodeTable;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable.KatakanaTable;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable.SujiTable;


/**
 * @author mutoh
 *
 */
public class TmSixPointsKanjiTokenizer {
	private BrailleSet			m_curBrailleSet;	// 現在の点字セット
	private ArrayList<TmBraille>	m_brailleList;
    private StringBuilder		m_wordBuf = new StringBuilder(100);
	
	// 点字hashテーブルのスタック
	private ArrayList<IMojiCodeTable>	m_brailleTableStack = new ArrayList<IMojiCodeTable>(10);
	
	private boolean			m_bCapsLock = false;
	
	// 外字符06を使うか否か
	// 現在使用していない理由：
	//   外字符06は、三点および四点漢字の内部、先頭に使用されている
	//   ため、漢字をサポートする限り外字符06は絶対にサポートできない。
	//   ただし将来、漢字をサポートしないモードを作ったのであれば、
	//   このフラグを立てて外字符06をサポートすることはできる。
	private boolean			m_bUseGaijiFu = false;
	
	// 全点字データ
	private HiraganaKanjiTable m_hirakanjiTable		= new HiraganaKanjiTable();
	private SujiTable			m_kansujiTable		= new SujiTable(true);
	private SujiTable			m_sanyosujiTable	= new SujiTable(false);
	private KatakanaTable		m_katakanaTable		= new KatakanaTable();
	private AsciiTable			m_alphabetTable		= new AsciiTable();
	private AsciiTable			m_gaijiTable		= new AsciiTable();
	private JisCodeTable		m_jisCodeTable		= new JisCodeTable();
	private int				m_kakkoCount;

	// カタカナトグル符(カタカナのon/off)
	private BrailleSet		m_katakanaToggleFu	= new BrailleSet(60, null);
	
	// モード名称
	private static final String	c_MODE_EIJI_OOMOJI_LARGE		= "大文字符";
	private static final String	c_MODE_EIJI_DOUBLE_LARGE		= "大文字連続モード";
	private static final String	c_MODE_EIJI_DOUBLE_LARGE_END	= "小文字モード";
	
	/**
	 * コンストラクタ
	 */
	public TmSixPointsKanjiTokenizer() {
		clear(true);
	}
	
	/**
	 * トークナイズした結果のデータフォルダ.
	 * @author mutoh
	 *
	 */
	public static class TokenizeResult {
		public boolean isError = false;
		public BrailleSet	errorBrailleSet = null;
		public String resultStr = null;
		public String newModeStr = null;
	}

	/**
	 * 文字の追加とtokenizeを行う。
	 * @param left
	 * @param right
	 */
	public TokenizeResult appendAndTokenize(int left, int right) {
		if( m_curBrailleSet == null ) {
			throw new RuntimeException("no braille set");
		}

		// 結果を返すデータフォルダ
		TokenizeResult result = new TokenizeResult();
		
		// 単に点字を足すだけ
		m_brailleList.add(new TmBraille(left, right));
		
		// 空白かどうかは気にしない。空白も含めてデータがあるから。
		boolean addResult = m_curBrailleSet.add(left, right);
		boolean isError = false;
		if( addResult ) {
			IMojiCodeTable tbl = getLastBrailleTable();
			// 探す
			String val = tbl.get(m_curBrailleSet, m_bCapsLock);
			
			// 英語モード時
			if( tbl.getClass() == AsciiTable.class ) {
				// 開始、終了符号を含んだ文字があるための特殊処理
				if( m_alphabetTable.getBeginFu().equals(m_curBrailleSet) ) {
					// 英語モード開始符号と同じ
					// カッコ開き
					m_kakkoCount++;
				} else if( m_alphabetTable.getEndFu().equals(m_curBrailleSet) ) {
					// 英語モード終了符号と同じ
					if( m_kakkoCount == 0 ) {
						// カッコは開いていないから終了符号として扱う
						val = null;
					} else {
						m_kakkoCount--;
					}
				}
				
				// 二重大文字連続モード中に終了符かつなぎ符が来たら小文字モードへ戻す
				if( m_bCapsLock ) {
					boolean bIsTunagiFu = 
						AsciiTable.getOomojiRenzokuTunagiFu().equals(m_curBrailleSet);
					boolean bIsEndFu =
						AsciiTable.getOomojiRenzokuOffFu().equals(m_curBrailleSet);

					if( bIsEndFu || bIsTunagiFu ) {
						if( bIsTunagiFu ) {
							// つなぎ符を消す
							m_curBrailleSet.clear();
						} else {
							// マスアケによる二重大文字連続モードの終了
						}

						// 英字二重大文字符終了
						m_bCapsLock = false;
					
						// 二重大文字の場合はモード変更じゃないのでちょっと特殊
						result.newModeStr = c_MODE_EIJI_DOUBLE_LARGE_END;
					}
				}
			}
			
			if( val != null ) {
				// あった
			} else {
				// 見つからない
				// テーブル変更時処理に備え、テーブルを覚えておく
				IMojiCodeTable beforeTable = tbl;
				
				// 囲み系の終了符が来たか調べる
				// 英語囲み終了か、カタカナ囲み終了か、現在数字モードであったか
				boolean bIsNumberMode = isNumberTable(tbl);
				if( !isError &&
					( (tbl == m_alphabetTable && m_alphabetTable.isEndFu(m_curBrailleSet)) || 
					(tbl == m_katakanaTable && m_katakanaTable.isEndFu(m_curBrailleSet)) ||
					(tbl == m_katakanaTable && m_curBrailleSet.equals(m_katakanaToggleFu)) ||
					bIsNumberMode )
				) {
					// 点字テーブルをpopする
					popBrailleTable();
					tbl = getLastBrailleTable();
					
					// 数字モードに入っていた場合はpopした後のテーブルでもう一回引く
					if( bIsNumberMode ) {
						// 数字モード中にハズレが来て、popしたテーブルでもハズレの場合は
						// 点字セットをクリアしない。何か符号である可能性があるから。
						// ただし、{4,4}のつなぎ符号であった場合を除く
						if( left == 4 && right == 4 ) {
							m_curBrailleSet.clear();
						} else {
							val = tbl.get(m_curBrailleSet, m_bCapsLock);
						}
					} else {
						// 上で囲み終了符号を足してしまっているから消す
						m_curBrailleSet.clear();
					}
				} else if( tbl == m_jisCodeTable ) {
					// JISコードモード入力可符号でなかったときの処理
					if( m_jisCodeTable.isInvalid(left, right) ) {
						// JISコードモード終了の定義
						// ４個の符号のうち、先頭が16進数値以外の符号だったとき。
						if( m_curBrailleSet.length() == 1 ) {
							// テーブルをpopしてから再度引く。
							// 点字テーブルをpopする
							popBrailleTable();
							tbl = getLastBrailleTable();
							val = tbl.get(m_curBrailleSet, m_bCapsLock);
						} else {
							// エラーの定義
							//   - ２個目の符号以降が16進数値以外
							isError = true;
						}
					}
				}
				
				// 囲み系がはじまったか？
				if ( !isError && val == null ) {
					IMojiCodeTable newTable = null;
					if( m_sanyosujiTable.isBeginFu(m_curBrailleSet) ) {
						newTable = m_sanyosujiTable;	// 算用数字
					} else if (m_kansujiTable.isBeginFu(m_curBrailleSet) ) {
						newTable = m_kansujiTable;		// 漢数字
					} else if (m_katakanaTable.isBeginFu(m_curBrailleSet) ) {
						newTable = m_katakanaTable;		// カタカナ
					} else if ( beforeTable != m_katakanaTable && 
							m_curBrailleSet.equals(m_katakanaToggleFu) )
					{
						// カタカナモードでないときだけ、トグルによりカタカナモードトグルが有効。
						newTable = m_katakanaTable;		// カタカナ
					} else if (m_alphabetTable.isBeginFu(m_curBrailleSet) ) {
						newTable = m_alphabetTable;	// 外国語引用符
					} else if (m_bUseGaijiFu && m_gaijiTable.isBeginFu(m_curBrailleSet) ) {
						newTable = m_gaijiTable;		// 外字開始符
					} else if (m_jisCodeTable.isBeginFu(m_curBrailleSet) ) {
						newTable = m_jisCodeTable;		// JISコード
					} else if ( tbl.getClass() == AsciiTable.class ) {
						// 大文字符か
						if( AsciiTable.getOomojiFu().equals(m_curBrailleSet) ) {
							result.newModeStr = c_MODE_EIJI_OOMOJI_LARGE;
						} else if( AsciiTable.getOomojiRenzokuFu().equals(m_curBrailleSet) ) {
							// 上で囲み符号を足してしまっているから消す
							m_curBrailleSet.clear();
							
							// 英字二重大文字符開始
							if( m_bCapsLock == false ) {
								m_bCapsLock = true;
							
								// 二重大文字の場合はモード変更じゃないのでちょっと特殊
								result.newModeStr = c_MODE_EIJI_DOUBLE_LARGE;
							}
						} else if( AsciiTable.getOomojiRenzokuOffFu().equals(m_curBrailleSet) ) {
							// 上で囲み符号を足してしまっているから消す
							m_curBrailleSet.clear();
							
							// 英字二重大文字符終了
							if( m_bCapsLock == true ) {
								m_bCapsLock = false;
							
								// 二重大文字の場合はモード変更じゃないのでちょっと特殊
								result.newModeStr = c_MODE_EIJI_DOUBLE_LARGE_END;
							}
						}
					}
				
					if( newTable != null ) {
						// 何か囲い系がはじまった
						
						// 外字符モード中に他の囲い系がはじまったときは、
						// 外字符モードを終了させる。
						if( tbl == m_gaijiTable ) {
							// 外字符モードを黙って終了
							popBrailleTable();
						}
						
						pushBrailleTable(newTable);
						// 上で囲み符号を足してしまっているから消す
						m_curBrailleSet.clear();
						val = null;
					}
				}
				
				// 数字 or カタカナ or 英字モード中に２文字に達していてハズレを引いてるならエラー
				// 囲み系が開始されている場合は、m_curBrailleSetの長さ0のはず。
				// ※数字ならば１文字目でエラーを判定できそうに思えるが、数字からカタカナや英字
				// に移行するケースがあるため、２文字来るまで判定を延期している。
				if( beforeTable != m_hirakanjiTable && 
					beforeTable != m_jisCodeTable && 
					val == null && 
					m_curBrailleSet.length() >= 2 )
				{
					isError = true;
				}
				
				// テーブルが変更されたとき
				if( !isError && (beforeTable != getLastBrailleTable()) ) {
					String lastMode = getLastBrailleTable().getModeName();
					result.newModeStr = lastMode;
				}
			}
			
			if( val != null ) {
				// マスアケにより入力モードが終了するケースがある
				// 例) 外字符06以降に現れたマスアケで外字符終了。
				if( left == 0 && right == 0 ) {
					// マスアケだった
					if( tbl == m_gaijiTable ) {
						// 外字符06の終了
						// 点字テーブルをpopする
						popBrailleTable();
						String lastMode = getLastBrailleTable().getModeName();
						result.newModeStr = lastMode;
					}
				}
				
				// 文字列を追加
				m_wordBuf.append(val);
				
				// 1語完成
				result.resultStr = val;
				
				m_curBrailleSet.clear();
			} else {
				// 最大マス入力したのに見つからなければエラー
				int maxLen = getLastBrailleTable().getMaxLength(m_curBrailleSet);
				if( m_curBrailleSet.length() >= maxLen ) {
					isError = true;
				}
			}
		} else {
			// もう足せないから、これは知らない点字だ。
			isError = true;
		}
		
		// エラー処理
		if( isError ) {
			result.isError = true;
			result.errorBrailleSet = m_curBrailleSet;
			
			// 点字セットをクリア
			pop(m_curBrailleSet.length());
			m_curBrailleSet.clear();
		}
		return(result);
	}
	
	/**
	 * tableが数字用のテーブルか.
	 * @param table
	 * @return
	 */
	private boolean isNumberTable(IMojiCodeTable table) {
		if( table == m_kansujiTable || table == m_sanyosujiTable ) {
			return(true);
		}
		return(false);
	}
	
	/**
	 * 最後の一文字を削除.
	 * 参考:m_braillListはこのpopの動作のためだけに用意されている。
	 *      点字一文字を削除される場合は相当難しい。たとえば削除していって数符から
	 *      続く数値列のうしろに出くわしたとき、そこが数符モードだと判定するロジック
	 *      が必要になってしまう。
	 *      で、仕方ないので点字を全部保管しておいて、一文字削られたら全部を再構成
	 *      することにした。
	 */
	public void pop(int popNum) {
		for( int i = 0; i < popNum; i++ ) {
			int size = m_brailleList.size();
			if( size > 0 ) {
				// brailleListをpopする
				m_brailleList.remove(size - 1);
			}
		}
			
		if( popNum > 0  ) {
			// 再構築
			reinit();
		}
	}
	
	/**
	 * 再構築する.
	 */
	private void reinit() {
		// clearで消えてしまうから取っておく
		ArrayList<TmBraille> oldBrailleList = m_brailleList;
		
		clear(true);
		for( TmBraille b : oldBrailleList ) {
			appendAndTokenize(b.getLeft(), b.getRight());
		}
	}
	
	/**
	 * クリア
	 */
	public void clear(boolean bClearInputModeStack) {
		// 入力モードスタックをクリアするか
		if( bClearInputModeStack ) {
			initBrailleTable();
		}
		
		m_kakkoCount = 0;
		m_curBrailleSet = new BrailleSet(4, true);
		m_brailleList = new ArrayList<TmBraille>(100);
		m_wordBuf.setLength(0);
	}
	
	/**
	 * 現在の入力モード名を取得する.
	 * @return
	 */
	public String getCurrentModeName() {
		return(getLastBrailleTable().getModeName());
	}
	
	/**
	 * 入力した文字列を得る
	 * @return
	 */
	public String getString() {
		return(m_wordBuf.toString());
	}
	
	/**
	 * 最後の点字テーブルを取得.
	 * @return
	 */
	private IMojiCodeTable getLastBrailleTable() {
		int size = m_brailleTableStack.size();
		if( size == 0 ) {
			throw new RuntimeException();
		}
		IMojiCodeTable tbl = m_brailleTableStack.get(size - 1);
		return(tbl);
	}
	
	/**
	 * 点字テーブルをpush.
	 * @param tbl
	 */
	private void pushBrailleTable(IMojiCodeTable tbl) {
		m_brailleTableStack.add(tbl);
	}
	
	/**
	 * 点字テーブルをpopする.
	 * @return 消去したオブジェクト.
	 */
	private IMojiCodeTable popBrailleTable() {
		// Capslockがonのまま抜けることがあるからここで常にoffる。
		m_bCapsLock = false;
		int size = m_brailleTableStack.size();
		if( size == 0 ) {
			throw new RuntimeException();
		}
		return(m_brailleTableStack.remove(size - 1));
	}
	
	/**
	 * 点字テーブルを初期化する.
	 */
	private void initBrailleTable() {
		m_brailleTableStack.clear();
	
		// デフォルトの平仮名を積む
		pushBrailleTable(m_hirakanjiTable);
	}
}

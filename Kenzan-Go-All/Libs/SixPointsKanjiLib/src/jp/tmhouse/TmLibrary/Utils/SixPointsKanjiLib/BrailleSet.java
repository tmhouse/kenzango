/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib;

import jp.tmhouse.TmLibrary.Utils.Data.TmBraille;


/**
 * 連続した1個以上の点字の連続の塊.
 * 
 * 文字と点字のhashテーブルのvalueで使う。
 * ただ、漢字などの数が多いやつはint,Stringのhashを使ってる。
 * 
 * @author mutoh
 *
 */
public class BrailleSet {
	private int		m_hashCode;
	private int[]		m_brailleArr; // {ab},{cd},...の点字配列。１個の点字は８進数２桁。
	private int		m_length;
	private String		m_str;
	private boolean	m_isVariable = false;	// 変更可能で生成されたか
	
	/**
	 * コンストラクタ.
	 */
	// 使わせない
	@SuppressWarnings("unused")
	private BrailleSet() {
	}
	
	public BrailleSet(int maxLength, boolean bIsVariable) {
		m_isVariable = bIsVariable;
		m_brailleArr = new int[maxLength];
		resetHash();
	}
	
	/**
	 * intの中に8/2式で全て入っている値を使ったコンストラクタ.
	 * @param intData
	 * @param str
	 * @param bIsVariable
	 */
	public BrailleSet(int intData, String str, boolean bIsVariable) {
		m_isVariable = bIsVariable;

		// ださいけど速そうだからいっか
		if( intData < 100 ) {
			m_length = 1;
		} else if( intData < 10000) {
			m_length = 2;
		} else if( intData < 1000000) {
			m_length = 3;
		} else if( intData < 100000000) {
			m_length = 4;
		} else {
			throw new RuntimeException();
		}

		m_brailleArr = new int[m_length];
		
		int tmpIntData = intData;
		for( int i = m_length -1; i >= 0 ; i-- ) {
			m_brailleArr[i] = tmpIntData % 100;
			tmpIntData = tmpIntData / 100;
		}
		
		m_str = str;
		resetHash();
	}

	public BrailleSet(int b1, String str) {
		this(1, false);
		m_length = 1;
		m_brailleArr[0] = b1;
		m_str = str;
		resetHash();
	}
	public BrailleSet(int b1, int b2, String str) {
		this(2, false);
		m_length = 2;
		m_brailleArr[0] = b1;
		m_brailleArr[1] = b2;
		m_str = str;
		resetHash();
	}
	public BrailleSet(int b1, int b2, int b3, String str) {
		this(3, false);
		m_length = 3;
		m_brailleArr[0] = b1;
		m_brailleArr[1] = b2;
		m_brailleArr[2] = b3;
		m_str = str;
		resetHash();
	}
	/***
	public BrailleSet(int b1, int b2, int b3, int b4, String str) {
		m_length = 4;
		m_brailleArr = new int[m_length];
		m_brailleArr[0] = b1;
		m_brailleArr[1] = b2;
		m_brailleArr[2] = b3;
		m_brailleArr[3] = b4;
		m_str = str;
		resetHash();
	}
	public BrailleSet(int b1, int b2, int b3, int b4, int b5, String str) {
		m_length = 5;
		m_brailleArr = new int[m_length];
		m_brailleArr[0] = b1;
		m_brailleArr[1] = b2;
		m_brailleArr[2] = b3;
		m_brailleArr[3] = b4;
		m_brailleArr[4] = b5;
		m_str = str;
		resetHash();
	}
	***/
	
	private void resetHash() {
		m_hashCode = -1;
	}
	
	/**
	 * 文字取得.
	 */
	public String getString() {
		return(m_str);
	}
	
	/**
	 * 点字をつなげてIngeterにする。
	 * @return
	 */
	public Integer toInteger() {
		if( m_brailleArr == null ) {
			return(null);
		}
		
		int len = length();
		int val = 0;
		for( int i = 0; i < len; i++ ) {
			val = (val*100) + m_brailleArr[i];
		}
		return(val);
	}
	
	/**
	 * デバッグ用.
	 * @return
	 */
	public String getDebugString() {
		if( m_brailleArr == null ) {
			return(null);
		}
		
		// 二桁ずつ頭ゼロ埋め数字の生成
		int len = length();
		StringBuilder sb = new StringBuilder(32);
		for( int i = 0; i < len; i++ ) {
			sb.append(String.format("%02d", m_brailleArr[i]));
		}
		// カッコ内に文字
		sb.append('(');
		sb.append(m_str);
		sb.append(')');
		return(sb.toString());
	}
	
	public String getDebugStringFor6p() {
		//TmBraille
		if( m_brailleArr == null ) {
			return(null);
		}
		StringBuilder sb = new StringBuilder(32);

		TmBraille b = new TmBraille();
		for( int i : m_brailleArr ) {
			b.clear();
			b.set(i);
			sb.append(String.format("[%s%s%s%s%s%s]", 
				b.isPointBlack(1) ? "1" : "",
				b.isPointBlack(2) ? "2" : "",
				b.isPointBlack(3) ? "3" : "",
				b.isPointBlack(4) ? "4" : "",
				b.isPointBlack(5) ? "5" : "",
				b.isPointBlack(6) ? "6" : ""
					));
		}
		sb.append(m_str);
		return(sb.toString());
	}
	
	/**
	 * 点字数値配列を返す.
	 * @return
	 */
	/**
	public final int[] getBrailleArray() {
		return(m_brailleArr);
	}
	**/
	
	/**
	 * 特定の位置の点字の値を取り出す.
	 * @param pos
	 * @return
	 */
	public int get(int pos) {
		return(m_brailleArr[pos]);
	}
	
	/**
	 * 最後の一文字を削除.
	 */
	public boolean pop() {
		if( m_length > 0 ) {
			m_brailleArr[m_length - 1] = 0;
			m_length--;
			resetHash();
			return(true);
		} else {
			return(false);
		}
	}
	
	/**
	 * 全消去.
	 */
	public void clear() {
		int len = m_brailleArr.length;
		for( int i = 0; i < len; i++) {
			m_brailleArr[i] = 0;
		}
		m_length = 0;
		resetHash();
	}
	
	/**
	 * 点字１個を足す.
	 * @param b
	 * @return
	 */
	public boolean add(int left, int right) {
		if( !m_isVariable ) {
			throw new RuntimeException("this is not variable");
		}
		if( m_length >= m_brailleArr.length ) {
			// もういっぱい
			return(false);
		}
		m_brailleArr[m_length] = (left * 10) + right;
		m_length++;
		resetHash();
		return(true);
	}
	
	/**
	 * 比較.
	 * @param bset
	 * @return
	 */
	@Override
	public boolean equals(Object obj) {
		if( obj.getClass() != getClass() ) {
			return(false);
		}
		BrailleSet bset = (BrailleSet)obj;
		if( length() != bset.length() ) {
			return(false);
		}
		if( hashCode() != bset.hashCode() ) {
			return(false);
		}
		
		// 完全比較
		int[] targArr = bset.m_brailleArr;
		int len = length();
		for( int i = 0; i < len; i++ ) {
			if( targArr[i] != m_brailleArr[i] ) {
				return(false);
			}
		}
		// 一致
		return(true);
	}
	
	/**
	 * hashCode取得.
	 */
	@Override
	public int hashCode() {
		if( m_hashCode < 0 ) {
			m_hashCode = 1;
			for( int braille : m_brailleArr ) {
				m_hashCode += braille * 9871;
			}
		}
		return(m_hashCode);
	}
	
	public int length() {
		return(m_length);
	}
}

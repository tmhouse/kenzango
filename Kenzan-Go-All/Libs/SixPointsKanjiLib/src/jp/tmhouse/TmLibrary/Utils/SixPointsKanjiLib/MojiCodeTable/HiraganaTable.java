/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable;

import java.util.HashMap;

import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;

public class HiraganaTable {
	private static final boolean c_USE_SMALL_KATAKANA = false;
	
	private static HashMap<BrailleSet, String> m_brailleStringHash;
	private static HashMap<String, BrailleSet> m_stringBrailleHash;

	public String get(BrailleSet brailleSet, boolean bCapsLock) {
		String str = m_brailleStringHash.get(brailleSet);
		return(str);
	}
	
	public BrailleSet get(String moji, boolean bCapsLock) {
		BrailleSet set = m_stringBrailleHash.get(moji);
		return(set);
	}

	/**
	 * ひらがなデータの生成.
	 * @param brailleHash
	 */
	public static void createHiragana()
	{
		// 半角スペースは、文字から引いたらマスアケとして当たりにする
		m_stringBrailleHash.put(" ", new BrailleSet(0," "));
		
		putBrailleSet(new BrailleSet(0,"　"));
		putBrailleSet(new BrailleSet(6,0,"、"));
		putBrailleSet(new BrailleSet(2,0,"・"));
		
		putBrailleSet(new BrailleSet(3,10,"ぁ"));
		putBrailleSet(new BrailleSet(3,30,"ぃ"));
		putBrailleSet(new BrailleSet(3,11,"ぅ"));
		putBrailleSet(new BrailleSet(3,31,"ぇ"));
		putBrailleSet(new BrailleSet(3,21,"ぉ"));
		putBrailleSet(new BrailleSet(3,40,"ゎ"));
		putBrailleSet(new BrailleSet(3,41,"ゃ"));
		putBrailleSet(new BrailleSet(3,45,"ゅ"));
		putBrailleSet(new BrailleSet(3,43,"ょ"));
		
		putBrailleSet(new BrailleSet(10,"あ"));
		putBrailleSet(new BrailleSet(11,"う"));
		putBrailleSet(new BrailleSet(12,"ら"));
		putBrailleSet(new BrailleSet(13,"る"));
		putBrailleSet(new BrailleSet(14,"か"));
		putBrailleSet(new BrailleSet(15,"く"));
		putBrailleSet(new BrailleSet(16,"さ"));
		putBrailleSet(new BrailleSet(17,"す"));
		putBrailleSet(new BrailleSet(20,"っ"));
		putBrailleSet(new BrailleSet(21,"お"));
		putBrailleSet(new BrailleSet(22,"ー"));
		putBrailleSet(new BrailleSet(23,"ろ"));
		putBrailleSet(new BrailleSet(24,0,"？"));
		putBrailleSet(new BrailleSet(25,"こ"));
		putBrailleSet(new BrailleSet(26,0,"。"));
		putBrailleSet(new BrailleSet(27,"そ"));
		putBrailleSet(new BrailleSet(30,"い"));
		putBrailleSet(new BrailleSet(31,"え"));
		putBrailleSet(new BrailleSet(32,"り"));
		putBrailleSet(new BrailleSet(33,"れ"));
		putBrailleSet(new BrailleSet(34,"き"));
		putBrailleSet(new BrailleSet(35,"け"));
		putBrailleSet(new BrailleSet(36,"し"));
		putBrailleSet(new BrailleSet(37,"せ"));
		putBrailleSet(new BrailleSet(40,"わ"));
		putBrailleSet(new BrailleSet(41,"や"));
		putBrailleSet(new BrailleSet(42,"を"));
		putBrailleSet(new BrailleSet(43,"よ"));
		putBrailleSet(new BrailleSet(45,"ゆ"));
		putBrailleSet(new BrailleSet(46,"ん"));
		putBrailleSet(new BrailleSet(50,"な"));
		putBrailleSet(new BrailleSet(51,"ぬ"));
		putBrailleSet(new BrailleSet(52,"た"));
		putBrailleSet(new BrailleSet(53,"つ"));
		putBrailleSet(new BrailleSet(54,"は"));
		putBrailleSet(new BrailleSet(55,"ふ"));
		putBrailleSet(new BrailleSet(56,"ま"));
		putBrailleSet(new BrailleSet(57,"む"));
		putBrailleSet(new BrailleSet(1,60,"ゐ"));
		putBrailleSet(new BrailleSet(1,62,"ゑ"));
		putBrailleSet(new BrailleSet(61,"の"));
		putBrailleSet(new BrailleSet(62,0,"！"));
		putBrailleSet(new BrailleSet(63,"と"));
		putBrailleSet(new BrailleSet(65,"ほ"));
		putBrailleSet(new BrailleSet(67,"も"));
		putBrailleSet(new BrailleSet(70,"に"));
		putBrailleSet(new BrailleSet(71,"ね"));
		putBrailleSet(new BrailleSet(72,"ち"));
		putBrailleSet(new BrailleSet(73,"て"));
		putBrailleSet(new BrailleSet(74,"ひ"));
		putBrailleSet(new BrailleSet(75,"へ"));
		putBrailleSet(new BrailleSet(76,"み"));
		putBrailleSet(new BrailleSet(77,"め"));
		putBrailleSet(new BrailleSet(2,14,"が"));
		putBrailleSet(new BrailleSet(2,15,"ぐ"));
		putBrailleSet(new BrailleSet(2,16,"ざ"));
		putBrailleSet(new BrailleSet(2,17,"ず"));
		putBrailleSet(new BrailleSet(2,25,"ご"));
		putBrailleSet(new BrailleSet(2,27,"ぞ"));
		putBrailleSet(new BrailleSet(2,34,"ぎ"));
		putBrailleSet(new BrailleSet(2,35,"げ"));
		putBrailleSet(new BrailleSet(2,36,"じ"));
		putBrailleSet(new BrailleSet(2,37,"ぜ"));
		putBrailleSet(new BrailleSet(2,52,"だ"));
		putBrailleSet(new BrailleSet(2,53,"づ"));
		putBrailleSet(new BrailleSet(2,54,"ば"));
		putBrailleSet(new BrailleSet(2,55,"ぶ"));
		putBrailleSet(new BrailleSet(2,63,"ど"));
		putBrailleSet(new BrailleSet(2,65,"ぼ"));
		putBrailleSet(new BrailleSet(2,72,"ぢ"));
		putBrailleSet(new BrailleSet(2,73,"で"));
		putBrailleSet(new BrailleSet(2,74,"び"));
		putBrailleSet(new BrailleSet(2,75,"べ"));
		putBrailleSet(new BrailleSet(4,54,"ぱ"));
		putBrailleSet(new BrailleSet(4,55,"ぷ"));
		putBrailleSet(new BrailleSet(4,65,"ぽ"));
		putBrailleSet(new BrailleSet(4,74,"ぴ"));
		putBrailleSet(new BrailleSet(4,75,"ぺ"));
		putBrailleSet(new BrailleSet(1,12,"りゃ"));
		putBrailleSet(new BrailleSet(1,13,"りゅ"));
		putBrailleSet(new BrailleSet(1,14,"きゃ"));
		putBrailleSet(new BrailleSet(1,15,"きゅ"));
		putBrailleSet(new BrailleSet(1,16,"しゃ"));
		putBrailleSet(new BrailleSet(1,17,"しゅ"));
		putBrailleSet(new BrailleSet(1,23,"りょ"));
		putBrailleSet(new BrailleSet(1,25,"きょ"));
		putBrailleSet(new BrailleSet(1,27,"しょ"));
		putBrailleSet(new BrailleSet(1,50,"にゃ"));
		putBrailleSet(new BrailleSet(1,51,"にゅ"));
		putBrailleSet(new BrailleSet(1,52,"ちゃ"));
		putBrailleSet(new BrailleSet(1,53,"ちゅ"));
		putBrailleSet(new BrailleSet(1,54,"ひゃ"));
		putBrailleSet(new BrailleSet(1,55,"ひゅ"));
		putBrailleSet(new BrailleSet(1,56,"みゃ"));
		putBrailleSet(new BrailleSet(1,57,"みゅ"));
		putBrailleSet(new BrailleSet(1,61,"にょ"));
		putBrailleSet(new BrailleSet(1,63,"ちょ"));
		putBrailleSet(new BrailleSet(1,65,"ひょ"));
		putBrailleSet(new BrailleSet(1,67,"みょ"));
		putBrailleSet(new BrailleSet(3,14,"ぎゃ"));
		putBrailleSet(new BrailleSet(3,15,"ぎゅ"));
		putBrailleSet(new BrailleSet(3,16,"じゃ"));
		putBrailleSet(new BrailleSet(3,17,"じゅ"));
		putBrailleSet(new BrailleSet(3,25,"ぎょ"));
		putBrailleSet(new BrailleSet(3,27,"じょ"));
		putBrailleSet(new BrailleSet(3,52,"ぢゃ"));
		putBrailleSet(new BrailleSet(3,53,"ぢゅ"));
		putBrailleSet(new BrailleSet(3,54,"びゃ"));
		putBrailleSet(new BrailleSet(3,55,"びゅ"));
		putBrailleSet(new BrailleSet(3,63,"ぢょ"));
		putBrailleSet(new BrailleSet(3,65,"びょ"));
		
		// 前置符号に続く「り」は躍り字に割り当て
		putBrailleSet(new BrailleSet(3,32,"々")); // 拗濁音符＋り
		putBrailleSet(new BrailleSet(1,32,"ゝ")); // 拗音符＋り
		putBrailleSet(new BrailleSet(2,32,"ゞ")); // 濁音符＋り
		
		putBrailleSet(new BrailleSet(5,54,"ぴゃ"));
		putBrailleSet(new BrailleSet(5,55,"ぴゅ"));
		putBrailleSet(new BrailleSet(5,65,"ぴょ"));
		
		////////////////////////////////////////////////////////
		//※以下、本来は6-60で囲い系だが、端折ってここに登録する。
		////////////////////////////////////////////////////////
		// 小さいひらがな
		putBrailleSet(new BrailleSet(6,14,60,"ヵ"));
		putBrailleSet(new BrailleSet(6,35,60,"ヶ"));
		putBrailleSet(new BrailleSet(6,61,60,"ノ"));
		//putBrailleSet(new BrailleSet(6,53,60,"っ"));
		
		// 小さいカタカナ
		if( c_USE_SMALL_KATAKANA ) {
		//putBrailleSet(new BrailleSet(6,4,44,14,60,"ヵ"));
		//putBrailleSet(new BrailleSet(6,4,44,35,60,"ヶ"));
		//putBrailleSet(new BrailleSet(6,4,44,61,60,"ノ"));
		//putBrailleSet(new BrailleSet(6,4,44,53,60,"ッ"));
		}
	}
	
	private static void putBrailleSet(BrailleSet bset)
	{
		m_brailleStringHash.put(bset, bset.getString());
		m_stringBrailleHash.put(bset.getString(), bset);
	}
	
	
	/**
	 * static処理
	 */
	static {
		m_brailleStringHash = new HashMap<BrailleSet, String>(337);
		m_stringBrailleHash = new HashMap<String, BrailleSet>(337);
		createHiragana();
	}
	
}

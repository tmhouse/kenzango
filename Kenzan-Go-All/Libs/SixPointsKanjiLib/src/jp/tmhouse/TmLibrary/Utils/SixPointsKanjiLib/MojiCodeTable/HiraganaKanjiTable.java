/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable;

import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.BrailleSet;

public class HiraganaKanjiTable extends IMojiCodeTable {
	private static HiraganaTable m_hiraganaTable;
	private static final String	c_MODENAME =  "基本モード";
	
	@Override
	public String get(BrailleSet brailleSet, boolean bCapsLock) {
		String str = m_hiraganaTable.get(brailleSet, bCapsLock);
		if( str == null ) {
			// 漢字を探す
			str = StaticKanjiTable.getKanji(brailleSet);
			
		}
		if( str == null ) {
			// 記号を探す
			str = StaticKigoTable.getKigo(brailleSet);
		}
		return(str);
	}

	@Override
	public BrailleSet get(String moji) {
		BrailleSet bs = m_hiraganaTable.get(moji, false);
		if( bs == null ) {
			// 漢字を探す
			bs = StaticKanjiTable.getKanji(moji);
		}
		if( bs == null ) {
			// 記号を探す
			bs = StaticKigoTable.getKigo(moji);
		}
		return(bs);
	}
	
	@Override
	public String getModeName() {
		return(c_MODENAME);
	}

	@Override
	public int getMaxLength(BrailleSet curBrailleSet) {
		return(StaticKanjiTable.getMaxKanjiLength(curBrailleSet));
	}
	
	@Override
	public boolean isInvalid(int left, int right) {
		return(false);
	}
	
	/**
	 * static処理
	 */
	static {
		m_hiraganaTable = new HiraganaTable();
	}
}


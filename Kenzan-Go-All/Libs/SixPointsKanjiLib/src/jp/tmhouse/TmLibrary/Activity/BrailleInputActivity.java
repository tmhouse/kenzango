/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright (C) 2011 Shigeo Mutoh (TM研究所), 長谷川貞夫（社会福祉法人 桜雲会 理事）
// 著作権は作者である「武藤　繁夫」および「長谷川　貞夫」が保持します。
// ソフトウェアを許可無く改変・再配布・転載することを禁止します。
// ソフトウェアのインストールや使用して起こった障害・トラブルや不具合に対して、
// 損害賠償を含む全ての責任を負いません。
// ソフトウェア製品のライセンス購入者に対しても同様です。
package jp.tmhouse.TmLibrary.Activity;

import jp.tmhouse.SixPointsKanjiLib.R;
import jp.tmhouse.TmLibrary.View.*;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

/**
 * スマート点字用Activity
 * @author mutoh
 *
 */
public abstract class BrailleInputActivity extends Activity {
    private TmIntegratedIppitsu8p2rCommonView		m_integ8p2rView;
    private LinearLayout							m_parentLL;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    	//Toast.makeText(this, "onCreate:" + savedInstanceState, Toast.LENGTH_LONG).show();
        
        // デフォルト値の初期化
        SettingsActivity.GuideSetting.initDefaultValues(this);
        
        m_parentLL = new LinearLayout(this);
        m_parentLL.setOrientation(LinearLayout.VERTICAL);
        m_parentLL.setLayoutParams(
            	new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,  LayoutParams.FILL_PARENT));
        
        setContentView(m_parentLL);
    }
    
    /**
     * 入力viewを作る。
     */
    private void recreateBrailleInputView() {
    	// 生成するViewを変える
    	m_integ8p2rView = SettingsActivity.createSmartBrailleView(this);
    	
    	// 入力方法を設定する.
    	m_integ8p2rView.set222Style();
    	
    	m_integ8p2rView.setLayoutParams(
            	new LinearLayout.LayoutParams(
            		LayoutParams.FILL_PARENT,  LayoutParams.FILL_PARENT));
    	
    	m_parentLL.removeAllViews();
        m_parentLL.addView(m_integ8p2rView);
    }
    
    @Override
    public void onDestroy() {
    	//android.os.Debug.stopMethodTracing();
        super.onDestroy();
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	
        // 六点漢字かそうでないかで生成するViewを変える
        recreateBrailleInputView();
        
        // ※先にi8p2rViewを作っておくこと。
        
        // 前回入力したあった文字を復帰
    	String[] savedBrailles = restoreBrailles();
    	int savedBraillesLen = savedBrailles != null ? savedBrailles.length : 0;
    	if( savedBraillesLen > 0 ) {
    		m_integ8p2rView.setWellcomeSpeech(getString(R.string.ACTRestoreBefore));
    	} else if( getWellcomeMessage() != null ) {
    		// 通常の起動の場合はwellcomeメッセージを表示
    		m_integ8p2rView.setWellcomeSpeech(getWellcomeMessage());
    	}
    }
    
    @Override
    public void onStop() {
    	super.onStop();
    	
    	if( m_integ8p2rView != null ) {
    		m_integ8p2rView.finish();
    		m_integ8p2rView = null;
    	}
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    	saveBrailles();
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	//restoreBrailles();
    }
    
    private SharedPreferences getMySharedPreferences(String name) {
    	return(getSharedPreferences(name, Activity.MODE_PRIVATE));
    }
    
    
 	// 入力済み文字列を永続領域へ保管
    private static String c_SP_NAME_BRAILLES	= "brailles";
    private static String c_SP_KEY_NAME_SAVED_BRAILLES	= "savedBrailles";
    private void saveBrailles() {
    	if( m_integ8p2rView == null ) {
    		return;
    	}
    	
    	// 点字列をViewから取得
    	String[] arr = m_integ8p2rView.getBrailleArray();
    	String saveStr = android.text.TextUtils.join(",", arr);
    	//Toast.makeText(this, "saveBrailles:" + saveStr, Toast.LENGTH_LONG).show();
    	
    	// 保管
    	SharedPreferences sp = getMySharedPreferences(c_SP_NAME_BRAILLES);
    	Editor ed = sp.edit();
    	ed.putString(c_SP_KEY_NAME_SAVED_BRAILLES, saveStr);
    	ed.commit();
    }
    
 	// 入力済み文字列を永続領域から取得しセットする
    private String[] restoreBrailles() {
    	if( m_integ8p2rView == null ) {
    		return(null);
    	}
    	
    	// 点字列をPreferencesから取得
    	SharedPreferences sp = getMySharedPreferences(c_SP_NAME_BRAILLES);
    	String savedStr = sp.getString(c_SP_KEY_NAME_SAVED_BRAILLES, "");
    	//Toast.makeText(this, "restoreBrailles:" + savedStr, Toast.LENGTH_LONG).show();
    	
    	// Viewへセット
    	String[] arr = android.text.TextUtils.split(savedStr, ",");
    	m_integ8p2rView.setBrailleArray(arr);
    	return(arr);
    }

    /**
     * wellcomeメッセージの文字列.
     * @return
     */
    abstract public String getWellcomeMessage();
    
    // Menu
    private static final int
		MENU_ITEM_OPEN_SETTINGS = 10,
		//MENU_ITEM_ABOUT = 0,
		//MENU_ITEM_HELP = 100,
		MENU_ITEM_TEST = 1000;
	private static final int
		MENU_GRP_TOP = 1;
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	menu.clear();
   		m_integ8p2rView.speechTextAppend(getString(R.string.SPmenuOpen), TmIntegratedIppitsu8p2rView.c_WORDS_SPEECH_RATE);
   		menu.add(MENU_GRP_TOP, MENU_ITEM_OPEN_SETTINGS, 0, getString(R.string.openSettings));
    	//menu.add(MENU_GRP_TOP, MENU_ITEM_ABOUT, 0, getString(R.string.MenuAbout));
    	menu.add(MENU_GRP_TOP, MENU_ITEM_TEST, 0, getString(R.string.MenuSample));
    	//menu.add(MENU_GRP_TOP, MENU_ITEM_HELP, 0, getString(R.string.MenuHelp));
    	return(super.onPrepareOptionsMenu(menu));
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	int itemId = item.getItemId();
    	int grpId = item.getGroupId();
    	if( itemId != 0 || grpId != 0 ) {
	    	if( MENU_GRP_TOP == grpId ) {
		    	switch (itemId) {
		    	case MENU_ITEM_OPEN_SETTINGS:
		    		startActivity(new Intent(this, SettingsActivity.class));
		    		break;
		    	//case MENU_ITEM_ABOUT:
		    	//	m_integ8p2rView.speechText(getString(R.string.SPshowAbout), TmIntegratedIppitsu8p2rView.c_WORDS_SPEECH_RATE);
		    	//	SettingsActivity.showAbout(this);
		    	//	break;
		    	case MENU_ITEM_TEST:
		    		m_integ8p2rView.openTestDialog();
		    		break;
		    	//case MENU_ITEM_HELP:
		    	//	SettingsActivity.showHelp(this);
		    	//	break;
		    	}
	    	}
    	}
    	return(super.onOptionsItemSelected(item));
    }
}
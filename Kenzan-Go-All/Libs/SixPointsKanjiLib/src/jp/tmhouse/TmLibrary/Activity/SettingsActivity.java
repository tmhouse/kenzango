/*
 * Copyright (C) 2015 Helen Keller System Project
 * Sadao Hasegawa, Shigeo Mutoh, Ichiro Narimatsu, Takashi Arai
 * http://helen-keller-project.appspot.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.tmhouse.TmLibrary.Activity;

import java.util.Locale;

import jp.tmhouse.SixPointsKanjiLib.R;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmBrailleVibrator;
import jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.TmBrailleVibrator.SingleKind;
import jp.tmhouse.TmLibrary.View.TmIntegratedIppitsu8p2rCommonView;
import jp.tmhouse.TmLibrary.View.TmIntegratedIppitsu8p2rKanjiView;
import jp.tmhouse.TmLibrary.View.TmIntegratedIppitsu8p2rView;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.widget.ScrollView;
import android.widget.TextView;

public class SettingsActivity extends PreferenceActivity 
	implements SharedPreferences.OnSharedPreferenceChangeListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// デフォルト値の初期化
		// ※SmartBrailleIMEの場合、IMEとして選ばれていないとき、
		//   設定画面を開かれると、ここが最初の初期化になる。
		GuideSetting.initDefaultValues(this);
		
		setupViews();
	}
	
	/**
	 * Viewの生成.
	 */
	@SuppressWarnings("deprecation")
	private void setupViews() {
		addPreferencesFromResource(R.layout.lang_settings);
		addPreferencesFromResource(R.layout.guide_settings);
		addPreferencesFromResource(R.layout.other_settings);
		
		setupOnClick();
	}
	
	private void setupOnClick() {
        // タップしたときの処理
        // Show About
        Preference aboutPref = findPreference(getString(R.string.showAboutKey));
        aboutPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				SettingsActivity.showAbout(SettingsActivity.this);
				return false;
			}
        });
        
        // Speech How to use
        Preference speekHowToUsePref = findPreference(getString(R.string.speekHowToUseKey));
        speekHowToUsePref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				SettingsActivity.showHelp(SettingsActivity.this);
				return false;
			}
        });
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		updateAllSummary();
		
		if( LangKind.queryBrailleLang(this) != LangKind.JAPANESE_ROKUTEN_KANJI ) {
			// カタカナ設定を非表示
			/*** なぜか非表示にならないので、setEnableで代用
			PreferenceScreen preferenceScreen = getPreferenceScreen();
			Preference katakanaPref = preferenceScreen.findPreference(
					getString(R.string.recvUseKatakanaFuType1ModeKey));
			preferenceScreen.removePreference(katakanaPref);
			***/
	        Preference katakanaPref = findPreference(
	        		getString(R.string.recvUseKatakanaFuType1ModeKey));
	        katakanaPref.setEnabled(false);
		}
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
	    getAppSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
	}
	 
	@Override
	protected void onPause() {
	    super.onPause();
	    getAppSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
	}
	
	@Override
	public void onSharedPreferenceChanged(
			SharedPreferences sharedPreferences,  String key)
	{
		// 言語が変わったときはリソース変更と再表示
		if( getString(R.string.brailleLangKey).equals(key) ) {
			// 使用するリソース言語の再セット
			LangKind.resetLangConfigration(this);
			
			finish();
			startActivity(new Intent().setClass(this, this.getClass()));
		} else {
			updateAllSummary();
		}
	}
	
	/**
	 * summary設定.
	 * 
	 */
	private void updateLangKindSummary(int id) {
		ListPreference langPref = (ListPreference)findPreference(
				getString(R.string.brailleLangKey));
		if( id == LangKind.UNKNOWN ) {
			id = LangKind.queryBrailleLang(this);
		}
		String str = LangKind.getEntryString(this, id);
		langPref.setSummary(str);
	}
	
	@SuppressWarnings("deprecation")
	private void updateListPrefSummary(int prefKeyName) {
		ListPreference pref = (ListPreference)findPreference(getString(prefKeyName));
		pref.setSummary(pref.getEntry());
	}
	
	private void updateAllSummary() {
		updateLangKindSummary(LangKind.UNKNOWN);

		//updateListPrefSummary(R.string.touchVibrateSpeedKey);
		updateListPrefSummary(R.string.touchBeepVolumeKey);
		updateListPrefSummary(R.string.touchBeepFreqKey);
		//updateListPrefSummary(R.string.touchBeepChanelNumKey);
		
		updateListPrefSummary(R.string.recvVibrateSpeedKey);
		updateListPrefSummary(R.string.recvVibrateEachSpaceTimeKey);
		updateListPrefSummary(R.string.recvVibrateShortLongRatioKey);
		updateListPrefSummary(R.string.recvVibrateMasuakeKindKey);
		updateListPrefSummary(R.string.recvVibrateSingleVibeKindKey);
	}
	
	// バージョン情報の取得
    public static String getVersion(Context ctx) {
		String versionName = "";
		try {
		    String packegeName = ctx.getPackageName();
		    PackageInfo packageInfo = ctx.getPackageManager().getPackageInfo(
		    		packegeName, PackageManager.GET_META_DATA);
		    versionName = packageInfo.versionName;
		} catch (NameNotFoundException e) {
		}
		return(versionName);
    }
    
    public static String getAboutThisApp(Context ctx) {
    	return(ctx.getString(R.string.aboutThisApp));
    }
    
    public static String getHelpMessage(Context ctx) {
    	return(ctx.getString(R.string.SPhelpMessage));
    }
    
    
	public static SharedPreferences getAppSharedPreferences(Context ctx) {
		Context appCtx = ctx.getApplicationContext();
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(appCtx);
		return(sp);
	}
	
	/**
	 * 言語の設定.
	 * @param locale
	 */
	/***
	private static final String c_KEY_LOCALE = "locale";
	private void saveLocale(Locale locale) {
		SharedPreferences sp = getAppSharedPreferences(this);
		Editor ed = sp.edit();
		String lang = locale.getLanguage();
		ed.putString(c_KEY_LOCALE, lang);
		ed.commit();
	}
	***/
	/**
	 * 言語の取得.
	 * @return
	 */
	/****
	public static Locale getLocale() {
		final String c_empty = "";
		SharedPreferences sp = getAppSharedPreferences(this);
		String val = sp.getString(c_KEY_LOCALE, c_empty);
		Locale locale = null;
		if( val.length() > 0 ) {
			locale = new Locale(val);
		} else {
			locale = Locale.getDefault();
		}
		return(locale);
	}
	***/
	
	/**
	 * 点字の言語の設定
	 * @param locale
	 */
	public static class LangKind {
		public static final int UNKNOWN = -1;
		public static final int ENGLISH_L2 = 0;
		public static final int JAPANESE_ROKUTEN_KANJI = 1;
		
		private static final String[] s_LANG_STRARR = {
			"engL2", "jpn"
		};
		
		private static int getLangKindId(String langStr) { 
			if( langStr == null || langStr.length() == 0 ) {
				return(UNKNOWN);
			}
			for( int i = 0; i < s_LANG_STRARR.length; i++ ) {
				if( langStr.equals(s_LANG_STRARR[i]) ) { 
					return(i);
				}
			}
			return(UNKNOWN);
		}
		private static String getLangKindString(int langId) {
			return(s_LANG_STRARR[langId]);
		}
		public static String getEntryString(Context ctx, int langId) {
			if( langId == UNKNOWN ) {
				return("unknown");
			}
			String[] names = ctx.getResources().getStringArray(R.array.brailleLangEntries);
			return(names[langId]);
		}
	
		public static int queryBrailleLang(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String lang = sp.getString(
					ctx.getString(R.string.brailleLangKey), "");
			return(getLangKindId(lang));
		}
	
		public static void saveBrailleLang(Context ctx, int lang) {
			String langStr = getLangKindString(lang);
			SharedPreferences sp = getAppSharedPreferences(ctx);
			Editor e = sp.edit();
			e.putString(ctx.getString(R.string.brailleLangKey), langStr);
			e.commit();
		}
		
		public static void saveDefault(Context ctx) {
			Locale loc = Locale.getDefault();
			// 日本語のときだけ日本語にセット
			if( loc.getISO3Language().equalsIgnoreCase("jpn") ) {
				saveBrailleLang(ctx, JAPANESE_ROKUTEN_KANJI);
			} else {
				saveBrailleLang(ctx, ENGLISH_L2);
			}
		}
		
		public static void resetLangConfigration(Context ctx) {
	    	int lang = LangKind.queryBrailleLang(ctx);
	    	if( lang  == LangKind.JAPANESE_ROKUTEN_KANJI ) {
	    		// 日本語リソースを使用
	    		setConfiguration(ctx, Locale.JAPANESE);
	    	} else if( lang == LangKind.ENGLISH_L2 ){
	    		// 英語リソースを使用
	    		setConfiguration(ctx, Locale.ENGLISH);
	    	} else {
	    		throw new RuntimeException();
	    	}
		}
		
		/**
		 * Configrationの設定.
		 * @param locale
		 */
		private static void setConfiguration(Context ctx, Locale locale) {
	        Configuration conf = ctx.getResources().getConfiguration();
	        conf.locale = locale;
	        ctx.getResources().updateConfiguration(conf, null);
		}
	}
	
	/**
	 * 設定言語に応じてスマート点字のViewを返す.
	 * @param ctx
	 * @return
	 */
	public static TmIntegratedIppitsu8p2rCommonView 
		createSmartBrailleView(Context ctx)
	{
		TmIntegratedIppitsu8p2rCommonView view = null;
		
    	// 生成するViewを変える
    	int lang = LangKind.queryBrailleLang(ctx);
    	if( lang  == LangKind.JAPANESE_ROKUTEN_KANJI ) {
    		// 六点漢字用view
    		view =  new TmIntegratedIppitsu8p2rKanjiView(ctx);
    		view.setLocale(Locale.JAPANESE);
    	} else if( lang == LangKind.ENGLISH_L2 ){
    		// 英語Level2用Viewを生成する.
    		view = new TmIntegratedIppitsu8p2rView(ctx);
    		view.setLocale(Locale.ENGLISH);
    	} else {
    		throw new RuntimeException();
    	}
		return(view);
	}
	
	/**
	 * 各種ガイドの設定.
	 * @author mutoh
	 *
	 */
	public static class GuideSetting {
		// デフォルト値の初期化
		public static void initDefaultValues(Context ctx) {
	    	//Locale loc = SettingsActivity.getLocale();
	    	int lang = LangKind.queryBrailleLang(ctx);
	    	if( lang == LangKind.UNKNOWN ) {
	    		// まだ言語が決定していないのでデフォルト値をセット
	    		LangKind.saveDefault(ctx);
	    	}
	    	
	    	// デフォルト値で設定を保管する。最終引数がfalseだと、一生に一回だけ利く。
	    	PreferenceManager.setDefaultValues(ctx, R.layout.guide_settings, false);
	    	PreferenceManager.setDefaultValues(ctx, R.layout.lang_settings, false);
		}
		
		// 振動ありか
		public static boolean queryEnableVibrate(Context ctx) {
			return(true);
			/***
			SharedPreferences sp = getAppSharedPreferences(ctx);
			boolean bEnableVibrate = sp.getBoolean(
					ctx.getString(R.string.useVibrateKey), true);
			return(bEnableVibrate);
			***/
		}
		
		// タッチ振動の速度の値
		// 1.0が標準。
		/***
		public static float queryVibrateSpeedValue(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.touchVibrateSpeedKey), "1.0f");
			float val = Float.parseFloat(valStr);
			return(val);
		}
		***/
		
		// ブザー振動の速度の値
		// 1.0が標準。
		/***
		public static float queryBeepGuideSpeedValue(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.touchBeepGuideSpeedKey), "1.0f");
			float val = Float.parseFloat(valStr);
			return(val);
		}
		***/
		
		// 音声出力メインスイッチ
		public static boolean queryEnableSpeechMainSwitch(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			boolean bEnableSpeech = sp.getBoolean(
					ctx.getString(R.string.useSpeechMainSwitchKey), true);
			return(bEnableSpeech);
		}
		public static void setEnableSpeechMainSwitch(Context ctx, boolean bEnable) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			Editor e = sp.edit();
			e.putBoolean(ctx.getString(R.string.useSpeechMainSwitchKey), bEnable);
			e.commit();
		}
		
		// フリック時の音声出力スイッチ
		public static boolean queryEnableSpeechFlickNumber(Context ctx) {
			if( false == queryEnableSpeechMainSwitch(ctx) ) {
				return(false);
			}
			// 保管してあるデータを読む
			SharedPreferences sp = getAppSharedPreferences(ctx);
			boolean bEnable = sp.getBoolean(
					ctx.getString(R.string.useSpeechFlickNumberKey), true);
			return(bEnable);
		}
		
		// 墨字の音声出力スイッチ
		public static boolean queryEnableSpeechInputChar(Context ctx) {
			if( false == queryEnableSpeechMainSwitch(ctx) ) {
				return(false);
			}
			// 保管してあるデータを読む
			SharedPreferences sp = getAppSharedPreferences(ctx);
			boolean bEnable = sp.getBoolean(
					ctx.getString(R.string.useSpeechInputCharKey), true);
			return(bEnable);
		}
		
		// ブザー振動のスイッチ
		public static boolean queryEnableBeepGuide(Context ctx) {
			// 保管してあるデータを読む
			SharedPreferences sp = getAppSharedPreferences(ctx);
			boolean bEnable = sp.getBoolean(
					ctx.getString(R.string.useBeepGuideKey), true);
			return(bEnable);
		}
		public static void _setEnableBeepGuide(Context ctx, boolean bEnable) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			Editor e = sp.edit();
			e.putBoolean(ctx.getString(R.string.useBeepGuideKey), bEnable);
			e.commit();
		}
		
		// ブザー振動の音量
		// 0.1 to 1.0
		public static float queryBeepGuideVolume(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.touchBeepVolumeKey), "0.2f");
			float val = Float.parseFloat(valStr);
			return(val);
		}
		
		// ブザー振動の周波数
		public static int queryBeepSoundFreq(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.touchBeepFreqKey), "200");
			int val = Integer.parseInt(valStr);
			return(val);
		}
		
		// ブザー振動のチャンネル数の
		/*** AudioManager.isWiredHeadset()とかで都度問い合わせる
		public static int queryBeepSoundChanelNum(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.touchBeepChanelNumKey), "1");
			int val = Integer.parseInt(valStr);
			return(val);
		}
		***/
		
		
		// UniChatXから引っ越してきた
		
		// 受信メッセージ振動の速度の値
		// 1.0が標準。
		public static float queryVibrateSpeedValue(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.recvVibrateSpeedKey), "1.0f");
			float val = Float.parseFloat(valStr);
			return(val);
		}
		
		// 短振動と長振動の比率
		public static int queryRecvVibrateShortLongRatioValue(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.recvVibrateShortLongRatioKey), "5");
			int val = Integer.parseInt(valStr);
			return(val);
		}
		
		// 文字間の空白
		public static long queryRecvVibrateEachSpaceTimeValue(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.recvVibrateEachSpaceTimeKey), "1000");
			long val = Long.parseLong(valStr);
			return(val);
		}
		
		// 末尾省略処理
		public static boolean queryRecvOmissionEndMode(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			boolean val = sp.getBoolean(
					ctx.getString(R.string.recvOmissionEndModeKey), false);
			return(val);
		}
		
		// 無点時右短振動置換
		public static boolean queryRecvReplaceNone2ShortRightMode(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			boolean val = sp.getBoolean(
					ctx.getString(R.string.recvReplaceNone2ShortRightModeKey), false);
			return(val);
		}
		
		// マスアケの振動設定
		public static TmBrailleVibrator.MasuakeVibeKind 
			queryRecvMasuakeVibeKind(Context ctx)
		{
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.recvVibrateMasuakeKindKey), "3");
			int val = Integer.valueOf(valStr);
			switch(val) {
			case 0:
				return(TmBrailleVibrator.MasuakeVibeKind.LONG_WAIT);
			case 1:
				return(TmBrailleVibrator.MasuakeVibeKind.SHORT_1);
			case 3:
				return(TmBrailleVibrator.MasuakeVibeKind.SHORT_3);
			}
			return(null);
		}
		
		// 一点式での点字再生順序
		public static SingleKind queryRecvSingleVibeKind(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.recvVibrateSingleVibeKindKey), "123456");
			if( valStr.equals("123456") ) {
				return(SingleKind.USE_SINGLE_123456);
			} else if( valStr.equals("142536") ) {
				return(SingleKind.USE_SINGLE_142536);
			}
			// default
			return(SingleKind.USE_SINGLE_123456);
		}

		
		public static boolean queryVolumeMaxMode(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			boolean val = sp.getBoolean(
					ctx.getString(R.string.volumeMaxModeKey), false);
			return(val);
		}
		
		// 再生時カタカナ符号を1, 14の点にするモード
		public static boolean queryUseKatakanaFuType1Mode(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			boolean val = sp.getBoolean(
					ctx.getString(R.string.recvUseKatakanaFuType1ModeKey), false);
			return(val);
		}
		
		/***
		// ブザー振動の速度の値
		// 1.0が標準。
		public static float queryBeepGuideSpeedValue(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.touchBeepGuideSpeedKey), "1.0f");
			float val = Float.parseFloat(valStr);
			return(val);
		}
		***/
		
		// ブザー振動のチャンネル数の
		/***
		public static int queryBeepSoundChanelNum(Context ctx) {
			SharedPreferences sp = getAppSharedPreferences(ctx);
			String valStr = sp.getString(
					ctx.getString(R.string.touchBeepChanelNumKey), "1");
			int val = Integer.parseInt(valStr);
			return(val);
		}
		***/
	}

    /**
     * Aboutを表示する.
     * @param ctx
     */
    public static void showAbout(Context ctx) {
    	String appName = getAppName(ctx);
    	String version = getVersion(ctx);
    	
    	TextView tv = new TextView(ctx);
    	tv.setText(getAboutThisApp(ctx));
    	tv.setTextSize(14.0f);
    	tv.setGravity(Gravity.CENTER);
    	
   		new AlertDialog.Builder(ctx)
   		.setTitle(appName + " ver" + version)
   		.setView(tv)
   		.setCancelable(true)
   		.setPositiveButton("OK", null)
   		.show();
    }
    
    /**
     * アプリケーション名を返す.
     * @param ctx
     * @return
     */
    private static String getAppName(Context ctx) {
    	String value = "";
    	ApplicationInfo appInfo = null;
    	try {
    		PackageManager pm = ctx.getPackageManager();
    	    appInfo = pm.getApplicationInfo(ctx.getPackageName(), 0);
    	    value = (String)appInfo.loadLabel(pm);
    	} catch (NameNotFoundException e) {
    	}
    	return(value);
    }
    
    /**
     * helpを表示する.
     * @param ctx
     */
    public static void showHelp(Context ctx) {
    	ScrollView sv = new ScrollView(ctx);
    	TextView tv = new TextView(ctx);
    	tv.setText(getHelpMessage(ctx));
    	tv.setTextSize(13.0f);
    	tv.setGravity(Gravity.LEFT);
    	sv.addView(tv);
    	
    	String title = ctx.getString(R.string.speekHowToUseTitle);
    	
   		AlertDialog.Builder ab = new AlertDialog.Builder(ctx)
   		.setTitle(title)
   		.setView(sv)
   		.setCancelable(true)
   		.setPositiveButton("OK", null);
   		
   		// 日本語の場合は長谷川式六点漢字のサイトへ行くボタンを用意
   		if( LangKind.queryBrailleLang(ctx) == LangKind.JAPANESE_ROKUTEN_KANJI )
   		{
   			String lbl = ctx.getString(R.string.hasegawa6pointsKanjiButtonLabel);
   			ab.setNeutralButton(lbl, new openHasegawaSite(ctx));
   		}
   		
   		ab.create().show();
    }
    
    /**
     * 長谷川式六点漢字のサイトへ行くボタンのリスナ.
     * @author mutoh
     *
     */
    public static class openHasegawaSite implements OnClickListener {
    	private Context m_ctx;
    	public openHasegawaSite(Context ctx) {
    		m_ctx = ctx;
    	}
    	
		@Override
		public void onClick(DialogInterface dialog, int which) {
			String url = m_ctx.getString(R.string.hasegawa6pointsKanjiSiteUrl);
			Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse(url));
			m_ctx.startActivity(intent);
		}
    	
    }
}

#!/usr/bin/perl


sub to8p2 {
	my $str = $_[0];
	#my $str = "123456";
	my @tenjiArr = (0,0,0,0,0,0,0);
	my $left = 0;
	my $right = 0;
	for (my $i = 0; $i < length($str); $i++) {
    	my $c = substr($str, $i, 1);
    	if ($c =~ /[\x80-\xff]/) {
        	$c = substr($str, $i++, 2);
    	}
    	#print "[$c] ";
		@tenjiArr[$c] = 1;
	}
	$left = $tenjiArr[1] * 1 + $tenjiArr[2] * 2 + $tenjiArr[3] * 4;
	$right = $tenjiArr[4] * 1 + $tenjiArr[5]* 2 +  $tenjiArr[6] * 4;
	#print "left [$left] right [$right]\n";

	return(($left * 10) +  $right);
}

#to8p2("456");

my $file = shift;

open( my $fh, "<", $file )
    or die "Cannot open $file: $!";


my @all;
my @allP4;

while( my $line = readline $fh ){ 
    chomp $line;
    
    # $line に対して何らかの処理。
	my @record = split('\t', $line);
    #print $record[3], "\n"; # 標準出力へ書き出し。
	#漢字を出す
    #print $record[0];
	my $out = $record[0]; 

	my @tenji = split(/,/, $record[3]);
	foreach (@tenji) {
   		#print ",";
		my $aaa = &to8p2($_);
    	#print $aaa;
    	$out = $out . "," . $aaa;
	}
   	#print "$out\n";
	my @outArr = split(",", $out);
	if( $#outArr eq 3 ) {
		#$comment3P[$outArr[1]][$outArr[2]][$outArr[3]] = 
		#		"$record[1]\t$record[2]\t$record[3]\t$record[4]"; 

		$numStr = sprintf("%2d%02d%02d", $outArr[1], $outArr[2], $outArr[3]);
		#print "\t\thash.put(" . $numStr . ", " .  $val . ");\t//" . $comment4P[$first][$i][$j][$k] . "\n";
	} elsif ( $#outArr eq 4 ) {
		#$allP4[$outArr[1]][$outArr[2]][$outArr[3]][$outArr[4]] = $outArr[0]; 
		#$comment4P[$outArr[1]][$outArr[2]][$outArr[3]][$outArr[4]] = 
		#		"$record[1]\t$record[2]\t$record[3]\t$record[4]"; 

		$numStr = sprintf("%2d%02d%02d%02d", $outArr[1], $outArr[2], $outArr[3], $outArr[4]);
	} elsif ( $#outArr eq 5 ) {
		$numStr = sprintf("%2d%02d%02d%02d%02d", $outArr[1], $outArr[2], $outArr[3], $outArr[4], $outArr[5]);
	} elsif ( $#outArr eq 2 ) {
		$numStr = sprintf("%2d%02d", $outArr[1], $outArr[2]);
	}

print "\t\thash.put(" . $numStr . ", \"" .  $outArr[0] . "\");\t// $record[3]\t$record[4]\n";

}

exit;

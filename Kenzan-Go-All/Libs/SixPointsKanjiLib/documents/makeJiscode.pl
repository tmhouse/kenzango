#!/bin/perl
# Copyright (C)1995-2000 ASH multimedia lab. (http://ash.jp/)
#
# create code table (JIS X0208)
# 

#use Data::Dumper;
use utf8;
#binmode(STDOUT, ":utf8");

# JIS漢字の表示
#print "\n●JIS漢字\n";

for ($c1 = 0x21; $c1 <= 0x7E; $c1++) { # $c1は第1バイト

    # スケールの表示
    #print "\n      JIS  SJIS EUC   ";
    #print "+0 +1 +2 +3 +4 +5 +6 +7 +8 +9 +A +B +C +D +E +F\n";

    # 区番号の設定
    $ku = $c1 - 0x20;

    $line = "";
    $c = 1; # 1行文字数(16文字)
    for ($c2=0x20; $c2 <= 0x7F; $c2++) { # $c2は第2バイト

       	if ($c == 1) { # コード変換
            ($c1_s, $c2_s) = &jis2sjis($c1, $c2);
            ($c1_e, $c2_e) = &jis2euc($c1, $c2);
            #$head = sprintf ("%02d区  %02X%02X %02X%02X %02X%02X ",
            #    $ku, $c1, $c2, $c1_s, $c2_s, $c1_e, $c2_e);
        } else {
            $c2_s++;
            $c2_e++;
        }

		$kanji = null;
        if (($c2 == 0x20) || ($c2 == 0x7F)) {
            $line .= "   ";
        } else { # 文字の表示
            $line .= " "; # 文字間の空白
            $kanji = pack("CC", $c1_e, $c2_e);
            $line .= $kanji;
        }

		$hex = sprintf("%02X%02X", $c1, $c2);
		@hexArr[0] = substr($hex, 0, 1);
		@hexArr[1] = substr($hex, 1, 1);
		@hexArr[2] = substr($hex, 2, 1);
		@hexArr[3] = substr($hex, 3, 1);

#print $hexArr[0] . $hexArr[1] . "\n";

		if( $kanji ne null && length($kanji) > 0 ) {
			my $v8p2 = to8p2Int(@hexArr);
			#my $vtouch = toTouch(@hexArr);
			#$out =sprintf("%d %s [%s]\n", $v8p2, $hex, $kanji);
			$out =sprintf("\t\thash.put(%d, \"%s\"\); // %s\n", $v8p2, $kanji, $hex);
			print $out;
		}

        #if ($c == 16) { # 16文字目で1行表示
        #    #print "$head$line\n";
        #    $line = "";
        #    $c = 1;
        #} else {
        #    $c++;
        #}

    }
}


exit;

sub to8p2Int {
	#print "auau:" . $_[0] . $_[1] . $_[2] . $_[3] . "\n";
	my $keta = 1000000;
	my $out = 0;
	foreach $i (@_) {
		#print "kk=" . $i . ", ";
		if( $i eq "0" ) { $out += 23 * $keta; }
		if( $i eq "1" ) { $out += 10 * $keta; }
		if( $i eq "2" ) { $out += 30 * $keta; }
		if( $i eq "3" ) { $out += 11 * $keta; }
		if( $i eq "4" ) { $out += 13 * $keta; }
		if( $i eq "5" ) { $out += 12 * $keta; }
		if( $i eq "6" ) { $out += 31 * $keta; }
		if( $i eq "7" ) { $out += 33 * $keta; }
		if( $i eq "8" ) { $out += 32 * $keta; }
		if( $i eq "9" ) { $out += 21 * $keta; }
		if( $i eq "A" ) { $out += 14 * $keta; }
		if( $i eq "B" ) { $out += 34 * $keta; }
		if( $i eq "C" ) { $out += 15 * $keta; }
		if( $i eq "D" ) { $out += 17 * $keta; }
		if( $i eq "E" ) { $out += 16 * $keta; }
		if( $i eq "F" ) { $out += 35 * $keta; }
		$keta = $keta / 100;
	}
#print "out=" . $out . ", ";
	return($out);
}

sub toTouch {
	#print "auau:" . $_[0] . $_[1] . $_[2] . $_[3] . "\n";
	#my $keta = 1000000;
	my $out = "";
	foreach $i (@_) {
		#print "kk=" . $i . ", ";
		if( $i eq "0" ) { $out += "0 右、下、タ: "; }
		if( $i eq "1" ) { $out += "1 左、タ、タ: "; }
		if( $i eq "2" ) { $out += "2 左、左、タ: "; }
		if( $i eq "3" ) { $out += "3 下、タ、タ: "; }
		if( $i eq "4" ) { $out += "4 下、右、タ: "; }
		if( $i eq "5" ) { $out += "5 左、右、タ: "; }
		if( $i eq "6" ) { $out += "6 下、左、タ: "; }
		if( $i eq "7" ) { $out += "7 下、下、タ: "; }
		if( $i eq "8" ) { $out += "8 左、下、タ: "; }
		if( $i eq "9" ) { $out += "9 右、左、タ: "; }
		if( $i eq "A" ) { $out += "A 左、タ、右: "; }
		if( $i eq "B" ) { $out += "B 左、左、右: "; }
		if( $i eq "C" ) { $out += "C 下、タ、右: "; }
		if( $i eq "D" ) { $out += "D 下、右、右: "; }
		if( $i eq "E" ) { $out += "E 左、右、右: "; }
		if( $i eq "F" ) { $out += "F 下、左、右: "; }
	}
#print "out=" . $out . ", \n";
	return($out);
	#return("E 左、右、右");
}


# jis コードから sjis コードに変換
sub jis2sjis {
    local($c1, $c2);

    # 1バイト目の変換
    if ($_[0] < 0x5f) { # 21-5E -> 81-9F
        $c1 = (($_[0] + 1) >> 1) + 0x70;
    } else { # 5F-7E -> E0-EF
        $c1 = (($_[0] + 1) >> 1) + 0xb0;
    }

    # 2バイト目の変換
    if ($_[0] % 2) { # 奇数区
        if ($_[1] < 0x60) { # 20-50 -> 3F-6F
            $c2 = $_[1] + 0x1f;
        } else { # 60-70 -> 80-90
            $c2 = $_[1] + 0x20;
        }
    } else { # 20-70 -> 9E-EE
        $c2 = $_[1] + 0x7e;
    }

    return($c1, $c2);
}


# jis コードから euc コードに変換
sub jis2euc {
    return($_[0] | 0x80, $_[1] | 0x80);
}


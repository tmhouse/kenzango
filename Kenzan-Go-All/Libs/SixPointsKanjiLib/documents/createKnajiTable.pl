#!/usr/bin/perl


sub to8p2 {
	my $str = $_[0];
	#my $str = "123456";
	my @tenjiArr = (0,0,0,0,0,0,0);
	my $left = 0;
	my $right = 0;
	for (my $i = 0; $i < length($str); $i++) {
    	my $c = substr($str, $i, 1);
    	if ($c =~ /[\x80-\xff]/) {
        	$c = substr($str, $i++, 2);
    	}
    	#print "[$c] ";
		@tenjiArr[$c] = 1;
	}
	$left = $tenjiArr[1] * 1 + $tenjiArr[2] * 2 + $tenjiArr[3] * 4;
	$right = $tenjiArr[4] * 1 + $tenjiArr[5]* 2 +  $tenjiArr[6] * 4;
	#print "left [$left] right [$right]\n";

	return(($left * 10) +  $right);
}

#to8p2("456");

my $file = shift;

open( my $fh, "<", $file )
    or die "Cannot open $file: $!";


my @all;
my @allP4;

while( my $line = readline $fh ){ 
    chomp $line;
    
    # $line に対して何らかの処理。
	my @record = split('\t', $line);
    #print $record[3], "\n"; # 標準出力へ書き出し。
	#漢字を出す
    #print $record[0];
	my $out = $record[0]; 

	my @tenji = split(/,/, $record[3]);
	foreach (@tenji) {
   		#print ",";
		my $aaa = &to8p2($_);
    	#print $aaa;
    	$out = $out . "," . $aaa;
	}
   	#print "$out\n";
	my @outArr = split(",", $out);
	if( $#outArr eq 3 ) {
		$all[$outArr[1]][$outArr[2]][$outArr[3]] = $outArr[0]; 
		$comment3P[$outArr[1]][$outArr[2]][$outArr[3]] = 
				"$record[1]\t$record[2]\t$record[3]\t$record[4]"; 
	} elsif ( $#outArr eq 4 ) {
		$allP4[$outArr[1]][$outArr[2]][$outArr[3]][$outArr[4]] = $outArr[0]; 
		$comment4P[$outArr[1]][$outArr[2]][$outArr[3]][$outArr[4]] = 
				"$record[1]\t$record[2]\t$record[3]\t$record[4]"; 
	}
   	#print $outArr[1] . "\n";
   	#print $all[$outArr[1]][$outArr[2]][$outArr[3]] . "\n";
   	#print $allP4[$outArr[1]][$outArr[2]][$outArr[3]][$outArr[4]] . "\n";
}


sub printKanji {
	my $first = $_[0];
	my $arrName = $_[1];
	my $total = 0;
	print "\tpublic static final String " . $arrName . "[][]={\n";
	for(my $i = 0; $i < 78; $i++ ) {
		#print "{" . $all[$first][$i][$j] . "} // $first,$i,$j idx=" . ++$total . "\n";
		print "\t{\n";
		for(my $j = 0; $j < 78; $j++ ) {
			my $val = "null";
			if( $all[$first][$i][$j] ) {
				$val = "\"" . $all[$first][$i][$j] . "\"";
			}
			print "\t\t" . $val . ", // $first,$i,$j idx=" . ++$total . "\n";
		}
		print "\t},\n";
	}
	print "\t};\n\n";
}
sub print4PKanji {
	my $first = $_[0];
	my $arrName = $_[1];
	my $total = 0;
	print "\tpublic static final String " . $arrName . "[][][]={\n";
	for(my $i = 0; $i < 78; $i++ ) {
		print "\t{\n";
		for(my $j = 0; $j < 78; $j++ ) {
			print "\t\t{\n";
			for(my $k = 0; $k < 78; $k++ ) {
				my $val = "null";
				if( $allP4[$first][$i][$j][$k] ) {
					$val = "\"" . $allP4[$first][$i][$j][$k] . "\"";
				}
				print "\t\t\t" . $val . ", // $first,$i,$j,$k idx=" . ++$total . "\n";
			}
			print "\t\t},\n";
		}
		print "\t},\n";
	}
	print "\t};\n\n";
}

sub print4PHash {
	my $first = $_[0];
	my $total = 0;
	print "\t// ４マス漢字だけはhashで作る\n";
	print "\tprivate static HashMap<Integer, String> m_4pKanjiHash;\n";
	print "\tprivate static void init4MasuKanjiHash(HashMap<Integer, String> hash) {\n";
	print "\t\t\t\t\t\t\t\t// JIS\t区点CD\t点Map\t音符号１字１セル\n";
	for(my $i = 0; $i < 78; $i++ ) {
		for(my $j = 0; $j < 78; $j++ ) {
			for(my $k = 0; $k < 78; $k++ ) {
				if( $allP4[$first][$i][$j][$k] ) {
					$val = "\"" . $allP4[$first][$i][$j][$k] . "\"";
					#print "\t" . $val . ", // $first,$i,$j,$k idx=" . ++$total . "\n";
					$numStr = sprintf("%02d%02d%02d%02d", $first, $i, $j, $k);
					print "\t\thash.put(" . $numStr . ", " .  $val . ");\t//" . $comment4P[$first][$i][$j][$k] . "\n";
				}
			}
		}
	}
	print "\t};\n";

	print "\tpublic static String get4pKanji(int p1, int p2, int p3, int p4) {\n" .
		"\t\tint key = (p1*1000000) + (p2*10000) + (p3*100) + p4;\n" .
		"\t\treturn(m_4pKanjiHash.get(key));\n" .
	"\t}\n";
	
}

sub print3PHash {
	my $total = 0;
	print "\t// ３マス漢字hash\n";
	print "\tprivate static HashMap<Integer, String> m_3pKanjiHash;\n";
	print "\tprivate static void init3MasuKanjiHash(HashMap<Integer, String> hash) {\n";
	print "\t\t\t\t\t\t\t\t// JIS\t区点CD\t点Map\t音符号１字１セル\n";
	for(my $i = 0; $i < 78; $i++ ) {
		for(my $j = 0; $j < 78; $j++ ) {
			for(my $k = 0; $k < 78; $k++ ) {
				if( $all[$i][$j][$k] ) {
					$val = "\"" . $all[$i][$j][$k] . "\"";
					$numStr = sprintf("%2d%02d%02d", $i, $j, $k);
					print "\t\thash.put(" . $numStr . ", " .  $val . ");\t// " . $comment3P[$i][$j][$k] . "\n";
				}
			}
		}
	}
	print "\t};\n";

	print "\tpublic static String get3pKanji(int p1, int p2, int p3) {\n" .
		"\t\tint key = (p1*10000) + (p2*100) + p3;\n" .
		"\t\treturn(m_3pKanjiHash.get(key));\n" .
	"\t}\n";
}


print "package jp.tmhouse.TmLibrary.Utils.SixPointsKanjiLib.MojiCodeTable;\n" .
"import java.util.HashMap;\n" .
"public class StaticKanjiTable {\n";

#printKanji(7, "m_3P07Kanji");
#printKanji(26, "m_3P26Kanji");
#printKanji(24, "m_3P24Kanji");
#printKanji(05, "m_3P05Kanji");
#printKanji(06, "m_3P06Kanji");
#printKanji(04, "m_3P04Kanji");

print3PHash();
print4PHash(62);

print "\tstatic {\n" .
		"\t\tm_4pKanjiHash = new HashMap<Integer, String>(8111);\n" .
		"\t\tm_3pKanjiHash = new HashMap<Integer, String>(8111);\n" .
		"\t\tinit4MasuKanjiHash(m_4pKanjiHash);\n" .
		"\t\tinit3MasuKanjiHash(m_3pKanjiHash);\n" .
	"\t}\n";

print "}\n";

close $fh

